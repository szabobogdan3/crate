/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.attributes;

import openapi.definitions : Example;

/// Attribute used to tag mapping middlewares. They will transform how a deserialized
/// item looks.
struct Mapper {}

/// ditto
auto mapper() { return Mapper(); }


/// Attribute used to tag middlewares that should be called for
/// any type of request
struct Any {}

/// ditto
auto any() { return Any(); }

/// Attribute used to tag middlewares that should be called for
/// get requests
struct Get {}

/// ditto
auto get() { return Get(); }

/// Attribute used to tag middlewares that should be called for
/// get list requests
struct GetList {}

/// ditto
auto getList() {
  return GetList();
}

/// Attribute used to tag middlewares that should be called for
/// get item requests
struct GetItem {}

/// ditto
auto getItem() {
  return GetItem();
}

/// Attribute used to tag middlewares that should be called for
/// requests that update records
struct Patch {}

/// ditto
auto patch() { return Patch(); }

/// Attribute used to tag middlewares that should be called for
/// requests that create records
struct Create {}

/// ditto
auto create() { return Create(); }

/// Attribute used to tag middlewares that should be called for
/// requests that change records
struct Update {}

/// ditto
auto update() { return Update(); }

/// Attribute used to tag middlewares that should be called for
/// requests that replace records
struct Replace {}

/// ditto
auto replace() { return Replace(); }

/// ditto
struct Put {}

/// ditto
auto put() { return Put(); }


/// Attribute used to tag middlewares that should be called for
/// requests that delete records
struct Delete {}

/// ditto
auto delete_() { return Delete(); }

/// Attribute used tto add a text help message
struct DescribeAttribute { string description; }

/// ditto
DescribeAttribute describe(string description) {
  return DescribeAttribute(description);
}

/// Attribute used to add an example value
Example example(string value, string description) {
  Example result;
  result.value = value;
  result.summary = description;
  return result;
}

/// Attribute used to define mime types
struct Mime {
  string value;
}

/// ditto
Mime mime(string value) {
  return Mime(value);
}

struct Hidden {}
Hidden hidden() { return Hidden(); }


/// ditto
struct Rule {}

/// ditto
auto rule() { return Rule(); }