/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.auth.usercollection;

import std.exception;
import std.algorithm;
import std.stdio;
import std.array;
import std.datetime;
import std.uni;

import crate.base;
import crate.error;

import vibe.data.json;

import vibeauth.collections.base;
import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.user;
import vibeauth.data.token;
import cachetools.cachelru;

version(unittest){
  import fluent.asserts;
}

/// User collection that stores the data inside a crate
class UserCrateCollection : UserCollection {
  /// All the user access rights
  immutable(string[]) accessList;

  private {
    CrateAccess crate;
    CacheLRU!(string, Json) cache;
  }

  this(immutable(string[]) accessList, CrateAccess crate) {
    this.accessList = accessList;
    this.crate = crate;
    this.cache = new CacheLRU!(string, Json);

    cache.size = 2048;
    cache.ttl = 10.seconds;
  }

  private Json getUserModel(string email) {
    auto query = crate.get;
    query.where("email").equalIgnoreCase(email);

    auto users = query.limit(1).exec;

    enforce!CrateNotFoundException(!users.empty, "The user does not exist.");

    enforce(users.front["email"].to!string.toLower == email.toLower, "Invalid fetched user.");

    return users.front;
  }

  private void updateUser(User user) {
    this.crate.updateItem(user.toJson);
  }

  void disempower(string email, string access) {
    auto user = getUserModel(email);

    enforce!CrateValidationException(accessList.canFind(access), "Unknown access");

    auto scopes = cast(Json[])(user["scopes"]);

    enforce!CrateValidationException(
      canFind(scopes, Json(access)),
      "The user already has this access");

    user["scopes"] = Json(scopes.filter!(a => a != access).array);

    crate.updateItem(user);
    cache.remove(email);
  }

  override {
    Token createToken(string email, SysTime expire, string[] scopes = [], string type = "Bearer", string[string] meta = null) {
      cache.remove(email);

      auto user = opIndex(email);
      auto token = user.createToken(expire, scopes, type, meta);
      user.lastActivity = Clock.currTime.toUnixTime;

      crate.updateItem(user.toJson);

      return token;
    }

    /// Remove a token
    void revoke(string token) {
      auto user = byToken(token);
      user.revoke(token);
      cache.remove(token);

      crate.updateItem(user.toJson);
    }

    /// Get an user using [] operator
    User opIndex(string email) {
      auto userData = cache.get(email);

      if(userData.isNull) {
        cache.put(email, getUserModel(email));
        userData = cache.get(email);
      }

      auto user = User.fromJson(userData.get);
      user.onChange = &this.updateUser;

      return user;
    }

    /// Add rights to the user
    void empower(string email, string access) {
      auto user = getUserModel(email);

      enforce!CrateValidationException(accessList.canFind(access), "Unknown access");
      enforce!CrateValidationException(
        !canFind(cast(Json[])(user["scopes"]), Json(access)),
        "The user already has this access");

      user["scopes"] ~= Json(access);

      crate.updateItem(user);
      cache.remove(email);
    }

    /// Get an user by token
    User byToken(string token) {
      auto userData = cache.get(token);
      if(userData.isNull) {
        auto query = crate.get;
        query.where("tokens").arrayFieldContains("name", token);

        auto users = query.limit(1).exec;
        enforce!CrateNotFoundException(!users.empty, "Invalid token.");

        cache.put(token, users.front);
        userData = cache.get(token);
      }

      auto user = new User(userData.get.deserializeJson!UserModel);
      user.onChange = &this.updateUser;
      user.removeExpiredTokens;

      if(!user.isValidToken(token)) {
        throw new CrateNotFoundException("Invalid token.");
      }

      return user;
    }

    /// Get an user by id
    User byId(string id) {
      auto users = crate.getItem(id).exec;

      enforce!CrateNotFoundException(!users.empty, "Invalid id.");

      auto user = new User(users.front.deserializeJson!UserModel);
      user.onChange = &this.updateUser;

      return user;
    }

    /// Check if the collection contains an user
    bool contains(string email) {
      auto query = crate.get;
      query.or
        .where("email").equalIgnoreCase(email)
        .or
        .where("username").equalIgnoreCase(email);

      return !query.exec.empty;
    }

    bool createUser(UserModel data, string password) {
      auto user = new User(data);
      user.setPassword(password);

      add(user);

      return true;
    }

    void add(User item) {
      crate.addItem(item.toJson);
    }

    void remove(const(string) id) {
      crate.deleteItem(id);
    }

    size_t length() {
      assert(false, "not implemented");
    }

    int opApply(int delegate(User) dg) {
      auto query = crate.get.exec;

      int result = 0;
      foreach(user; query) {
        auto userData = user.deserializeJson!UserModel;
        result = dg(new User(userData));

        if(result) {
          break;
        }
      }

      return result;
    }

    int opApply(int delegate(ulong, User) dg) {
      assert(false, "not implemented");
    }

    bool empty() @property {
      assert(false, "not implemented");
    }

    ICollection!User save() {
      assert(false, "not implemented");
    }
  }
}

version(unittest) {
  import crate.collection.memory;

  auto userJson = `{
    "_id": "1",
    "email": "test@asd.asd",
    "name": "John",
    "username": "test",
    "password": "password",
    "isActive": true,
    "salt": "salt",
    "scopes": ["scopes"],
    "tokens": [ { "name": "token", "expire": "2100-01-01T00:00:00", "type": "Bearer", "scopes": [] } ],
  }`;
}

/// it should find the users
unittest {
  auto crate = new MemoryCrate!UserModel;
  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection([], crate);

  collection["test@asd.asd"].email.should.equal("test@asd.asd");
  collection["other@asd.asd"].should.throwException!CrateNotFoundException.withMessage("The user does not exist.");
}

/// it should update an user on activation
unittest {
  auto crate = new MemoryCrate!UserModel;
  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection([], crate);

  collection["test@asd.asd"].isActive = false;
  crate.getItem("000000000000000000000001").exec.front["isActive"].should.equal(false);
}

/// it should empower an user
unittest {
  auto crate = new MemoryCrate!UserModel;
  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection(["access1"], crate);
  collection.empower("test@asd.asd", "access1");

  auto query = crate.get;
  query.where("email").equal("test@asd.asd");
  auto result = query.exec.front;

  assert(result["scopes"].length == 2);
  assert(result["scopes"][1] == "access1");

  bool found = true;
  try {
    collection.empower("test@asd.asd", "invalid_access");
  } catch(CrateValidationException e) {
    found = false;
  }

  assert(!found, "It should not add the access");

  found = true;
  try {
    collection.empower("test@asd.asd", "access1");
  } catch(CrateValidationException e) {
    found = false;
  }

  assert(!found, "It should not add the the same access twice");
}

/// it should disempower an user
unittest {
  auto crate = new MemoryCrate!UserModel;
  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection(["scopes"], crate);
  collection.disempower("test@asd.asd", "scopes");

  auto query = crate.get;
  query.where("email").equal("test@asd.asd");
  auto result = query.exec.front;

  assert(result["scopes"].length == 0);

  bool found = true;
  try {
    collection.disempower("test@asd.asd", "invalid_access");
  } catch(CrateValidationException e) {
    found = false;
  }

  assert(!found, "It should not remove missing access");
}

/// it should generate user tokens with metadata
unittest {
  auto crate = new MemoryCrate!UserModel;
  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection(["scopes"], crate);
  auto token = collection.createToken("test@asd.asd", Clock.currTime + 3600.seconds, ["api"], "api", ["someKey": "some value"]);

  auto query = crate.get;
  query.where("email").equal("test@asd.asd");
  auto result = query.exec.front;

  result["tokens"].length.should.equal(2);
  result["tokens"][1]["name"].should.equal(token.name);
  result["tokens"][1]["meta"].deserializeJson!(string[string]).should.equal(["someKey": "some value"]);
}

/// it should find an user by a valid token
unittest {
  auto userJson2 = `{
    "_id": 1,
    "email": "test2@asd.asd",
    "name": "test",
    "username": "test",
    "isActive": true,
    "password": "password",
    "salt": "salt",
    "scopes": ["scopes"],
    "tokens": [{ "name": "token2", "expire": "2100-01-01T00:00:00", "type": "Bearer", "scopes": [] }],
  }`;

  auto crate = new MemoryCrate!UserModel;

  crate.addItem(userJson2.parseJsonString);
  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection(["scopes"], crate);

  assert(collection.byToken("token2").email == "test2@asd.asd");
}


/// it should not find an user by an expired token and it should be removed
unittest {
  auto userJson2 = `{
    "_id": 2,
    "email": "test2@asd.asd",
    "name": "test",
    "username": "test",
    "isActive": true,
    "password": "password",
    "salt": "salt",
    "scopes": ["scopes"],
    "tokens": [{ "name": "token2", "expire": "1100-01-01T00:00:00", "type": "Bearer", "scopes": [] }],
  }`;

  auto crate = new MemoryCrate!UserModel;

  crate.addItem(userJson2.parseJsonString);

  auto collection = new UserCrateCollection(["scopes"], crate);

  ({
    collection.byToken("token2");
  }).should.throwException!CrateNotFoundException.withMessage("Invalid token.");

  crate.get.exec.front["tokens"].length.should.equal(0);
}

/// it should check if contains user
unittest
{
  auto crate = new MemoryCrate!UserModel;

  crate.addItem(userJson.parseJsonString);

  auto collection = new UserCrateCollection(["scopes"], crate);

  collection.contains("test@asd.asd").should.equal(true);
  collection.contains("other@asd.asd").should.equal(false);
}

/// it should add an user
unittest
{
  auto user = User.fromJson(userJson.parseJsonString);
  auto crate = new MemoryCrate!UserModel;
  auto collection = new UserCrateCollection(["scopes"], crate);

  assert(!collection.contains("test@asd.asd"));
  collection.add(user);
  assert(collection.contains("test@asd.asd"));
}

/// it should delete an user
unittest
{
  auto crate = new MemoryCrate!UserModel;
  crate.addItem(userJson.parseJsonString);
  auto collection = new UserCrateCollection(["scopes"], crate);

  assert(collection.contains("test@asd.asd"));
  collection.remove("000000000000000000000001");
  assert(!collection.contains("test@asd.asd"));
}
