/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.auth.middleware;

import openapi.definitions;
import vibeauth.authenticators.OAuth2;
import vibeauth.authenticators.EmberSimpleAuth;
import vibeauth.authenticators.BaseAuth;
import vibeauth.router.request;
import vibeauth.router.responses;
import vibeauth.collections.usercollection;
import vibeauth.collections.client;

import vibe.http.router;

import crate.base;

/// Base class for defining auth middlewares
class AuthMiddleware {

  protected {
    OAuth2 oAuth2;
    EmberSimpleAuth emberAuth;
  }

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    this.oAuth2 = new OAuth2(userCollection, configuration);
    this.emberAuth = new EmberSimpleAuth(userCollection);
  }

  bool hasEmberSession(HTTPServerRequest req) {
    return "email" !in req.context && "ember_simple_auth-session" in req.cookies;
  }

  bool hasOAuth2(HTTPServerRequest req) {
    return "email" !in req.context && "Authorization" in req.headers;
  }

  ///
  void identifiableAuth(HTTPServerRequest req, HTTPServerResponse res) {
    req.cleanRequest;

    if(!res.headerWritten && hasOAuth2(req)) {
      oAuth2.permisiveAuth(req);
    }

    if(!res.headerWritten && hasEmberSession(req)) {
      emberAuth.permisiveAuth(req);
    }
  }

  ///
  void permisiveAuth(HTTPServerRequest req, HTTPServerResponse res) {
    req.cleanRequest;
    auto result = AuthResult.success;

    if(!res.headerWritten && hasOAuth2(req)) {
      result = oAuth2.permisiveAuth(req);
    }

    if(!res.headerWritten && hasEmberSession(req)) {
      result = emberAuth.permisiveAuth(req);
    }

    if(result == AuthResult.invalidToken) {
      respondInvalidToken(res);
    }

    if(result == AuthResult.unauthorized) {
      respondUnauthorized(res);
    }
  }

  ///
  void mandatoryAuth(HTTPServerRequest req, HTTPServerResponse res) {
    req.cleanRequest;

    auto result = AuthResult.unauthorized;

    if(!res.headerWritten && hasOAuth2(req)) {
      result = oAuth2.mandatoryAuth(req);
    }

    if(!res.headerWritten && hasEmberSession(req)) {
      result = emberAuth.mandatoryAuth(req);
    }

    if(result == AuthResult.invalidToken) {
      respondInvalidToken(res);
    }

    if(result == AuthResult.unauthorized) {
      respondUnauthorized(res);
    }
  }

  void respondUnauthorized(HTTPServerResponse res) {
    res.setCookie("ember_simple_auth-session", null);
    vibeauth.router.responses.respondUnauthorized(res);
  }

  void respondInvalidToken(HTTPServerResponse res) {
    res.setCookie("ember_simple_auth-session", null);
    vibeauth.router.responses.respondUnauthorized(res, "Invalid token.", 400);
  }

  void any(ref CrateRule rule) {
    CrateResponse invalidTokenResponse;
    CrateResponse unauthorizedResponse;

    invalidTokenResponse.mime = "application/json";
    invalidTokenResponse.statusCode = 400;
    invalidTokenResponse.description = "The provided auth token is rejected.";
    invalidTokenResponse.schema = new Schema;
    invalidTokenResponse.schema._ref = "#/components/schemas/ErrorMessage";

    unauthorizedResponse.mime = "application/json";
    unauthorizedResponse.statusCode = 401;
    unauthorizedResponse.description = "The auth token is missing from the request.";
    unauthorizedResponse.schema = new Schema;
    unauthorizedResponse.schema._ref = "#/components/schemas/ErrorMessage";

    rule.errorResponses ~= unauthorizedResponse;
    rule.errorResponses ~= invalidTokenResponse;
  }
}

/// Like PublicContributionMiddleware but it will not fail if a token is invalid
class IdentifiableContributionMiddleware : AuthMiddleware {

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    super(userCollection, clientCollection, configuration);
  }

  @getItem @getList
  void identify(HTTPServerRequest req, HTTPServerResponse res) {
    identifiableAuth(req, res);
  }

  @create
  void permisive(HTTPServerRequest req, HTTPServerResponse res) {
    permisiveAuth(req, res);
  }

  @replace @patch @delete_
  void mandatory(HTTPServerRequest req, HTTPServerResponse res) {
    mandatoryAuth(req, res);
  }
}


/// Allow getting lists, items and creating items without authentication
class PublicContributionMiddleware : AuthMiddleware {

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    super(userCollection, clientCollection, configuration);
  }

  @getItem @getList @create
  void permisive(HTTPServerRequest req, HTTPServerResponse res) {
    permisiveAuth(req, res);
  }

  @replace @patch @delete_
  void mandatory(HTTPServerRequest req, HTTPServerResponse res) {
    mandatoryAuth(req, res);
  }
}

/// Allow creating items without authentication
class ContributionMiddleware : AuthMiddleware {

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    super(userCollection, clientCollection, configuration);
  }

  @create
  void permisive(HTTPServerRequest req, HTTPServerResponse res) {
    permisiveAuth(req, res);
  }

  @getItem @getList @replace @patch @delete_
  void mandatory(HTTPServerRequest req, HTTPServerResponse res) {
    mandatoryAuth(req, res);
  }
}

/// Allow getting lists and items without authentication
class PublicDataMiddleware : AuthMiddleware {

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    super(userCollection, clientCollection, configuration);
  }

  @getItem @getList
  void permisive(HTTPServerRequest req, HTTPServerResponse res) {
    permisiveAuth(req, res);
  }

  @create @replace @patch @delete_
  void mandatory(HTTPServerRequest req, HTTPServerResponse res) {
    mandatoryAuth(req, res);
  }
}

/// Allow only items to be fetched without authentication
class PublicItemDataMiddleware : AuthMiddleware {

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    super(userCollection, clientCollection, configuration);
  }

  @getItem
  void permisive(HTTPServerRequest req, HTTPServerResponse res) {
    permisiveAuth(req, res);
  }

  @getList @create @replace @patch @delete_
  void mandatory(HTTPServerRequest req, HTTPServerResponse res) {
    mandatoryAuth(req, res);
  }
}

/// Dissalow requests without authentication
class PrivateDataMiddleware : AuthMiddleware {

  this (UserCollection userCollection, ClientCollection clientCollection, const OAuth2Configuration configuration) {
    super(userCollection, clientCollection, configuration);
  }

  @any
  void mandatory(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    mandatoryAuth(req, res);
  }
}
