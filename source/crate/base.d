/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.base;

import std.conv;
import std.range;
import std.ascii;
import std.range.interfaces;
import std.datetime;

import vibe.data.json;
import vibe.http.server;
import vibe.http.router;
import vibe.data.bson;
import vibe.core.log;

import crate.http.request;
import crate.ctfe;
import crate.error;
import openapi.definitions : Parameter, Schema, Example;

public import crate.attributes;

version(unittest) {
  shared static this() {
    foreach(logger; getLoggers()) {
      deregisterLogger(logger);
    }
  }
}

alias CrateListGetter = IQuery delegate() @safe;
alias CrateGetter = IQuery delegate(string) @safe;
CrateGetter[string] crateGetters;

alias BsonAppender = Appender!(Bson[]);

/// A vibe.d http request handler
alias VibeHandler = void delegate(HTTPServerRequest request, HTTPServerResponse response) @safe;

/// A handler used to filter data based on the user request
alias CrateFilterHandler = IQuery delegate(IQuery, CrateHttpRequest, CrateHttpResponse);

/// A crate http request handler
alias CrateHandler = void delegate(CrateHttpRequest, CrateHttpResponse) @safe;

/// Type of mappers used in middlewares
alias CustomMapper = Json delegate(CrateHttpRequest req, const Json item) @safe;

/// Type of custom mappers used by serializers
alias MappingSerializer = Json delegate(Json item) nothrow @safe;

///
alias GetItemDelegate = IQuery delegate(string id) @safe;

///
alias GetListDelegate = IQuery delegate() @safe;

/// Representation for a primary key
struct ObjectId {
  Bson bsonObjectID;

  this(Json src) @safe {
    if(src.type != Json.Type.string) {
      return;
    }

    opAssign(src.get!string);
  }

  this(Bson src) @safe {
    bsonObjectID = src;
  }

  this(string src) @safe {
    opAssign(src);
  }

  static ObjectId generate() @safe {
    return ObjectId(Bson(BsonObjectID.generate));
  }

  static ObjectId fromString(string value) @safe {
    ObjectId id;

    if(value != "null") {
      id = value;
    }

    return id;
  }

  static ModelFields fields() @safe pure {
    return ModelFields();
  }

  bool isValid() {
    return bsonObjectID.type == bsonObjectID.Type.objectID;
  }

  Bson toBson() const @safe {
    if(bsonObjectID.type != Bson.Type.objectID) {
      return Bson.fromJson(Json.undefined);
    }

    return bsonObjectID;
  }

  static ObjectId fromBson(Bson src) @safe {
    return ObjectId(src);
  }

  Json toJson() const @safe {
    if(bsonObjectID.type != Bson.Type.objectID) {
      return Json.undefined;
    }

    return Json(toString);
  }

  static ObjectId fromJson(Json src) @safe {
    if(src.type != Json.Type.string) {
      return ObjectId(Bson.fromJson(Json.undefined));
    }

    string strId = src.get!string;

    return ObjectId.fromString(strId);
  }

  static ObjectId[] fromJsonList(T)(const T src) @trusted if(is(T == Json) || is(T == Json[])) {
    ObjectId[] list;
    list.length = src.length;

    static if(is(T == Json)) {
      enforce(src.type == Json.Type.array, "Expected a json list.");
    }

    foreach(size_t index, item; src) {
      if(item.type == Json.Type.object && "_id" in item) {
        list[index] = ObjectId.fromString(item["_id"].get!string);
        continue;
      }

      if(item.type != Json.Type.string) {
        list[index] = ObjectId(Bson.fromJson(Json.undefined));
        continue;
      }

      list[index] = ObjectId.fromString(item.get!string);
    }

    return list;
  }

  static ObjectId[] fromStringList(const string[] src) {
    ObjectId[] list;
    list.reserve(src.length);

    foreach(item; src) {
      list ~= ObjectId.fromString(item);
    }

    return list;
  }

  string toString() const @safe {
    if(bsonObjectID.type != Bson.Type.objectID) {
      return "";
    }

    string value = bsonObjectID.to!string;

    if(value[0] == '"') {
      return value[1..$-1];
    }

    if(value[0..9] == "ObjectID(") {
      return value[9..$-1];
    }

    return value;
  }

  void opAssign(ObjectId value) @safe {
    this.bsonObjectID = value.bsonObjectID;
  }

  void opAssign(string value) @safe {
    static immutable fill = "000000000000000000000000";

    if(value.length > 2 && value[0] == '"' && value[$-1..$] == `"`) {
      value = value[1..$-1];
    }

    if(value.length > 10 && value[0..9] == "ObjectID(") {
      value = value[9..$-1];
    }

    if(value.length < 24) {
      value = fill[0..$-value.length] ~ value;
    }

    try {
      bsonObjectID = BsonObjectID.fromString(value);
    } catch(Exception error) {
      () @trusted { logWarn("Invalid ObjectId: ", error.message); } ();
    }
  }

  bool opEquals (ref ObjectId other) {
    return bsonObjectID.opEquals(other.bsonObjectID);
  }

  bool opEquals (ObjectId other) {
    return bsonObjectID.opEquals(other.bsonObjectID);
  }

  static FieldDescription describe() pure @safe {
    FieldDescription description;
    description.isId = true;
    description.type = "string";

    return description;
  }
}

bool isObjectId(const string id) @safe {
  if(id.length != 24) {
    return false;
  }

  foreach(ch; id) {
    if(!isHexDigit(ch)) {
      return false;
    }
  }

  return true;
}

bool isObjectId(const Json id) @safe {
  if(id.type != Json.Type.string) {
    return false;
  }

  return isObjectId(id.to!string);
}

/// A Crate filter is a specialized middelware which filters the database data
/// based on a request parameters
interface ICrateFilter {
  /// Filter applied to all operations. This has priority over get and update
  IQuery any(HTTPServerRequest, IQuery);

  /// Filter applied to get operations. This should filter only the elements that
  /// a client has rights to see
  IQuery get(HTTPServerRequest, IQuery);

  /// Filter applied to get operations. This should filter only the elements that
  /// a client has rights to change
  IQuery update(HTTPServerRequest, IQuery);
}

/// Middlewares suported by the crate router
interface ICrateMiddleware {
  /// Middleware applied before all operations. This has priority over any other
  /// binded middlewares
  void any(HTTPServerRequest, HTTPServerResponse);

  /// Middleware applied for get list routes
  void getList(HTTPServerRequest, HTTPServerResponse);

  /// Middleware applied for get item routes
  void getItem(HTTPServerRequest, HTTPServerResponse);

  /// Middleware applied for create routes
  void create(HTTPServerRequest, HTTPServerResponse);

  /// Middleware applied for replace routes
  void replace(HTTPServerRequest, HTTPServerResponse);

  /// Middleware applied for patch routes
  void patch(HTTPServerRequest, HTTPServerResponse);

  /// Middleware applied for delete_ routes
  void delete_(HTTPServerRequest, HTTPServerResponse);
}

///
interface IFieldQuery {
  @safe {
    /// returns true if is the main query
    bool isRootQuery();

    ///
    IFieldQuery isNull();

    /// Match an item if exactly one field value is equal
    IFieldQuery equal(string value);
    /// ditto
    IFieldQuery equal(int value);
    /// ditto
    IFieldQuery equal(ulong value);
    /// ditto
    IFieldQuery equal(bool value);
    /// ditto
    IFieldQuery equal(ObjectId value);

    ///
    IFieldQuery greaterThan(SysTime);

    ///
    IFieldQuery lessThan(SysTime);

    /// ditto
    IFieldQuery exists();

    /// Match an item if the field equals the same string by ignoring the case
    IFieldQuery equalIgnoreCase(string value);

    /// Match an item if a value from an array contains one field value
    IFieldQuery arrayContains(string value);
    /// ditto
    IFieldQuery arrayContains(ulong value);
    /// ditto
    IFieldQuery arrayContains(ObjectId value);

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery anyOf(string[] values);
    /// ditto
    IFieldQuery anyOf(ObjectId[] values);
    /// ditto
    IFieldQuery anyOf(int[] values);

    /// Match an item using a substring
    IFieldQuery like(string value);

    /// Match an item if a field value contains at least one value from the values list
    IFieldQuery containsAny(string[] values);
    /// ditto
    IFieldQuery containsAny(ObjectId[] values);

    /// Match an item if an array field value contains all values from the values list
    IFieldQuery containsAll(string[] values);
    /// ditto
    IFieldQuery containsAll(ObjectId[] values);

    /// Match an item if an array field contains an object that has the field equals with the value
    IFieldQuery arrayFieldContains(string field, string value);

    /// ditto
    IFieldQuery arrayFieldContains(string field, ObjectId value);

    /// Match an item if an array field contains an object that has the field contains the value
    IFieldQuery arrayFieldLike(string field, string value);

    /// Match an item if it's coordinates is inside a polygon
    IFieldQuery insidePolygon(double[][][] value);

    /// Match an item if it's coordinates intersects a polygon
    IFieldQuery intersectsPolygon(double[][][] value);

    /// Match an item if it's coordinates intersects a geometry
    IFieldQuery intersects(Json geometry);

    /// Match an item if the polygon contains a point
    IFieldQuery containsPoint(double longitude, double latitude);

    /// Match/sort an item relative to a geographical point
    IFieldQuery near(double longitude, double latitude, double meters);

    ///
    IFieldQuery not();

    ///
    IQuery and();

    ///
    IQueryBuilder or();
  }
}

///
interface IQueryBuilder {
  @safe:
    ///
    IFieldQuery where(string field);

    /// Perform an `or` logical operation
    IQueryBuilder or();

    /// Returns the parent IQuery
    IQuery and();
}

///
interface IQuery : IQueryBuilder {
  @safe {
    ///
    IFieldQuery where(string field);

    ///
    IQuery searchText(string query);

    ///
    IQuery withProjection(string[] fields);

    /// Sort by fields
    IQuery sort(string field, int order);

    /// Execute the selector and return a range of JSONs
    InputRange!Json exec();

    /// Execute the selector and return a range of JSONs
    InputRange!Bson execBson();

    /// Execute the selector and return all distinct values of a key
    InputRange!Json distinct(string key);

    /// Limit the number of results
    IQuery limit(size_t nr);

    /// Skip a number of results
    IQuery skip(size_t nr);

    /// Get the size of the selected items
    size_t size();

    ///
    string debugString();
  }
}

/// Struct used to define response rules
struct CrateResponse {
  /// Response mime
  string mime;

  /// Response status code
  uint statusCode;

  ///
  string[string] headers;

  ///
  string description;

  /// Serializer used to convert the json
  @ignore ModelSerializer serializer;

  /// How the data will look after serialization
  Schema schema;

  ///
  Json toJson() const @safe {
    auto result = Json.emptyObject;
    result["mime"] = mime;
    result["statusCode"] = statusCode;
    result["headers"] = headers.serializeToJson;
    result["description"] = description;

    if(schema !is null) {
      result["schema"] = schema.serializeToJson;
    }

    return result;
  }

  ///
  static CrateResponse fromJson(Json src) @safe {
    return CrateResponse();
  }
}

/// Struct used to define request rules
struct CrateRequest {
  /// Request mime
  string mime;

  ///
  Parameter[] parameters;

  /// The expected method
  HTTPMethod method;

  ///
  string path;

  /// Serializer used to normalize the json
  @ignore ModelSerializer serializer;

  /// How the sent data should look like
  Schema schema;

  ///
  Json toJson() const @safe {
    auto result = Json.emptyObject;

    result["mime"] = mime;
    result["parameters"] = parameters.serializeToJson;
    result["method"] = method.serializeToJson;
    result["path"] = path;

    if(schema !is null) {
      result["schema"] = schema.serializeToJson;
    }

    return result;
  }

  ///
  static CrateRequest fromJson(Json src) @safe {
    return CrateRequest();
  }
}

/// Crate rules definitions
struct CrateRule {
  CrateRequest request;
  CrateResponse response;
  CrateResponse[] errorResponses;
  Schema[string] schemas;

  ModelDescription modelDescription;
}

/// Apply a rule to a URLRouter
URLRouter addRule(T)(URLRouter router, CrateRule rule, T handler) {
  import crate.http.cors : Cors;

  auto cors = Cors(router, rule.request.path);

  return router.match(rule.request.method, rule.request.path, cors.add(rule.request.method, handler));
}

struct CrateConfig(T) {
  bool getList = true;
  bool getItem = true;
  bool addItem = true;
  bool deleteItem = true;
  bool replaceItem = true;
  bool updateItem = true;

  static if(is(T == void)) {
    string singular;
    string plural;
  } else {
    string singular = Singular!T;
    string plural = Plural!T;
  }
}

///
Json itemResolver(string model, string id) @trusted {
  enforce(model in crateGetters, "There is no getter for the `" ~ model ~ "` model. Can not get item with id `" ~ id ~ "`.");

  try {
    return crateGetters[model](id).exec.front;
  } catch(Exception e) {
    logError("Can't fetch `%s` with id `%s. %s.`", model, id, e.message);

    return Json(null);
  }
}

/// Base interface for classes that store json items
interface CrateAccess {
  @safe:
    /// Get items from the crate
    IQuery get();

    /// Get one item by id
    IQuery getItem(const string id);

    /// Add a new item
    Json addItem(const Json item);

    /// Update an existing item
    Json updateItem(const Json item);

    /// Delete an existing item
    void deleteItem(const string id);
}

/// Base interface for classes that store json items
interface Crate(Type) : CrateAccess {
}

///
interface ModelSerializer {
  @safe {
    /// Get the model definitin assigned to the serializer
    ModelDescription definition();

    /// Prepare the data to be sent to the client
    InputRange!string denormalise(InputRange!Json data, MappingSerializer mapper = null);

    /// ditto
    InputRange!string denormalise(Json data, MappingSerializer mapper = null);

    /// Get the client data and prepare it for deserialization
    Json normalise(string id, Json data);

    /// The name of the schema that contains the response for one item
    string respondItemSchemaKey();

    /// The name of the schema that contains the response for an item list
    string respondListSchemaKey();

    /// The name of schema that defines one item
    string itemKey();

    /// The schemas for the data used by the serialzier
    Schema[string] schemas();
  }
}

/// Interface used to validate string values
interface IValueValidator {
  /// Check if the value is valid and return a list of error messages
  string[] validate(string value);
}

/// Interface for classes that wrap middlewares with the same purpose
interface IRequestWrapper {
  /// Applies the middlewares and passes the result to the next handler
  CrateHandler handler(CrateHandler next) @safe;
}

/// Interface for classes that wrap middlewares with the same purpose
interface IMapperWrapper {
  /// Applies the mappers and returns the result
  Json handler(CrateHttpRequest, const Json) @safe;
}


/// Convert a vibe handler to a crate handler
CrateHandler toCrateHandler(VibeHandler handler) {
  void f(CrateHttpRequest req, CrateHttpResponse res) {
    handler(req.toVibeRequest, res.toVibeResponse);
  }

  return &f;
}

/// Convert a crate handler to a vibe handler
VibeHandler toVibeHandler(CrateHandler handler) {
  void f(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    handler(new CrateVibeHttpRequest(req), new CrateVibeHttpResponse(res));
  }

  return &f;
}