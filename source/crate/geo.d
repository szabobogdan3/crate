/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.geo;

import std.algorithm;
import std.math;
import std.exception;
import vibe.data.json;
import geo.json;

version(unittest) {
  import fluent.asserts;
}

alias P = PositionT!double;


//// Given three colinear points p, q, r, the function checks if
//// point q lies on line segment 'pr'
bool onSegment(const Point p, const Point q, const Point r) @safe pure {
  if (q.coordinates[0] <= max(p.coordinates[0], r.coordinates[0]) &&
      q.coordinates[0] >= min(p.coordinates[0], r.coordinates[0]) &&
      q.coordinates[1] <= max(p.coordinates[1], r.coordinates[1]) &&
      q.coordinates[1] >= min(p.coordinates[1], r.coordinates[1])) {
        return true;
  }

  return false;
}

///
enum Orientation {
  ///
  colinear,
  ///
  clockwise,
  ///
  counterclockwise
}

/// To find orientation of ordered triplet (p, q, r).
Orientation orientation(const Point p, const Point q, const Point r) @safe pure {
  auto val = (q.coordinates[1] - p.coordinates[1]) * (r.coordinates[0] - q.coordinates[0]) -
            (r.coordinates[1] - q.coordinates[1]) * (q.coordinates[0] - p.coordinates[0]);

  if (val == 0) {
    return Orientation.colinear;
  }

  return (val > 0) ? Orientation.clockwise : Orientation.counterclockwise;
}

/// check colinear points
unittest {
  // orientation(Point([1, 2]), Point([2, 2]), Point([3, 2])).should.equal(Orientation.colinear);
  // orientation(Point([1, 1]), Point([1, 2]), Point([1, 3])).should.equal(Orientation.colinear);
  // orientation(Point([1, 1]), Point([2, 2]), Point([3, 3])).should.equal(Orientation.colinear);

  // orientation(Point([1, 1]), Point([4, 3]), Point([4, 0])).should.equal(Orientation.clockwise);

  // orientation(Point([2, 2]), Point([1, 1]), Point([1, -1])).should.equal(Orientation.counterclockwise);
}

/// The function that returns true if line segment 'p1q1'
/// and 'p2q2' intersect.
bool doIntersect(const LineString line1, const LineString line2) @safe pure {
  auto p1 = Point(line1.coordinates[0]);
  auto q1 = Point(line1.coordinates[1]);
  auto p2 = Point(line2.coordinates[0]);
  auto q2 = Point(line2.coordinates[1]);

  // Find the four orientations needed for general and
  // special cases
  auto o1 = orientation(p1, q1, p2);
  auto o2 = orientation(p1, q1, q2);
  auto o3 = orientation(p2, q2, p1);
  auto o4 = orientation(p2, q2, q1);

  // General case
  if (o1 != o2 && o3 != o4) {
    return true;
  }

  // Special Cases
  // p1, q1 and p2 are colinear and p2 lies on segment p1q1
  if (o1 == Orientation.colinear && onSegment(p1, p2, q1)) {
    return true;
  }

  // p1, q1 and p2 are colinear and q2 lies on segment p1q1
  if (o2 == Orientation.colinear && onSegment(p1, q2, q1)) {
    return true;
  }

  // p2, q2 and p1 are colinear and p1 lies on segment p2q2
  if (o3 == Orientation.colinear && onSegment(p2, p1, q2)) {
    return true;
  }

  // p2, q2 and q1 are colinear and q1 lies on segment p2q2
  if (o4 == Orientation.colinear && onSegment(p2, q1, q2)) {
    return true;
  }

  return false;
}

/// doIntersect test
unittest {
  //LineString([[-70, 30], [-70, 50]]).doIntersect(LineString([[-73.856077, 40.848447], [1000, 40.848447]]))
  //  .should.equal(true);

  // LineString([[-70, 30], [-70, 50]]).doIntersect(LineString([[-63, 40], [1000, 40]]))
  //   .should.equal(false);
}

/// Returns true if the point lies inside the polygon

bool isInside(const(PositionT!double) point, const Polygon polygon) @safe pure {
  return isInside(Point(point), polygon);
}

bool isInside(const Point point, const Polygon polygon) @safe pure {
  if(polygon.coordinates.length == 0 || polygon.coordinates[0].length < 3) {
    return false;
  }

  auto extreme = Point(PositionT!double(double.max, point.coordinates[1]));

  size_t count;
  size_t i;

  do {
      auto next = (i + 1) % polygon.coordinates[0].length;

      // Check if the line segment from 'p' to 'extreme' intersects
      // with the line segment from 'polygon[i]' to 'polygon[next]'
      auto segment = LineString([polygon.coordinates[0][i], polygon.coordinates[0][next]]);
      auto pointSegment = LineString([point.coordinates, extreme.coordinates]);

      if (doIntersect( segment, pointSegment )) {
        count++;
      }

      i = next;
  } while (i != 0);

  return count % 2 == 1;
}

/// ditto
bool isInside(const MultiPoint points, const Polygon polygon) @safe pure {
  bool result;

  foreach(coordinates; points.coordinates) {
    result = isInside(coordinates, polygon);

    if(result) {
      break;
    }
  }

  return result;
}

// ditto
bool isInside(const LineString line, const Polygon polygon) @safe pure {
  return line.coordinates.map!(point => isInside(point, polygon)).canFind(true);
}

// ditto
bool isInside(const Polygon value, const Polygon polygon) @safe pure {
  return value.coordinates[0].map!(point => isInside(point, polygon)).canFind(true);
}

// ditto
bool isInside(const MultiLineString line, const Polygon polygon) @safe pure {
  bool result;

  foreach(coordinates; line.coordinates) {
    result = coordinates.map!(point => isInside(point, polygon)).canFind(true);

    if(result) {
      break;
    }
  }

  return result;
}

// ditto
bool isInside(const MultiPolygon value, const Polygon polygon) @safe pure {
  bool result;

  foreach(coordinateGroup; value.coordinates) {
    foreach(coordinates; coordinateGroup) {
      result = coordinates.map!(point => isInside(point, polygon)).canFind(true);

      if(result) {
        break;
      }
    }
  }

  return result;
}

/// Check if a point is inside a polygon
unittest {
  ///
  // auto polygon = Polygon([[[0, 0], [10, 0], [10, 10], [0, 10]]]);

  // Point([20, 20]).isInside(polygon).should.equal(false);
  // Point([-1, 10]).isInside(polygon).should.equal(false);
  // Point([5, 5]).isInside(polygon).should.equal(true);

  // polygon = Polygon([[[0, 0], [5, 5], [5, 0]]]);
  // Point([3, 3]).isInside(polygon).should.equal(true);
  // Point([5, 1]).isInside(polygon).should.equal(true);
  // Point([8, 1]).isInside(polygon).should.equal(false);

  // polygon = Polygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30]]]);
  // Point([-73.856077, 40.848447]).isInside(polygon).should.equal(true);
}


/// ditto
bool intersects(const MultiPoint points, const Polygon polygon) @safe pure {
  bool result;

  foreach(coordinates; points.coordinates) {
    result = isInside(coordinates, polygon);

    if(result) {
      break;
    }
  }

  return result;
}

//
bool intersects(Polygon polygon1, Polygon polygon2) @safe pure {
  if(polygon1 == polygon2) {
    return true;
  }

  foreach(ring1; polygon1.coordinates) {
    foreach(ring2; polygon2.coordinates) {
      const len1 = ring1.length;
      const len2 = ring2.length;

      foreach(i; 2 .. len1 ) {
        foreach(j; 2 .. len2 ) {
          if(LineString(ring1[i-2 .. i]).doIntersect(LineString(ring2[j-2 .. j]))) {
            return true;
          }
        }
      }
    }
  }

  return false;
}

//
bool intersects(MultiPolygon polygon1, Polygon polygon2) @safe pure {
  foreach(coordinates; polygon1.coordinates) {
    if(intersects(Polygon(coordinates), polygon2)) {
      return true;
    }
  }

  return false;
}

bool intersects(LineString line, Polygon polygon2) @safe pure {
  import std.stdio;

  foreach(ring; polygon2.coordinates) {
    const len = ring.length;

    foreach(j; 2 .. len ) {
      if(line.doIntersect(LineString(ring[j-2 .. j]))) {
        return true;
      }
    }
  }

  return false;
}

//
bool intersects(MultiLineString line, Polygon polygon2) @safe pure {
  foreach(coordinates; line.coordinates) {
    if(intersects(LineString(coordinates), polygon2)) {
      return true;
    }
  }

  return false;
}

/// Two polygons with no common areas should not intersect
unittest {
  auto polygon1 = Polygon([[[0, 0], [10, 0], [10, 10], [0, 10]]]);
  auto polygon2 = Polygon([[[20, 20], [30, 20], [30, 30], [20, 30]]]);

  polygon1.intersects(polygon2).should.equal(false);
}

/// Two polygons with common areas should intersect
unittest {
  auto polygon1 = Polygon([[[0, 0], [10, 0], [10, 10], [0, 10]]]);
  auto polygon2 = Polygon([[[5, 5], [30, 5], [30, 30], [5, 30]]]);

  polygon1.intersects(polygon2).should.equal(true);
}

/// A polygon should intersect itself
unittest {
  auto polygon = Polygon([[[0, 0], [10, 0], [10, 10], [0, 10]]]);

  polygon.intersects(polygon).should.equal(true);
}

/// Calculate a ring area
double area(const double[2][] coordinates) @safe pure {
  if (coordinates.length <= 2) {
    return 0;
  }

  ///
  double rad(const double a) pure @safe @nogc {
    return  a * PI / 180;
  }

  double result = 0;
  enum radius = 6_378_137;

  foreach(i; 0 .. coordinates.length - 1) {
    auto p1 = coordinates[i];
    auto p2 = coordinates[i + 1];

    result += rad(p2[0] - p1[0]) * (2 + sin(rad(p1[1])) + sin(rad(p2[1])));
  }

  return result * radius * radius / 2;
}

/// poluygon area using Shoelace formula
double absoluteArea(const double[2][] coordinates) @safe pure {
  double result = 0;

  foreach(i; 0..coordinates.length-1) {
    auto x = coordinates[i][0];
    auto y = coordinates[i][1];

    result = result + (coordinates[i + 1][0] - x) * (coordinates[i + 1][1] + y);
  }

  return result;
}
