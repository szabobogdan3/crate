/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.api.rest.policy;

import crate.base;
import crate.api.rest.serializer;
import crate.routing.restapi;
import crate.http.request;
import crate.ctfe;

import vibe.data.json;
import vibe.http.common;

import openapi.definitions;
import introspection.callable;

import std.string, std.stdio;

struct RestApi {
  static immutable {
    string name = "Rest API";
    string mime = "application/json";
  }

  alias Serializer = RestApiSerializer;
  alias Routing = RestApiRouting;

  static:
    private CrateRule templateRule(const ModelDescription definition) {
      auto serializer = new RestApiSerializer(definition);
      CrateRule rule;
      rule.request.serializer = serializer;
      rule.response.serializer = serializer;
      rule.response.description = "[no description]";

      rule.schemas = serializer.schemas;

      return rule;
    }

    private static Parameter[] idParameter(const string action) {
      Parameter parameter;

      parameter.name = "id";
      parameter.in_ = ParameterIn.path;
      parameter.required = true;
      parameter.description = "The id of the object that you want to " ~ action;
      parameter.schema = new Schema;
      parameter.schema.type = SchemaType.string;

      return [parameter];
    }

    CrateRule replace(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.put;
      rule.request.method = HTTPMethod.PUT;
      rule.request.mime = this.mime;
      rule.request.parameters = idParameter("replace");
      rule.request.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.response.statusCode = 200;
      rule.response.mime = this.mime;
      rule.response.description = "Returns the new `" ~ definition.singular ~ "` using the Rest API format.";
      rule.response.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.modelDescription = rule.response.serializer.definition;

      return rule;
    }

    CrateRule create(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.post;
      rule.request.method = HTTPMethod.POST;
      rule.request.mime = this.mime;
      rule.response.description = "Returns the newly created `" ~ definition.singular ~ "` using the Rest API format.";
      rule.request.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.response.statusCode = 200;
      rule.response.mime = this.mime;
      rule.response.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.modelDescription = rule.response.serializer.definition;

      return rule;
    }

    CrateRule delete_(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.delete_;
      rule.request.method = HTTPMethod.DELETE;
      rule.request.parameters = idParameter("delete");

      rule.response.statusCode = 204;
      rule.response.mime = "";
      rule.response.description = "Returns a void body.";

      return rule;
    }

    CrateRule getItem(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.get;
      rule.request.method = HTTPMethod.GET;
      rule.request.parameters = idParameter("retreive");
      rule.request.mime = this.mime;

      rule.response.statusCode = 200;
      rule.response.mime = this.mime;
      rule.response.description = "Returns the requested `" ~ definition.singular ~ "` using the Rest API format.";
      rule.response.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.modelDescription = rule.response.serializer.definition;

      return rule;
    }

    CrateRule patch(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.method = HTTPMethod.PATCH;
      rule.request.path = routing.get;
      rule.request.parameters = idParameter("patch");
      rule.request.mime = this.mime;
      rule.request.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.response.mime = "application/json";
      rule.response.statusCode = 200;
      rule.response.mime = this.mime;
      rule.response.description = "Returns the updated `" ~ definition.singular ~ "` using the Rest API format.";
      rule.response.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondItemSchemaKey ~ `" }`).parseJsonString);

      rule.modelDescription = rule.response.serializer.definition;

      return rule;
    }

    CrateRule getList(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.getList;
      rule.request.method = HTTPMethod.GET;

      rule.response.mime = "application/json";
      rule.response.statusCode = 200;
      rule.response.description = "Returns all `" ~ definition.plural ~ "` using the Rest API format.";
      rule.response.schema = Schema.fromJson(
        (`{ "$ref": "#/components/schemas/` ~ rule.response.serializer.respondListSchemaKey ~ `" }`).parseJsonString);

      rule.modelDescription = rule.response.serializer.definition;

      return rule;
    }

    CrateRule getResource(string path)(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.get ~ "/" ~ path.replace("[", "/").replace("].", "/").replace(".", "/");

      rule.request.method = HTTPMethod.GET;
      rule.request.parameters = idParameter("access");
      rule.response.description = "Returns the requested resource content.";
      rule.response.statusCode = 200;

      return rule;
    }

    CrateRule setResource(string path)(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.get ~ "/" ~ path.replace("[", "/").replace("].", "/").replace(".", "/");

      rule.request.method = HTTPMethod.POST;
      rule.request.parameters = idParameter("access");
      rule.response.statusCode = 201;
      rule.response.description = "A void body.";

      return rule;
    }

    CrateRule action(MethodReturnType, ParameterType, string actionName)(const ModelDescription definition) {
      auto routing = new RestApiRouting(definition);
      CrateRule rule;

      rule.request.path = routing.get ~ "/" ~ actionName;
      rule.request.parameters = idParameter("run the action");

      static if(is(ParameterType == void) || is(ParameterType == CrateHttpRequest)) {
        rule.request.method = HTTPMethod.GET;
      } else {
        rule.request.method = HTTPMethod.POST;
      }

      rule.response.statusCode = 200;
      rule.response.description = "The action response.";
      rule.response.mime = "text/plain; charset=UTF-8";

      return rule;
    }

    CrateRule itemOperation(string operationId)(const ModelDescription definition, const Callable operation) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.get ~ "/" ~ operationId;
      rule.request.method = HTTPMethod.GET;
      rule.request.parameters = idParameter("use");
      rule.request.mime = "text/plain";

      if(operation.parameters.length == 1 && operation.parameters[0].type.isBuiltinType) {
        rule.request.method = HTTPMethod.POST;
        rule.request.schema = new Schema();
        rule.request.schema.type = SchemaType.string;
      }

      rule.response.statusCode = 200;

      return rule;
    }
}
