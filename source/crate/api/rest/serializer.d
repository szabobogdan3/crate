/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.api.rest.serializer;

import crate.base, crate.ctfe;
import crate.error;
import crate.generators.openapi;

import vibe.data.json;
import vibe.data.bson;

import openapi.definitions;

import std.meta, std.string, std.exception, std.array;
import std.algorithm;
import std.traits, std.stdio, std.meta, std.conv;
import std.range;

version(unittest) {
  import fluent.asserts;
}

///
class RestApiSerializer : ModelSerializer {
  private {
    ModelDescription _definition;
  }

  this(const ModelDescription definition) pure {
    _definition = definition.dup;
  }

  @safe:
    /// Get the model definitin assigned to the serializer
    ModelDescription definition() {
      return _definition;
    }

    /// Prepare the data to be sent to the client
    InputRange!string denormalise(InputRange!Json data, MappingSerializer mappingSerializer = null) @trusted {
      auto name = definition.plural[0..1].toLower ~ definition.plural[1 .. $];
      auto prefix = [ `{"`, name, `":[` ].inputRangeObject;
      auto postfix = [ `]}` ].inputRangeObject;

      InputRange!string items;

      if(mappingSerializer is null) {
        items = data.map!(a => a.to!string).inputRangeObject;
      } else {
        items = data
          .map!(a => mappingSerializer(a).to!string)
          .inputRangeObject;
      }

      if(items.empty) {
        return prefix.chain(postfix).inputRangeObject;
      }

      auto firstItem = items.front;
      items.popFront;

      auto otherItems = items.map!(a => "," ~ a);

      return prefix
        .chain([ firstItem ])
        .chain(otherItems)
        .chain(postfix)
        .inputRangeObject;
    }

    /// ditto
    InputRange!string denormalise(Json data, MappingSerializer mappingSerializer = null)@trusted {
      Json result = Json.emptyObject;
      const name = _definition.singular[0..1].toLower ~ _definition.singular[1 .. $];

      string value;

      if(mappingSerializer is null) {
        value = data.to!string;
      } else {
        value = mappingSerializer(data).to!string;
      }

      auto prefix = [ `{"`, name, `":` ].inputRangeObject;
      auto postfix = [ `}` ].inputRangeObject;

      return prefix
        .chain([value])
        .chain(postfix)
        .inputRangeObject;

    }

    /// Get the client data and prepare it for deserialization
    Json normalise(string id, Json data) @trusted {
      auto name = definition.singular[0..1].toLower ~ definition.singular[1 .. $];

      enforce!CrateValidationException(data.type == Json.Type.object,
          "Expected to get a Json object instead of `" ~ data.type.to!string ~ "`");
      enforce!CrateValidationException(name in data,
          "object type expected to be `" ~ name ~ "`");

      foreach(field; _definition.fields.all) {
        if(field.isId) {
          if(id != "") {
            data[name][field.name] = id;
          } else {
            data[name][field.name] = null;
          }
        }
      }

      return data[name];
    }

    ///
    string singularKey() {
      return _definition.singular[0..1].toLower ~ _definition.singular[1 .. $];
    }

    ///
    string pluralKey() {
      return _definition.plural[0..1].toLower ~ _definition.plural[1 .. $];
    }

    ///
    string respondItemSchemaKey() {
      return "oneRest" ~ _definition.singular;
    }

    ///
    string respondListSchemaKey() {
      return "manyRest" ~ _definition.plural;
    }

    ///
    string itemKey() {
      return _definition.singular;
    }

    /// get the list of schemas used by the serializer
    Schema[string] schemas() {
      Schema[string] result;

      auto key = itemKey;
      auto singular = respondItemSchemaKey;
      auto plural = respondListSchemaKey;

      result[key] = _definition.fields.toOpenApiSchema;

      foreach(field; _definition.fields.relations) {
        auto description = field.model.idField;
        description.listType = field.listType;

        result[key].properties[field.publicName] = description.toOpenApiSchema;
        result[key].properties[field.publicName].title = "id of [" ~ field.model.singular ~ "]";
      }

      result[singular] = new Schema;
      result[plural] = new Schema;

      result[singular].type = SchemaType.object;
      result[singular].properties[singularKey] = new Schema;
      result[singular].properties[singularKey]._ref = "#/components/schemas/" ~ key;

      result[plural].type = SchemaType.object;
      result[plural].properties[pluralKey] = new Schema;
      result[plural].properties[pluralKey].items = new Schema;
      result[plural].properties[pluralKey].type = SchemaType.array;
      result[plural].properties[pluralKey].items._ref = "#/components/schemas/" ~ key;

      return result;
    }
}

/// Serialize/deserialize a simple struct
unittest {
  struct TestModel {
    string id;

    string field1;
    int field2;
  }

  auto fields = describe!TestModel;
  auto serializer = new RestApiSerializer(fields);

  //test the deserialize method
  auto serialized = `{
    "testModel": {
        "field1": "Ember Hamster",
        "field2": 5
    }
  }`.parseJsonString;

  auto deserialized = serializer.normalise("ID", serialized);
  assert(deserialized["id"] == "ID");
  assert(deserialized["field1"] == "Ember Hamster");
  assert(deserialized["field2"] == 5);

  //test the denormalise method
  auto value = serializer.denormalise(deserialized).joiner.array.to!string.parseJsonString;
  assert(value["testModel"]["id"] == "ID");
  assert(value["testModel"]["field1"] == "Ember Hamster");
  assert(value["testModel"]["field2"] == 5);
}

/// Apply a custom mapper
unittest {
  struct TestModel {
    string id;
  }

  auto fields = describe!TestModel;
  auto serializer = new RestApiSerializer(fields);

  Json mapper(Json data) nothrow {
    try {
      data["other"] = true;
    } catch(Exception e) {}
    return data;
  }

  /// test the denormalise method
  auto value = serializer.denormalise(`{ "id": "1" }`.parseJsonString, &mapper).joiner.array.to!string.parseJsonString;
  value.should.equal(`{ "testModel": { "id": "1", "other": true } }`.parseJsonString);

  value = serializer.denormalise([`{ "id": "1" }`.parseJsonString].inputRangeObject, &mapper).joiner.array.to!string.parseJsonString;
  value.should.equal(`{ "testModels": [{ "id": "1", "other": true }] }`.parseJsonString);
}

/// Serialize an array of structs
unittest {
  struct TestModel {
    string id;

    string field1;
    int field2;
  }

  auto fields = describe!TestModel;
  auto serializer = new RestApiSerializer(fields);

  auto data = [
    TestModel("ID1", "Ember Hamster", 5).serializeToJson,
    TestModel("ID2", "Ember Hamster2", 6).serializeToJson
  ].inputRangeObject;

  //test the serialize method
  auto value = serializer.denormalise(data).joiner.array.to!string.parseJsonString;

  assert(value["testModels"][0]["id"] == "ID1");
  assert(value["testModels"][0]["field1"] == "Ember Hamster");
  assert(value["testModels"][0]["field2"] == 5);

  assert(value["testModels"][1]["id"] == "ID2");
  assert(value["testModels"][1]["field1"] == "Ember Hamster2");
  assert(value["testModels"][1]["field2"] == 6);
}

/// Check denormalised type
unittest {
  @("singular: SingularModel", "plural: PluralModel")
  struct TestModel {
    @optional string _id;
  }

  auto fields = describe!TestModel;
  auto serializer = new RestApiSerializer(fields);
  auto valueSingular = serializer.denormalise(TestModel().serializeToJson).joiner.array.to!string.parseJsonString;
  auto valuePlural = serializer.denormalise([ TestModel().serializeToJson ].inputRangeObject).joiner.array.to!string.parseJsonString;

  valueSingular.should.equal(`{
    "singularModel": {
      "_id": ""
    }
  }`.parseJsonString);

  valuePlural.should.equal(`{
    "pluralModel": [{ "_id": "" }]
  }`.parseJsonString);

  assert("singularModel" in valueSingular);
  assert("pluralModel" in valuePlural);

  assert("_id" in serializer.normalise("", valueSingular));
}

/// Check denormalised static arrays
unittest {
  struct TestModel {
    string _id;
    double[2][] child;
  }

  auto fields = describe!TestModel;

  TestModel test = TestModel("id1");

  test.child = [[1, 2]];

  auto serializer = new RestApiSerializer(fields);
  auto value = serializer.denormalise(test.serializeToJson).joiner.array.to!string.parseJsonString;

  value["testModel"]["child"].length.should.equal(1);
  value["testModel"]["child"][0][0].to!string.should.equal("1");
  value["testModel"]["child"][0][1].to!string.should.equal("2");
}

/// Get the schema for a struct
unittest {
  struct SomeData {
    bool value;
  }

  struct SomeModel {
    string _id;
  }

  struct TestModel {
    string _id;
    double value;
    @optional double[2][] pairList;

    SomeData data1;
    @optional SomeData data2;

    SomeModel model1;
    @optional SomeModel model2;
  }

  auto fields = describe!TestModel;
  auto serializer = new RestApiSerializer(fields);

  auto schemas = serializer.schemas();

  schemas["TestModel"].toJson.should.equal(`{
    "required": ["value","data1","model1"],
    "type": "object",
    "properties": {
      "data1": {
        "required": ["value"],
        "type": "object",
        "properties": {"value": { "type": "boolean" }}
      },
      "pairList": {
        "type": "array",
        "items": { "type": "array", "items": { "type": "number"}}
      },
      "value": {"type": "number"},
      "model1": {
        "type": "string",
        "title": "id of [SomeModel]"
      },
      "_id": {
        "type": "string"
      },
      "data2": {
        "required": ["value"],
        "type": "object",
        "properties": { "value": { "type": "boolean" }}
      },
      "model2": { "type": "string", "title": "id of [SomeModel]" }
    }}`.parseJsonString);

  schemas["manyRestTestModels"].toJson.should.equal(`{
    "type": "object",
    "properties": {
      "testModels": {
        "type": "array",
        "items": {
          "$ref": "#/components/schemas/TestModel"
  }}}}`.parseJsonString);

  schemas["oneRestTestModel"].toJson.should.equal(`{
   "type": "object",
    "properties": {
      "testModel": {
          "$ref": "#/components/schemas/TestModel"
  }}}`.parseJsonString);

  schemas.length.should.equal(3);
}
