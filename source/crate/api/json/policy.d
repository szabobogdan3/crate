/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.api.json.policy;

import crate.base;
import crate.api.json.serializer;
import crate.routing.jsonapi;
import crate.ctfe;

import vibe.data.json;
import vibe.http.common;

import openapi.definitions;

import std.string, std.stdio, std.algorithm, std.array, std.typecons;

/// The Json Api policy. http://jsonapi.org/
struct JsonApi {
  static immutable {
    string name = "Json API";
    string mime = "application/vnd.api+json";
  }

  alias Serializer = JsonApiSerializer;
  alias Routing = JsonApiRouting;

  static:
    private CrateRule templateRule(const ModelDescription definition) {
      auto serializer = new Serializer(definition);
      CrateRule rule;

      rule.request.serializer = serializer;

      rule.response.mime = "application/vnd.api+json";
      rule.response.serializer = serializer;

      return rule;
    }

    CrateRule create(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.post;
      rule.request.method = HTTPMethod.POST;

      rule.response.statusCode = 201;
      rule.response.headers["Location"] = ":base_uri" ~ routing.getList ~ "/:id";

      return rule;
    }

    CrateRule replace(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.put;
      rule.request.method = HTTPMethod.PUT;
      rule.response.statusCode = 200;

      return rule;
    }

    CrateRule delete_(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.delete_;
      rule.request.method = HTTPMethod.DELETE;
      rule.response.statusCode = 204;

      return rule;
    }

    CrateRule getItem(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.method = HTTPMethod.GET;
      rule.request.path = routing.get;
      rule.response.statusCode = 200;

      return rule;
    }

    CrateRule patch(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.method = HTTPMethod.PATCH;
      rule.request.path = routing.get;
      rule.response.statusCode = 200;

      return rule;
    }

    CrateRule getList(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.getList;
      rule.request.method = HTTPMethod.GET;
      rule.response.statusCode = 200;

      return rule;
    }

    CrateRule getResource(string path)(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.get ~ "/" ~ path.replace("[", "/").replace("].", "/").replace(".", "/");
      rule.request.method = HTTPMethod.GET;
      rule.response.statusCode = 200;

      return rule;
    }

    CrateRule setResource(string path)(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule = templateRule(definition);

      rule.request.path = routing.get ~ "/" ~ path.replace("[", "/").replace("].", "/").replace(".", "/");
      rule.request.method = HTTPMethod.POST;
      rule.response.statusCode = 201;

      return rule;
    }

    CrateRule action(MethodReturnType, ParameterType, string actionName)(const ModelDescription definition) {
      auto routing = new Routing(definition);
      CrateRule rule;

      rule.request.path = routing.get ~ "/" ~ actionName;
      rule.request.method = HTTPMethod.GET;
      rule.response.statusCode = 200;

      return rule;
    }
}

class CrateJsonApiPolicy
{
  string name() inout pure nothrow
  {
    return "Json API";
  }

  string mime() inout pure nothrow
  {
    return "application/vnd.api+json";
  }
}

CrateRoutes defineRoutes(T)(const CrateJsonApiPolicy, const CrateConfig!T config)
{
  CrateRoutes definedRoutes;

  definedRoutes.schemas = schemas!T;
  definedRoutes.paths = config.paths!T;

  return definedRoutes;
}

string basePath(T)(const CrateConfig!T config) pure
{
  return "/" ~ config.plural.toLower;
}

version (unittest)
{
  struct TestModel
  {
    string _id;
  }
}

@("It should have the right mime")
unittest
{
  auto policy = new const CrateJsonApiPolicy();
  assert(policy.mime == "application/vnd.api+json");
}
