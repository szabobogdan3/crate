/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.api.json.serializer;

import crate.base, crate.ctfe;
import crate.error;

import vibe.data.json;
import vibe.data.bson;

import openapi.definitions;

import std.meta, std.conv, std.exception;
import std.algorithm.searching, std.algorithm.iteration, std.array;
import std.traits, std.stdio, std.string;
import std.range.interfaces;

///
class JsonApiSerializer : ModelSerializer {
  private {
    ModelDescription _definition;
    const CrateJsonApiSerializer serializer;
  }

  this(const ModelDescription definition) pure {
    _definition = definition.dup;
    serializer = new const CrateJsonApiSerializer();
  }

  @safe:
    Schema[string] schemas() {
      assert(false, "not implemented");
    }

    /// Get the model definitin assigned to the serializer
    ModelDescription definition() {
      return _definition;
    }

    /// Prepare the data to be sent to the client
    InputRange!string denormalise(InputRange!Json data, MappingSerializer mappingSerializer = null) {
      return serializer.denormalise(data, _definition);
    }

    //dito
    InputRange!string denormalise(Json data, MappingSerializer mappingSerializer = null) {
      return serializer.denormalise(data, _definition);
    }

    /// Get the client data and prepare it for deserialization
    Json normalise(string id, Json data) {
      return serializer.normalise(id, data, _definition);
    }

    string respondItemSchemaKey(){
      return "";
    }

    string respondListSchemaKey() {
      return "";
    }

    string itemKey() {
      return "";
    }
}

///
class CrateJsonApiSerializer
{
  @trusted:
  /// Prepare the data to be sent to the client
  InputRange!string denormalise(InputRange!Json data, ModelDescription definition) inout {
    Json value = Json.emptyObject;

    value["data"] = Json.emptyArray;

    foreach(item; data) {
      value["data"] ~= denormalise(item, definition).joiner.array.to!string.parseJsonString["data"];
    }

    return [ value.to!string ].inputRangeObject;
  }

  /// ditto
  InputRange!string denormalise(Json data, ModelDescription definition) inout {
    Json value = Json.emptyObject;

    value["data"] = Json.emptyObject;
    value["data"]["type"] = type(definition);
    value["data"]["attributes"] = Json.emptyObject;
    value["data"]["relationships"] = Json.emptyObject;
    foreach(field; definition.fields.basic) {
      if(field.isId) {
        value["data"]["id"] = data[field.publicName];
      } else {
        value["data"]["attributes"][field.publicName] = data[field.publicName];
      }
    }

    foreach(field; definition.fields.objects) {
      if(field.isId) {
        value["data"]["id"] = data[field.publicName];
      } else {
        value["data"]["attributes"][field.publicName] = data[field.publicName];
      }
    }

    foreach(field; definition.fields.relations) {
      value["data"]["relationships"][field.publicName] = Json.emptyObject;

      if(data[field.publicName].type == Json.Type.object) {
        value["data"]["relationships"][field.publicName] = denormalise(data[field.publicName], field.model).joiner.array.to!string.parseJsonString;
      }

      if(data[field.publicName].type == Json.Type.array) {
        auto list = (cast(Json[]) data[field.publicName]).inputRangeObject;
        value["data"]["relationships"][field.publicName] = denormalise(list, field.model).joiner.array.to!string.parseJsonString;
      }
    }

    return [ value.to!string ].inputRangeObject;
  }

  /// Get the client data and prepare it for deserialization
  Json normalise(string id, Json data, ModelDescription definition) inout
  {
    enforce!CrateValidationException("data" in data, "`data` field is missing");
    enforce!CrateValidationException("type" in data["data"], "`data.type` field is missing");
    enforce!CrateValidationException(data["data"]["type"].to!string == type(definition),
        "data.type expected to be `" ~ type(definition) ~ "` instead of `" ~ data["data"]["type"].to!string ~ "`");

    auto normalised = Json.emptyObject;

    foreach(field; definition.fields.basic) {
      if(field.isId) {
        normalised[field.publicName] = id;
      } else {
        normalised[field.publicName] = data["data"]["attributes"][field.publicName];
      }
    }

    foreach(field; definition.fields.objects) {
      if(field.isId) {
        normalised[field.publicName] = id;
      } else {
        normalised[field.publicName] = data["data"]["attributes"][field.publicName];
      }
    }

    foreach(field; definition.fields.relations) {

      if(data["data"]["relationships"][field.publicName]["data"].type == Json.Type.object) {
        normalised[field.publicName] = data["data"]["relationships"][field.publicName]["data"]["id"];
      }

      if(data["data"]["relationships"][field.publicName]["data"].type == Json.Type.array) {
        normalised[field.publicName] = (cast(Json[]) data["data"]["relationships"][field.publicName]["data"]).map!(a => a["id"]).array;
      }
    }

    return normalised;
  }


  private inout pure {
    string type(const ModelDescription definition) {
      return definition.plural.toLower;
    }
  }
}

version(unittest) {
  import fluent.asserts;
}

/// json api serialization and deserialization
unittest
{
  struct TestModel
  {
    string id;

    string field1;
    int field2;
  }

  auto serializer = new const CrateJsonApiSerializer;

  //test the deserialize method
  auto serialized = `{
    "data": {
      "type": "testmodels",
      "id": "ID",
      "attributes": {
        "field1": "Ember Hamster",
        "field2": 5
      }
    }
  }`.parseJsonString;

  auto fields = describe!TestModel;

  auto deserialized = serializer.normalise("ID", serialized, fields);
  deserialized["id"].should.equal("ID");
  deserialized["field1"].should.equal("Ember Hamster");
  deserialized["field2"].should.equal(5);

  /// test the serialize method
  auto value = serializer.denormalise(deserialized, fields).joiner.array.to!string.parseJsonString;
  value["data"]["type"].should.equal("testmodels");
  value["data"]["id"].should.equal("ID");
  value["data"]["attributes"]["field1"].should.equal("Ember Hamster");
  value["data"]["attributes"]["field2"].should.equal(5);
}

unittest
{
  struct TestModel
  {
    ObjectId _id;

    string field1;
    int field2;
  }

  auto serializer = new const CrateJsonApiSerializer;

  //test the deserialize method
  auto serialized = `{
    "data": {
      "type": "testmodels",
      "id": "570d5afa999f19d459000000",
      "attributes": {
        "field1": "Ember Hamster",
        "field2": 5
      }
    }
  }`.parseJsonString;

  auto fields = describe!TestModel;

  auto deserialized = serializer.normalise("570d5afa999f19d459000000", serialized, fields);
  assert(deserialized["_id"].to!string == "570d5afa999f19d459000000");

  //test the serialize method
  auto value = serializer.denormalise(deserialized, fields).joiner.array.to!string.parseJsonString;
  assert(value["data"]["id"] == "570d5afa999f19d459000000");
}

unittest
{
  struct TestModel
  {
    ObjectId _id;

    string field1;
    int field2;
  }

  auto serializer = new const CrateJsonApiSerializer;
  auto fields = describe!TestModel;

  //test the deserialize method
  bool raised;

  try
  {
    serializer.normalise("570d5afa999f19d459000000", `{
      "data": {
        "type": "unknown",
        "id": "570d5afa999f19d459000000",
        "attributes": {
          "field1": "Ember Hamster",
          "field2": 5
        }
      }
    }`.parseJsonString, fields);
  }
  catch (Throwable)
  {
    raised = true;
  }

  assert(raised);
}

/// denormalise nested models
unittest
{
  struct TestModel
  {
    string name;
  }

  struct ComposedModel
  {
    string _id;

    TestModel child;
  }

  auto fields = describe!ComposedModel;
  auto serializer = new const CrateJsonApiSerializer;

  auto value = ComposedModel();
  value.child.name = "test";

  auto serializedValue = serializer.denormalise(value.serializeToJson, fields).joiner.array.to!string.parseJsonString;
  serializedValue["data"]["attributes"]["child"]["name"].should.equal("test");
}

unittest
{
  struct TestModel
  {
    string id;
    string name;
  }

  struct ComposedModel
  {
    @optional
    {
      string _id;
    }

    TestModel child;
  }

  auto fields = describe!ComposedModel;
  auto serializer = new const CrateJsonApiSerializer;

  auto value = ComposedModel();
  value._id = "id1";
  value.child.name = "test";
  value.child.id = "id2";

  auto serializedValue = serializer.denormalise(value.serializeToJson, fields).joiner.array.to!string.parseJsonString;

  assert(serializedValue["data"]["relationships"]["child"]["data"]["type"] == "testmodels");
  assert(serializedValue["data"]["relationships"]["child"]["data"]["id"] == "id2");
  assert(serializedValue["data"]["id"] == "id1");
}

/// should exract only the id for the nested relationships
unittest
{
  struct TestModel
  {
    string id;
    string name;
  }

  struct ComposedModel
  {
    @optional
    {
      string _id;
    }

    TestModel child;
  }

  auto fields = describe!ComposedModel;
  auto serializer = new const CrateJsonApiSerializer;

  auto serializedValue = q{{
    "data": {
      "attributes": {},
      "relationships": {
        "child": {
          "data": {
            "attributes": {
              "name": "test"
            },
            "relationships": {},
            "type": "testmodels",
            "id": "id2"
          }
        }
      },
      "type": "composedmodels",
      "id": "id1"
    }
  }}.parseJsonString;

  auto value = serializer.normalise("id1", serializedValue, fields);

  value["child"].should.equal("id2");
}

unittest
{
  struct TestModel
  {
    string name;
  }

  struct ComposedModel
  {
    @optional
    {
      string _id;
    }

    TestModel child;
  }

  auto fields = describe!ComposedModel;
  auto serializer = new const CrateJsonApiSerializer;

  auto serializedValue = q{{
    "data": {
      "attributes": {
        "child": {
          "name": "test"
        }
      },
      "type": "composedmodels",
      "id": "id1"
    }
  }}.parseJsonString;

  auto value = serializer.normalise("id1", serializedValue, fields);

  assert(value["child"]["name"] == "test");
}

/// Check denormalised type
unittest
{
  @("plural: Plural2")
  struct TestModel
  {
    string _id;
    string name;
  }

  @("plural: Plural1")
  struct ComposedModel
  {
    @optional
    {
      string _id;
    }

    TestModel child;
  }

  auto fields = describe!ComposedModel;
  auto serializer = new const CrateJsonApiSerializer;
  auto value = serializer.denormalise(ComposedModel().serializeToJson, fields).joiner.array.to!string.parseJsonString;

  value["data"]["type"].should.equal("plural1");
  value["data"]["relationships"]["child"]["data"]["type"].should.equal("plural2");

  assert("child" in serializer.normalise("", value, fields));
}

/// Relation list
unittest
{
  struct TestModel
  {
    string _id;
    string name;
  }

  struct ComposedModel
  {
    @optional
    {
      string _id;
    }

    TestModel[] child;
  }

  auto serializer = new const CrateJsonApiSerializer;
  auto value = ComposedModel();
  value.child ~= TestModel("1");
  value.child ~= TestModel("2");

  auto fields = describe!ComposedModel;

  auto apiValue = serializer.denormalise(value.serializeToJson, fields).joiner.array.to!string.parseJsonString;

  apiValue["data"]["relationships"]["child"].should.equal(`{
    "data": [
      {"id": "1", "type": "testmodels", "attributes": { "name": "" }, "relationships": {} },
      {"id": "2", "type": "testmodels", "attributes": { "name": "" }, "relationships": {} }
  ]}`.parseJsonString);

  auto normalisedValue = serializer.normalise("", apiValue, fields);

  normalisedValue["child"].type.should.equal(Json.Type.array);
  normalisedValue["child"].length.should.equal(2);
  normalisedValue["child"][0].to!string.should.equal("1");
  normalisedValue["child"][1].to!string.should.equal("2");
}


@("String list")
unittest
{
  struct ComposedModel
  {
    @optional
    {
      string _id;
    }

    string[] child;
  }

  auto serializer = new const CrateJsonApiSerializer;
  auto value = ComposedModel();
  value.child ~= "1";
  value.child ~= "2";

  auto fields = describe!ComposedModel;
  auto apiValue = serializer.denormalise(value.serializeToJson, fields).joiner.array.to!string.parseJsonString;

  assert(apiValue["data"]["attributes"]["child"].type == Json.Type.array);
  assert(apiValue["data"]["attributes"]["child"].length == 2);
  assert(apiValue["data"]["attributes"]["child"][0] == "1");
  assert(apiValue["data"]["attributes"]["child"][1] == "2");

  auto normalisedValue = serializer.normalise("", apiValue, fields);

  assert(normalisedValue["child"].type == Json.Type.array);
  assert(normalisedValue["child"].length == 2);
  assert(normalisedValue["child"][0] == "1");
  assert(normalisedValue["child"][1] == "2");
}
