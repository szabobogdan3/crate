/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.routing.jsonapi;

import crate.base;
import crate.ctfe;
import std.string;


/// The default routes used by the REST API
class JsonApiRouting {
  const {
    ModelDescription definition;
  }

  this(const ModelDescription definition) pure {
    this.definition = definition;
  }

  string item() pure {
    return model() ~ "/:id";
  }

  string model() pure {
    return "/" ~ definition.plural.toLower;
  }

  alias get = item;
  alias put = item;
  alias delete_ = item;

  alias post = model;
  alias getList = model;
}
