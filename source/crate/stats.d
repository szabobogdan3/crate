/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.stats;

static this() {
  Stats.instance = new Stats();
}

class Stats {
  static Stats instance;

  long[string][string] values;
}
