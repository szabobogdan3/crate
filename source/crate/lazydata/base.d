/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.lazydata.base;

import std.algorithm;
import std.array;
import std.traits;
import std.exception;
import std.conv;
import std.string;
import std.datetime;
import std.functional;

import crate.base;
import crate.ctfe;
import crate.error;

import vibe.data.json;
import vibe.data.bson;
import vibe.core.log;

version (unittest) {
  import fluent.asserts;
}

/// Lazy delegate evaluation
struct LazyField(T) {

  private {
    T delegate() _compute;
    T value;
    bool computed;
    SysTime lastUpdate;
    Duration expire;
  }

  ///
  this(T delegate() value, Duration expire = Duration.zero) {
    this._compute = value;
    this.expire = expire;
  }

  /// Return the computed value
  T compute() {
    if(expire > 0.seconds && Clock.currTime - lastUpdate > expire) {
      computed = false;
      lastUpdate = Clock.currTime;
    }

    if(!computed) {
      this.value = _compute();
      computed = true;
    }

    return this.value;
  }
}

/// Create a lazy field
auto lazyField(T)(T delegate() compute, Duration expire = Duration.zero) {
  return LazyField!T(compute, expire);
}

/// Lazy value usage
unittest {
  int count;

  auto value = lazyField({
    count++;
    return count;
  });

  value.compute.should.equal(1);
  value.compute.should.equal(1);
}

alias ResolveHandler = Json delegate(string model, string id) @safe;
alias ResolveFuncHandler = Json function(string model, string id) @safe;
alias NotifierHandler = void delegate(Json) @safe;

template LazyArrayImpl() {
  private {
    Json __data;
    ResolveHandler __resolve;
    NotifierHandler __notifier;
  }

  auto length() const @safe {
    return __data.length;
  }

  auto opIndexAssign(V, K)(V value, K key) {
    __data[key] = value.serializeToJson;
  }

  auto opIndex(K)(K key) const {
    static if(listType!Parent.length == 0) {
      static if(isAggregateType!Parent) {
        return Lazy!(Unqual!(Parent))(__data[key], __resolve);
      } else {
        return __data[key].deserializeJson!(Parent);
      }
    } else {
      return LazyArrayProxy!(field, Parent)(__data[key]);
    }
  }

  auto opOpAssign(string op)(Parent value) {
    opOpAssign!op(value.serializeToJson);
  }

  auto opOpAssign(string op)(Json value) {
    static if(op == "~") {
      __data ~= value;
      __notifier(__data);
    } else {
      static assert(false, "not implemented.");
    }
  }

  auto opIndex() {
    return __data.deserializeJson!(UnqualRecurse!T);
  }

  Json toJson(bool returnId)() const {
    return toJson();
  }

  Json toJson() const {
    static if(__traits(hasMember, field, "model")) {
      auto result = Json.emptyArray;

      () @trusted {
        foreach(size_t index, value; __data) {
          result ~= opIndex(index).toJson!true;
        }
      }();

      return result;
    } else {
      return __data;
    }
  }

  T toType() {
    static if(__traits(hasMember, field, "model")) {
      T result;

      foreach(size_t index, value; __data) {
        result ~= opIndex(index).toType;
      }
    } else {
      static if(is(KeyType == size_t)) {
        Unqual!Parent[] r;
        r.length = __data.length;

        foreach (size_t index, value; __data) {
          try {
            r[index] = value.deserializeJson!(Unqual!Parent);
          } catch(Exception e) {
            throw new CrateValidationException("`" ~ Parent.stringof ~ "` at index `" ~ index.to!string ~ "` can not be deserialized: " ~ e.message.to!string);
          }
        }

        T result = r.to!T;
      } else {
        T result = __data.deserializeJson!(UnqualRecurse!T).to!T;
      }
    }

    return result;
  }

  static auto fromModel(U)(U value, ResolveHandler resolve = null) {
    static if(is(KeyType == size_t)) {
      Json[] result;
      result.length = value.length;
    } else {
      Json[KeyType] result;
    }

    static if(listType!Parent.length == 0) {
      foreach(KeyType key, val; value) {
        static if (is(Parent == class) || is(Parent == struct)) {
          Json tmp = Lazy!(Unqual!Parent).fromModel(val, resolve).toJson;
          result[key] = tmp;
        } else {
          result[key] = val.serializeToJson;
        }
      }
    }

    return LazyArrayProxy!(field, T)(Json(result), resolve);
  }
}

struct LazyArrayProxy(alias field, T: Parent[], Parent) {
  alias KeyType = size_t;
  mixin LazyArrayImpl;

  this(Json data, ResolveHandler resolve = null, NotifierHandler notifier = null) {
    this.__resolve = resolve;
    this.__notifier = notifier;

    if(data.type == Json.Type.array) {
      this.__data = data;
      return;
    }

    this.__data = Json.emptyArray;

    if(data.type == Json.Type.null_ || data.type == Json.Type.undefined) {
      return;
    }

    this.__data ~= data;
  }
}

// throws an error when an array can not be deserialized
unittest {
  auto data = `[{ "a": 1 },{}]`.parseJsonString;

  struct Test {
    int a;
  }

  struct TestParent {
    Test[] list;
  }

  enum field = describe!TestParent;

  auto proxy = LazyArrayProxy!(field.fields.objects[0], typeof(TestParent.list))(data);

  proxy.toType[0].should.throwException!CrateValidationException.withMessage("`Test` at index `1` can not be deserialized: Missing non-optional field 'a' of type 'Test' (DefaultPolicy(T)).");
}

struct LazyArrayProxy(alias field, T: Parent[K], Parent, K) {
  alias KeyType = K;
  mixin LazyArrayImpl;
}

struct Lazy(T) if(is(T == Json)) {
  private {
    Json value;
    ResolveHandler __resolve;
  }

  static Lazy!Json fromModel(Json value, ResolveHandler resolve = null) {
    return Lazy!Json(value, resolve);
  }

  Json toJson() const @safe {
    return value;
  }
}

struct Lazy(T) if(is(T == SysTime)) {
  private {
    SysTime value;
    ResolveHandler __resolve;
  }

  this(SysTime value, ResolveHandler resolve = null) {
    this.value = value;
    this.__resolve = resolve;
  }

  this(Json value, ResolveHandler resolve = null) {
    this.value = SysTime.fromISOExtString(value.to!string);
    this.__resolve = resolve;
  }

  static Lazy!SysTime fromModel(SysTime value, ResolveHandler resolve = null) {
    return Lazy!SysTime(value, resolve);
  }

  Json toJson() const @safe {
    return Json(value.toISOExtString);
  }

  SysTime toType() {
    return this.value;
  }

  auto opDispatch(string method, T...)(T params) {
    mixin("return this.value." ~ method ~ "(params);");
  }
}

struct Lazy(T) if(!is(T == SysTime) && !is(T == Json) && (is(T == class) || is(T == struct))) {
  /// The type description
  enum Description = describe!T;

  private {
    Json __data = Json.emptyObject;
    ResolveHandler __resolve;
  }

  /// set proxy functions for basic fields
  static foreach(field; Description.fields.basic) {
    static if(field.listType.length == 0) {
      mixin(`void ` ~ field.name ~ `(const typeof(T.` ~ field.name ~ `) value) {
        __data["` ~ field.name ~ `"] = value.serializeToJson;
      }`);

      mixin(`auto ` ~ field.name ~ `() {

        if(__data.type != Json.Type.object) {
          loadData();
        }

        static if(is(typeof(T.` ~ field.name ~ `) == Json)) {
          return __data["` ~ field.name ~ `"];
        } else static if(IsEnumType!(typeof(T.` ~ field.name ~ `))) {
          return __data["` ~ field.name ~ `"].to!string.to!(typeof(T.` ~ field.name ~ `));
        } else {
          return __data["` ~ field.name ~ `"].deserializeJson!(typeof(T.` ~ field.name ~ `));
        }
      }`);
    } else {
      mixin(`auto ` ~ field.name ~ `() {
        return LazyArrayProxy!(field, typeof(T.` ~ field.name ~ `))(__data["` ~ field.name ~ `"]);
      }`);

      mixin(`void ` ~ field.name ~ `(const typeof(T.` ~ field.name ~ `) value) {
        __data["` ~ field.name ~ `"] = value.serializeToJson;
      }`);
    }
  }

  /// set proxy functions for object fields
  static foreach(field; Description.fields.objects) {
    static if(field.listType.length == 0) {
      mixin(`void ` ~ field.name ~ `(const typeof(T.` ~ field.name ~ `) value) {
        __data["` ~ field.name ~ `"] = value.serializeToJson;
      }`);

      mixin(`auto ` ~ field.name ~ `() {
        if(__data.type == Json.Type.null_ || __data.type == Json.Type.undefined) {
          return Lazy!(typeof(T.` ~ field.name ~ `))();
        }

        auto result = Lazy!(typeof(T.` ~ field.name ~ `))(__data["` ~ field.name ~ `"]);
        result.__resolve = __resolve;
        return result;
      }`);
    } else {
      mixin(`auto ` ~ field.name ~ `() {
        return LazyArrayProxy!(field, typeof(T.` ~ field.name ~ `))(__data["` ~ field.name ~ `"]);
      }`);

      mixin(`void ` ~ field.name ~ `(const typeof(T.` ~ field.name ~ `) value) {
        __data["` ~ field.name ~ `"] = value.serializeToJson;
      }`);
    }
  }

  /// set proxy functions for model fields
  static foreach(field; Description.fields.relations) {
    static if(field.listType.length == 0) {
      mixin(`void ` ~ field.name ~ `(const typeof(T.` ~ field.name ~ `) value) {
        __data["` ~ field.name ~ `"] = value.serializeToJson;
      }`);

      mixin(`auto ` ~ field.name ~ `() const {
        auto result = Lazy!(typeof(T.` ~ field.name ~ `))(__data["` ~ field.name ~ `"]);
        result.__resolve = __resolve;
        return result;
      }`);
    } else {
      mixin(`auto ` ~ field.name ~ `() {
        auto val = LazyArrayProxy!(field, typeof(T.` ~ field.name ~ `))
          (this.__data["` ~ field.name ~ `"], this.__resolve);

        val.__notifier = &this.__notifier!"` ~ field.name ~ `";

        return val;
      }`);

      mixin(`auto ` ~ field.name ~ `() const {
        return const LazyArrayProxy!(field, typeof(T.` ~ field.name ~ `))
          (this.__data["` ~ field.name ~ `"], this.__resolve);
      }`);

      mixin(`void ` ~ field.name ~ `(typeof(T.` ~ field.name ~ `) value) {
        __data["` ~ field.name ~ `"] = value.serializeToJson;
      }`);
    }
  }

  void __notifier(string property)(Json value) {
    this.__data[property] = value;
  }

  static {
    auto fromModel(const T item, ResolveHandler resolve = null) {
      static if(is(T == class)) {
        if(item is null) {
          return Lazy!T.fromJson(Json(null), resolve);
        }
      }

      static if(__traits(hasMember, T, "fromString") && __traits(hasMember, T, "toString")) {
        return Lazy!T.fromJson(Json(item.toString), resolve);
      } else static if(__traits(hasMember, T, "fromJson") && __traits(hasMember, T, "toJson")) {
        return Lazy!T.fromJson(item.toJson, resolve);
      } else {
        auto result = Json.emptyObject;

        static foreach(field; Description.fields.basic) {
          result[field.name] = __traits(getMember, item, field.name).serializeToJson;
        }

        static foreach(field; Description.fields.objects) {
          static if(field.listType.length == 0) {{
            mixin("auto tmp = item." ~ field.name ~ ";");

            result[field.name] = Lazy!(Unqual!(typeof(tmp)))
              .fromModel(tmp)
              .toJson;
          }} else static if(field.listType[0] == ListType.list) {{
            alias Type = typeof(__traits(getMember, item, field.name));

            result[field.name] = LazyArrayProxy!(field, Unqual!Type)
              .fromModel(__traits(getMember, item, field.name)).__data;
          }} else static if(field.listType[0] == ListType.hashmap) {{
            alias Type = typeof(__traits(getMember, item, field.name));

            result[field.name] = LazyArrayProxy!(field, Unqual!Type)
              .fromModel(__traits(getMember, item, field.name)).__data;
          }}
        }

        static foreach(field; Description.fields.relations) {
          static if(field.listType.length == 0) {
            result[field.name] = Lazy!(Unqual!(typeof(__traits(getMember, item, field.name))))
              .fromModel(__traits(getMember, item, field.name))
              .toJson;
          } else static if(field.listType[0] == ListType.list) {
            result[field.name] = LazyArrayProxy!(field, Unqual!(typeof(__traits(getMember, item, field.name))))
              .fromModel(__traits(getMember, item, field.name)).__data;
          } else {
            static assert(false, "not implemented");
          }
        }

        return Lazy!T.fromJson(result, resolve);
      }
    }

    ///
    auto fromJsonString(const string data, ResolveHandler resolve = null) {
      return Lazy!T(data.parseJsonString, resolve);
    }

    auto fromJson(Json data) {
      return Lazy!T(data);
    }

    auto fromJson(Json data, ResolveHandler resolve) {
      return Lazy!T(data, resolve);
    }

    auto fromJson(Json data, ResolveFuncHandler resolve) {
      return Lazy!T(data, resolve.toDelegate);
    }
  }

  ///
  Json toJson(bool returnId)() const @safe {
    if(this.__data == Json.Type.null_) {
      return this.__data;
    }

    static if(returnId && hasId!T) {
      if(__data.type == Json.Type.object) {
        if(__data[getIdField!(Description).name] == "") {
          return Json();
        }

        return __data[getIdField!(Description).name];
      } else {
        return __data;
      }
    } else {
      return this.toJson;
    }
  }

  ///
  Json toJson() const @safe {
    if(this.__data.type == Json.Type.null_) {
      return this.__data;
    }

    static if(__traits(hasMember, T, "fromString") && __traits(hasMember, T, "toString")) {
      return this.__data;
    } else static if(__traits(hasMember, T, "fromJson") && __traits(hasMember, T, "toJson")) {
      return this.__data;
    } else {
      Json data = this.__data.clone;

      static if(hasId!T) {
        if(this.__data.type != Json.Type.object) {
          enforce(this.__resolve !is null, "The resolve is not set.");
          data = this.__resolve(T.stringof, this.__data.to!string);
        }
      }

      auto result = Json.emptyObject;

      static foreach(field; Description.fields.basic) {
        if(data[field.name].type != Json.Type.null_ && data[field.name].type != Json.Type.undefined) {
          result[field.name] = data[field.name];
        }
      }

      static foreach(field; Description.fields.objects) {
        if(data[field.name].type != Json.Type.null_ && data[field.name].type != Json.Type.undefined) {
          result[field.name] = data[field.name];
        }
      }

      static foreach(field; Description.fields.relations) {
        if(data[field.name].type != Json.Type.null_ && data[field.name].type != Json.Type.undefined) {
          mixin("result[`" ~ field.name ~ "`] = this." ~ field.name ~ "().toJson!true;");
        }
      }

      return result;
    }
  }

  ///
  Json to(U)() if(is(U == Json)) {
    return this.toJson;
  }

  ///
  T toType() {
    T result;

    static if(!hasId!T && __traits(hasMember, T, "fromString")) {
      if(__data.type == Json.Type.string) {
        return T.fromString(__data.to!string);
      }
    }

    static if(!hasId!T && __traits(hasMember, T, "fromJson")) {
      if(__data.type == Json.Type.object) {
        return T.fromJson(__data);
      }
    }

    static if(hasId!T) {
      enum idFieldName = getIdField!Description.name;
    } else {
      enum idFieldName = "";
    }

    bool handle;
    static if(is(T == class)) {
      result = new T;
    }

    if(__data.type != Json.Type.object) {
      loadData();
    }

    if(__data.type == Json.Type.null_) {
      return T.init;
    }

    foreach(string key, val; __data) {
      if(val.type == Json.Type.null_) {
        __data.remove(key);
      }
    }

    static foreach(field; Description.fields.basic) {
      if(!field.isOptional && !field.isId) {
        enforce!CrateValidationException(field.publicName in __data, "The `" ~ T.stringof ~ "." ~ field.publicName ~ "` field is required: " ~ __data.toString);
      }

      if(field.publicName in __data) {
        mixin("result." ~ field.name ~ " = __data[`" ~ field.publicName ~ "`].deserializeJson!(typeof(result." ~ field.name ~ "));");
      }
    }

    static foreach(field; Description.fields.objects) {
      if(!field.isOptional&& !field.isId) {
        enforce!CrateValidationException(field.publicName in __data, "The `" ~ field.publicName ~ "` object is required");
      }

      handle = true;
      static if(idFieldName == field.name) {
        if(__data[field.publicName].type == Json.Type.null_) {
          handle = false;
        }
      }

      if(field.publicName in __data && handle) {
        mixin("result." ~ field.name ~ " = this." ~ field.name ~ ".toType;");
      }
    }

    static foreach(field; Description.fields.relations) {
      if(!field.isOptional && field.publicName !in __data && field.listType.length > 0) {
        if(field.listType[0] == ListType.list) {
          __data[field.publicName] = Json.emptyArray;
        }

        if(field.listType[0] == ListType.hashmap) {
          __data[field.publicName] = Json.emptyObject;
        }
      }

      if(!field.isOptional) {
        enforce!CrateValidationException(field.publicName in __data, "The `" ~ T.stringof ~ "." ~ field.publicName ~ "` relation is required");
      }

      if(field.publicName in __data) {
        if(__data[field.publicName].type == Json.Type.object) {
          enforce!CrateValidationException(__data[field.publicName].byKeyValue.count > 0, "The `" ~ field.publicName ~ "` relation can't be an empty object");
        }

        mixin("result." ~ field.name ~ " = this." ~ field.name ~ ".toType;");
      }
    }
    return result;
  }

  void loadData() {
    enforce(__resolve !is null, "The resolve function for `Lazy!" ~ T.stringof ~ "` is not set.");
    __data = __resolve(T.stringof, __data.to!string);
  }
}

/// An empty struct should be serialized to an empty Json
unittest {
  struct Empty {}

  Lazy!Empty.fromJsonString(`{}`).toJson.should.equal(`{}`.parseJsonString);
}

/// A struct with a string value should put the string value into the json
unittest {
  struct Test {
    string value;
  }

  Lazy!Test.fromJsonString(`{ "value": "test" }`).toJson.should.equal(`{"value": "test"}`.parseJsonString);
}

/// A struct with a string member should be converted from it's lazy version
unittest {
  struct Test {
    string value;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "value": "test" }`);
  auto test = lazyTest.toType;

  test.value.should.equal("test");
}

/// A struct with an ObjectId member should be converted from it's lazy version
unittest {
  struct Test {
    ObjectId _id;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "_id": "1" }`);
  auto test = lazyTest.toType;

  test._id.to!string.should.equal("000000000000000000000001");
}

/// A struct with a 5 int members should be converted from it's lazy version
unittest {
  struct Test {
    int a;
    int b;
    int c;
    int d;
    int e;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "a": 1,"b": 2,"c": 3,"d": 4, "e": 5}`);
  auto test = lazyTest.toType;

  test.a.should.equal(1);
  test.b.should.equal(2);
  test.c.should.equal(3);
  test.d.should.equal(4);
  test.e.should.equal(5);
}

/// It should be able to get and set basic values
unittest {
  struct Test {
    int a;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "a": 1 }`);
  lazyTest.a = 10;

  lazyTest.a.should.equal(10);

  auto test = lazyTest.toType;
  test.a.should.equal(10);

  lazyTest.toJson["a"].to!int.should.equal(10);
}

/// It should be able to get and set arrays of basic values
unittest {
  struct Test {
    int[] a;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "a": [1,2,3] }`);
  lazyTest.a[0] = 10;

  lazyTest.a[].should.equal([10,2,3]);
  auto r = lazyTest.a[0];

  r.should.equal(10);
  lazyTest.a[1].should.equal(2);
  lazyTest.a[2].should.equal(3);

  auto test = lazyTest.toType;
  test.a.should.equal([10,2,3]);

  lazyTest.toJson["a"][0].to!int.should.equal(10);
  lazyTest.toJson["a"][1].to!int.should.equal(2);
  lazyTest.toJson["a"][2].to!int.should.equal(3);

  /// assign an array
  lazyTest.a = [100,200,300];
  lazyTest.a[].should.equal([100,200,300]);
  lazyTest.a[0].should.equal(100);
  lazyTest.a[1].should.equal(200);
  lazyTest.a[2].should.equal(300);

  test = lazyTest.toType;
  test.a.should.equal([100,200,300]);

  lazyTest.toJson["a"][0].to!int.should.equal(100);
  lazyTest.toJson["a"][1].to!int.should.equal(200);
  lazyTest.toJson["a"][2].to!int.should.equal(300);
}

/// It should be able to get and set nested arrays of basic values
unittest {
  struct Test {
    int[][] a;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "a": [[1,2,3]] }`);
  lazyTest.a[0][0] = 10;

  lazyTest.a[].should.equal([[10,2,3]]);
  lazyTest.a[0][0].should.equal(10);
  lazyTest.a[0][1].should.equal(2);
  lazyTest.a[0][2].should.equal(3);

  auto test = lazyTest.toType;
  test.a.should.equal([[10,2,3]]);
  lazyTest.toJson["a"][0][0].to!int.should.equal(10);
  lazyTest.toJson["a"][0][1].to!int.should.equal(2);
  lazyTest.toJson["a"][0][2].to!int.should.equal(3);

  /// assign an array
  lazyTest.a = [[100,200,300]];
  lazyTest.a[].should.equal([[100,200,300]]);

  lazyTest.a[0][0].should.equal(100);
  lazyTest.a[0][1].should.equal(200);
  lazyTest.a[0][2].should.equal(300);

  test = lazyTest.toType;
  test.a.should.equal([[100,200,300]]);

  lazyTest.toJson["a"][0][0].to!int.should.equal(100);
  lazyTest.toJson["a"][0][1].to!int.should.equal(200);
  lazyTest.toJson["a"][0][2].to!int.should.equal(300);
}

/// It should be able to get and set arrays of objects
unittest {
  struct Child {
    string value;
  }

  struct Test {
    Child[] child;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{ "child": [ { "value": "a" }, { "value": "b" } ] }`);
  lazyTest.child[0] = Child("aa");

  lazyTest.child[].should.equal([Child("aa"), Child("b")]);
  lazyTest.child[0].toType.should.equal(Child("aa"));
  lazyTest.child[1].toType.should.equal(Child("b"));

  auto test = lazyTest.toType;
  test.child.should.equal([Child("aa"), Child("b")]);
  lazyTest.toJson["child"][0].deserializeJson!Child.should.equal(Child("aa"));
  lazyTest.toJson["child"][1].deserializeJson!Child.should.equal(Child("b"));

  /// assign an array
  lazyTest.child = [Child("aaa"), Child("bbb")];
  lazyTest.child[].should.equal([Child("aaa"), Child("bbb")]);

  lazyTest.child[0].toType.should.equal(Child("aaa"));
  lazyTest.child[1].toType.should.equal(Child("bbb"));

  test = lazyTest.toType;
  test.child.should.equal([Child("aaa"), Child("bbb")]);

  lazyTest.toJson["child"][0].deserializeJson!Child.should.equal(Child("aaa"));
  lazyTest.toJson["child"][1].deserializeJson!Child.should.equal(Child("bbb"));
}

/// It should be able to access undefined objects as they would be the .init value
unittest {
  struct Child {
    string value;
  }

  struct Test {
    Child[] child;
  }

  auto lazyTest = Lazy!Test.fromJsonString(`{}`);
  lazyTest.child.length.should.equal(0);
  lazyTest.toJson.should.equal(`{}`.parseJsonString);
}

/// It should be able to serialize/deserialize relations inside structs
unittest {
  struct Team {
    ObjectId _id;
    string value;
    bool flag;
  }

  struct Child {
    Team team;
  }

  struct Test {
    ObjectId _id;
    Child child;
  }

  Json mockResolve(string model, string id) @trusted {
    return `{"_id": "000000000000000000000123", "value": "test", "flag": true}`.parseJsonString;
  }

  auto clientData = `{ "child": { "team": "000000000000000000000123" } }`.parseJsonString;
  auto value = Lazy!Test(clientData, &mockResolve);
  value = Lazy!Test.fromModel(value.toType);

  value.toJson.should.equal(`{ "child": { "team": "000000000000000000000123" } }`.parseJsonString);
}
