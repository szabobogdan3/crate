/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.ctfe;

import crate.base;

import crate.resource.base;
import vibe.data.json;
import vibe.data.serialization;

import std.algorithm.searching;
import std.traits, std.meta;
import std.typetuple;
import std.string;
import std.datetime;
import std.exception;
import std.algorithm;
import std.array;
import std.range;

/// Check for enum values
template IsEnumType(T, string name) {
  static if(__traits(compiles, IsEnumType!(__traits(getMember, T, name)))) {
    alias IsEnumType = IsEnumType!(__traits(getMember, T, name));
  } else {
    enum IsEnumType = false;
  }
}

/// ditto
template IsEnumType(T) if(is(T == enum)) {
  enum IsEnumType = false;
}
/// ditto
template IsEnumType(T) if(!is(T == enum)) {
  enum IsEnumType = false;
}
/// ditto
template IsEnumType(alias T) if(is(T == enum)) {
  enum IsEnumType = true;
}
/// ditto
template IsEnumType(alias T) if(!is(T == enum)) {
  enum IsEnumType = false;
}

///
template IsPublic(T, string name) {
  static if(__traits(compiles, IsPublic!(__traits(getMember, T, name)))) {
    alias overloads = __traits(getOverloads, T, name);

    static if(overloads.length == 0) {
      alias IsPublic = IsPublic!(__traits(getMember, T, name));
    } else {
      alias IsPublic = IsPublic!(overloads[0]);
    }
  } else {
    enum IsPublic = false;
  }
}

/// ditto
template IsPublic(alias T){
  enum IsPublic = __traits(getProtection, T) == "public";
}

/// ditto
template IsStatic(T, string name){
  enum IsStatic = hasStaticMember!(T, "name");
}

template UnqualRecurse(T: V[], V) if(!is(T == string)) {
  alias UnqualRecurse = UnqualRecurse!(V)[];
}

template UnqualRecurse(T: V[], V) if(is(T == string)) {
  alias UnqualRecurse = string;
}

template UnqualRecurse(T: V[K], V, K) {
  alias UnqualRecurse = UnqualRecurse!(V)[UnqualRecurse!(K)];
}

template UnqualRecurse(T) if(!isAssociativeArray!T && !isArray!T) {
  alias UnqualRecurse = Unqual!T;
}

///
bool isValidField(T, alias name)() @safe pure {
  static if(name == "this" || name == "Monitor" || name == "toHash" || name == "opCmp"
    || name == "opEquals" || name == "opAssign" || name == "factory" ||
    !IsPublic!(T, name) || IsEnumType!(T, name) || IsStatic!(T, name) || isType!(__traits(getMember, T, name))) {
    return false;
  } else {
    alias overloads = __traits(getOverloads, T, name);

    static if(overloads.length == 0) {
      alias member = __traits(getMember, T, name);
    } else {
      alias member = overloads[0];
    }

    alias Type = typeof(member);

    static if (hasFieldDescription!Type) {
      return true;
    } else static if (isCallable!member && !hasUDA!(member, "computed")) {
      return false;
    } else static if(!is(Type == void)) {
      return true;
    } else {
      return false;
    }
  }
}

///
template ArrayType(T : T[])
{
  alias ArrayType = T;
}

/**
 * Get all attributes
 */
template GetAttributes(string name, Prototype)
{
  template GetFuncAttributes(TL...)
  {
    static if (TL.length == 1)
    {
      alias GetFuncAttributes = AliasSeq!(__traits(getAttributes, TL[0]));
    }
    else static if (TL.length > 1)
    {
      alias GetFuncAttributes = AliasSeq!(GetFuncAttributes!(TL[0 .. $ / 2]),
          GetFuncAttributes!(TL[$ / 2 .. $]));
    }
    else
    {
      alias GetFuncAttributes = AliasSeq!();
    }
  }

  static if (is(FunctionTypeOf!(ItemProperty!(Prototype, name)) == function))
  {
    static if (__traits(getOverloads, Prototype, name).length == 1)
    {
      alias GetAttributes = AliasSeq!(__traits(getAttributes,
          ItemProperty!(Prototype, name)));
    }
    else
    {
      alias GetAttributes = AliasSeq!(GetFuncAttributes!(AliasSeq!(__traits(getOverloads,
          Prototype, name))));
    }
  }
  else
  {
    alias GetAttributes = AliasSeq!(__traits(getAttributes, ItemProperty!(Prototype, name)));
  }
}

////
template StringOfSeq(TL...)
{
  static if (TL.length == 1)
  {
    static if (is(typeof(TL[0]) == string))
      alias StringOfSeq = AliasSeq!(TL[0]);
    else
    {
      enum strVal = TL[0].stringof;
      alias StringOfSeq = AliasSeq!(strVal);
    }
  }
  else static if (TL.length > 1)
  {
    alias StringOfSeq = AliasSeq!(StringOfSeq!(TL[0 .. $ / 2]), StringOfSeq!(TL[$ / 2 .. $]));
  }
  else
  {
    alias StringOfSeq = AliasSeq!();
  }
}

/**
 * Get a class property.
 *
 * Example:
 * --------------------
 * class BookItemPrototype {
 * 	@("field", "primary")
 *	ulong id;
 *
 *	@("field") string name = "unknown";
 * 	@("field") string author = "unknown";
 * }
 *
 * assert(__traits(isIntegral, ItemProperty!(BookItemPrototype, "id")) == true);
 * --------------------
 */
template ItemProperty(item, string method)
{
  static if (__traits(hasMember, item, method))
  {
    static if (__traits(getProtection, mixin("item." ~ method)).stringof[1 .. $ - 1] == "public")
    {
      alias ItemProperty = AliasSeq!(__traits(getMember, item, method));
    }
    else
    {
      alias ItemProperty = AliasSeq!();
    }
  }
  else
  {
    alias ItemProperty = AliasSeq!();
  }
}

///
template FieldName(string property, Prototype)
{
  alias NameAttr = typeof(name(""));

  static if (hasUDA!(ItemProperty!(Prototype, property), NameAttr))
  {
    enum fieldName = getUDAs!(ItemProperty!(Prototype, property), NameAttr)[0].name;
  }
  else
  {
    enum fieldName = property;
  }

  alias FieldName = fieldName;
}

private string prefixedAttribute(string prefix, string defaultValue, attributes...)()
{
  static if (attributes.length == 0)
  {
    return defaultValue;
  }
  else
  {
    import std.string : strip;

    auto len = prefix.length;

    foreach (value; attributes)
    {
      if (value.length > len && value[0 .. len] == prefix)
      {
        return value[len .. $].strip;
      }
    }

    return defaultValue;
  }
}

/// Set the plural name of a model
template Plural(Type)
{
  import std.uni : toLower;

  enum ATTR = __traits(getAttributes, Type);
  enum defaultPlural = Type.stringof ~ "s";
  enum plural = prefixedAttribute!("plural:", defaultPlural, ATTR)();

  alias Plural = plural;
}

/// Set the singular name of a model
template Singular(Type)
{
  import std.uni : toLower;

  enum ATTR = __traits(getAttributes, Type);
  enum defaultSingular = Type.stringof;
  enum singular = prefixedAttribute!("singular:", defaultSingular, ATTR)();

  alias Singular = singular;
}

/// Set the source of a model
template Source(Type)
{
  import std.uni : toLower;

  enum ATTR = __traits(getAttributes, Type);
  enum defaultSource = Type.stringof ~ "s";
  enum source = prefixedAttribute!("source:", defaultSource.toLower, ATTR)();

  alias Source = source;
}

version(unittest)
{
  import fluent.asserts;

  struct ActionModel
  {
    string _id;
    string name;

    void action()
    {
    }
  }
}

/// ditto
string[] flattenFieldNames(ModelFields fields, string prefix = "") @safe pure {
  return fields.basic.map!(a => prefix ~ a.name)
    .array
    .chain(
        fields
          .objects
          .map!(a => a.fields.flattenFieldNames( prefix ~ a.name ~ "." ))
          .join)
    .chain(
        fields
          .relations
          .map!(a => a.model.fields.flattenFieldNames( prefix ~ a.name ~ "." ))
          .join)
    .array;
}

/// Get all the field names as list
unittest {
  flattenFieldNames(describe!ActionModel.fields).should.equal(["_id", "name"]);
}

///
unittest {
  describe!ActionModel.fields.all.map!(a => a.name).should.containOnly([ "_id", "name" ]);
}

/// It should describe objects that can be serializet to strings as strings
unittest {
  import std.datetime;

  struct StringSerializable {
    string toString() const {
      return "";
    }

    static StringSerializable fromString(string) {
      throw new Exception("");
    }
  }

  struct Model {
    string _id;
    StringSerializable str;
    SysTime time;
  }

  enum def = describe!Model;

  def.fields.objects[0].name.should.equal("str");
  def.fields.objects[0].type.should.equal("string");

  def.fields.objects[1].name.should.equal("time");
  def.fields.objects[1].type.should.equal("SysTime");
  def.fields.objects[1].fields.basic.should.equal([]);
  def.fields.objects[1].fields.objects.should.equal([]);
}

template isVibeHandler(Type, alias member) {
  import vibe.http.server: HTTPServerRequest, HTTPServerResponse;

  static if(__traits(hasMember, Type, member)) {
    static foreach (Method; __traits(getOverloads, Type, member)) {
      static if(Parameters!Method.length == 2 &&
        is(Parameters!Method[0] == HTTPServerRequest) &&
        is(Parameters!Method[1] == HTTPServerResponse)) {

        enum result = true;
      }
    }
  }

  static if(!__traits(compiles, result)) {
    enum result = false;
  }

  alias isVibeHandler = result;
}

/// is true for all vibe handlers that have an extra struct param with a set of params that
/// need to be parsed
template isVibeWithParametersHandler(Type, alias member) {
  import vibe.http.server: HTTPServerRequest, HTTPServerResponse;

  static if(__traits(hasMember, Type, member)) {
    static foreach (Method; __traits(getOverloads, Type, member)) {
      static if(Parameters!Method.length == 3 &&
        is(Parameters!Method[0] == struct) &&
        is(Parameters!Method[1] == HTTPServerRequest) &&
        is(Parameters!Method[2] == HTTPServerResponse)) {

        enum result = true;
      }
    }
  }

  static if(!__traits(compiles, result)) {
    enum result = false;
  }

  alias isVibeWithParametersHandler = result;
}

/// Check if the member is a query filter
template isVibeFilter(Type, alias member) {
  import vibe.http.server: HTTPServerRequest, HTTPServerResponse;

  static if(__traits(hasMember, Type, member)) {
    static foreach (Method; __traits(getOverloads, Type, member)) {
      static if(!__traits(compiles, result)) {
        static if(Parameters!Method.length == 2 &&
          is(Parameters!Method[0] == IQuery) &&
          is(Parameters!Method[1] == HTTPServerRequest)) {

          enum result = true;
        }

        static if(Parameters!Method.length == 3 &&
          is(Parameters!Method[0] == IQuery) &&
          is(Parameters!Method[1] == HTTPServerRequest) &&
          is(Parameters!Method[2] == HTTPServerResponse)) {

          enum result = true;
        }
      }
    }
  }

  static if(!__traits(compiles, result)) {
    enum result = false;
  }

  alias isVibeFilter = result;
}


/// Check if the member is a query filter
template isVibeQueryParameterFilter(Type, alias member) {
  import vibe.http.server: HTTPServerRequest, HTTPServerResponse;

  static if(__traits(hasMember, Type, member)) {
    static foreach (Method; __traits(getOverloads, Type, member)) {
      static if(!__traits(compiles, result)) {
        static if(Parameters!Method.length == 2 &&
          is(Parameters!Method[0] == IQuery) &&
          !is(Parameters!Method[1] == HTTPServerRequest) &&
          is(Parameters!Method[1] == struct)) {

          enum result = true;
        }

        static if(Parameters!Method.length == 3 &&
          is(Parameters!Method[0] == IQuery) &&
          !is(Parameters!Method[1] == HTTPServerRequest) &&
          is(Parameters!Method[1] == struct) &&
          is(Parameters!Method[2] == HTTPServerRequest)) {

          enum result = true;
        }

        static if(Parameters!Method.length == 4 &&
          is(Parameters!Method[0] == IQuery) &&
          !is(Parameters!Method[1] == HTTPServerRequest) &&
          is(Parameters!Method[1] == struct) &&
          is(Parameters!Method[2] == HTTPServerRequest) &&
          is(Parameters!Method[3] == HTTPServerResponse)) {

          enum result = true;
        }
      }
    }
  }

  static if(!__traits(compiles, result)) {
    enum result = false;
  }

  alias isVibeQueryParameterFilter = result;
}

///
template isQueryResponse(Type, alias member) {
  import vibe.http.server: HTTPServerRequest, HTTPServerResponse;

  static if(__traits(hasMember, Type, member)) {
    static foreach (Method; __traits(getOverloads, Type, member)) {
      static if(Parameters!Method.length == 3 &&
        is(Parameters!Method[0] == IQuery) &&
        is(Parameters!Method[1] == HTTPServerRequest) &&
        is(Parameters!Method[2] == HTTPServerResponse)) {

        enum result = true;
      }
    }
  }

  static if(!__traits(compiles, result)) {
    enum result = false;
  }

  alias isQueryResponse = result;
}

///
struct ModelFields {
  /// The model basic fields
  FieldDescription[] basic;

  /// The list of basic objects
  ObjectFieldDescription[] objects;

  /// The list of objects that are stored in other
  /// tables or collections
  ModelFieldDescription[] relations;

  ///
  auto all() pure {
    return basic
      .chain(objects.map!(a => a.toFieldDescription))
      .chain(relations.map!(a => a.toFieldDescription));
  }

  ///
  auto allObjects() pure {
    return objects.map!(a => a.toFieldDescription)
      .chain(relations.map!(a => a.toFieldDescription));
  }

  @safe pure ModelFields dup() const {
    FieldDescription[] tmpBasic;
    tmpBasic.length = basic.length;
    foreach(i, item; basic) {
      tmpBasic[i] = item.dup;
    }

    ObjectFieldDescription[] tmpObjects;
    tmpObjects.length = objects.length;
    foreach(i, item; objects) {
      tmpObjects[i] = item.dup;
    }

    ModelFieldDescription[] tmpRelations;
    tmpRelations.length = relations.length;
    foreach(i, item; relations) {
      tmpRelations[i] = item.dup;
    }

    return ModelFields(tmpBasic, tmpObjects, tmpRelations);
  }
}

/// A model description, used to introspect models for api generators
struct ModelDescription {
  /// The singular name
  string singular;

  /// The plural name
  string plural;

  /// The name of the table or collection, where this model is stored
  string source;

  /// The model fileds
  ModelFields fields;

  ///
  string[] attributes;

  @safe pure ModelDescription dup() const {
    return ModelDescription(singular, plural, source, fields.dup, attributes.dup);
  }
}

///
enum ListType {
  list,
  hashmap
}

///
mixin template DefaultDescription() {
  string type;
  string name;
  string publicName;

  string[] attributes;
  bool isId;
  bool isOptional;

  bool canRead;
  bool canWrite;

  ListType[] listType;

  string description;

  ///
  FieldDescription toFieldDescription() pure {
    FieldDescription result;

    result.type = type;
    result.name = name;
    result.publicName = publicName;

    result.attributes = attributes;
    result.isId = isId;
    result.isOptional = isOptional;

    result.canRead = canRead;
    result.canWrite = canWrite;
    result.description = description;

    return result;
  }
}

/// Field description used to introspect model fields for api generators
struct FieldDescription {
  mixin DefaultDescription;
  mixin EnableDup;
}

///
struct ObjectFieldDescription {
  mixin DefaultDescription;
  mixin EnableDup;

  ///
  ModelFields fields;
}

///
struct ModelFieldDescription {
  mixin DefaultDescription;
  mixin EnableDup;

  ///
  ModelDescription model;
}

mixin template EnableDup() {
  @safe pure auto dup() const {
    Unqual!(typeof(this)) tmp;

    static foreach(member; __traits(allMembers, typeof(this))) {
      static if(member != "dup" && !isCallable!(__traits(getMember, tmp, member))) {
        static if(__traits(compiles, __traits(getMember, tmp, member) =__traits(getMember, this, member).dup)) {
          mixin("tmp." ~ member ~ " = " ~ member ~ ".dup;");
        } else {
          mixin("tmp." ~ member ~ " = " ~ member ~ ";");
        }
      }
    }

    return tmp;
  }
}

///
bool isSetter(Type)() {
  static if(isCallable!Type && arity!Type == 1) {
    return true;
  } else {
    return false;
  }
}

///
bool isGetter(Type)() {
  static if(isCallable!Type && arity!Type == 0 && !is(ReturnType!Type == void)) {
    return true;
  } else {
    return false;
  }
}

///
bool isBasicField(T, string name)() {
  static if(!isValidField!(T, name)) {
    return false;
  } else {
    bool result;

    alias FieldType = typeof(__traits(getMember, T, name));
    alias Type = ModelType!FieldType;

    static if(hasFieldDescription!Type || isBasicType!Type || isSomeString!Type || IsEnumType!Type || is(Type == Json)) {
      result = true;
    } else static if (isCallable!Type && hasUDA!(__traits(getMember, T, name), "computed")) {
      static if(isGetter!Type) {
        result = isBasicType!(ReturnType!Type) || isSomeString!(ReturnType!Type);
      } else static if(isSetter!Type) {
        result = isBasicType!(Parameters!Type[0]) || isSomeString!(Parameters!Type[0]);
      }
    }

    return result;
  }
}

///
bool hasId(T)() {
  bool result;

  static if(is(T == Json) || isArray!T) {
    result = false;
  } else {
    static foreach(field; __traits(allMembers, T)) {
      static if(isValidField!(T, field)) {
        static if(isBasicField!(T, field) || isAggregateField!(T, field)) {{
          alias Type = typeof(__traits(getMember, T, field));
          static if(isId(stringType!Type, field)) {
            result = true;
          }
        }}
      }
    }
  }

  return result;
}

///
bool isAggregateField(T, string name)() {

  static if(!isValidField!(T, name)) {
    return false;
  } else {
    bool result;
    alias Type = typeof(__traits(getMember, T, name));

    static if(is(Type == T)) {
      result = false;
    } else static if(isAggregateType!(ModelType!Type)) {
      result = true;
    } else static if (isCallable!Type && hasUDA!(__traits(getMember, T, name), "computed")) {
      static if(isGetter!Type && isAggregateType!(ModelType!(ReturnType!Type))) {
        result = true;
      } else static if(isSetter!Type && isAggregateType!(ModelType!(Parameters!Type[0]))) {
        result = true;
      }
    }

    return result;
  }
}

///
bool isObjectField(T, string name)() {
  static if(isAggregateField!(T, name) && !isBasicField!(T, name)) {
    alias Type = ModelType!(typeof(__traits(getMember, T, name)));

    static if(is(Type == Json)) {
      return false;
    } else {
      return !hasId!Type;
    }
  } else {
    return false;
  }
}

///
bool isRelationField(T, string name)() {
  static if(isAggregateField!(T, name)) {
    alias Type = ModelType!(typeof(__traits(getMember, T, name)));
    return hasId!Type;
  } else {
    return false;
  }
}

/// Check if the model has a custom object describe function
bool hasModelDescription(T)() {
  static if(hasStaticMember!(T, "describe")) {
    alias Type = T.describe;
    return is(ReturnType!Type == ModelDescription);
  } else {
    return false;
  }
}

/// check a genuine describe method
unittest {
  struct Model {
    static ModelDescription describe();
  }

  hasModelDescription!Model.should.equal(true);
}

/// check a static describe with an unexpected return
unittest {
  struct Model {
    static int describe();
  }

  hasModelDescription!Model.should.equal(false);
}

/// check a describe with an expected return
unittest {
  struct Model {
    static int describe();
    ModelDescription describe();
  }

  hasModelDescription!Model.should.equal(false);
}

/// Check if the struct has a custom field describe function
bool hasFieldDescription(T)() {
  static if(hasStaticMember!(T, "describe")) {
    alias Type = T.describe;
    return is(ReturnType!Type == FieldDescription);
  } else {
    return false;
  }
}

/// check a genuine describe method
unittest {
  struct CustomValue {
    static FieldDescription describe();
  }

  struct Custom {
    CustomValue value;
  }

  hasFieldDescription!CustomValue.should.equal(true);
  isBasicField!(Custom, "value").should.equal(true);
  isObjectField!(Custom, "value").should.equal(false);
  isRelationField!(Custom, "value").should.equal(false);
}

/// check a static describe with an unexpected return
unittest {
  struct CustomValue {
    static int describe();
  }

  struct Custom {
    CustomValue value;
  }

  hasFieldDescription!CustomValue.should.equal(false);
  isBasicField!(Custom, "value").should.equal(false);
  isObjectField!(Custom, "value").should.equal(true);
  isRelationField!(Custom, "value").should.equal(false);
}


/// check a describe with an expected return
unittest {
  struct CustomValue {
    static int describe();
    FieldDescription describe();
  }

  struct Custom {
    CustomValue value;
  }

  hasFieldDescription!CustomValue.should.equal(false);
  isBasicField!(Custom, "value").should.equal(false);
  isObjectField!(Custom, "value").should.equal(true);
  isRelationField!(Custom, "value").should.equal(false);
}

/// Get a model description
ModelDescription describe(T)() @safe pure {
  static if(hasModelDescription!T) {
    return T.describe;
  } else {
    ModelDescription description;

    description.singular = Singular!T;
    description.plural = Plural!T;
    description.source = Source!T;

    static foreach(attribute; __traits(getAttributes, T)) {
      static if(is(typeof(attribute) == string)) {
        description.attributes ~= attribute;
      }
    }

    return fillFields!T(description);
  }
}

/// It should get a model with basic types
unittest {
  struct Child {
    int b;
  }

  @("source:custommodel")
  struct OtherModel {
    int _id;
  }

  struct Model {
    int _id;
    int a;
    int[] list;

    Child child;
    OtherModel other;
    OtherModel[] others;

    enum CustomType {
      a = "a", b = "b"
    }

    CustomType type;

    OtherModel otherModel();

    void action();
  }

  enum description = describe!Model();

  description.singular.should.equal("Model");
  description.plural.should.equal("Models");
  description.source.should.equal("models");

  description.fields.basic.length.should.equal(4);
  description.fields.basic.should.contain(FieldDescription(
    "int", "a", "a", [""], false, false, true, true
  ));

  description.fields.basic.should.contain(FieldDescription(
    "string", "type", "type", [""], false, false, true, true
  ));

  description.fields.basic.should.contain(FieldDescription(
    "int", "list", "list", [""], false, false, true, true, [ ListType.list ]
  ));

  description.fields.objects.should.contain(ObjectFieldDescription(
    "Child", "child", "child", [""], false, false, true, true, [], "",
    ModelFields([
      FieldDescription(
        "int", "b", "b", [""], false, false, true, true
      )]))
  );

  description.fields.relations.should.contain(
    ModelFieldDescription("OtherModel", "other", "other", [""], false, false, true, true, [], "",
      ModelDescription("OtherModel", "OtherModels", "custommodel",
        ModelFields([FieldDescription("int", "_id", "_id", [""], true, false, true, true, [], "")], [], []), ["source:custommodel"])));

  auto other = description.fields.relations
    .filter!(a => a.name == "others").front;

    other.listType.should.equal( [ ListType.list ]);
}


/// It should describe an object with json type
unittest {
  struct JsonModel {
    Json a;
  }

  enum description = describe!JsonModel();
  description.fields.basic.length.should.equal(1);
  description.fields.objects.length.should.equal(0);

  description.fields.basic.should.contain(FieldDescription(
    "Json", "a", "a", [""], false, false, true, true
  ));
}

///
Description fillFields(T, Description)(Description description) @safe pure {
  description.fields = getFieldsDescription!T;

  return description;
}

///
ModelFields getFieldsDescription(T)() @safe pure {
  ModelFields fields;

  static foreach(field; __traits(allMembers, T)) {
    static if(isValidField!(T, field)) {{
      alias Type = typeof(__traits(getMember, T, field));

      static if(isBasicField!(T, field)) {
        fields.basic ~= describeField!(T, field);
      }

      static if(isObjectField!(T, field)) {
        fields.objects ~= describeObject!(T, field);
      }

      static if(isRelationField!(T, field)) {
        fields.relations ~= describeModel!(T, field);
      }
    }}
  }

  return fields;
}

///
bool isId(const string type, const string name) @safe pure{
  if(name.toLower == "_id") {
    return true;
  }

  if(name.toLower == "id") {
    return true;
  }

  if(type.toLower == "objectid") {
    return true;
  }

  return false;
}

///
unittest {
  isId("", "").should.equal(false);
  isId("", "_id").should.equal(true);
  isId("", "id").should.equal(true);
  isId("ObjectId", "").should.equal(true);
}

///
bool canRead(T, string name)() {
  alias Type = typeof(__traits(getMember, T, name));

  static if(isAggregateType!Type || isBasicType!Type || isSomeString!Type || isArray!Type || isAssociativeArray!Type) {
    return true;
  } else static if (isCallable!Type) {
    bool canBeRead;

    static foreach (OverloadedType; __traits(getOverloads, T, name)) {
      static if(!is(ReturnType!OverloadedType == void) && arity!OverloadedType == 0) {
        canBeRead = true;
      }
    }

    return canBeRead;
  } else {
    return false;
  }
}

///
bool canWrite(T, string name)() {
  alias Type = typeof(__traits(getMember, T, name));

  static if(isAggregateType!Type || isBasicType!Type || isSomeString!Type || isArray!Type || isAssociativeArray!Type) {
    return true;
  } else static if (isCallable!Type) {
    bool canBeWritten;

    static foreach (OverloadedType; __traits(getOverloads, T, name)) {
      static if(is(ReturnType!OverloadedType == void) && arity!OverloadedType == 1) {
        canBeWritten = true;
      }
    }

    return canBeWritten;
  } else {
    return false;
  }
}

///
bool isStringSerializable(Type)() {
  static if(hasStaticMember!(Type, "fromString") && __traits(hasMember, Type, "toString")){
    return true;
  } else if(__traits(hasMember, Type, "toISOExtString")) {
    return true;
  } else {
    return false;
  }
}

/// Returns the type as a string. It considers functions like
/// T value() as void value(T) as valid fields and the string
/// of T will be returned
string stringType(Type)() {
  alias modelType = ModelType!Type;
  string result;

  static if(is(Type == Json)) {
    result = "Json";
  } else static if(isAggregateType!Type && isStringSerializable!modelType) {
    result = "string";
  } else static if(!isSomeString!Type && isArray!Type && isStringSerializable!modelType) {
    result = stringType!(modelType);
  } else {
    result = modelType.stringof;
  }

  return result;
}
///
template GetModelFieldType(alias T) {

  static if(IsEnumType!T) {
    alias GetModelFieldType = T;
  } else {
    alias GetModelFieldType = GetModelFieldType!(typeof(T));
  }
}

///
template GetModelFieldType(Type) {
  static if(isGetter!Type) {
    alias GetModelFieldType = ReturnType!Type;
  } else static if(isSetter!Type) {
    alias GetModelFieldType = Parameters!Type[0];
  } else {
    alias GetModelFieldType = Type;
  }
}

/// Returns the type as a string. It considers functions like
/// T value() as void value(T) as valid fields and the string
/// of T will be returned
template ModelType(Type) {
  alias FieldType = GetModelFieldType!Type;

  static if(IsEnumType!Type) {
    alias ModelType = Type;
  } else static if(isAssociativeArray!FieldType) {
    alias ModelType = ModelType!(ValueType!FieldType);
  } else static if(isArray!FieldType && !isSomeString!FieldType) {
    alias ModelType = ModelType!(ArrayType!FieldType);
  } else {
    alias ModelType = FieldType;
  }
}

/// ditto
template ModelType(alias Type) {
  static if(IsEnumType!Type) {
    alias ModelType = OriginalType!Type;
  } else static if(__traits(compiles, ModelType!(typeof(Type)))) {
    alias ModelType = ModelType!(typeof(Type));
  } else {
    alias ModelType = ModelType!Type;
  }
}

///
ListType[] listType(FieldType)() @safe pure {
  ListType[] result;

  static if(isAssociativeArray!FieldType) {
    result ~= ListType.hashmap;
    result ~= listType!(ValueType!FieldType);
  } else static if(isArray!FieldType && !isSomeString!(OriginalType!FieldType)) {
    result ~= ListType.list;
    result ~= listType!(ArrayType!FieldType);
  }

  return result;
}

///
private Description describeDefaultFields(Description, T, string name)() @safe pure {
  alias Type = typeof(__traits(getMember, T, name));
  alias FieldType = GetModelFieldType!(__traits(getMember, T, name));

  Description definition;

  definition.type = stringType!Type;
  definition.name = name;
  definition.publicName = FieldName!(name, T);

  definition.attributes = [ "", StringOfSeq!(GetAttributes!(name, T)) ];

  if(definition.attributes.length > 0) {
    definition.attributes = definition.attributes.uniq.array;
  }

  definition.isId = isId(definition.type, name) || Type.stringof == "ObjectId";
  definition.isOptional = definition.attributes.canFind("optional()");

  definition.canRead = canRead!(T, name);
  definition.canWrite = canWrite!(T, name);
  definition.listType = listType!FieldType;

  static if(hasUDA!(__traits(getMember, T, name), DescribeAttribute)) {
    string glue;
    static foreach(attr; getUDAs!(__traits(getMember, T, name), DescribeAttribute)) {
      definition.description ~= glue ~ attr.description;
      glue = "\n\n";
    }
  }

  return definition;
}

/// Describe a struct or class field
FieldDescription describeField(T, string name)() @safe pure {
  return describeDefaultFields!(FieldDescription, T, name);
}

/// describing basic fields
unittest {
  struct A {
    @optional string b;

    @name("cc") ulong c;

    @ignore ulong d;

    ulong[][][] list;
    ulong[string][string][] hash;

    string _id;
    ObjectId otherId;

    @("computed") {
      ulong value();
      void value(ulong);

      ulong readValue();
      void writeValue(ulong);
    }
  }

  describeField!(A, "b").should.equal(FieldDescription(
    "string", "b", "b", ["", "optional()"], false, true, true, true
  ));

  describeField!(A, "c").should.equal(FieldDescription(
    "ulong", "c", "cc", ["", "NameAttribute(\"cc\")"], false, false, true, true
  ));

  describeField!(A, "d").should.equal(FieldDescription(
    "ulong", "d", "d", ["", "ignore()"], false, false, true, true
  ));

  describeField!(A, "list").should.equal(FieldDescription(
    "ulong", "list", "list", [""], false, false, true, true, [ListType.list, ListType.list, ListType.list]
  ));

  describeField!(A, "hash").should.equal(FieldDescription(
    "ulong", "hash", "hash", [""], false, false, true, true, [ListType.list, ListType.hashmap, ListType.hashmap]
  ));

  describeField!(A, "_id").should.equal(FieldDescription(
    "string", "_id", "_id", [""], true, false, true, true
  ));

  describeField!(A, "value").should.equal(FieldDescription(
    "ulong", "value", "value", ["", "computed" ], false, false, true, true
  ));

  describeField!(A, "readValue").should.equal(FieldDescription(
    "ulong", "readValue", "readValue", [ "", "computed" ], false, false, true, false
  ));

  describeField!(A, "writeValue").should.equal(FieldDescription(
    "ulong", "writeValue", "writeValue", [ "", "computed" ], false, false, false, true
  ));
}

/// Check if the object has a custom object fields function
bool hasCustomFieldDescription(T)() {
  static if(hasStaticMember!(T, "fields")) {
    alias Type = T.fields;
    return is(ReturnType!Type == ModelFields);
  } else {
    return false;
  }
}

/// Describe a struct or class field
ObjectFieldDescription describeObject(T, string name)() @safe pure {
  alias Type = ModelType!(__traits(getMember, T, name));

  enum tmp = describeDefaultFields!(ObjectFieldDescription, T, name);
  auto definition = tmp;

  static if(is(Type == SysTime)) {
    definition.type = "SysTime";
  } else static if(hasCustomFieldDescription!Type) {
    definition.fields = Type.fields();
  } else static if(!isStringSerializable!Type) {
    definition = fillFields!Type(definition);
  }

  return definition;
}

/// Describing a struct with custom field list
unittest {
  struct ChildWithFields {
    string name;

    static ModelFields fields() @safe pure {
      return ModelFields();
    }
  }

  struct Test {
    ChildWithFields childWithFields;
  }

  auto description = describeObject!(Test, "childWithFields");
  description.fields.basic.should.equal([]);
}

/// Describing the ObjectId
unittest {
  struct Test {
    ObjectId _id;
  }

  auto description = describeObject!(Test, "_id");
  description.name.should.equal("_id");
  description.type.should.equal("string");
  description.publicName.should.equal("_id");
  description.attributes.should.equal([""]);
  description.isId.should.equal(true);
  description.isOptional.should.equal(false);
  description.canRead.should.equal(true);
  description.canWrite.should.equal(true);

  description.fields.objects.map!(a => a.type).should.not.contain("Bson");
}

/// Describing a Bson object
unittest {
  import vibe.data.bson;

  struct Test {
    Bson bson;
  }

  auto description = describeObject!(Test, "bson");
  description.fields.basic.map!(a => a.type).should.not.contain("void");
}

/// Describe a model field
ModelFieldDescription describeModel(T, string name)() @safe pure {
  alias Type = ModelType!(__traits(getMember, T, name));
  auto definition = describeDefaultFields!(ModelFieldDescription, T, name);

  definition.model = describe!Type;

  return definition;
}

/// describing model fields
unittest {
  struct SomeType {
    int _id;
  }

  struct A {
    SomeType[][][] list;
    SomeType[string][string][] hash;

    @optional SomeType b;

    @name("cc") SomeType c;

    @ignore SomeType d;

    ObjectId otherId;

    @("computed") {
      SomeType value();
      void value(SomeType);

      SomeType[] valueList();
      void valueList(SomeType[]);

      SomeType[string] valueHash();
      void valueHash(SomeType[string]);

      SomeType readValue();
      SomeType[] readValueList();
      SomeType[string] readValueHash();

      void writeValue(SomeType);
      void writeValueList(SomeType[]);
      void writeValueHash(SomeType[string]);
    }
  }

  auto model = ModelDescription("SomeType", "SomeTypes", "sometypes",
    ModelFields([
      FieldDescription("int", "_id", "_id", [""], true, false, true, true)]));

  describeModel!(A, "list").should.equal(ModelFieldDescription(
    "SomeType", "list", "list", [""], false, false, true, true, [ListType.list, ListType.list, ListType.list], "", model
  ));

  describeModel!(A, "hash").should.equal(ModelFieldDescription(
    "SomeType", "hash", "hash", [""], false, false, true, true, [ListType.list, ListType.hashmap, ListType.hashmap], "", model
  ));

  describeModel!(A, "b").should.equal(ModelFieldDescription(
    "SomeType", "b", "b", ["", "optional()"], false, true, true, true, [], "", model
  ));

  describeModel!(A, "c").should.equal(ModelFieldDescription(
    "SomeType", "c", "cc", ["", "NameAttribute(\"cc\")"], false, false, true, true, [], "", model
  ));

  describeModel!(A, "d").should.equal(ModelFieldDescription(
    "SomeType", "d", "d", ["", "ignore()"], false, false, true, true, [], "", model
  ));

  /// computed read write
  describeModel!(A, "valueList").should.equal(ModelFieldDescription(
    "SomeType", "valueList", "valueList", [ "", "computed" ], false, false, true, true, [ ListType.list ], "", model
  ));

  describeModel!(A, "valueHash").should.equal(ModelFieldDescription(
    "SomeType", "valueHash", "valueHash", [ "", "computed" ], false, false, true, true, [ ListType.hashmap ], "", model
  ));

  describeModel!(A, "value").should.equal(ModelFieldDescription(
    "SomeType", "value", "value", [ "", "computed" ], false, false, true, true, [], "", model
  ));

  /// readonly
  describeModel!(A, "readValue").should.equal(ModelFieldDescription(
    "SomeType", "readValue", "readValue", [ "", "computed" ], false, false, true, false, [], "", model
  ));

  describeModel!(A, "readValueList").should.equal(ModelFieldDescription(
    "SomeType", "readValueList", "readValueList", [ "", "computed" ], false, false, true, false, [ListType.list], "", model
  ));

  describeModel!(A, "readValueHash").should.equal(ModelFieldDescription(
    "SomeType", "readValueHash", "readValueHash", [ "", "computed" ], false, false, true, false, [ListType.hashmap], "", model
  ));

  /// write only
  describeModel!(A, "writeValue").should.equal(ModelFieldDescription(
    "SomeType", "writeValue", "writeValue", [ "", "computed" ], false, false, false, true, [], "", model
  ));

  describeModel!(A, "writeValueList").should.equal(ModelFieldDescription(
    "SomeType", "writeValueList", "writeValueList", [ "", "computed" ], false, false, false, true, [ListType.list], "", model
  ));

  describeModel!(A, "writeValueHash").should.equal(ModelFieldDescription(
    "SomeType", "writeValueHash", "writeValueHash", [ "", "computed" ], false, false, false, true, [ListType.hashmap], "", model
  ));
}

/// custom model description
unittest {
  struct Model {
    string id;

    static ModelDescription describe() @safe pure {
      return ModelDescription("Model", "Models", "models",
        ModelFields([
          FieldDescription("int", "id", "id", [""], true, false, true, true)]));
    }
  }

  describe!Model.should.equal(ModelDescription("Model", "Models", "models",
    ModelFields([
      FieldDescription("int", "id", "id", [""], true, false, true, true)])));
}


/// get the field description for the id field
FieldDescription idField(ModelDescription modelDescription) @trusted {
  auto result = modelDescription.fields.all.filter!(a => a.isId);

  assert(!result.empty, modelDescription.singular ~ " does not have an id field.");

  return result.front;
}

/// get the field description for the id field
FieldDescription getIdField(ModelDescription modelDescription)() @trusted {
  auto result = modelDescription.fields.all.filter!(a => a.isId);

  assert(!result.empty, modelDescription.singular ~ " does not have an id field.");

  return result.front;
}

///
bool isAction(Type, string name)() if(isCallable!Type) {
  if(["toJson", "toString", "fromJson", "fromString", "opAssign", "__ctor"].canFind(name)) {
    return false;
  }

  return true;
}

///
bool isAction(Type, string name)() if(!isCallable!Type) {
  return false;
}

string[] resourceMembers(T)() {
  string[] list;

  static foreach(member; __traits(allMembers, T)) {
    static if(__traits(getProtection, __traits(getMember, T, member)) == "public" &&
      !isCallable!(__traits(getMember, T, member)) &&
      member != "Monitor" && __traits(compiles, typeof(__traits(getMember, T, member)))) {{
      alias MemberType = typeof(__traits(getMember, T, member));

      static if(staticIndexOf!(CrateResource, InterfacesTuple!MemberType) != -1) {
        list ~= member;
      }
    }}
  }

  return list;
}