/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.validation.csv;

import crate.base;
import crate.ctfe;
import crate.collection.csv;

import std.string;
import std.algorithm;
import std.range;
import std.csv;
import std.conv;
import std.array;

version(unittest) {
  import fluent.asserts;
}

/// Check if the header has all the model fields
string[] validateHeadersFields(Type)(Head[string] header, string[] ignoredFields = [], string[][] other = cast(string[][]) []) pure {
  return validateHeadersFields!Type(header.keys, ignoredFields, other);
}

/// ditto
string[] validateHeadersFields(Type)(string header, string[] ignoredFields = [], string[][] other = cast(string[][]) []) pure {
  auto range = csvReader!string(header);

  if(range.empty) {
    return [ "The csv is empty." ];
  }

  string[] pieces = range.front.map!(a => a.strip).array;

  return validateHeadersFields!Type(pieces, ignoredFields, other);
}

/// ditto
string[] validateHeadersFields(Type)(string[] header, string[] ignoredFields = [], string[][] other = cast(string[][]) []) pure {
  string[] result;

  auto model = describe!Type;
  header = header.map!(a => a.toLower).array;

  string[] missing = model.fields.flattenFieldNames
                          .filter!(a => !ignoredFields.canFind(a))
                          .filter!(a => !header.canFind(a))
                          .map!(a =>  "`" ~ a ~ "`")
                          .array;

  foreach(list; other) {
    if(header.filter!(a => list.canFind(a)).empty) {
      missing ~= "`" ~ list[0] ~ "`";
    }
  }

  if(missing.length > 1)
  {
    result ~= missing.join(", ") ~ " are missing";
  }
  else if(missing.length == 1)
  {
    result ~= missing[0] ~ " is missing";
  }

  return result;
}

version(unittest) {
  struct TestStruct {
    string field1;
    string field2;
    string field3;
  }

  struct NestedTestStruct {
    string field00;
    TestStruct level1;
  }
}

/// It should find the missing fields from a csv header
unittest {
  "".validateHeadersFields!TestStruct.should.equal(["The csv is empty."]);
}

/// It should find one missing field from a csv header
unittest {
  "field1,field2".validateHeadersFields!TestStruct.should.equal(["`field3` is missing"]);
  `"field1","field2"`.validateHeadersFields!TestStruct.should.equal(["`field3` is missing"]);
}

/// It should ignore the white spaces
unittest {
  "field1 , field2\n\n".validateHeadersFields!TestStruct.should.equal(["`field3` is missing"]);
}

/// It should return an empty array if all fields are provided
unittest {
  "field1,field2,field3".validateHeadersFields!TestStruct.should.equal([]);
}

/// It should ignore certain fields from validation
unittest {
  "".validateHeadersFields!TestStruct([`field2`, `field3`]).should.equal(["The csv is empty."]);
}

/// It should validate a nested structure
unittest {
  "field00,level1.field1".validateHeadersFields!NestedTestStruct.should.equal(["`level1.field2`, `level1.field3` are missing"]);
}

///
class CsvLineValidator {

  private {
    Head[string] header;
    string[] headerList;
    IValueValidator[] validators;
  }

  this(Head[string] header) {
    this.validators.length = header.length;
    this.headerList.length = header.length;

    foreach(key, value; header) {
      this.header[key.toLower] = value;
    }

    foreach(name, head; header) {
      if(head.index < this.headerList.length) {
        this.headerList[head.index] = name.toLower;
      }
    }
  }

  void add(string field, IValueValidator validator) {
    if(field !in header) {
      return;
    }

    validators[header[field].index] = validator;
  }

  string[] validate(string line) {
    try {
      return csvReader!string(line)
        .front
        .enumerate
        .filter!(a => a.index < validators.length)
        .filter!(a => validators[a.index] !is null)
        .map!(a =>
          validators[a.index]
            .validate(a.value)
            .map!(b => "`" ~ headerList[a.index] ~ "` " ~ b) )
            .array
        .joiner
          .array;
    } catch (IncompleteCellException e) {
      return [ e.message.to!string ];
    }
  }
}

/// It should return no message if there is no validator attached
unittest {
  auto header = "field1,field2,field3".getHeader;
  auto lineValidator = new CsvLineValidator(header);

  lineValidator.validate("0,0,0").should.equal([]);
}

/// It should return the validation message if a field is not valid
unittest {
  class MockFieldValidator : IValueValidator {
    string[] validate(string value) {
      return value == "1" ? [ "is not valid" ] : [];
    }
  }

  auto header = "field1,field2,field3".getHeader;
  auto lineValidator = new CsvLineValidator(header);
  lineValidator.add("field2", new MockFieldValidator);

  lineValidator.validate("0,0,0").should.equal([]);
  lineValidator.validate("0,1,0").should.equal([ "`field2` is not valid" ]);
}
