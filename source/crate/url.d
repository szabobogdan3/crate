module crate.url;

import std.conv;
import std.string;
import vibe.inet.url;

version(unittest){
  import fluent.asserts;
}

/// replace the host, port and schema of an existing url
string replaceHost(string url, string host, bool isTls) @safe {
  auto parsedUrl = URL(url);

  if(host.length > 0) {
    auto tmpHost = host.split(":");
    parsedUrl.host = tmpHost[0];

    if(tmpHost.length > 1) {
      parsedUrl.port = tmpHost[1].to!ushort;
    }
  }

  if(isTls) {
    parsedUrl.schema = "https";
  }

  return parsedUrl.to!string;
}

/// it does nothing when there is no host and isTls is false
unittest {
  replaceHost("http://a.b", "", false).should.equal("http://a.b");
}

/// it replaces the host when there is no port
unittest {
  replaceHost("http://a.b", "c.d", false).should.equal("http://c.d");
}

/// it replaces the host and port when set
unittest {
  replaceHost("http://a.b", "c.d:3000", false).should.equal("http://c.d:3000");
}

/// it sets https when isTls is true
unittest {
  replaceHost("http://a.b", "", true).should.equal("https://a.b");
}