/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.events;

import eventcore.core;
import vibe.core.core;
import vibe.core.log;
import core.thread;

/// blocks until the waiters number reaches a threshold
void throttle(size_t totalWaiters) {
  while(eventDriver.core.waiterCount > totalWaiters) {
    auto threadCount = Thread.getAll.length;
    logDebug("waiterCount: %s Threads: %s", eventDriver.core.waiterCount, threadCount);
    sleep(1.msecs);
  }
}
