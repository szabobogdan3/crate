/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.resource.sound;

import crate.mime;
import crate.base;
import crate.error;
import crate.resource.base;
import crate.http.request;
import crate.http.router;

import std.conv;
import std.exception;
import std.string;
import std.path;
import std.file;
import std.uuid;
import std.process;
import std.datetime;

import vibe.internal.interfaceproxy : InterfaceProxy, interfaceProxy;
import vibe.inet.webform;
import vibe.http.router;
import vibe.http.server;
import vibe.core.log;
import vibe.core.stream;
import vibe.core.file;
import vibe.core.core;
import vibe.stream.wrapper;

version(unittest) {
  import fluent.asserts;
  import crate.resource.file;
  import std.file;
  import std.path;
}

///
InputStream convertSound(InputStream input, AudioSettings settings) {
  auto tmpFile = createTmpFile(input);

  auto resultName = buildPath(tempDir, randomUUID.toString ~ ".tmp" ~ settings.container);

  string command = "ffmpeg -i " ~ tmpFile.path.to!string ~ " -c:a " ~ settings.codec ~ " -b:a " ~ settings.bitrate ~ " " ~ resultName;

  logInfo("convert: %s", command);
  auto pid = spawnShell(command);
  scope(exit) removeFile(resultName);

  auto waitResult = tryWait(pid);
  while(!waitResult.terminated) {
    sleep(1.msecs);
    waitResult = tryWait(pid);
  }

  enforce!CrateException(waitResult.status == 0, "Can not convert the audio file. Command `" ~ command ~ "` failed");

  auto resultFile = openFile(resultName, FileMode.read);
  return resultFile.createProxyStream;
}

struct AudioSettings {
  string name;
  string bitrate;
  string codec;
  string container;
}

template SoundResource(Storage : CrateResource) {
  alias SoundResource = SoundResource!(Storage, []);
}

class SoundResource(Storage : CrateResource, AudioSettings[] settings) : CrateResource {
  alias ParentOperation = ResourceApiOperation!(typeof(this), DefaultStorage);

  private {
    CrateResource storage;
    alias This = typeof(this);
  }

  static ApiOperation!DefaultStorage[] customOperations(ParentOperation operation) {
    ApiOperation!DefaultStorage[] operations;

    static foreach(option; settings) {{
      operations ~= new CustomResourceOperation!(typeof(this), DefaultStorage, "httpHandler")("/" ~ option.name, operation);
    }}

    return operations;
  }

  this(CrateResource storage) {
    this.storage = storage;
  }

  this() {
    this.storage = new Storage;
  }

  /// The chunk size property
  size_t chunkSize() {
    return storage.chunkSize;
  }

  /// ditto
  void chunkSize(size_t value) {
    storage.chunkSize = value;
  }

  string id() {
    return storage.id;
  }

  string contentType() {
    return storage.contentType;
  }

  void contentType(string) {
    enforce!CrateException(false, "Can't change the content type.");
  }

  void setMetadata(string key, string value) {
    storage.setMetadata(key, value);
  }

  string fileName() {
    return storage.fileName;
  }

  void fileName(string newName) {
    storage.fileName(newName);
  }

  void write(InterfaceProxy!(OutputStream) bodyWriter) {
    storage.write(bodyWriter);
  }

  void read(const(FilePart) file) {
    assert(false, "not implemented");
  }

  /// Consume an input stream and store it
  void read(InputStream input) {
    storage.read(input);
  }

  InputStream inputStream() {
    return storage.inputStream;
  }

  bool exists() {
    return storage.exists;
  }

  void remove() {
    storage.remove;
  }

  bool hasSize() {
    return storage.hasSize;
  }

  ulong size() {
    return storage.size;
  }

  string etag() {
    return storage.etag;
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    storage.setChunk(index, data);
  }

  void httpHandler(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    string path = req.requestURI;
    auto pos = path.indexOf("?");
    if(pos > 0) {
      path = path[0..pos];
    }

    static foreach(option; settings) {
      if(path.endsWith("/" ~ option.name)) {
        logInfo("GET sound `" ~ id ~ "` as `" ~ option.name ~ "`.");
        httpOptionHandler!(option)(req, res);
        return;
      }
    }
  }

  void httpOptionHandler(AudioSettings option)(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    scope path = id ~ "." ~ option.name ~ option.container.toMime.toExtension;
    logDebug("Sound path: " ~ path ~ ".");

    scope sound = new SoundResource!Storage(Storage.fromString(path));

    if(!sound.exists) {
      logInfo("Sound does not exist. Copying from original.");
      sound.setMetadata("parentId", id);

      auto convertedStream = convertSound(inputStream, option);
      sound.read(convertedStream);
    }

    res.flushResource(req, sound);
  }

  static This fromString(string serialized) @safe {
    Storage storage;
    enum dataLength = "data:".length;

    if(serialized.length > dataLength && serialized[0..dataLength] == "data:") {
      auto mimeEnd = serialized.indexOf(";base64,");
      auto mime = serialized[dataLength..mimeEnd];

      enforce!CrateValidationException(
        mime == "audio/mpeg" || mime == "audio/wav" || mime == "audio/aac" || mime == "audio/ogg" || mime == "audio/webm" || mime == "audio/mp4",
        "Only audio/mpeg, audio/aac, audio/ogg, audio/webm, audio/mp4 files are supported. You send a " ~ mime ~ " file.");

      storage = Storage.fromBase64(serialized);
    }

    storage = Storage.fromString(serialized);
    return new This(storage);
  }

  override string toString() const @trusted {
    return storage.to!string();
  }
}

/// It can be converted to a string
unittest {
  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.toString.should.equal("audio.mp3");
}

/// It can create a file from string when it contains a path
unittest {
  if(!exists(DefaultCrateFileSettings.path)) {
    mkdirRecurse(DefaultCrateFileSettings.path);
  }

  if(exists("files/audio.mp3")) {
    remove("files/audio.mp3");
  }

  auto filePath = buildPath(DefaultCrateFileSettings.path, "audio.mp3");
  copy("tests/audio.mp3", filePath);

  auto resource = SoundResource!(CrateFile).fromString(filePath);
  resource.toString.should.equal("files/audio.mp3");
}

/// It can create a file from string when it contains a path
unittest {
  auto resource = SoundResource!(CrateFile).fromString("data:audio/mpeg;base64,Y2hhbmdlMg==");
  resource.toString.should.endWith(".mp3");
}

/// It rejects a text file
unittest {
  ({
    SoundResource!(CrateFile).fromString("data:text/plain;base64,Y2hhbmdlMg==");
  }).should.throwAnyException.withMessage.equal("Only audio/mpeg, audio/aac, audio/ogg, audio/webm files are supported. You send a text/plain file.");
}

/// It should be an mp3
unittest {
  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.contentType.should.equal("audio/mpeg");
}

/// It should have an id
unittest {
  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.id.should.equal("audio.mp3");
}

/// It should be able to get the filename
unittest {
  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.fileName.should.equal("audio.mp3");
}

/// It should be able to check that the file exists and delete it
unittest {
  if(!exists(DefaultCrateFileSettings.path)) {
    mkdirRecurse(DefaultCrateFileSettings.path);
  }

  if(exists("files/audio.mp3")) {
    remove("files/audio.mp3");
  }

  copy("tests/audio.mp3", buildPath(DefaultCrateFileSettings.path, "audio.mp3"));

  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.exists.should.equal(true);
  resource.remove;
  resource.exists.should.equal(false);
}

/// It should have a size
unittest {
  if(!exists(DefaultCrateFileSettings.path)) {
    mkdirRecurse(DefaultCrateFileSettings.path);
  }

  if(exists("files/audio.mp3")) {
    remove("files/audio.mp3");
  }

  copy("tests/audio.mp3", buildPath(DefaultCrateFileSettings.path, "audio.mp3"));

  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.hasSize.should.equal(true);
  resource.size.should.equal(198658);
}

/// It should have an etag
unittest {
  if(!exists(DefaultCrateFileSettings.path)) {
    mkdirRecurse(DefaultCrateFileSettings.path);
  }

  if(exists("files/audio.mp3")) {
    remove("files/audio.mp3");
  }

  copy("tests/audio.mp3", buildPath(DefaultCrateFileSettings.path, "audio.mp3"));

  auto resource = new SoundResource!(CrateFile)(new CrateFile("audio.mp3"));
  resource.etag.should.not.equal("");
}

/// get the test subresource
// unittest {
//   if(!exists(DefaultCrateFileSettings.path)) {
//     mkdirRecurse(DefaultCrateFileSettings.path);
//   }

//   if(exists("files/audio.mp3")) {
//     remove("files/audio.mp3");
//   }

//   enum settings = [ AudioSettings("ogg1m", "192k", "libvorbis", ".ogg") ];

//   copy("tests/audio.mp3", buildPath(DefaultCrateFileSettings.path, "audio.mp3"));
//   auto resource = new SoundResource!(CrateFile, settings)(new CrateFile("audio.mp3"));

//   auto router = new URLRouter;
//   router.get("/file/ogg1m", &resource.httpHandler);

//   request(router)
//     .get("/file/ogg1m")
//       .expectStatusCode(200)
//       .expectHeader("Content-Type", "audio/ogg")
//       .end;
// }
