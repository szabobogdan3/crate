/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.resource.image;

import crate.mime;
import crate.base;
import crate.error;
import crate.resource.base;
import crate.http.request;
import crate.http.router;
import crate.http.headers;

import std.process;
import std.datetime;
import std.conv;
import std.uuid;
import std.string;
import std.path;
import std.file;
import std.exception;

import vibe.core.stream;
import vibe.core.file;
import vibe.core.core;
import vibe.core.log;
import vibe.inet.webform;
import vibe.stream.wrapper;
import vibe.http.router;
import vibe.http.server;

import openapi.definitions : Parameter, ParameterIn, Schema, SchemaType, SchemaFormat;

version(unittest) {
  import fluent.asserts;
  import crate.resource.file;
  import std.file;
  import std.path;
}

///
InputStream imageAction(InputStream input, string format, string action) {
  auto tmpFile = createTmpFile(input);

  auto resultName = buildPath(tempDir, randomUUID.toString ~ ".tmp");

  logInfo("convert %s %s %s:%s", tmpFile.path.to!string, action, format, resultName);
  auto pid = spawnShell("convert " ~ tmpFile.path.to!string ~ " " ~ action ~ " " ~ format ~ ":" ~ resultName);
  scope(exit) removeFile(resultName);

  auto waitResult = tryWait(pid);
  while(!waitResult.terminated) {
    sleep(1.msecs);
    waitResult = tryWait(pid);
  }

  enforce!CrateException(waitResult.status == 0, "Can not convert the image. " ~
  "Command `convert " ~ tmpFile.path.to!string ~ " " ~ action ~ " " ~ format ~ ":" ~ resultName ~ "` failed.");

  auto resultFile = openFile(resultName, FileMode.read);
  return resultFile.createProxyStream;
}

///
InputStream svgAction(InputStream input, string action) {
  auto tmpFile = createTmpFile(input);
  auto resultName = buildPath(tempDir, randomUUID.toString ~ ".tmp");

  logInfo("rsvg-convert %s %s -o %s", action, tmpFile.path.to!string, resultName);
  auto pid = spawnShell("rsvg-convert " ~ action ~ " " ~ tmpFile.path.to!string ~ " -o " ~ resultName);
  scope(exit) removeFile(resultName);

  auto waitResult = tryWait(pid);
  while(!waitResult.terminated) {
    sleep(1.msecs);
    waitResult = tryWait(pid);
  }

  enforce!CrateException(waitResult.status == 0, "Can not convert the image.");

  auto resultFile = openFile(resultName, FileMode.read);
  return resultFile.createProxyStream;
}

/// Resource stored in a local file
class CrateImageResource(Resource : CrateResource, string width, string height, string format) : CrateResource {
  private {
    Resource resource;
    alias This = typeof(this);


    static if(format == "png") {
      enum svgFormat = "png --background-color=transparent";
    } else {
      enum svgFormat = "svg";
    }
  }

  ///
  this() {
    this.resource = new Resource();
  }

  ///
  this(Resource resource) {
    this.resource = resource;
  }

  /// The chunk size property
  size_t chunkSize() {
    return resource.chunkSize;
  }

  /// ditto
  void chunkSize(size_t value) {
    resource.chunkSize = value;
  }

  void setMetadata(string key, string value) {
    resource.setMetadata(key, value);
  }

  ///
  string id() {
    return resource.id;
  }

  ///
  string etag() {
    return this.resource.etag;
  }

  /// The resource content type
  string contentType() {
    static if(format == "png") {
      return "image/png";
    } else {
      return this.resource.contentType;
    }
  }

  /// The resource content type
  void contentType(string value) {
    this.resource.contentType(value);
  }

  /// The resource name
  string fileName() {
    return this.resource.fileName;
  }

  /// The resource name
  void fileName(string value) {
    this.resource.fileName(value);
  }

  /// write the resource to a http request body
  void write(BodyOutputStream bodyWriter) {
    resource.write(bodyWriter);
  }

  /// Read a resource from FilePart request
  void read(const FilePart file) {
    assert(false, "not implemented");
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    assert(false, "not implemented");
  }

  /// Consume an input stream and store it
  void read(InputStream input) {
    enum bmpResize = "-resize " ~ width ~ "x" ~ height;
    enum svgResize = "-a -f " ~ svgFormat ~ " -w " ~ width ~ " -h " ~ height;

    if(this.contentType == "image/svg+xml") {
      resource.read(svgAction(input, svgResize));
      return;
    }

    if(this.contentType == "image/png") {
      resource.read(imageAction(input, "PNG", bmpResize));
      return;
    }

    if(this.contentType == "image/webp") {
      resource.read(imageAction(input, "WEBP", bmpResize));
      return;
    }

    resource.read(imageAction(input, "JPG", bmpResize));
  }

  /// ditto
  void read(string streamContentType, InputStream input) {
    if(streamContentType == this.contentType) {
      return this.read(input);
    }

    if(streamContentType == "image/svg+xml") {
      string svgConvert = "-a -f " ~ svgFormat ~ " -w " ~ width ~ " -h " ~ height;
      resource.read(svgAction(input, svgConvert));
      return;
    }

    string format = toExtension(this.contentType)[1..$];
    format = format.toUpper;
    enum bmpResize = "-resize " ~ width ~ "x" ~ height;
    resource.read(imageAction(input, format, bmpResize));
  }

  ///
  void remove() {
    resource.remove();
  }

  ///
  InputStream inputStream() {
    return resource.inputStream;
  }

  /// Check if the resource size can be computed
  bool hasSize() {
    return resource.hasSize;
  }

  ///
  bool exists() {
    return this.resource.exists;
  }

  /// Get the resource size in bytes
  ulong size() {
    return resource.size;
  }

  /// Get the file path
  override string toString() const @safe {
    return resource.toString;
  }

  void httpHandler(HTTPServerRequest req, HTTPServerResponse response) {
    throw new Exception("not implemented");
  }

  static {
    /// Create an image resource from a string
    This fromString(string value) @trusted {
      return new This(Resource.fromString(value));
    }
  }
}

///
struct ImageSettings {
  string name;
  string width;
  string height;
  string format;
}

/// It should be an image
unittest {
  auto resource = new CrateImageResource!(CrateFile, "10", "10", "")(new CrateFile("pixel.10.jpg"));
  resource.contentType.should.equal("image/jpeg");
}

/// It should check if the resource exists
unittest {
  if(!exists(DefaultCrateFileSettings.path)) {
    mkdirRecurse(DefaultCrateFileSettings.path);
  }

  if(exists("files/pixel.jpg")) {
    remove("files/pixel.jpg");
  }

  if(exists("files/pixel.10.jpg")) {
    remove("files/pixel.10.jpg");
  }

  copy("tests/pixel.jpg", buildPath(DefaultCrateFileSettings.path, "pixel.jpg"));

  auto pixel = new CrateFile("pixel.jpg");

  auto resource = new CrateImageResource!(CrateFile, "10", "10", "")(new CrateFile("pixel.10.jpg"));
  pixel.exists.should.equal(true);
  resource.exists.should.equal(false);

  void run() @trusted nothrow {
    try {
      resource.read(pixel.inputStream);
    } catch(Exception e) {}
  }

  runTask(&run);

  int trials;
  while(!resource.exists && trials < 100) {
    runEventLoopOnce();
    trials++;
  }

  resource.exists.should.equal(true);
}

/// It should resize a svg
unittest {
  if(!exists(DefaultCrateFileSettings.path)) {
    mkdirRecurse(DefaultCrateFileSettings.path);
  }

  if(exists("files/smile.svg")) {
    remove("files/smile.svg");
  }

  if(exists("files/smile.100.svg")) {
    remove("files/smile.100.svg");
  }

  copy("tests/smile.svg", buildPath(DefaultCrateFileSettings.path, "smile.svg"));

  auto smile = new CrateFile("smile.svg");

  auto resource = new CrateImageResource!(CrateFile, "100", "100", "")(new CrateFile("smile.100.svg"));
  smile.exists.should.equal(true);
  resource.exists.should.equal(false);
  resource.contentType.should.equal("image/svg+xml");

  void run() @trusted nothrow {
    try {
      resource.read(smile.inputStream);
    } catch(Exception e) {}
  }

  runTask(&run);

  int trials;
  while(!resource.exists && trials < 100) {
    runEventLoopOnce();
    trials++;
  }

  resource.exists.should.equal(true);
}

///
class CrateImageCollectionResource(Resource : CrateResource, ImageSettings[] settings) : CrateResource {
  alias ParentOperation = ResourceApiOperation!(typeof(this), DefaultStorage);

  private {
    Resource resource;
    alias This = typeof(this);
  }

  ///
  this() {
    this.resource = new Resource();
  }

  ///
  this(string id) {
    this.resource = new Resource(id);
  }

  ///
  this(Resource resource) {
    this.resource = resource;
  }

  /// The chunk size property
  size_t chunkSize() {
    return resource.chunkSize;
  }

  /// ditto
  void chunkSize(size_t value) {
    resource.chunkSize = value;
  }

  ///
  string id() {
    return resource.id;
  }


  void setMetadata(string key, string value) {
    resource.setMetadata(key, value);
  }

  ///
  string etag() {
    return this.resource.etag;
  }

  /// The resource content type
  string contentType() {
    return this.resource.contentType;
  }

  /// The resource content type
  void contentType(string value) {
    this.resource.contentType(value);
  }

  /// The resource name
  string fileName() {
    return this.resource.fileName;
  }

  /// The resource name
  void fileName(string value) {
    this.resource.fileName(value);
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    this.resource.setChunk(index, data);
  }

  /// write the resource to a http request body
  void write(BodyOutputStream bodyWriter) {
    resource.write(bodyWriter);
  }

  /// Read a resource from FilePart request
  void read(const FilePart file) {
    clearCache();
    resource.read(file);
  }

  /// Consume an input stream and store it
  void read(InputStream input) {
    clearCache();
    resource.read(input);
  }

  ///
  void remove() {
    clearCache();
    resource.remove();
  }

  void clearCache() {
    static foreach(option; settings) {{
      scope path = id ~ "." ~ option.name ~ toExtension(this.contentType);

      scope image = new CrateImageResource!(Resource, option.width, option.height, option.format)(new Resource(path));
      image.remove;
    }}
  }

  ///
  InputStream inputStream() {
    return resource.inputStream;
  }

  /// Check if the resource size can be computed
  bool hasSize() {
    return resource.hasSize;
  }

  ///
  bool exists() {
    return this.resource.exists;
  }

  /// Get the resource size in bytes
  ulong size() {
    return resource.size;
  }

  static ApiOperation!DefaultStorage[] customOperations(ParentOperation operation) {
    ApiOperation!DefaultStorage[] operations;

    static foreach(option; settings) {{
      operations ~= new CustomResourceOperation!(typeof(this), DefaultStorage, "httpHandler")("/" ~ option.name, operation);
    }}


    return operations;
  }

  /// Get the file path
  override string toString() const @safe {
    return resource.toString;
  }

  static {
    /// Create an image resource from a string
    This fromString(string value) @trusted {
      return new This(Resource.fromString(value));
    }

    ///
    This fromLocalFile(string value) @trusted {
      static if(__traits(hasMember, Resource, "fromLocalFile")) {
        return new This(Resource.fromLocalFile(value));
      } else {
        assert(false, "Not supported.");
      }
    }
  }

  ///
  void httpHandler(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    string path = req.requestURI;
    auto pos = path.indexOf("?");
    if(pos > 0) {
      path = path[0..pos];
    }

    static foreach(option; settings) {
      if(path.endsWith("/" ~ option.name)) {
        logInfo("GET image `" ~ id ~ "` as `" ~ option.name ~ "`.");
        crateHandler!(option)(req, res);
        return;
      }
    }
  }

  ///
  void crateHandler(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    string path = req.requestURI;
    auto pos = path.indexOf("?");
    if(pos > 0) {
      path = path[0..pos];
    }

    static foreach(option; settings) {
      if(path.endsWith("/" ~ option.name)) {
        crateHandler!(option)(req, res);
        return;
      }
    }
  }

  ///
  void crateHandler(ImageSettings option)(HTTPServerRequest req, HTTPServerResponse res) @trusted {
    string expectedContentType = this.contentType;

    if("type" in req.query) {
      switch(req.query["type"]) {
        case "jpg":
        case "jpeg":
          expectedContentType = toMime(".jpg");
          break;

        case "png":
          expectedContentType = toMime(".png");
          break;

        case "webp":
          expectedContentType = toMime(".webp");
          break;

        case "ico":
          expectedContentType = toMime(".ico");
          break;

        default:
          expectedContentType = this.contentType;
      }
    }

    scope path = id ~ "." ~ option.name ~ toExtension(expectedContentType);
    logDebug("Image path: " ~ path ~ ".");

    scope image = new CrateImageResource!(Resource, option.width, option.height, option.format)(new Resource(path));
    if(!image.exists) {
      logInfo("Image does not exist. Copying from original.");
      image.contentType = expectedContentType;
      image.setMetadata("parentId", id);
      image.setMetadata("format", option.format);
      image.read(this.contentType, resource.inputStream);
    }

    res.headers["Content-Type"] = image.contentType;
    res.headers["ETag"] = `"` ~ image.etag ~ `"`;
    res.headers["Cache-Control"] = "public";

    if(ifNoneMatch(req.headers, image.etag)) {
      res.statusCode = 304;
      res.contentType = image.contentType;
      res.writeVoidBody;
      return;
    }

    res.flushResource(req, image);
  }
}

/// get the test subresource
unittest {
  enum ImageSettings[] settings = [ ImageSettings("small", "100", "100") ];
  auto collection = new CrateImageCollectionResource!(CrateFile, settings)(new CrateFile("test.jpg"));

  mkdirRecurse(DefaultCrateFileSettings.path);
  std.file.write(buildPath(DefaultCrateFileSettings.path, "test.jpg"), "test");
  std.file.write(buildPath(DefaultCrateFileSettings.path, "test.jpg.small.jpg"), "small");

  auto router = new URLRouter;
  router.get("/file/small", &collection.httpHandler);

  request(router)
    .get("/file/small")
      .expectStatusCode(200)
      .expectHeader("Content-Type", "image/jpeg")
      .end((Response response) => () {
        response.bodyString.should.equal("small");
      });
}

/// it should resize the pixel
unittest {
  enum ImageSettings[] settings = [ ImageSettings("small", "100", "100") ];
  auto collection = new CrateImageCollectionResource!(CrateFile, settings)(new CrateFile("pixel.jpg"));

  if(exists("files/pixel.jpg")) {
    remove("files/pixel.jpg");
  }

  if(exists("files/pixel.jpg.small.jpg")) {
    remove("files/pixel.jpg.small.jpg");
  }

  copy("tests/pixel.jpg", buildPath(DefaultCrateFileSettings.path, "pixel.jpg"));

  auto router = new URLRouter;
  router.get("/file/small", &collection.httpHandler);

  request(router)
    .get("/file/small")
      .expectStatusCode(200)
      .expectHeader("Content-Type", "image/jpeg")
      .end((Response response) => () {
        response.bodyRaw.length.should.not.equal(0);
      });
}
