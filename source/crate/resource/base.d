/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.resource.base;

import crate.http.request;
import vibe.core.stream;

import vibe.data.json;
import vibe.inet.webform;
import vibe.http.router;
import vibe.http.server;
import vibe.core.file;
import vibe.core.log;
public import vibe.core.stream;

import std.conv;
import std.file;
import std.stdio;
import std.uuid;

alias BodyOutputStream = typeof(HTTPServerResponse.bodyWriter);

///
interface CrateResource {
  /// unique id for the resource
  string id();

  /// The resource content type
  string contentType();

  /// The resource content type
  void contentType(string);

  /// The resource name
  string fileName();

  /// The resource name
  void fileName(string);

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data);

  /// write the resource to a http request body
  void write(BodyOutputStream bodyWriter);

  /// Read a resource from FilePart request
  void read(const FilePart file);

  /// Consume an input stream and store it
  void read(InputStream input);

  /// Get a input stream for the resource content
  InputStream inputStream();

  ///
  bool exists();

  /// Remove the resource
  void remove();

  /// Check if the resource size can be computed
  bool hasSize();

  /// Get the resource size in bytes
  ulong size();

  /// Returns a hash that uniq identify the resource content
  string etag();

  /// The chunk size property
  size_t chunkSize();

  /// ditto
  void chunkSize(size_t value);

  ///
  void setMetadata(string key, string value);

  ///
  void httpHandler(HTTPServerRequest req, HTTPServerResponse response);
}

version(unittest) {
  import fluent.asserts;
  import crate.collection.memory;
  import vibe.http.server;

  class TestResource : CrateResource {
    static string lastRead;

    string contentType() {
      return "test/resource";
    }

    void contentType(string) {
    }

    ulong chunkSize() {
      return 0;
    }

    void setMetadata(string key, string value) {}

    void chunkSize(ulong value) {}

    string fileName() {
      return "";
    }

    void fileName(string) {
    }

    void write(BodyOutputStream bodyWriter) {
      bodyWriter.write("test body");
    }

    void read(const FilePart file) {
      lastRead = readText(file.tempPath.toString);
    }

    void remove() {
    }

    string id() {
      return "";
    }

    bool exists() {
      return true;
    }

    bool hasSize() {
      return true;
    }

    ulong size() {
      return "test body".length+1;
    }

    /// sets the chunk data
    void setChunk(size_t index, ubyte[] data) {
      assert(false, "not implemented");
    }

    override string toString() const @safe {
      return "test resource";
    }

    ///
    InputStream inputStream() {
      assert(false, "not implemented");
    }

    ///
    void read(InputStream input) {
      assert(false, "not implemented");
    }

    static TestResource fromString(string src) @safe {
      return new TestResource;
    }

    string etag() {
      return "ETAG";
    }

    void httpHandler(HTTPServerRequest req, HTTPServerResponse response) {
      throw new Exception("not implemented");
    }
  }

  struct ResourceEmbeded
  {
    string name = "test";
    TestResource resource = new TestResource;
  }

  struct ResourceModel
  {
    @optional string _id = "1";
    string name = "test";
    TestResource resource = new TestResource;
  }

  struct RelationModel
  {
    @optional string _id = "1";
    string name = "test";
    ResourceEmbeded relation;
  }

  struct ArrayModel {
    @optional string _id = "1";
    string name = "test";
    ResourceEmbeded[] relation = [ ResourceEmbeded() ];
  }
}

/// Access a model with resources
unittest {
  import vibe.http.router;
  import crate.api.rest.policy;
  import crate.http.router;
  import std.stdio;

  auto router = new URLRouter();
  auto resourceCrate = new MemoryCrate!ResourceModel;

  resourceCrate.addItem(ResourceModel().serializeToJson);

  router
    .crateSetup
      .add(resourceCrate);

  request(router)
    .get("/resourcemodels/000000000000000000000001")
      .expectStatusCode(200)
      .end((Response response) => () {
        assert(response.bodyJson["resourceModel"]["resource"] == "test resource");
      });

  request(router)
    .get("/resourcemodels/000000000000000000000001/resource")
      .expectStatusCode(200)
      .expectHeader("Content-Type", "test/resource")
      .end((Response response) => () {
        response.bodyString.should.equal("test body");
      });


  string data = "-----------------------------9855312492823326321373169801\r\n";
  data ~= "Content-Disposition: form-data; name=\"resource\"; filename=\"resource.txt\"\r\n";
  data ~= "Content-Type: text/plain\r\n\r\n";
  data ~= "hello\r\n";
  data ~= "-----------------------------9855312492823326321373169801--\r\n";

  TestResource.lastRead = "";
  request(router)
    .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
    .post("/resourcemodels/000000000000000000000001/resource")
    .send(data)
      .expectStatusCode(201)
      .end((Response response) => () {
        TestResource.lastRead.should.equal("hello");
      });

  data = "-----------------------------9855312492823326321373169801\r\n";
  data ~= "Content-Disposition: form-data; name=\"other\" filename=\"resource.txt\"\r\n";
  data ~= "Content-Type: text/plain\r\n\r\n";
  data ~= "hello\r\n";
  data ~= "-----------------------------9855312492823326321373169801--\r\n";

  TestResource.lastRead = "";
  request(router)
    .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
    .post("/resourcemodels/000000000000000000000001/resource")
    .expectStatusCode(201)
    .send(data)
    .end((Response response) => () {
      TestResource.lastRead.should.equal("hello");
    });
}

/// Access a relation with resources
unittest {
  import vibe.http.router;
  import crate.api.rest.policy;
  import crate.http.router;
  import std.stdio;

  auto router = new URLRouter();
  auto resourceCrate = new MemoryCrate!RelationModel;

  resourceCrate.addItem(RelationModel().serializeToJson);

  router
    .crateSetup
      .add(resourceCrate);

  request(router)
    .get("/relationmodels/000000000000000000000001")
      .expectStatusCode(200)
      .end((Response response) => () {
        assert(response.bodyJson["relationModel"]["relation"]["resource"] == "test resource");
      });

  request(router)
    .get("/relationmodels/000000000000000000000001/relation/resource")
      .expectStatusCode(200)
      .expectHeader("Content-Type", "test/resource")
      .end((Response response) => () {
        response.bodyString.should.equal("test body");
      });

  string data = "-----------------------------9855312492823326321373169801\r\n";
  data ~= "Content-Disposition: form-data; name=\"resource\"; filename=\"resource.txt\"\r\n";
  data ~= "Content-Type: text/plain\r\n\r\n";
  data ~= "hello\r\n";
  data ~= "-----------------------------9855312492823326321373169801--\r\n\r\n";

  request(router)
    .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
    .post("/relationmodels/000000000000000000000001/relation/resource")
    .expectStatusCode(201)
    .send(data)
    .end((Response response) => () {
      TestResource.lastRead.should.equal("hello");
    });
}

/// Access resources from an relation array
unittest {
  import vibe.http.router;
  import crate.api.rest.policy;
  import crate.http.router;
  import std.stdio;

  auto router = new URLRouter();
  auto resourceCrate = new MemoryCrate!ArrayModel;

  resourceCrate.addItem(ArrayModel().serializeToJson);

  router
    .crateSetup
      .add(resourceCrate);

  request(router)
    .get("/arraymodels/000000000000000000000001")
      .expectStatusCode(200)
      .end((Response response) => () {
        assert(response.bodyJson["arrayModel"]["relation"][0]["resource"] == "test resource");
      });

  request(router)
    .get("/arraymodels/000000000000000000000001/relation/0/resource")
      .expectStatusCode(200)
      .expectHeader("Content-Type", "test/resource")
      .end((Response response) => () {
        response.bodyString.should.equal("test body");
      });

  string data = "-----------------------------9855312492823326321373169801\r\n";
  data ~= "Content-Disposition: form-data; name=\"resource\"; filename=\"resource.txt\"\r\n";
  data ~= "Content-Type: text/plain\r\n\r\n";
  data ~= "hello\r\n";
  data ~= "-----------------------------9855312492823326321373169801--\r\n";

  TestResource.lastRead = "";
  request(router)
    .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
    .post("/arraymodels/000000000000000000000001/relation/0/resource")
    .expectStatusCode(201)
    .send(data)
    .end((Response response) => () {
      TestResource.lastRead.should.equal("hello");
    });
}

///
string cleanFileName(const string name) @safe pure nothrow {
  char[] result;
  result.length = name.length;

  foreach(i, ch; name) {
    if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')|| (ch >= '0' && ch <= '9') || (i > 0 && ch == '-') || ch == '.') {
      result[i] = ch;
    } else {
      result[i] = '_';
    }
  }

  return result.idup;
}

///
unittest {
  "file.jpg".cleanFileName.should.equal("file.jpg");
  "../file.jpg".cleanFileName.should.equal(".._file.jpg");
  ":1234file.jpg".cleanFileName.should.equal("_1234file.jpg");
  "-1-2.jpg".cleanFileName.should.equal("_1-2.jpg");
}


void flushResource(HTTPServerResponse res, HTTPServerRequest req, CrateResource resource) {
  immutable etag = resource.etag;
  res.statusCode = 200;
  res.setHeader("Content-Type", resource.contentType);
  res.setHeader("ETag", `"` ~ etag ~ `"`);
  res.setHeader("Cache-Control", "public");

  immutable expectedEtag = `"` ~ etag ~ `"`;

  if("If-None-Match" in req.headers && req.headers["If-None-Match"] == expectedEtag) {
    logDebug("ETag matched: " ~ expectedEtag ~ ". Sending 304.");
    res.writeEmptyBody(304, resource.contentType);
    return;
  }

  if(resource.hasSize) {
    logDebug("Flushing file with known size: " ~ resource.size.to!string);
    res.setHeader("Content-Length", resource.size.to!string);
  }

  logDebug("Responding with the file content.");
  resource.write(res.bodyWriter);
}

///
auto createTmpFile(InputStream input) {
  auto tmpFile = createTempFile();
  input.pipe(tmpFile);
  tmpFile.flush;

  return tmpFile;
}
