/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.resource.file;

import std.range;
import std.traits;
import std.conv;
import std.stdio;
import std.string;
import std.file;
import std.path;
import std.base64;
import std.algorithm;
import std.uuid;
import std.path;
import std.exception;
import std.string;

import crate.mime;
import crate.resource.base;

import vibe.stream.memory;
import vibe.stream.operations;
import vibe.inet.webform;
import vibe.core.file;
import vibe.core.stream;
import vibe.stream.wrapper;
import vibe.http.server;

import std.digest.sha;

struct DefaultCrateFileSettings {
  static string path = "files/";
  static string baseUrl = "";
}

version(unittest) {
  import fluent.asserts;
}

alias CrateFile = CrateFileResource!(DefaultCrateFileSettings);

/// Resource stored in a local file
class CrateFileResource(Settings) : CrateResource {
  ///
  protected {
    string currentFileName;
  }

  /// Create an empty file
  this() {}

  /// Create a file from a path
  this(string name) @safe {
    enforce(!name.startsWith("data:"));
    currentFileName = name;
  }

  /// The chunk size property
  size_t chunkSize() {
    throw new Exception("not supported");
  }

  void setMetadata(string key, string value) {
  }

  /// ditto
  void chunkSize(size_t value) {
    throw new Exception("not supported");
  }

  ///
  string id() {
    return currentFileName;
  }


  /// The resource name
  string fileName() {
    return currentFileName;
  }

  /// The resource name
  void fileName(string) {
    throw new Exception("You can't rename the file");
  }

  string etag() {
    auto fileInfo = getFileInfo(localPath);
    string key = fileInfo.timeModified.toISOExtString ~ fileInfo.size.to!string;
    string hex = hexDigest!SHA1(key).idup;

    return hex;
  }

  /// get the local file path
  string localPath() @safe {
    return buildPath(Settings.path, currentFileName);
  }

  ///
  bool exists() {
    return existsFile(this.localPath);
  }

  ///
  void remove() {
    if(!exists) {
      return;
    }

    removeFile(this.localPath);
  }

  /// Read a file from FilePart request
  void read(const FilePart file) {
    auto path = localPath;

    if(path.exists) {
      path.remove;
    }

    auto ext = file.headers["Content-Type"].to!string.toExtension;
    auto name = randomUUID.to!string.replace("-", "");

    file.tempPath.toString.copy(path);
  }

  /// Consume an input stream and store it
  void read(InputStream input) {
    auto output = openFile(localPath, FileMode.createTrunc);
    input.pipe(output);

    output.flush;
    output.close;
  }

  ///
  InputStream inputStream() {
    return openFile(localPath, FileMode.read).createProxyStream;
  }

  StreamLines byLine() {
    return StreamLines(inputStream);
  }

  /// Get the file path
  override string toString() const @safe {
    enforce(!currentFileName.isAbsolute, "You are going to expose absolute paths");

    return currentFileName;
  }

  void setBase64(Range)(Range r) @trusted {
    enum dataLength = "data:".length;
    auto contentStart = r.indexOf(";base64,");
    enum base64Length = ";base64,".length;

    auto f = File(localPath, "w");
    scope(exit) f.close;

    foreach(decoded; Base64.decoder(r[contentStart + base64Length ..$].map!(a => cast(char) a))) {
      f.write(cast(char) decoded);
    }
  }

  void httpHandler(HTTPServerRequest req, HTTPServerResponse response) {
    throw new Exception("not implemented");
  }

  static {
    /// Create a file from base64 encoded string or from a path. It must start with "data" for base64
    CrateFileResource!Settings fromString(string encoded) @trusted {
      enum dataLength = "data:".length;

      if(encoded.length > dataLength && encoded[0..dataLength] == "data:") {
        return CrateFileResource!Settings.fromBase64(encoded);
      }

      return new CrateFileResource!Settings(encoded);
    }

    /// Create file from base64 range
    CrateFileResource!Settings fromBase64(Range)(Range r) @trusted if (isInputRange!(Unqual!Range)) {
      return fromBase64(randomUUID.to!string.replace("-", ""), r);
    }

    /// ditto
    CrateFileResource!Settings fromBase64(Range)(string name, Range r) @trusted if (isInputRange!(Unqual!Range)) {
      return fromBase64("", name, r);
    }

    /// ditto
    CrateFileResource!Settings fromBase64(Range)(string path, string name, Range r) @trusted if (isInputRange!(Unqual!Range)) {
      enforce(!path.isAbsolute, "Can not create resources with absolute paths");

      enum dataLength = "data:".length;
      auto contentStart = r.indexOf(";base64,");
      const string mime = r[dataLength..contentStart];
      enforce!Exception(r[0..dataLength] == "data:", "Invalid file format. Expected `data:[mime];base64,[content]`");

      string filePath = chainPath(path, name).to!string ~ mime.toExtension;
      string fullPath = chainPath(Settings.path, filePath).to!string;

      if(!fullPath.dirName.exists) {
        fullPath.dirName.mkdirRecurse;
      }

      auto f = File(fullPath, "w");
      auto file = new CrateFileResource!Settings(filePath);
      file.setBase64(r);

      return file;
    }
  }

  /// get the file mime
  string contentType() {
    return currentFileName.extension.toMime;
  }

  /// set the file mime
  void contentType(string) {
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    assert(false, "not implemented");
  }

  /// Write the file content to a vibe output stream
  void write(BodyOutputStream bodyWriter) {
    auto f = openFile(localPath, FileMode.read);
    scope(exit) f.close;

    f.pipe(bodyWriter);
  }

  /// Returns true if the file size can be calculated
  bool hasSize() {
    return true;
  }

  /// Get the file size
  ulong size() {
    string path = localPath;

    debug {
      enforce(path.exists, path.to!string ~ " does not exist");
    } else {
      enforce(path.exists, currentFileName ~ " does not exist");
    }

    return path.getSize;
  }
}

version (unittest)
{
  import crate.base;
  import fluent.asserts;
  import crate.http.router;
  import crate.collection.memory;

  import vibe.data.json;
  import vibe.data.bson;
  import vibe.http.router;

  struct Item {
    @optional string _id = "item_id";
    Child child;
    CrateFile file = new CrateFile;
  }

  struct Child
  {
    CrateFile file = new CrateFile;
  }
}

/// the user should not see the absolute file path
unittest {
  auto oldPath = DefaultCrateFileSettings.path;
  scope(exit) DefaultCrateFileSettings.path = oldPath;

  DefaultCrateFileSettings.path = "files".asAbsolutePath.to!string;
  "files/user".mkdirRecurse;
  scope(exit) "files/".rmdirRecurse;

  auto file = new CrateFile("user/id/data.txt");
  file.toString.should.equal("user/id/data.txt");

  file = CrateFile.fromBase64("user/secret", "data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==");
  file.toString.should.equal("user/secret.txt");
}

/// the user should be able to get the etag
unittest {
  auto oldPath = DefaultCrateFileSettings.path;
  scope(exit) DefaultCrateFileSettings.path = oldPath;

  DefaultCrateFileSettings.path = "files".asAbsolutePath.to!string;
  "files/user".mkdirRecurse;
  scope(exit) "files/".rmdirRecurse;

  auto file = CrateFile.fromBase64("user/secret", "data:text/plain;base64,aGVsbG8gd29ybGQ=");
  file.etag.should.not.equal("");
}

/// the user should be able to upload a file as a base64 data using POST
unittest {
  auto router = new URLRouter();
  auto baseCrate = new MemoryCrate!Item;
  router
    .crateSetup
      .add(baseCrate);

  Json data = `{
    "item": {
      "file": "data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==",
      "child": {
        "file": "data:text/plain;base64,aGVsbG8gd29ybGQ="
      }
    }
  }`.parseJsonString;

  scope(exit) "files/".rmdirRecurse;

  request(router)
    .post("/items")
      .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          string parentFile = "files/" ~ response.bodyJson["item"]["file"].to!string;
          string childFile = "files/" ~ response.bodyJson["item"]["child"]["file"].to!string;

          parentFile.should.endWith(".txt");
          childFile.should.endWith(".txt");

          exists(parentFile).should.be.equal(true).because("The parent file `" ~ parentFile ~ "` should be created");
          exists(childFile).should.be.equal(true).because("The child file `" ~ childFile ~ "` should be created");

          readText(parentFile).should.equal("this is a text file");
          readText(childFile).should.equal("hello world");
        });
}

/// the user should be able to upload a file as a base64 data using PUT
unittest {
  auto router = new URLRouter();
  auto baseCrate = new MemoryCrate!Item;
  router
    .crateSetup
      .add(baseCrate);

  Item item = Item("1");
  item.file = new CrateFile("files/item.txt");
  item.child.file = new CrateFile("files/child.txt");

  baseCrate.addItem(item.serializeToJson);

  scope(exit) "files/".rmdirRecurse;

  auto data = `{
    "item": {
      "file": "data:text/plain;base64,Y2hhbmdlMQ==",
      "child": {
        "file": "data:text/plain;base64,Y2hhbmdlMg=="
      }
    }
  }`.parseJsonString;

  request(router)
    .put("/items/000000000000000000000001")
      .send(data)
        .expectStatusCode(200)
        .end((Response response) => () {
          response.bodyJson["item"]["file"].to!string.should.not.startWith("data:");
        });
}

/// It should return 304 when the etag matches
unittest {
  import crate.api.rest.policy;

  "files".mkdirRecurse;

  auto f = File("files/item.txt", "w");
  f.write("this is the item");
  f.close;

  f = File("files/child.txt", "w");
  f.write("this is the child");
  f.close;

  scope(exit) "files/".rmdirRecurse;

  auto baseCrate = new MemoryCrate!Item;
  auto item = Item();

  item.file = new CrateFile("item.txt");
  item.child.file = new CrateFile("child.txt");

  auto jsonItem = item.serializeToJson;
  baseCrate.addItem(jsonItem);

  auto router = new URLRouter();

  router
    .crateSetup
      .add(baseCrate);

  request(router)
    .get("/items/000000000000000000000001/file")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.headers.keys.should.contain("ETag");
        response.headers["ETag"].should.equal(`"` ~ item.file.etag ~ `"`);
      });

  request(router)
    .header("If-None-Match", `"` ~ item.file.etag ~ `"`)
    .get("/items/000000000000000000000001/file")
      .expectStatusCode(304)
      .end((Response response) => () {
        response.headers.keys.should.contain("ETag");
        response.headers["ETag"].should.equal(`"` ~ item.file.etag ~ `"`);
        response.bodyString.should.equal("");
      });
}

/// the user should be able to download a file
unittest {
  import crate.api.rest.policy;

  "files".mkdirRecurse;

  auto f = File("files/item.txt", "w");
  f.write("this is the item");
  f.close;

  f = File("files/child.txt", "w");
  f.write("this is the child");
  f.close;

  scope(exit) "files/".rmdirRecurse;

  auto baseCrate = new MemoryCrate!Item;
  auto item = Item();

  item.file = new CrateFile("item.txt");
  item.child.file = new CrateFile("child.txt");

  auto jsonItem = item.serializeToJson;
  baseCrate.addItem(jsonItem);

  auto router = new URLRouter();

  router
    .crateSetup
      .add(baseCrate);

  request(router)
    .get("/items/000000000000000000000001/file")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.headers.keys.should.contain("ETag");
        response.bodyString.should.equal("this is the item");
      });

  request(router)
    .get("/items/000000000000000000000001/child/file")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyString.should.equal("this is the child");
      });

  string data = "-----------------------------9855312492823326321373169801\r\n";
  data ~= "Content-Disposition: form-data; name=\"file\"; filename=\"resource.txt\"\r\n";
  data ~= "Content-Type: text/plain\r\n\r\n";
  data ~= "new file\r\n";
  data ~= "-----------------------------9855312492823326321373169801--\r\n";

  request(router)
    .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
    .post("/items/000000000000000000000001/file")
      .send(data)
      .expectStatusCode(201)
      .end((Response response) => () { });

  request(router)
    .get("/items/000000000000000000000001/file")
      .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyString.should.equal("new file");
      });
}

struct StreamLines {
  InputStream input;

  string front;
  bool empty;

  this(InputStream input) {
    this.input = input;

    popFront();
  }

  void popFront() {
    this.empty = input.empty;

    if(!empty) {
      this.front = input.readLine(size_t.max, "\n").assumeUTF;
    } else {
      this.front = "";
    }
  }
}
