/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.resource.gridfs;

import std.base64;
import std.string;
import std.exception;
import std.range;
import std.datetime;
import std.conv;
import std.path;

import crate.base;
import crate.error;
import crate.resource.base;
import crate.mime;

import vibe.inet.webform;
import vibe.stream.memory;
import vibe.stream.wrapper;
import vibe.core.file;
import vibe.core.log;
import vibe.db.mongo.collection;
import vibe.db.mongo.mongo;
import vibe.data.bson;
import vibe.http.server;

version(unittest) {
  import fluent.asserts;
}

class GridFsFileInputStream(T) : InputStream {
  private {
    T cursor;
    size_t remainingChunkSize;
  }

  @trusted:
    this(T cursor) {
      this.cursor = cursor;
    }

    bool empty() @property {
      return cursor.empty;
    }

    ulong leastSize() @property {
      auto data = (cast(BsonBinData) cursor.front["data"]).rawData;

      if(remainingChunkSize == 0) {
        return data.length;
      }

      return data.length - remainingChunkSize;
    }

    bool dataAvailableForRead() @property {
      return !cursor.empty;
    }

    const(ubyte)[] peek() {
      if(cursor.empty) {
        return [];
      }

      return (cast(BsonBinData) cursor.front["data"]).rawData;
    }

    ulong readOnce(scope ubyte[] dst) {
      auto data = peek;
      size_t start = remainingChunkSize;
      size_t end = start + dst.length;

      logDebug("reading block from `%s` to `%s`", start, end);

      if(end > data.length) {
        end = data.length;
      }

      auto len = end - start;
      dst[0..len] = data[start..end];

      if(end == data.length) {
        cursor.popFront;
        remainingChunkSize = 0;
      } else {
        remainingChunkSize = end;
      }

      return len;
    }

    ulong read(scope ubyte[] dst, IOMode mode = IOMode.once) {
      if(mode == IOMode.all) {
        size_t begin;

        while(!empty && dst.length > begin) {
          begin += readOnce(dst[begin..$]);
        }

        return begin;
      }

      return readOnce(dst);
    }
}

struct GridFsConfig {
  static {
    MongoCollection files;
    MongoCollection chunks;

    size_t chunkSize = 255 * 1024;
  }
}

class GridFsResource(Settings) : CrateResource {

  private {
    Bson fileRecord;
    Bson metadata;
  }

  @trusted:
  this() {}

  this(string idOrName) {
    static if (is(typeof(Settings.files) == class)) {
      enforce(Settings.files !is null, "`" ~ Settings.stringof ~ ".files` is null.");
    }

    if(isObjectId(idOrName)) {
      auto cursor = Settings.files.find(["_id" : BsonObjectID.fromString(idOrName)]);

      enforce!CrateNotFoundException(!cursor.empty, "The resource with id `" ~ idOrName ~ "` was not found.");
      fileRecord = cursor.front;
      metadata = fileRecord["metadata"];
      return;
    }

    auto cursor = Settings.files.find(["filename": Bson(idOrName)]);
    if(cursor.empty) {
      this.fileName = idOrName;
      return;
    }

    fileRecord = cursor.front;
    metadata = fileRecord["metadata"];
  }

  void setupFileRecord() @trusted {
    if(fileRecord.type != Bson.Type.object) {
      fileRecord = Bson.emptyObject;
      metadata = Bson.emptyObject;
      fileRecord["metadata"] = metadata;
    }

    enforce!Exception(fileRecord["_id"].isNull, "The file already has an id.");

    fileRecord["_id"] = BsonObjectID.generate();
    fileRecord["length"] = 0;
    fileRecord["chunkSize"] = Settings.chunkSize;
    fileRecord["uploadDate"] = BsonDate(Clock.currTime);
    fileRecord["filename"] = fileRecord["_id"].toJson.to!string;
    fileRecord["metadata"] = metadata;
  }

    /// The chunk size property
  size_t chunkSize() {
    return fileRecord["chunkSize"].to!string.to!size_t;
  }

  /// ditto
  void chunkSize(size_t value) {
    fileRecord["chunkSize"] = value;
  }

  void createFileRecord() @trusted {
    setupFileRecord();

    static if (is(typeof(Settings.files) == class)) {
      enforce(Settings.files !is null, "`" ~ Settings.stringof ~ ".files` is null.");
    }

    Settings.files.insertOne(fileRecord);
  }

  /// The resource name
  string fileName() {
    return fileRecord["filename"].toJson.to!string;
  }

  /// The resource name
  void fileName(string value) {
    if(fileRecord.type != Bson.Type.object) {
      setupFileRecord();
    }

    fileRecord["filename"] = value;
  }

  void setMetadata(string key, string value) {
    metadata[key] = value;
  }

  void update() {
    static if (is(typeof(Settings.files) == class)) {
      enforce(Settings.files !is null, "`" ~ Settings.stringof ~ ".files` is null.");
    }

    enforce!Exception(!fileRecord["_id"].isNull, "The file does not have an id.");
    fileRecord["metadata"] = metadata;

    if(!exists) {
      logDiagnostic("The file record does not exist. Adding it.");
      Settings.files.insertOne(fileRecord);
      return;
    }

    logDiagnostic("The file record exist. Updating it.");
    Settings.files.replaceOne(["_id": fileRecord["_id"]], fileRecord);
  }

  /// The resource content type
  string contentType() {
    if(fileRecord.type != Bson.Type.object || metadata.isNull || metadata["mime"].isNull) {
      return "application/octet-stream";
    }

    return metadata["mime"].toJson.to!string;
  }

  /// Set the resource content type
  void contentType(string value) {
    if(fileRecord.type != Bson.Type.object) {
      createFileRecord();
    }

    metadata["mime"] = value;
  }

  /// unique id for the resource
  string id() const {
    if(fileRecord.type != Bson.Type.object) {
      return "";
    }

    return fileRecord["_id"].toJson.to!string;
  }

  /// write the resource to a http request body
  void write(BodyOutputStream bodyWriter) {
    inputStream.pipe(bodyWriter);
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    Bson selector = Bson.emptyObject;
    selector["files_id"] = fileRecord["_id"];
    selector["n"] = index;

    Settings.chunks.deleteMany(selector);

    auto chunk = Bson.emptyObject;
    chunk["files_id"] = fileRecord["_id"];
    chunk["n"] = index;
    chunk["data"] = BsonBinData(BsonBinData.Type.generic, data.idup);

    Settings.chunks.insertOne(chunk);

    size_t length = 0;
    auto cursor = Settings.chunks.find([
      "files_id": fileRecord["_id"]
    ]);

    foreach(record; cursor) {
      if(record["data"].type == Bson.Type.binData) {
        auto binRecord = cast(BsonBinData) record["data"];
        length += binRecord.rawData.length;
      }
    }


    fileRecord["length"] = length;
    update;
  }

  /// Read a resource from FilePart request
  void read(const FilePart file) {
    if(fileRecord.type != Bson.Type.object) {
      createFileRecord();
    }

    if(file.headers["Content-Type"]) {
      contentType(file.headers["Content-Type"]);
    }

    read(openFile(file.tempPath, FileMode.read).createProxyStream);
  }

  /// Consume an input stream and store it
  void read(InputStream input) {
    if(fileRecord.type != Bson.Type.object || fileRecord["_id"].isNull) {
      createFileRecord();
    } else {
      Settings.chunks
        .deleteMany(["files_id" : BsonObjectID.fromString(id)]);
    }

    scope buffer = new ubyte[Settings.chunkSize];

    size_t n;
    size_t length;

    auto chunk = Bson.emptyObject;
    chunk["files_id"] = fileRecord["_id"];

    while(!input.empty) {
      if(buffer.length > input.leastSize) {
        buffer.length = input.leastSize;
      } else {
        buffer.length = Settings.chunkSize;
      }

      logDiagnostic("Consuming `%s` bytes from the input stream", buffer.length);

      auto size = input.read(buffer, IOMode.once);
      length += size;

      chunk["n"] = n;
      chunk["data"] = BsonBinData(BsonBinData.Type.generic, buffer[0..size].idup);

      Settings.chunks.insertOne(chunk);

      logDiagnostic("Wrote the chunk no. `%s`", n);
      n++;
    }

    logDiagnostic("The range is empty.");

    metadata["etag"] = BsonObjectID.generate.toString;
    fileRecord["length"] = length;
    fileRecord["chunkSize"] = Settings.chunkSize;
    fileRecord["uploadDate"] = BsonDate(Clock.currTime);

    update();
  }

  /// Get a input stream for the resource content
  InputStream inputStream() {
    try {
      logDebug("Creating grid fs input stream");
    } catch(Exception) {}

    enforce(fileRecord.type == Bson.Type.object, "The filerecord does not exist!");

    auto cursor = Settings.chunks.find([
      "files_id": fileRecord["_id"]
    ]);

    cursor.sort(["n": 1]);

    return new GridFsFileInputStream!(typeof(cursor))(cursor);
  }

  /// Check if the file exists
  bool exists() {
    if(id.length != 24) {
      return false;
    }

    return Settings
      .files
      .countDocuments(["_id" : BsonObjectID.fromString(id)]) > 0;
  }

  /// Remove the resource
  void remove() {
    Settings
      .files
      .deleteMany(["_id" : BsonObjectID.fromString(id)]);

    Settings
      .chunks
      .deleteMany(["files_id" : BsonObjectID.fromString(id)]);
  }

  /// Check if the resource size can be computed
  bool hasSize() {
    if(fileRecord.isNull || fileRecord.type != Bson.Type.object || fileRecord["length"].isNull) {
      return false;
    }

    return !fileRecord["length"].isNull;
  }

  /// Get the resource size in bytes
  ulong size() {
    if(fileRecord["length"].isNull) {
      return 0;
    }

    return fileRecord["length"].toJson.to!ulong;
  }

  /// Returns a hash that uniq identify the resource content
  string etag() {
    if(metadata.isNull || metadata.type != Bson.Type.object || metadata["etag"].isNull) {
      return BsonObjectID.generate.toString;
    }

    return metadata["etag"].toJson.to!string;
  }

  /// Get the file path
  override string toString() const {
    return id;
  }

  void httpHandler(HTTPServerRequest req, HTTPServerResponse response) {
    throw new Exception("not implemented");
  }

  static {
    GridFsResource!Settings fromString(string encoded) {
      enum dataLength = "data:".length;

      if(encoded.length > dataLength && encoded[0..dataLength] == "data:") {
        return GridFsResource!Settings.fromBase64(encoded);
      }

      if(encoded != "") {
        return new GridFsResource!Settings(encoded);
      }

      return new GridFsResource!Settings();
    }

    GridFsResource!Settings fromBase64(string encoded) {
      auto resource = new GridFsResource!Settings;

      enum dataLength = "data:".length;
      enum base64Length = ";base64,".length;

      auto contentStart = encoded.indexOf(";base64,");
      const string mime = encoded[dataLength..contentStart];
      enforce!Exception(encoded[0..dataLength] == "data:", "Invalid file format. Expected `data:[mime];base64,[content]`");

      auto base64Data = encoded[contentStart+base64Length..$];
      auto decoded = Base64.decode(base64Data);
      auto stream = createMemoryStream(decoded, false, decoded.length);

      resource.contentType = mime;
      resource.read(stream);

      return resource;
    }

    GridFsResource!Settings fromLocalFile(string path) {
      auto resource = new GridFsResource!Settings;

      resource.contentType = path.extension.toMime;
      resource.fileName = path.baseName;

      auto resultFile = openFile(path, FileMode.read);
      auto stream = resultFile.createProxyStream;

      resource.read(stream);

      return resource;
    }
  }
}

interface IFSCursor {
  @safe:
    size_t size();
    bool empty();
    Bson front();
    void popFront();
    void sort(int[string]);
}

class FSCursor : IFSCursor {
  private {
    MongoCursor!Bson cursor;
  }

  @safe:
    this(MongoCursor!Bson cursor) {
      this.cursor = cursor;
    }

    bool empty() {
      return cursor.empty;
    }

    Bson front() {
      return cursor.front;
    }

    void popFront() {
      cursor.popFront;
    }

    void sort(int[string] order) {
      cursor.sort(order);
    }
    size_t size() {
      return cursor.count;
    }
}

interface IGridFsCollection {
  @safe:
    size_t countDocuments(BsonObjectID[string]);
    IFSCursor find(BsonObjectID[string]);
    IFSCursor find(Bson[string]);
    void replaceOne(Bson[string], Bson);
    void insertOne(Bson);
    void deleteMany(BsonObjectID[string]);
    void deleteMany(Bson);
    IFSCursor get();
}

class MongoGridFsCollection : IGridFsCollection {
  @safe:

    private {
      MongoCollection collection;
    }

    this(MongoCollection collection) {
      this.collection = collection;
    }

    size_t countDocuments(BsonObjectID[string] query) {
      return this.collection.countDocuments(query);
    }

    IFSCursor find(BsonObjectID[string] query) {
      return new FSCursor(this.collection.find(query));
    }

    IFSCursor find(Bson[string] query) {
      return new FSCursor(this.collection.find(query));
    }

    IFSCursor get() {
      return new FSCursor(this.collection.find(Bson.emptyObject));
    }

    void replaceOne(Bson[string] query, Bson value) {
      this.collection.replaceOne(query, value);
    }

    void insertOne(Bson value) {
      this.collection.insertOne(value);
    }

    void deleteMany(BsonObjectID[string] query) {
      this.collection.deleteMany(query);
    }

    void deleteMany(Bson query) {
      this.collection.deleteMany(query);
    }
}
