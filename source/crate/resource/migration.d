/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.resource.migration;

import crate.base;
import crate.error;
import crate.resource.base;

import vibe.inet.webform;
import vibe.stream.memory;
import vibe.stream.wrapper;
import vibe.core.stream;
import vibe.core.log;
import vibe.data.json;
import vibe.http.server;

class MigrationResource(From : CrateResource, Destination : CrateResource) : CrateResource {
  private {
    Destination resource;
  }

  @safe:
  this() {
    resource = new Destination();
  }

  this(string id) {
    resource = new Destination(id);
  }

  this(Destination resource) {
    this.resource = resource;
  }

  ulong chunkSize() {
    return resource.chunkSize;
  }

  void chunkSize(ulong value) {
    resource.chunkSize = value;
  }

  void setMetadata(string key, string value) {
    resource.setMetadata(key, value);
  }

  string id() {
    return resource.id;
  }

  string contentType() {
    return resource.contentType;
  }

  void contentType(string value) {
    resource.contentType(value);
  }

  /// The resource name
  string fileName() {
    return resource.fileName;
  }

  /// The resource name
  void fileName(string value) {
    resource.fileName(value);
  }

  void write(BodyOutputStream bodyWriter) {
    return resource.write(bodyWriter);
  }

  void read(const(FilePart) file) {
    return resource.read(file);
  }

  void read(InputStream input) {
    return resource.read(input);
  }

  InputStream inputStream() {
    return resource.inputStream;
  }

  bool exists() {
    return resource.exists;
  }

  void remove() {
    resource.remove;
  }

  bool hasSize() {
    return resource.hasSize;
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    resource.setChunk(index, data);
  }

  ulong size() {
    return resource.size;
  }

  string etag() {
    return resource.etag;
  }

  override string toString() const {
    return resource.toString;
  }

  static MigrationResource!(From, Destination) fromString(string encoded) @trusted {
    Destination destination;
    bool transform;

    if(encoded == "") {
      return new MigrationResource!(From, Destination)();
    }

    try {
      destination = Destination.fromString(encoded);
    } catch(Exception e) {
      logError(e.message);
      transform = true;
    }

    if(transform || !destination.exists) {
      auto tmp = From.fromString(encoded);
      enforce!CrateNotFoundException(tmp.exists, "The original resource `" ~ encoded ~ "` does not exist.");

      ubyte[] data;
      tmp.inputStream.read(data);

      destination = new Destination;
      destination.contentType = tmp.contentType;
      destination.read(tmp.inputStream);

      tmp.remove;
    }

    return new MigrationResource!(From, Destination)(destination);
  }

  void httpHandler(HTTPServerRequest req, HTTPServerResponse response) {
    throw new Exception("not implemented");
  }
}

void migrate(T, string path)(CrateAccess crate) {
  foreach(item; crate.get.exec) {
    mixin(`string value = item` ~ path ~ `.to!string;`);
    if(value.length != 24) {
      try {
        string newValue = T.fromString(value).toString;

        if(value != newValue) {
          mixin(`item` ~ path ~ ` = newValue;`);
          crate.updateItem(item);
          logInfo("Updating `%s` with id `%s`", T.stringof, item["_id"]);
        }
      } catch(Exception e){
        logError(e.msg);
      }
    }
  }
}