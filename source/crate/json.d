/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.json;

import crate.error;

public import vibe.data.json;

import std.exception;
import std.typecons;
import std.array;
import std.conv;
import std.string;
import std.algorithm;


version(unittest) {
  import fluent.asserts;
}


///
Json mixRecursive(Json data, Json newData, string prefix = "") {
  Json mixedData = data;
  enforce!CrateRelationNotFoundException(data.type == newData.type, "Invalid `" ~ prefix ~ "` type.");

  foreach (string key, newValue; newData) {
    if (mixedData[key].type == Json.Type.object && newValue.type == Json.Type.object) {
      mixedData[key] = mix(mixedData[key], newValue, prefix ~ (prefix == "" ? "" : ".") ~ key);
      continue;
    }

    if (mixedData[key].type == Json.Type.array && newValue.type == Json.Type.array) {
      foreach(size_t index, itemValue; newValue) {
        if(index >= mixedData[key].length) {
          mixedData[key].appendArrayElement(itemValue);
          continue;
        }

        mixedData[key][index] = mix(mixedData[key][index], itemValue, prefix ~ (prefix == "" ? "" : ".") ~ key);
      }
      continue;
    }

    mixedData[key] = newValue;
  }

  return mixedData;
}

///
Json mix(Json data, Json newData, string prefix = "") {
  Json mixedData = data;
  enforce!CrateRelationNotFoundException(data.type == newData.type, "Invalid `" ~ prefix ~ "` type.");

  foreach (string key, value; newData) {
    if (mixedData[key].type == Json.Type.object && value.type == Json.Type.object) {
      mixedData[key] = mix(mixedData[key], value, prefix ~ (prefix == "" ? "" : ".") ~ key);
    } else {
      mixedData[key] = value;
    }
  }

  return mixedData;
}

/// check the json mixer with simple values
unittest {
  Json data = Json.emptyObject;
  Json newData = Json.emptyObject;

  data["key1"] = 1;
  newData["key2"] = 2;

  auto result = data.mix(newData);
  assert(result["key1"].to!int == 1);
  assert(result["key2"].to!int == 2);
}

/// check the json mixer with nested values
unittest {
  Json data = Json.emptyObject;
  Json newData = Json.emptyObject;

  data["key"] = Json.emptyObject;
  data["key"]["nested1"] = 1;

  newData["key"] = Json.emptyObject;
  newData["key"]["nested2"] = 2;

  auto result = data.mix(newData);
  assert(result["key"]["nested1"].to!int == 1);
  assert(result["key"]["nested2"].to!int == 2);
}

/// Takes a nested Json object and moves the values to a Json assoc array where the key
/// is the path from the original object to that value
Json[string] toFlatJson(Json object) @trusted {
  Json[string] elements;

  auto root = tuple("", object);
  Tuple!(string, Json)[] queue = [ root ];

  while(queue.length > 0) {
    auto element = queue[0];

    if(element[0] != "") {
      if(element[1].type != Json.Type.object && element[1].type != Json.Type.array) {
        elements[element[0]] = element[1];
      }

      if(element[1].type == Json.Type.object && element[1].length == 0) {
        elements[element[0]] = element[1];
      }

      if(element[1].type == Json.Type.array && element[1].length == 0) {
        elements[element[0]] = element[1];
      }
    }

    if(element[1].type == Json.Type.object) {
      foreach(string key, value; element[1].byKeyValue) {
        string nextKey = key;

        if(element[0] != "") {
          nextKey = element[0] ~ "." ~ nextKey;
        }

        queue ~= tuple(nextKey, value);
      }
    }

    if(element[1].type == Json.Type.array) {
      size_t index;

      foreach(value; element[1].byValue) {
        string nextKey = element[0] ~ "[" ~ index.to!string ~ "]";

        queue ~= tuple(nextKey, value);
        index++;
      }
    }

    queue = queue[1..$];
  }

  return elements;
}

/// Get a flatten object
unittest {
  import std.algorithm;

  auto obj = Json.emptyObject;
  obj["key1"] = 1;
  obj["key2"] = 2;
  obj["key3"] = Json.emptyObject;
  obj["key3"]["item1"] = "3";
  obj["key3"]["item2"] = Json.emptyObject;
  obj["key3"]["item2"]["item4"] = Json.emptyObject;
  obj["key3"]["item2"]["item5"] = Json.emptyObject;
  obj["key3"]["item2"]["item5"]["item6"] = Json.emptyObject;

  auto result = obj.toFlatJson;
  result.byKeyValue.map!(a => a.key).should.containOnly(["key1", "key2", "key3.item1", "key3.item2.item4", "key3.item2.item5.item6"]);
  result["key1"].should.equal(1);
  result["key2"].should.equal(2);
  result["key3.item1"].should.equal("3");
  result["key3.item2.item4"].should.equal(Json.emptyObject);
  result["key3.item2.item5.item6"].should.equal(Json.emptyObject);
}

/// Get the missing elements from the `after` json
Json[string] jsonDiff(const Json before, const Json after) {
  Json[string] diff;

  foreach(string key, value; before) {
    if(key !in after) {
      diff[key] = value;
      continue;
    }

    if(key in after && after[key] != before[key]) {
      diff[key] = value;
    }
  }

  return diff;
}

/// compare two jsons and get the diff
unittest {
  auto item1 = `{ "a": "1", "b": 2, "c": 3}`.parseJsonString;
  auto item2 = `{ "b": 2, "c": 30 }`.parseJsonString;

  auto result = jsonDiff(item2, item1);
  result.should.equal(["c": Json(30)]);

  result = jsonDiff(item1, item2);
  result.should.equal(["a": Json("1"), "c": Json(3)]);
}

///
string serializeToSortedJsonString(const Json value) {
  if(value.type != Json.Type.object && value.type != Json.Type.array) {
    return escapeJson(value);
  }

  string result;
  string glue;
  if(value.type == Json.Type.object) {
    result = `{`;
    string[] keys;

    foreach(string key, val; value) {
      keys ~= key;
    }

    keys.sort;
    foreach(key; keys) {
      result ~= glue ~ escapeJson(key) ~ `:` ~ serializeToSortedJsonString(value[key]);
      glue = ",";
    }

    result ~= `}`;
  }

  if(value.type == Json.Type.array) {
    result = `[`;
    foreach(val; value) {
      result ~= glue ~ serializeToSortedJsonString(val);
      glue = ",";
    }
    result ~= `]`;
  }

  return result;
}

/// It should sort objects
unittest {
  auto value = `{"a": 1, "b": 2, "c": 3, "d": 4}`.parseJsonString;

  value.serializeToSortedJsonString().should.equal(`{"a":1,"b":2,"c":3,"d":4}`);
}

/// It should sort objects inside arrays
unittest {
  auto value = `{"test":[{"a": 1, "b": 2, "c": 3, "d": 4},{"a": 1, "b": 2, "c": 3, "d": 4}]}`.parseJsonString;

  value.serializeToSortedJsonString().should.equal(`{"test":[{"a":1,"b":2,"c":3,"d":4},{"a":1,"b":2,"c":3,"d":4}]}`);
}

/// It should escape keys and values
unittest {
  auto value = `{" \b \f \n \r \t \" \\":" \b \f \n \r \t \" \\"}`.parseJsonString;

  value.serializeToSortedJsonString().should.equal(`{" \u0008 \u000C \n \r \t \" \\":" \u0008 \u000C \n \r \t \" \\"}`);
}

string escapeJson(string value) {
  return Json(value).toString;
}

string escapeJson(Json value) {
  return value.toString;
}

string getRestApiKey(Json json) {
  auto keys = json.byKeyValue.map!(a => a.key).array;

  enforce!CrateValidationException(keys.length > 0, "There are no keys in your object.");
  enforce!CrateValidationException(keys.length == 1, "There are more than one key in your object.");
  enforce!CrateValidationException(json[keys[0]].type == Json.Type.object, "You provided an invalid object.");

  return keys[0];
}

bool exists(ref Json json, string key) {
  auto path = getObjectPath(key);

  return json.exists(path);
}

bool exists(ref Json json, ObjectPath[] path) {
  if(path.length == 0) {
    return false;
  }

  if(path[0].type == ObjectPath.Type.key && json.type != Json.Type.object) {
    return false;
  }

  if(path[0].type == ObjectPath.Type.index && json.type != Json.Type.array) {
    return false;
  }

  if(path[0].type == ObjectPath.Type.key && path[0].key !in json) {
    return false;
  }

  if(path[0].type == ObjectPath.Type.key && json[path[0].key].type == Json.Type.undefined) {
    return false;
  }

  if(path[0].type == ObjectPath.Type.key && json[path[0].key].type == Json.Type.null_) {
    return false;
  }

  if(path.length == 1) {
    return true;
  }

  if(path[0].type == ObjectPath.Type.key) {
    return json[path[0].key].exists(path[1..$]);
  }

  if(path[0].type == ObjectPath.Type.index) {
    return json[path[0].index].exists(path[1..$]);
  }

  return false;
}

bool exists(ref Json json, size_t index) {
  if(json.type != Json.Type.array) {
    return false;
  }

  if(index < json.length) {
    return true;
  }

  return false;
}

bool exists(ref Json json, ObjectPath path) {
  if(path.type == ObjectPath.Type.key && json.type == Json.Type.object && path.key in json) {
    return true;
  }

  if(path.type == ObjectPath.Type.index && path.index < json.length) {
    if(json[path.index].type == Json.Type.undefined) {
      return false;
    }

    if(json[path.index].type == Json.Type.null_) {
      return false;
    }

    return true;
  }

  return false;
}

/// an empty object should not have any properties
unittest {
  auto object = Json.emptyObject;

  object.exists("a").should.equal(false);
  object.exists("a.b.c").should.equal(false);
  object.exists("a[0].c").should.equal(false);
  object.exists("a[0]").should.equal(false);
  object.exists("[0]").should.equal(false);
  object.exists(0).should.equal(false);
}

/// an object whth nested properties should have them
unittest {
  auto object = `{
    "a": {
      "b": 5
    },
    "c": [],
    "d": [2]
  }`.parseJsonString;

  object.exists("a").should.equal(true);
  object.exists("a.b").should.equal(true);
  object.exists("a.b.c").should.equal(false);
  object.exists("c").should.equal(true);
  object.exists("f").should.equal(false);
  object.exists("d[0]").should.equal(true);
  object.exists("d[1]").should.equal(true);
  object.exists(0).should.equal(false);
}

///
Json toNestedJson(Json[string] flatJson) {
  auto result = Json.emptyObject;

  foreach(string key, Json value; flatJson) {
    result.set(key, value);
  }

  return result;
}

Json toNestedJson(Json flatJson) {
  auto result = Json.emptyObject;

  foreach(string key, Json value; flatJson) {
    result.set(key, value);
  }

  return result;
}

/// It keeps a value in the right place when there is no . inside
unittest {
  auto flatJson = `{
    "key": "value"
  }`.parseJsonString;

  auto result = flatJson.toNestedJson;

  result.should.equal(`{
    "key": "value"
  }`.parseJsonString);
}

/// It unpacks a complex object
unittest {
  auto flatJson = `{
    "a": "value",
    "b.c": 2,
    "b.d": 3,
    "d.e.f": 4,
  }`.parseJsonString;

  auto result = flatJson.toNestedJson;

  result.should.equal(`{
    "a": "value",
    "b": { "c": 2, "d": 3 },
    "d": {
      "e": { "f": 4 } }
    }`.parseJsonString);
}

struct ObjectPath {
  string key;
  size_t index;
  Type type;

  enum Type {
    key, index
  }
}

ObjectPath[] getObjectPath(string path) {
  ObjectPath[] objectPath;

  auto pieces = path.split('.');

  foreach(item; pieces) {
    auto indexPieces = item.split('[');
    objectPath ~= ObjectPath(indexPieces[0], 0, ObjectPath.Type.key);

    if(indexPieces.length > 1) {
      foreach(indexPiece; indexPieces[1..$]) {
        objectPath ~= ObjectPath("", indexPiece[0..$-1].to!size_t, ObjectPath.Type.index);
      }
    }
  }

  return objectPath;
}

/// it returns a list with one element when there is no . or [
unittest {
  getObjectPath("key").should.equal([ObjectPath("key", 0, ObjectPath.Type.key)]);
}

/// it returns a list with all elements when they are sepparated by .
unittest {
  getObjectPath("key1.key2.key3").should.equal([
    ObjectPath("key1", 0, ObjectPath.Type.key),
    ObjectPath("key2", 0, ObjectPath.Type.key),
    ObjectPath("key3", 0, ObjectPath.Type.key)]);
}

/// it adds an index entry when [ is present
unittest {
  getObjectPath("key1[2]").should.equal([
    ObjectPath("key1", 0, ObjectPath.Type.key),
    ObjectPath("", 2, ObjectPath.Type.index) ]);
}

/// it parses path with an array of objects
unittest {
  getObjectPath("key1[2].a[3].b").should.equal([
    ObjectPath("key1", 0, ObjectPath.Type.key),
    ObjectPath("", 2, ObjectPath.Type.index),
    ObjectPath("a", 0, ObjectPath.Type.key),
    ObjectPath("", 3, ObjectPath.Type.index),
    ObjectPath("b", 0, ObjectPath.Type.key) ]);
}

///
void set(T)(ref Json object, string key, T value) if(!is(T == Json)) {
  object.set(key, value.serializeToJson);
}

///
void set(ref Json object, string key, Json value) {
  auto path = getObjectPath(key);

  object.set(path, value);
}

///
void set(ref Json object, ObjectPath[] path, Json value) {
  if(path.length == 0) {
    return;
  }

  if(path.length == 1) {
    object.set(path[0], value);
    return;
  }

  if(!object.exists(path[0])) {
    object.add(path, value);
    return;
  }

  if(path[0].type == ObjectPath.Type.key) {
    object[path[0].key].set(path[1..$], value);
  }

  if(path[0].type == ObjectPath.Type.index) {
    object[path[0].index].set(path[1..$], value);
  }
}

///
void set(ref Json object, ObjectPath path, Json value) {
  if(path.type == ObjectPath.Type.key) {
    object[path.key] = value;
  }

  if(path.type == ObjectPath.Type.index) {
    foreach (i; object.length .. path.index + 1) {
      object ~= Json();
    }

    object[path.index] = value;
  }
}

/// It creates a new object property when is not an object
unittest {
  auto obj = Json.emptyObject;

  obj.set("a.b", 5);
  obj.should.equal(`{ "a": { "b": 5 } }`.parseJsonString);
}

/// It creates a new array property when there is no array
unittest {
  auto obj = Json.emptyObject;

  obj.set("a[0].b", 5);
  obj.should.equal(`{ "a": [{ "b": 5 }] }`.parseJsonString);
}

/// It can add 2 values to the same array item
unittest {
  auto obj = Json.emptyObject;

  obj.set("a[0].b", 5);
  obj.set("a[0].c", 6);

  obj.should.equal(`{ "a": [{ "b": 5, "c": 6 }] }`.parseJsonString);
}

/// It can add 2 values to separate array items
unittest {
  auto obj = Json.emptyObject;

  obj.set("a[0].b", 5);
  obj.set("a[1].c", 6);
  obj.should.equal(`{ "a": [{ "b": 5 }, { "c": 6 }] }`.parseJsonString);
}

/// It can add a primitive value to an array
unittest {
  auto obj = Json.emptyObject;

  obj.set("a[0]", 5);
  obj.set("a[1]", 6);
  obj.should.equal(`{ "a": [ 5, 6 ] }`.parseJsonString);
}

/// It can add a multi level array value
unittest {
  auto obj = Json.emptyObject;

  obj.set("a[0][0][0].a", 5);
  obj.set("a[1][0].b", 6);
  obj.set("a[1][1]", 7);
  obj.should.equal(`{ "a": [[[ { "a": 5 } ]], [{ "b": 6 }, 7]] }`.parseJsonString);
}

///
void add(ref Json object, ObjectPath[] path, Json value) {
  if(path.length == 0) {
    return;
  }

  if(path.length == 1) {
    object.set(path[0], value);
    return;
  }

  auto nextJson = Json.emptyObject;

  if(path.length > 2 && path[1].type == ObjectPath.Type.index) {
    nextJson = Json.emptyArray;
  }

  if(path.length == 2 && path[1].type == ObjectPath.Type.index) {
    nextJson = Json.emptyArray;
  }

  if(path[0].type == ObjectPath.Type.index) {
    object.ensureIndex(path[0].index);
  }

  if(path[0].type == ObjectPath.Type.key && path[1].type == ObjectPath.Type.index) {
    object[path[0].key] = Json.emptyArray;
    object[path[0].key].ensureIndex(path[1].index);
  }

  if(path[0].type == ObjectPath.Type.key && path[1].type == ObjectPath.Type.key) {
    object[path[0].key] = Json.emptyObject;
  }

  if(path[0].type == ObjectPath.Type.key) {
    object[path[0].key] = nextJson;
    object[path[0].key].add(path[1..$], value);
  }

  if(path[0].type == ObjectPath.Type.index) {
    object[path[0].index] = nextJson;
    object[path[0].index].add(path[1..$], value);
  }
}

///
void add(ref Json object, ObjectPath path, Json value) {
  object.set(path, value);
}

/// It can add a value to an object
unittest {
  auto result = Json.emptyObject;

  result.add(ObjectPath("a", 0, ObjectPath.Type.key), Json(5));

  result.should.equal(`{ "a": 5 }`.parseJsonString);
}

/// It can add a nested value to an object
unittest {
  auto result = Json.emptyObject;

  result.add([
    ObjectPath("a", 0, ObjectPath.Type.key),
    ObjectPath("b", 0, ObjectPath.Type.key),
    ObjectPath("c", 0, ObjectPath.Type.key)
  ], Json(5));

  result.should.equal(`{ "a": { "b": { "c": 5 } } }`.parseJsonString);
}

/// It can add an array item
unittest {
  auto result = Json.emptyObject;

  result.add([
    ObjectPath("a", 0, ObjectPath.Type.key),
    ObjectPath("", 1, ObjectPath.Type.index)
  ], Json(5));

  auto expected = `{ "a": [ 1, 5 ] }`.parseJsonString;
  expected["a"][0] = Json();

  result.should.equal(expected);
}

/// It can add an object inside an array
unittest {
  auto result = Json.emptyObject;

  result.add([
    ObjectPath("a", 0, ObjectPath.Type.key),
    ObjectPath("", 1, ObjectPath.Type.index),
    ObjectPath("b", 0, ObjectPath.Type.key)
  ], Json(5));

  auto expected = `{"a": [ 1, { "b": 5 }]}`.parseJsonString;
  expected["a"][0] = Json();

  result.should.equal(expected);
}

void ensureIndex(ref Json list, size_t index) {
  if(list.length > index) {
    return;
  }

  foreach (i; list.length .. index + 1) {
    list ~= Json();
  }
}


///
Json get(ref Json object, string path) {
  return get(object, path.getObjectPath);
}

///
Json get(ref Json object, ObjectPath[] path) {
  auto value = object;

  foreach (crumb; path) {
    if(crumb.type == ObjectPath.Type.key && value[crumb.key].type != Json.Type.null_ && value[crumb.key].type != Json.Type.undefined) {
      value = value[crumb.key];
      continue;
    }

    if(crumb.type == ObjectPath.Type.index && value[crumb.index].type != Json.Type.null_ && value[crumb.index].type != Json.Type.undefined) {
      value = value[crumb.index];
      continue;
    }

    return Json();
  }

  return value;
}