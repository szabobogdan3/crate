/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.generators.kotlin;

import crate.base;
import crate.ctfe;
import crate.http.router;
import crate.generators.transpiler;
import crate.attributes;

import std.conv;
import std.array;
import std.algorithm;
import std.traits;
import std.datetime;
import std.string;
import openapi.definitions;

import vibe.http.common;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

///
string[string] toKotlinModels(T)(CrateRouter!T router) {
  string[string] models;

  foreach(rule; router.rules) {
    foreach(key, value; rule.toKotlinModels) {
      if(key == "") continue;
      models[key] = value;
    }
  }

  return models;
}

///
string[string] toKotlinModels(CrateRule rule) {
  string[string] models;

  foreach(name, code; rule.modelDescription.toKotlinClass) {
    models[name] = code;
  }

  foreach(name, code; rule.modelDescription.toKotlinViewModel) {
    models[name] = code;
  }

  return models;
}

string[string] toKotlinViewModel(ModelDescription model) {
  string[string] viewModels;

  if(model.singular == "") {
    return viewModels;
  }

  string className = model.singular ~ "ViewModel";
  string code = model.toKotlinViewModelImports ~
    "\n\n" ~
    "class " ~ className ~ ": ViewModel() {
    private val " ~ model.lowerCasePlural ~ "Store = Store.instance." ~ model.lowerCasePlural ~ "
    private var " ~ model.lowerCaseSingular ~ "Id: String? = null\n\n";

  code ~= "    " ~ model.toLoadModel ~ "\n\n";

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    if(field.listType.length == 0) {
      code ~= "    " ~ toLoadRelation(field.model, model.lowerCaseSingular, field.publicName) ~ "\n\n";
    } else {
      code ~= "    " ~ toLoadRelationList(field.model, model.lowerCaseSingular, field.publicName) ~ "\n\n";
      code ~= "    " ~ toLoadFirstRelation(field.model, model.lowerCaseSingular, field.publicName) ~ "\n\n";

    }
  }

  code ~= "\n}";
  viewModels[className] = code;
  return viewModels;
}

/// It creates a view model with the right name
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Team {
    string id;
    string value;
  }

  struct Map {
    string id;
    Team team;
  }

  auto code = describe!Map.toKotlinViewModel["MapViewModel"];

  code.should.contain("class MapViewModel: ViewModel() {
    private val mapsStore = Store.instance.maps
    private var mapId: String? = null");
}

string toKotlinViewModelImports(ModelDescription model) {
  string imports = "package com.example.opengreenmap.models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.opengreenmap.stores.Store
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch\n";

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    imports ~= "import com.example.opengreenmap.models." ~ field.model.singular ~ "\n";
  }

  return imports;
}

string toLoadModel(ModelDescription model) {
  return "private val _" ~ model.lowerCaseSingular ~ " = MutableStateFlow(" ~ model.singular ~ "())
    val " ~ model.lowerCaseSingular ~ ": StateFlow<" ~ model.singular ~ ">
        get() = _" ~ model.lowerCaseSingular ~ "

    fun load" ~ model.singular ~ "(" ~ model.lowerCaseSingular ~ "Id: String) {
        this." ~ model.lowerCaseSingular ~ "Id = " ~ model.lowerCaseSingular ~ "Id

        viewModelScope.launch {
            if(" ~ model.lowerCaseSingular ~ "Id != null) {
                " ~ model.lowerCasePlural ~ "Store.getById(id = " ~ model.lowerCaseSingular ~ "Id).collect { " ~ model.lowerCaseSingular ~ " ->
                    _" ~ model.lowerCaseSingular ~ ".value = " ~ model.lowerCaseSingular ~ "
                }
            }
        }
    }

    fun load" ~ model.singular ~ "(" ~ model.lowerCaseSingular ~ ": " ~ model.singular ~ ") {
        this." ~ model.lowerCaseSingular ~ "Id = " ~ model.lowerCaseSingular ~ "._id

        viewModelScope.launch {
          _" ~ model.lowerCaseSingular ~ ".value = " ~ model.lowerCaseSingular ~ "
        }
    }";
}

/// It creates the code that loads the model
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Team {
    string id;
    string value;
  }

  struct Map {
    string id;
    Team team;
  }

  auto code = describe!Map.toLoadModel;

  code.should.equal("private val _map = MutableStateFlow(Map())
    val map: StateFlow<Map>
        get() = _map

    fun loadMap(mapId: String) {
        this.mapId = mapId

        viewModelScope.launch {
            if(mapId != null) {
                mapsStore.getById(id = mapId).collect { map ->
                    _map.value = map
                }
            }
        }
    }

    fun loadMap(map: Map) {
        this.mapId = map._id

        viewModelScope.launch {
          _map.value = map
        }
    }");
}

string toLoadRelation(ModelDescription model, string parentProperty, string relationName) {
  return "private val _" ~ relationName ~ " = MutableStateFlow(" ~ model.singular ~ "())
    val " ~ relationName ~ ": StateFlow<" ~ model.singular ~ ">
        get() = _" ~ relationName ~ "

    fun load" ~ relationName.upperCaseFirstLetter ~ "() {
        viewModelScope.launch {
            Store.instance." ~ model.lowerCasePlural ~ ".getById(id = _" ~ parentProperty ~ ".value." ~ relationName ~ ").collect { value ->
              _" ~ relationName ~ ".value = value
            }
        }
    }";
}

/// It creates the code that loads the relation
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Team {
    string id;
    string value;
  }

  auto code = describe!Team.toLoadRelation("map", "team");

  code.should.equal("private val _team = MutableStateFlow(Team())
    val team: StateFlow<Team>
        get() = _team

    fun loadTeam() {
        viewModelScope.launch {
            Store.instance.teams.getById(id = _map.value.team).collect { value ->
              _team.value = value
            }
        }
    }");
}

string toLoadRelationList(ModelDescription model, string parentProperty, string relationName) {
  return "private val _" ~ relationName ~ " = MutableStateFlow(listOf<" ~ model.singular ~ ">())
    val " ~ relationName ~ ": StateFlow<List<" ~ model.singular ~ ">>
        get() = _" ~ relationName ~ "

    fun load" ~ relationName.upperCaseFirstLetter ~ "() {
        viewModelScope.launch {
            var mutableList: MutableList<" ~ model.singular ~ "> = mutableListOf()

            for (id in _" ~ parentProperty ~ ".value." ~ relationName ~ ") {
              val item = Store.instance." ~ model.lowerCasePlural ~ ".getByIdWithSuspend(id)
              mutableList.add(item);
            }

            _" ~ relationName ~ ".value = mutableList
        }
    }";
}

/// It creates the code that loads the relation list
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Team {
    string id;
    string value;
  }

  auto code = describe!Team.toLoadRelationList("map", "teams");

  code.should.equal("private val _teams = MutableStateFlow(listOf<Team>())
    val teams: StateFlow<List<Team>>
        get() = _teams

    fun loadTeams() {
        viewModelScope.launch {
            var mutableList: MutableList<Team> = mutableListOf()

            for (id in _map.value.teams) {
              val item = Store.instance.teams.getByIdWithSuspend(id)
              mutableList.add(item);
            }

            _teams.value = mutableList
        }
    }");
}

string toLoadFirstRelation(ModelDescription model, string parentProperty, string relationName) {
  return "private val _first" ~ relationName.upperCaseFirstLetter ~ " = MutableStateFlow(" ~ model.singular ~ "())
    val first" ~ relationName.upperCaseFirstLetter ~ ": StateFlow<" ~ model.singular ~ ">
        get() = _first" ~ relationName.upperCaseFirstLetter ~ "

    fun loadFirst" ~ relationName.upperCaseFirstLetter ~ "() {
        if(_first" ~ relationName.upperCaseFirstLetter ~ ".value._id != \"\") {
            return;
        }

        viewModelScope.launch {
          if (_" ~ parentProperty ~ ".value." ~ relationName ~ ".isNotEmpty()) {
            val id = _" ~ parentProperty ~ ".value." ~ relationName ~ "[0]
            Store.instance." ~ model.lowerCasePlural ~ ".getById(id).collect { value ->
              _first" ~ relationName.upperCaseFirstLetter ~ ".value = value
            }
          }
        }
    }";
}

/// It creates the code that loads the relation list
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Team {
    string id;
    string value;
  }

  auto code = describe!Team.toLoadFirstRelation("map", "teams");

  code.should.equal("private val _firstTeams = MutableStateFlow(Team())
    val firstTeams: StateFlow<Team>
        get() = _firstTeams

    fun loadFirstTeams() {
        if(_firstTeams.value._id != \"\") {
            return;
        }

        viewModelScope.launch {
          if (_map.value.teams.isNotEmpty()) {
            val id = _map.value.teams[0]
            Store.instance.teams.getById(id).collect { value ->
              _firstTeams.value = value
            }
          }
        }
    }");
}

string[string] toKotlinClass(ModelDescription model) {
  string[string] models;

  if(model.singular == "") {
    return models;
  }

  if(model.fields.basic.length == 0 && model.fields.objects.length == 0 && model.fields.relations.length == 0) {
    return models;
  }

  string code = `package com.example.opengreenmap.models

import androidx.compose.runtime.Immutable
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import kotlinx.serialization.json.*
import kotlinx.serialization.*
import org.threeten.bp.OffsetDateTime
import com.example.opengreenmap.components.articleBlocks.ArticleBody
import com.example.opengreenmap.serializers.DateSerializer

@Immutable
@Serializable
data class ` ~ model.singular ~ `(`~ model.toProperties ~"\n)\n\n" ~ model.toResponseClasses;

  models[model.singular] = code;

  foreach(field; model.fields.objects) {
    foreach(name, localCode; field.toKotlinClass) {
      models[name] = localCode;
    }
  }

  return models;
}

/// It converts a schema with a relation to a kotlin class
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Team {
    string id;
    string value;
  }

  struct Map {
    string id;
    Team team;
  }

  auto code = describe!Map.toKotlinClass["Map"];

  code.should.equal("package com.example.opengreenmap.models

import androidx.compose.runtime.Immutable
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import kotlinx.serialization.json.*
import kotlinx.serialization.*
import org.threeten.bp.OffsetDateTime
import com.example.opengreenmap.components.articleBlocks.ArticleBody
import com.example.opengreenmap.serializers.DateSerializer

@Immutable\n@Serializable\ndata class Map(
    @PrimaryKey @ColumnInfo(name = \"id\") val id: String = \"\",
    @ColumnInfo(name = \"team\") val team: String = \"\",
)

@Serializable
sealed class MapListResponse {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : MapListResponse()

    @Serializable
    data class Success(
        val maps: List<Map>
    ) : MapListResponse()
}

@Serializable
sealed class MapItemResponse {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : MapItemResponse()

    @Serializable
    data class Success(
        val map: Map
    ) : MapItemResponse()
}");
}

/// It converts a schema with a struct property to a kotlin class
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct ModelInfo {
    string value;
  }

  struct Map {
    string id;
    ModelInfo modelInfo;
  }

  auto code = describe!Map.toKotlinClass["Map"];

  code.should.equal("package com.example.opengreenmap.models

import androidx.compose.runtime.Immutable
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import kotlinx.serialization.json.*
import kotlinx.serialization.*
import org.threeten.bp.OffsetDateTime
import com.example.opengreenmap.components.articleBlocks.ArticleBody
import com.example.opengreenmap.serializers.DateSerializer

@Immutable\n@Serializable\ndata class Map(
    @PrimaryKey @ColumnInfo(name = \"id\") val id: String = \"\",
    @ColumnInfo(name = \"modelInfo\") val modelInfo: ModelInfo = ModelInfo(),
)

@Serializable
sealed class MapListResponse {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : MapListResponse()

    @Serializable
    data class Success(
        val maps: List<Map>
    ) : MapListResponse()
}

@Serializable
sealed class MapItemResponse {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : MapItemResponse()

    @Serializable
    data class Success(
        val map: Map
    ) : MapItemResponse()
}");
}

/// It generates the code for a struct property
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct ModelInfo {
    string value;
  }

  struct Map {
    string id;
    ModelInfo modelInfo;
  }

  auto code = describe!Map.toKotlinClass["ModelInfo"];

  code.should.equal("package com.example.opengreenmap.models

import androidx.compose.runtime.Immutable
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import kotlinx.serialization.json.*
import kotlinx.serialization.*
import org.threeten.bp.OffsetDateTime
import com.example.opengreenmap.serializers.DateSerializer

@Immutable\n@Serializable\ndata class ModelInfo(
    @ColumnInfo(name = \"value\") val value: String = \"\",
)");
}

string[string] toKotlinClass(ObjectFieldDescription model) {
  string[string] models;

  if(model.fields.basic.length == 0 && model.fields.objects.length == 0 && model.fields.relations.length == 0) {
    return models;
  }

  string code = `package com.example.opengreenmap.models

import androidx.compose.runtime.Immutable
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import kotlinx.serialization.json.*
import kotlinx.serialization.*
import org.threeten.bp.OffsetDateTime
import com.example.opengreenmap.serializers.DateSerializer

@Immutable
@Serializable
data class ` ~ model.type ~ `(`~ model.toProperties ~`
)`;

  models[model.type] = code;

  foreach(field; model.fields.objects) {
    foreach(name, localCode; field.toKotlinClass) {
      models[name] = localCode;
    }
  }

  return models;
}

///
string toProperties(T)(T model) {
  string code;

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    code ~= "\n    ";

    if(field.isId) {
      code ~= "@PrimaryKey ";
    }

    code ~= "@ColumnInfo(name = \"" ~ field.publicName ~ "\") val " ~ field.publicName ~ ": " ~ field.toKotlinType ~ ` = ` ~ field.toKotlinDefault ~ `,`;
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    string type = field.toKotlinType;

    code ~= "\n    ";

    if(type == "OffsetDateTime") {
      code ~= "@Serializable(DateSerializer::class) ";
    }

    code ~= "@ColumnInfo(name = \"" ~ field.publicName ~ "\") val " ~ field.publicName ~ ": " ~ type ~ ` = ` ~ field.toKotlinDefault ~ ",";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    string defaultValue = field.listType.length > 0 ? field.toKotlinDefault() : `""`;
    code ~= "\n    ";
    code ~= "@ColumnInfo(name = \"" ~ field.publicName ~ "\") val " ~ field.publicName ~ ": " ~ "String".wrapList(field.listType) ~ " = " ~ defaultValue ~ ",";
  }

  return code;
}

/// It generates the list of strings props
unittest {
  struct TestModel {
    string id;
    string[] field1;
    string[][] field2;
    string[string][string] field3;
  }

  auto description = describe!TestModel;

  auto code = description.toProperties;
  code.should.equal("\n    @PrimaryKey @ColumnInfo(name = \"id\") val id: String = \"\",
    @ColumnInfo(name = \"field1\") val field1: List<String> = emptyList(),
    @ColumnInfo(name = \"field2\") val field2: List<List<String>> = emptyList(),
    @ColumnInfo(name = \"field3\") val field3: HashMap<String, HashMap<String, String>> = HashMap(),");
}

/// It generates the list of relation props
unittest {
  struct Other {
    string id;
  }

  struct TestModel {
    string id;
    Other[] field1;
    Other[][] field2;
    Other[string][string] field3;
  }

  auto description = describe!TestModel;

  auto code = description.toProperties;
  code.should.equal("\n    @PrimaryKey @ColumnInfo(name = \"id\") val id: String = \"\",
    @ColumnInfo(name = \"field1\") val field1: List<String> = emptyList(),
    @ColumnInfo(name = \"field2\") val field2: List<List<String>> = emptyList(),
    @ColumnInfo(name = \"field3\") val field3: HashMap<String, HashMap<String, String>> = HashMap(),");
}

/// It ignores hidden fields
unittest {
  struct OtherModel {
    string id;
  }

  struct Other {
    string value;
  }

  struct TestModel {
    string id;

    @hidden {
      string field0;
      Other field1;
      OtherModel field2;
    }
  }

  auto description = describe!TestModel;

  auto code = description.toProperties;
  code.should.equal("\n    @PrimaryKey @ColumnInfo(name = \"id\") val id: String = \"\",");
}

string toKotlinType(T)(T field) {
  auto customType = getAttribute!("kotlin type")(field);
  if(customType != "") {
    return customType;
  }

  string type = "unknown";

  switch(field.type) {
    case "string":
      type = "String";
      break;
    case "bool":
      type = "Boolean";
      break;
    case "double":
      type = "Double";
      break;
    case "float":
      type = "Float";
      break;
    case "long":
      type = "Long";
      break;
    case "ulong":
      type = "ULong";
      break;
    case "int":
      type = "Int";
      break;
    case "uint":
      type = "UInt";
      break;
    case "Json":
      type = "JsonObject";
      break;
    case "SysTime":
      type = "OffsetDateTime";
      break;
    default:
      type = field.type;
  }

  type = type.wrapList(field.listType);

  return type;
}

/// It matches D types to kotlin types
unittest {
  struct TestModel {
    string _String;
    SysTime _Date;
    bool _Bool;
    double _Double;
    float _Double2;
    long _Int;
    ulong _UInt;
    uint _UInt32;
    Json _json;

    @("kotlin type: KotlinBool")
    string customType;

    SysTime time;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toKotlinType.should.equal("String");
  description.fields.basic[1].toKotlinType.should.equal("Boolean");
  description.fields.basic[2].toKotlinType.should.equal("Double");
  description.fields.basic[3].toKotlinType.should.equal("Float");
  description.fields.basic[4].toKotlinType.should.equal("Long");
  description.fields.basic[5].toKotlinType.should.equal("ULong");
  description.fields.basic[6].toKotlinType.should.equal("UInt");
  description.fields.basic[7].toKotlinType.should.equal("JsonObject");
  description.fields.basic[8].toKotlinType.should.equal("KotlinBool");
  description.fields.objects[0].toKotlinType.should.equal("OffsetDateTime");
}

string toKotlinDefault(T)(T field) {
  auto customDefault = getAttribute!("kotlin default")(field);
  if(customDefault != "") {
    return customDefault;
  }

  if(field.listType.length > 0 && field.listType[0] == ListType.list) {
    return "emptyList()";
  }

  if(field.listType.length > 0 && field.listType[0] == ListType.hashmap) {
    return "HashMap()";
  }

  string value = "unknown";

  switch(field.type) {
    case "string":
      value = `""`;
      break;
    case "bool":
      value = "false";
      break;
    case "double":
      value = "0.0";
      break;
    case "float":
      value = "0.0";
      break;
    case "long":
      value = "0";
      break;
    case "ulong":
      value = "0UL";
      break;
    case "int":
      value = "0";
      break;
    case "uint":
      value = "0U";
      break;
    case "Json":
      value = "JsonObject(emptyMap())";
      break;
    case "SysTime":
      value = "OffsetDateTime.now()";
      break;
    default:
      value = field.type ~ "()";
  }

  return value;
}


/// Returns the custom kotlin default value when it is set
unittest {
  struct TestModel {
    @(`kotlin default: JsonObject(emptyMap())`)
    string customType;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toKotlinDefault.should.equal("JsonObject(emptyMap())");
}

string wrapList(string type, ListType[] list) {
  foreach(item; list) {
    if(item == ListType.list) {
      type = `List<` ~ type ~ `>`;
    } else {
      type = `HashMap<String, ` ~ type ~ `>`;
    }
  }

  return type;
}

string toResponseClasses(ModelDescription model) {
  string code;

  string listTypeName = model.singular ~ "ListResponse";
  string itemTypeName = model.singular ~ "ItemResponse";

  code ~= `@Serializable
sealed class ` ~ listTypeName ~ ` {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : ` ~ listTypeName ~ `()

    @Serializable
    data class Success(
        val ` ~ model.lowerCasePlural ~ `: List<` ~ model.singular ~ `>
    ) : ` ~ listTypeName ~ `()
}

@Serializable
sealed class ` ~ itemTypeName ~ ` {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : ` ~ itemTypeName ~ `()

    @Serializable
    data class Success(
        val ` ~ model.lowerCaseSingular ~ `: ` ~ model.singular ~ `
    ) : ` ~ itemTypeName ~ `()
}`;


  return code;
}

/// It can generate the response classes for a model
unittest {
  struct Map {
    string id;
  }

  auto code = describe!Map.toResponseClasses;

  code.should.equal("@Serializable
sealed class MapListResponse {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : MapListResponse()

    @Serializable
    data class Success(
        val maps: List<Map>
    ) : MapListResponse()
}

@Serializable
sealed class MapItemResponse {
    @Serializable
    data class Error(
        val error: @Contextual Throwable?,
        val errors: List<@Contextual Throwable>?,
    ) : MapItemResponse()

    @Serializable
    data class Success(
        val map: Map
    ) : MapItemResponse()
}");
}
