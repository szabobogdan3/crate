/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.generators.swift;

import crate.base;
import crate.ctfe;
import crate.http.router;
import crate.generators.transpiler;

import std.conv;
import std.array;
import std.algorithm;
import std.traits;
import std.datetime;
import std.string;
import openapi.definitions;

import vibe.http.common;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

///
string[string] toSwiftModels(T)(CrateRouter!T router) {
  string[string] models;

  foreach(rule; router.rules) {
    foreach(key, value; rule.toSwiftModels) {
      if(key == "") continue;
      models[key] = value;
    }
  }

  return models;
}

///
string[string] toSwiftModels(CrateRule rule) {
  string[string] models;

  foreach(name, code; rule.modelDescription.toSwiftClass) {
    models[name] = code;
  }

  return models;
}

/// convert a rest api get operation
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;

  struct TestModel {
    string id;
    string field;
  }

  auto fields = describe!TestModel;

  auto generatedCode = RestApi.getItem(fields).toSwiftModels;
  generatedCode.keys.should.equal(["TestModel"]);
}

string toSwiftImports(ModelDescription model) {
  return model.attributes.toSwiftImports;
}

string toSwiftImports(ObjectFieldDescription model) {
  return model.attributes.toSwiftImports;
}

string toSwiftImports(string[] attributes) {
  static immutable keyLen = "swift import:".length;
  return attributes.filter!(a => a.indexOf("swift import:") == 0).map!(a => "import " ~ a[keyLen..$].strip).joiner("\n").array.to!string ~ "\n";
}

string[string] toSwiftClass(ObjectFieldDescription model) {
  if(model.type == "SysTime" || model.type == "string") {
    return null;
  }

  bool hasRelations = model.fields.relations.length > 0;

  string code = `import Foundation
import Combine
` ~ model.toSwiftImports ~ `
class ` ~ model.type ~ `: Hashable, Codable, @unchecked Sendable {
`;

  code ~= model.toPlainEquable;
  code ~= model.toPlainHash;

  if(!hasRelations) {
    code ~= model.toProperties(false);
  } else {
    code ~= "    private var cancellables = Set<AnyCancellable>()\n";
    code ~= model.toProperties;
    code ~= model.toHashInit;
    code ~= model.toCodingKeys;
    code ~= model.toEncoderFunction;
    code ~= model.toLoadingFunctions;
  }
  code ~= model.toCopyfunction;
  code ~= "\n}\n";


  string[string] models;
  models[model.type] = code;

  foreach(field; model.fields.objects) {
    foreach(name, localcode; field.toSwiftClass) {
      models[name] = localcode;
    }
  }

  foreach(field; model.fields.relations) {
    foreach(name, localCode; field.model.toSwiftClass) {
      models[name] = localCode;
    }
  }

  return models;
}

///
string[string] toSwiftClass(ModelDescription model) {
  string idField = "id";

  foreach(field; model.fields.basic) {
    if(field.isId) {
      idField = field.publicName;
    }
  }

  if(model.singular == "") {
    return null;
  }

  string code = `import Foundation
import Combine
` ~ model.toSwiftImports ~ `
struct ` ~ model.singular ~ `List : CrateModelList, @unchecked Sendable {
    static let name: String = "` ~ model.plural.toLower ~ `"
    var ` ~ model.lowerCasePlural ~ `: [` ~ model.singular ~ `]
    var items: [` ~ model.singular ~ `] { get { return ` ~ model.lowerCasePlural ~ ` } }

    init(_ items: [` ~ model.singular ~ `]) {
        ` ~ model.lowerCasePlural ~ ` = items
    }
}

struct ` ~ model.singular ~ `Item : CrateModelItem, @unchecked Sendable {
    static let name: String = "` ~ model.plural.toLower ~ `"
    var item: ` ~ model.singular ~ ` { get { return ` ~ model.lowerCaseSingular ~ ` } }
    var ` ~ model.lowerCaseSingular ~ `: ` ~ model.singular ~ `

    init(_ item: ` ~ model.singular ~ `) {
        ` ~ model.lowerCaseSingular ~ ` = item
    }
}


class ` ~ model.singular ~ `: CrateModel, @unchecked Sendable {

    static let modelName: String = "` ~ model.plural.toLower ~ `";

    static func == (lhs: ` ~ model.singular ~ `, rhs: ` ~ model.singular ~ `) -> Bool {
        return lhs.id == rhs.id;
    }` ~ "\n";

  code ~= model.toProperties;
  code ~= model.toHashInit;

  code ~= "\n    required convenience init(id: String) {
        self.init()
        self.id = id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }\n";

  code ~= model.toCodingKeys;
  code ~= model.toEncoderFunction;
  code ~= model.toLoadingFunctions;
  code ~= model.toCopyfunction;
  code ~= "\n}";


  string[string] models;
  models[model.singular] = code;

  foreach(field; model.fields.objects) {
    foreach(name, localCode; field.toSwiftClass) {
      models[name] = localCode;
    }
  }

  foreach(field; model.fields.relations) {
    foreach(name, localCode; field.model.toSwiftClass) {
      models[name] = localCode;
    }
  }

  return models;
}

/// It converts a schema with a relation to a swift class
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Other {
    string id;
    string value;
  }

  struct TestModel {
    string id;
    Other field;
  }

  auto code = describe!TestModel.toSwiftClass["TestModel"];

  code.should.contain("class TestModel: CrateModel, @unchecked Sendable {");
  code.should.contain(`static let modelName: String = "testmodels";`);
  code.should.contain(`static func == (lhs: TestModel, rhs: TestModel) -> Bool {
        return lhs.id == rhs.id;
    }`);
  code.should.contain(`@Published var id: String = ""
    @Published var field: Other = Other.init()
    @Published var fieldId: String = "" {
        didSet {
        }
    }`);
  code.should.contain(`required convenience init(from decoder: Decoder) throws {`);
  code.should.contain(`required convenience init(id: String) {`);
  code.should.contain(`func hash(into hasher: inout Hasher) {`);
  code.should.contain(`enum CodingKeys: String, CodingKey {`);
  code.should.contain(`func encode(to encoder: Encoder) throws {`);
  code.should.contain(`func loadField() async throws {`);
}

///
string toHashInit(T)(T model) {
  string code = "
    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? self.decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)\n";

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    auto fieldName = field.publicName;
    if(field.isId) {
      fieldName = "id";
    }

    code ~= `
        if let value = try? values.decode(` ~ field.toSwiftType ~ `.self, forKey: .` ~ fieldName ~ `) {
            self.` ~ fieldName ~ ` = value
        }` ~ "\n";
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    code ~= `
        if let value = try? values.decode(` ~ field.toSwiftType ~ `.self, forKey: .` ~ field.publicName ~ `) {
            self.` ~ field.publicName ~ ` = value
        }` ~ "\n";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    auto relationIdField = field.model.idField;
    relationIdField.listType = field.listType;
    relationIdField.isOptional = field.isOptional;

    code ~= `
        if let value = try? values.decode(` ~ relationIdField.toSwiftType ~ `.self, forKey: .` ~ field.publicName ~ `Id) {
            self.` ~ field.publicName ~ `Id = value
        }` ~ "\n";
  }

  code ~= "\n    }\n";
  return code;
}

/// It generates the hash init
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct OtherModel {
    string id;
    string value;
  }

  struct SomeStruct {
    string value;
  }

  struct TestModel {
    string _id;
    OtherModel field;
    SomeStruct field2;
  }

  auto code = describe!TestModel.toHashInit;

  code.should.equal("
    required convenience init(from decoder: Decoder) throws {
        self.init()
        try? self.decode(from: decoder)
    }

    func decode(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? values.decode(String.self, forKey: .id) {
            self.id = value
        }

        if let value = try? values.decode(SomeStruct.self, forKey: .field2) {
            self.field2 = value
        }

        if let value = try? values.decode(String.self, forKey: .fieldId) {
            self.fieldId = value
        }

    }\n");
}

///
string toEncoderFunction(T)(T model) {
  string code = "
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)\n\n";

  foreach(field; model.fields.basic) {
    if(!field.canWrite || field.isHidden) continue;

    auto fieldName = field.publicName;
    if(field.isId) {
      fieldName = "id";
    }

    code ~= `        try? container.encode(` ~ fieldName ~ `, forKey: .` ~ fieldName ~ ")\n";
  }

  foreach(field; model.fields.objects) {
    if(!field.canWrite || field.isHidden) continue;
    code ~= `        try? container.encode(` ~ field.publicName ~ `, forKey: .` ~ field.publicName ~ ")\n";
  }

  foreach(field; model.fields.relations) {
    if(!field.canWrite || field.isHidden) continue;
    code ~= `        try? container.encode(` ~ field.publicName ~ `Id, forKey: .` ~ field.publicName ~ "Id)\n";
  }

  code ~= "    }\n";
  return code;
}

/// It generates the encoder function
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct OtherModel {
    string id;
    string value;
  }

  struct SomeStruct {
    string id;
    string value;
  }

  struct TestModel {
    string id;
    OtherModel field;
    SomeStruct field2;
  }

  auto code = describe!TestModel.toEncoderFunction;

  code.should.equal("\n    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try? container.encode(id, forKey: .id)
        try? container.encode(fieldId, forKey: .fieldId)
        try? container.encode(field2Id, forKey: .field2Id)
    }\n");
}

///
string toSwiftType(T)(T field, bool addOptional = true) {
  string type = "unknown";

  switch(field.type) {
    case "string":
      type = "String";
      break;
    case "SysTime":
      type = "Date";
      break;
    case "bool":
      type = "Bool";
      break;
    case "double":
      type = "Double";
      break;
    case "float":
      type = "Double";
      break;
    case "long":
      type = "Int";
      break;
    case "ulong":
      type = "UInt";
      break;
    case "int":
      type = "Int32";
      break;
    case "uint":
      type = "UInt32";
      break;
    case "Json":
      type = "Json";
      break;
    default:
      type = field.type;
  }

  type = type.wrapList(field.listType);

  auto customType = getAttribute!("swift type")(field);
  if(customType != "") {
    type = customType;
  }

  if(field.isOptional && addOptional) {
    return type ~ "?";
  }

  return type;
}

/// It matches D types to swift types
unittest {
  struct TestModel {
    string _String;
    SysTime _Date;
    bool _Bool;
    double _Double;
    float _Double2;
    long _Int;
    ulong _UInt;
    uint _UInt32;
    Json _json;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toSwiftType.should.equal("String");
  description.fields.basic[1].toSwiftType.should.equal("Bool");
  description.fields.basic[2].toSwiftType.should.equal("Double");
  description.fields.basic[3].toSwiftType.should.equal("Double");
  description.fields.basic[4].toSwiftType.should.equal("Int");
  description.fields.basic[5].toSwiftType.should.equal("UInt");
  description.fields.basic[6].toSwiftType.should.equal("UInt32");
  description.fields.basic[7].toSwiftType.should.equal("Json");
}

/// It matches D optional types to swift optional types
unittest {
  struct TestModel {
    @optional:
      string _String;
      SysTime _Date;
      bool _Bool;
      double _Double;
      float _Double2;
      long _Int;
      ulong _UInt;
      uint _UInt32;
      Json _json;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toSwiftType.should.equal("String?");
  description.fields.basic[1].toSwiftType.should.equal("Bool?");
  description.fields.basic[2].toSwiftType.should.equal("Double?");
  description.fields.basic[3].toSwiftType.should.equal("Double?");
  description.fields.basic[4].toSwiftType.should.equal("Int?");
  description.fields.basic[5].toSwiftType.should.equal("UInt?");
  description.fields.basic[6].toSwiftType.should.equal("UInt32?");
  description.fields.basic[7].toSwiftType.should.equal("Json?");
}

string toCodingKeys(T)(T model) {
  string idField = "id";

  foreach(field; model.fields.basic) {
    if(field.isId) {
      idField = field.publicName;
    }
  }

  string code = "\n    enum CodingKeys: String, CodingKey {\n";

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    if(!field.isId) {
      code ~= `        case ` ~ field.publicName ~ ` = "` ~ field.publicName ~ `"` ~ "\n";
    } else {
      code ~= `        case id = "` ~ idField ~ `"` ~ "\n";
    }
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    code ~= `        case ` ~ field.publicName ~ ` = "` ~ field.publicName ~ `"` ~ "\n";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    code ~= `        case ` ~ field.publicName ~ `Id = "` ~ field.publicName ~ `"` ~ "\n";
  }

  code ~= "    }\n";

  return code;
}

/// It generates the coding keys function
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct OtherModel {
    string _id;
    string value;
  }

  struct SomeStruct {
    string id;
    string value;
  }

  struct TestModel {
    string id;
    OtherModel field;
    SomeStruct field2;
  }

  describe!TestModel.toCodingKeys.should.equal("\n    enum CodingKeys: String, CodingKey {
        case id = \"id\"
        case fieldId = \"field\"
        case field2Id = \"field2\"
    }\n");

  describe!OtherModel.toCodingKeys.should.equal("\n    enum CodingKeys: String, CodingKey {
        case id = \"_id\"
        case value = \"value\"
    }\n");
}

///
string toProperties(T)(T model, bool usePublish = true) {
  string code = "\n";
  string published = usePublish ? "@Published" : "";

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    if(!field.isId) {
      code ~= "    " ~ published ~ " var " ~ field.publicName ~ ": " ~ field.toSwiftType ~ ` = ` ~ field.toSwiftDefault ~ "\n";
    } else {
      code ~= "    " ~ published ~ " var id: " ~ field.toSwiftType ~ ` = ` ~ field.toSwiftDefault ~ "\n";
    }
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    code ~= "    " ~ published ~ " var " ~ field.publicName ~ ": " ~ field.toSwiftType ~ ` = ` ~ field.toSwiftDefault ~ "\n";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    auto relationIdField = field.model.idField;
    relationIdField.listType = field.listType;
    relationIdField.isOptional = field.isOptional;

    code ~= "    " ~ published ~ " var " ~ field.publicName ~ ": " ~ field.toSwiftType ~ ` = ` ~ field.toSwiftDefault ~ "\n";
    code ~= "    " ~ published ~ " var " ~ field.publicName ~ "Id: " ~ relationIdField.toSwiftType ~ " = " ~ relationIdField.toSwiftDefault ~ " {\n        didSet {\n";

    if(field.listType.length > 0) {
      code ~= "            observable" ~ field.funcName ~ " = []\n";
    }

    code ~= "        }\n    }\n";
  }

  return code;
}

/// It generates the list of strings props
unittest {
  struct TestModel {
    string id;
    string[] field1;
    string[][] field2;
    string[string][string] field3;
  }

  auto description = describe!TestModel;

  auto code = description.toProperties;
  code.should.equal("\n    @Published var id: String = \"\"
    @Published var field1: [String] = []
    @Published var field2: [[String]] = [[]]
    @Published var field3: [String:[String:String]] = [:[:]]\n");
}

///
string toCopyfunction(T)(T model) {
  static if(is(T == ObjectFieldDescription)) {
    string type = model.type;
  } else {
    string type = model.singular;
  }

  string code = "\n    func copy(_ from: " ~ type ~ ") {\n";

  foreach(field; model.fields.basic) {
    if(field.isHidden) continue;

    if(!field.isId) {
      code ~= "        self." ~ field.publicName ~ " = from." ~ field.publicName ~ "\n";
    }
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) continue;

    code ~= "        self." ~ field.publicName ~ " = from." ~ field.publicName ~ "\n";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) continue;

    code ~= "        self." ~ field.publicName ~ "Id = from." ~ field.publicName ~ "Id\n";
  }

  code ~= "    }\n";

  return code;
}

///
string toRequiredFieldLoadingFunctions(T)(T field) {
  string storeName = field.model.plural[0..1].toLower ~ field.model.plural[1..$];

  return `
    func load` ~ field.funcName ~`() async throws {
        let result = try await Store.instance.` ~ storeName ~ `.getById(id: ` ~ field.publicName ~ `Id)

        let _ : Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.` ~ field.publicName ~ ` = result
                continuation.resume(returning: true)
            }
        }
    }
`;
}

///
string toOptionalFieldLoadingFunctions(T)(T field) {
  string storeName = field.model.plural[0..1].toLower ~ field.model.plural[1..$];

  return `
    func load` ~ field.funcName ~`() async throws {
        if let ` ~ field.publicName ~ `Id = ` ~ field.publicName ~ `Id {
          let result = try await Store.instance.` ~ storeName ~ `.getById(id: ` ~ field.publicName ~ `Id)

          let _ : Bool = await withUnsafeContinuation { continuation in
              DispatchQueue.main.async {
                  self.` ~ field.publicName ~ ` = result
                  continuation.resume(returning: true)
              }
          }
        }
    }
`;
}

///
string toLoadingFunctions(T)(T model) {
  string code;

  foreach(field; model.fields.relations) {
    string storeName = field.model.plural[0..1].toLower ~ field.model.plural[1..$];

    if(field.listType.length == 0) {
      code ~= field.isOptional ? field.toOptionalFieldLoadingFunctions : field.toRequiredFieldLoadingFunctions;
    } else {
      auto fieldValue = field;
      fieldValue.listType = [];
      fieldValue.isOptional = false;

      auto forceUnwrap = "";
      auto defaultCount = "";
      auto idPropertyName = `self.` ~ field.name ~ `Id`;
      auto observableDefault = "";
      auto defaultAssign = `self.` ~ field.publicName ~ ` = self.observable` ~ field.funcName ~ `.map { $0.value ?? ` ~ fieldValue.toSwiftDefault ~ ` }`;

      if(field.isOptional) {
        forceUnwrap = "?";
        observableDefault = " ?? []";
        defaultCount = " ?? 0";
      }

      code ~= `
    private var observable` ~ field.funcName ~ ` : [ ObservableModel<` ~ field.type ~ `> ] = []
    @Published var first` ~ field.funcName ~ `: ` ~ field.type ~ `?

    func load` ~ field.funcName ~`() async throws {
        if self.` ~ field.name ~ forceUnwrap ~ `.count` ~ defaultCount ~ ` > 0 {
            return
        }

        var result : [` ~ field.type ~ `] = []
        for id in ` ~ idPropertyName ~ observableDefault ~` {
            let item = try await Store.instance.` ~ storeName ~ `.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.` ~ field.name ~ ` = []
                self.` ~ field.name ~ forceUnwrap ~ `.append(contentsOf: iresult)
                self.first` ~ field.funcName ~ ` = self.` ~ field.name ~ forceUnwrap ~ `.first

                continuation.resume(returning: true)
            }
        }
    }
`;
    }
  }

  if(model.fields.relations.length > 0) {
    code ~= `
    func loadAll() async throws {` ~ "\n";

    code ~= "        let _ = try await (";
    string glue;

    foreach(field; model.fields.relations) {
      code ~= glue ~ "load" ~ field.funcName ~ "()";
      glue = ", ";
    }
    code ~= ")\n    }\n";
  }

  return code;
}

/// It generates the loading function for a simple relataion
unittest {
  struct OtherModel {
    string _id;
  }

  struct TestModel {
    string id;
    OtherModel field1;
  }

  auto description = describe!TestModel;

  auto code = description.toLoadingFunctions;
  code.should.equal("\n    func loadField1() async throws {
        let result = try await Store.instance.otherModels.getById(id: field1Id)

        let _ : Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.field1 = result
                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await (loadField1())
    }\n");
}

/// It generates the loading function for a list of relataions
unittest {
  struct OtherModel {
    string _id;
  }

  struct TestModel {
    string id;
    OtherModel[] field1;
  }

  auto description = describe!TestModel;

  auto code = description.toLoadingFunctions;
  code.should.equal("\n    private var observableField1 : [ ObservableModel<OtherModel> ] = []
    @Published var firstField1: OtherModel?

    func loadField1() async throws {
        if self.field1.count > 0 {
            return
        }

        var result : [OtherModel] = []
        for id in self.field1Id {
            let item = try await Store.instance.otherModels.getById(id: id)
            result.append(item)
        }

        let iresult = result

        let _: Bool = await withUnsafeContinuation { continuation in
            DispatchQueue.main.async {
                self.field1 = []
                self.field1.append(contentsOf: iresult)
                self.firstField1 = self.field1.first

                continuation.resume(returning: true)
            }
        }
    }

    func loadAll() async throws {
        let _ = try await (loadField1())
    }\n");
}


string wrapList(string type, ListType[] list) {

  foreach(item; list) {
    if(item == ListType.list) {
      type = `[` ~ type ~ `]`;
    } else {
      type = `[String:` ~ type ~ `]`;
    }
  }

  return type;
}

string funcName(T)(T field) {
  return field.publicName[0..1].toUpper ~ field.publicName[1..$];
}

string toSwiftDefault(T)(T field) {
  if(field.isOptional) {
    return "nil";
  }

  string initValue;

  if(field.listType.length) {
    foreach(item; field.listType) {
      if(item == ListType.list) {
        initValue = `[` ~ initValue ~ `]`;
      } else {
        initValue = `[:` ~ initValue ~ `]`;
      }
    }

    return initValue;
  }

  auto customTypes = field.attributes.filter!(a => a.indexOf("swift default:") == 0);
  if(!customTypes.empty) {
    static immutable start = "swift default:".length;
    return customTypes.front[start..$].strip;
  }

  if(field.type == "string") {
    return `""`;
  }

  if(field.type == "bool") {
    return "false";
  }

  return field.toSwiftType(false) ~ ".init()";
}

///
string toPlainEquable(T)(T model) {

  static if(is(T == ObjectFieldDescription)) {
    string type = model.type;
  } else {
    string type = model.singular;
  }

  auto code = "\n
    static func == (lhs: " ~ type ~ ", rhs: " ~ type ~ ") -> Bool {\n";

  foreach(field; model.fields.basic) {
    code ~= "        if(lhs." ~ field.publicName ~ " != rhs." ~ field.publicName ~ ") { return false }\n";
  }

  foreach(field; model.fields.objects) {
    code ~= "        if(lhs." ~ field.publicName ~ " != rhs." ~ field.publicName ~ ") { return false }\n";
  }

  foreach(field; model.fields.relations) {
    code ~= "        if(lhs." ~ field.publicName ~ " != rhs." ~ field.publicName ~ ") { return false }\n";
  }

  code ~= "\n        return true\n";
  code ~= "    }\n\n";

  return code;
}

/// It generates the plain equable function
unittest {
  struct OtherModel {
    string _id;
  }

  struct TestModel {
    string a;
    OtherModel[] field1;
  }

  auto description = describe!TestModel;

  auto code = description.toPlainEquable;
  code.should.equal("\n
    static func == (lhs: TestModel, rhs: TestModel) -> Bool {
        if(lhs.a != rhs.a) { return false }
        if(lhs.field1 != rhs.field1) { return false }

        return true
    }
\n");
}


///
string toPlainHash(T)(T model) {
  auto code = "\n
    func hash(into hasher: inout Hasher) {\n";

  foreach(field; model.fields.basic) {
    code ~= "        hasher.combine(self." ~ field.publicName ~ ")\n";
  }

  foreach(field; model.fields.objects) {
    code ~= "        hasher.combine(self." ~ field.publicName ~ ")\n";
  }

  foreach(field; model.fields.relations) {
    code ~= "        hasher.combine(self." ~ field.publicName ~ ")\n";
  }

  code ~= "    }\n\n";

  return code;
}

/// It generates the plain hash function
unittest {
  struct OtherModel {
    string _id;
  }

  struct TestModel {
    string a;
    OtherModel[] field1;
  }

  auto description = describe!TestModel;

  auto code = description.toPlainHash;
  code.should.equal("\n
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.a)
        hasher.combine(self.field1)
    }\n\n");
}
