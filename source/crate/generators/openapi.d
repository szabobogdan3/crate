/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.generators.openapi;

import crate.base;
import crate.ctfe;
import crate.http.router;

import std.conv;
import std.array;
import std.algorithm;
import std.traits;
import std.string;
import openapi.definitions;

import vibe.http.common;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

///
Schema toOpenApiSchema(ModelFields fields) @trusted pure {
  auto schema = new Schema;

  schema.type = SchemaType.object;

  foreach(field; fields.basic) {
    if(!field.isOptional && !field.isId) {
      schema.required ~= field.publicName;
    }

    schema.properties[field.publicName] = toOpenApiSchema(field, field.listType);
  }

  foreach(field; fields.objects) {
    if(!field.isOptional) {
      schema.required ~= field.publicName;
    }

    if(field.type == "string" || field.type == "SysTime") {
      schema.properties[field.publicName] = toOpenApiSchema(field, field.listType);
    } else {
      schema.properties[field.publicName] = toOpenApiSchema(field.fields, field.listType);
    }
  }

  foreach(field; fields.relations) {
    if(!field.isOptional) {
      schema.required ~= field.publicName;
    }

    auto description = FieldDescription();
    description.listType = field.listType;
    description.type = SchemaType.string;

    schema.properties[field.publicName] = toOpenApiSchema(description, field.listType);
  }

  return schema;
}

///
Schema toOpenApiSchema(T)(T field, ListType[] listType = []) @trusted pure
  if(is(T == FieldDescription) || is(T == ObjectFieldDescription) || is(T == ModelFields)) {

  if(listType.length > 0) {
    auto schema = new Schema;

    if(listType[0] == ListType.list) {
      schema.type = SchemaType.array;
    }

    if(listType[0] == ListType.hashmap) {
      schema.type = SchemaType.object;
    }

    schema.items = toOpenApiSchema(field, listType[1..$]);
    return schema;
  }

  return field.toOpenApiSchema;
}

///
Schema toOpenApiSchema(T)(T field) @trusted pure
  if(is(T == FieldDescription) || is(T == ObjectFieldDescription)) {
  auto schema = new Schema;

  switch(field.type) {
    case "string":
      schema.type = SchemaType.string;
      break;
    case "SysTime":
      schema.type = SchemaType.string;
      schema.format = SchemaFormat.dateTime;
      break;
    case "bool":
      schema.type = SchemaType.boolean;
      break;
    case "double":
      schema.type = SchemaType.number;
      //schema.format = SchemaFormat.double_;
      break;
    case "float":
      schema.type = SchemaType.number;
      schema.format = SchemaFormat.float_;
      break;
    case "long":
    case "ulong":
      schema.type = SchemaType.integer;
      schema.format = SchemaFormat.int64;
      break;
    case "int":
    case "uint":
      schema.type = SchemaType.integer;
      schema.format = SchemaFormat.int32;
      break;
    case "Json":
      schema.type = SchemaType.object;
      break;
    default:
      assert(false, "unsupported field type `" ~ field.type ~ "`");
  }

  return schema;
}

openapi.definitions.Response toOpenApiSchema(CrateResponse crateResponse) @trusted pure {
  openapi.definitions.Response response;
  response.description = crateResponse.description;

  if(crateResponse.mime && crateResponse.schema) {
    response.content[crateResponse.mime] = MediaType(crateResponse.schema);
  }

  return response;
}

Schema joinSchemas(Schema source, Schema other) @safe pure {
  if(source._ref != "" && other._ref == source._ref) {
    return source;
  }

  foreach(schema; source.oneOf) {
    if(schema._ref == other._ref) {
      return source;
    }
  }

  if(source._ref == "" && source.oneOf.length > 0) {
    source.oneOf ~= other;
    return source;
  }

  auto newSchema = new Schema;
  newSchema.oneOf = [ source, other ];

  return newSchema;
}

openapi.definitions.Response toOpenApiSchema(CrateResponse[] crateResponses) @trusted pure {
  if(crateResponses.length == 1) {
    return crateResponses[0].toOpenApiSchema;
  }

  openapi.definitions.Response response;

  auto schemas = crateResponses.map!(a => a.toOpenApiSchema).array;

  string glue = "";
  foreach(crateResponse; crateResponses) {
    response.description ~= glue ~ crateResponse.description;
    glue = "\n\n";

    if(crateResponse.mime && crateResponse.schema) {
      if(crateResponse.mime !in response.content) {
        response.content[crateResponse.mime] = MediaType(crateResponse.schema);
      } else {
        response.content[crateResponse.mime].schema = joinSchemas(response.content[crateResponse.mime].schema, crateResponse.schema);
      }
    }
  }

  return response;
}

///
Operation toOpenApiSchema(CrateRule rule) @trusted pure {
  Operation operation;
  const pathPieces = rule.request.path.split("/");
  string operationName;

  operationName = rule.request.method.to!string.toLower();

  if(pathPieces.length >= 4) {
    operationName ~= pathPieces[3..$].map!(a => a[0..1].capitalize ~ a[1..$]).join().to!string ~ "For";
  }

  operationName ~= pathPieces[1].capitalize;

  if(pathPieces.canFind(":id")) {
    operationName ~= "ItemById";
  }

  operation.parameters = rule.request.parameters;
  operation.operationId = operationName;

  string responseCode = rule.response.statusCode.to!string;
  operation.responses[responseCode] = rule.response.toOpenApiSchema;

  auto errorCodes = rule.errorResponses.map!(a => a.statusCode).uniq;

  foreach(statusCode; errorCodes) {
    responseCode = statusCode.to!string;
    operation.responses[responseCode] = rule.errorResponses.filter!(a => a.statusCode == statusCode).array.toOpenApiSchema;
  }

  if(rule.request.mime && rule.request.schema) {
    operation.requestBody.content[rule.request.mime] = MediaType(rule.request.schema);
  }

  return operation;
}

/// convert a rest api get
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;

  struct TestModel {
    string id;
    string field;
  }

  auto fields = describe!TestModel;

  auto rule = RestApi.getItem(fields).toOpenApiSchema;
  rule.serializeToJson.should.equal((`{
    "operationId": "getTestmodelsItemById",
    "parameters": [
      {
        "name": "id",
        "required": true,
        "in": "path",
        "description": "The id of the object that you want to retreive",
        "schema": { "type": "string" }
      }
    ],
    "responses": {
      "200": {
        "description": "Returns the requested ` ~ "`TestModel`" ~ ` using the Rest API format.",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "#/components/schemas/oneRestTestModel"
            }}}}}}`).parseJsonString);
}

OperationsType toOpenApi(HTTPMethod method) {

  switch(method) {
    case HTTPMethod.GET:
      return OperationsType.get;

    case HTTPMethod.PUT:
      return OperationsType.put;

    case HTTPMethod.POST:
      return OperationsType.post;

    case HTTPMethod.DELETE:
      return OperationsType.delete_;

    case HTTPMethod.OPTIONS:
      return OperationsType.options;

    case HTTPMethod.HEAD:
      return OperationsType.head;

    case HTTPMethod.PATCH:
      return OperationsType.patch;

    case HTTPMethod.TRACE:
      return OperationsType.trace;

    default:
  }

  throw new Exception(method.to!string ~ " is not supported.");
}

auto add(Policy)(ref OpenApi api, ModelDescription definition) {
  enum methods = ["replace", "create", "delete_", "getItem", "patch", "getList"];

  static foreach (method; methods) {{
    mixin(`auto rule = Policy.` ~ method ~ `(definition);`);

    OperationsType operation = rule.request.method.toOpenApi;
    auto path = rule.request.path.vibeToOpenApiPath;

    if(rule.request.path !in api.paths) {
      api.paths[path] = Path();
    }

    api.paths[path].operations[operation] = rule.toOpenApiSchema;
  }}

  foreach(name, schema; Policy.schemas(definition)) {
    api.components.schemas[name] = schema;
  }

  return api;
}

auto add(ref OpenApi api, CrateRule rule) {
  OperationsType operation = rule.request.method.toOpenApi;

  auto path = rule.request.path.vibeToOpenApiPath;
  if(path !in api.paths) {
    api.paths[path] = Path();
  }

  api.paths[path].operations[operation] = rule.toOpenApiSchema;

  foreach(name, schema; rule.schemas) {
    api.components.schemas[name] = schema;
  }

  return api;
}

OpenApi toOpenApi(T)(CrateRouter!T router) {
  OpenApi api;

  foreach(rule; router.rules) {
    api.add(rule);
  }

  return api;
}

/// Convert from vibe path to open api
string vibeToOpenApiPath(string path) {

  string transform(string item)
  {
    if(item.length == 0 || item[0] != ':') {
      return item;
    }

    return "{" ~ item[1..$] ~ "}";
  }

  return path.splitter("/").map!(a => transform(a)).joiner("/").array.to!string;
}

unittest {
  "".vibeToOpenApiPath.should.equal("");
  "a/b/c".vibeToOpenApiPath.should.equal("a/b/c");
  "a/:b/c".vibeToOpenApiPath.should.equal("a/{b}/c");
}

Parameter[] describeQueryParameters(T)() if(is(T == struct)) {
  Parameter[] parameters;

  static foreach(member; __traits(allMembers, T)) {{
    enum fieldDescription = describeField!(T, member);

    Parameter parameter;
    parameter.name = fieldDescription.publicName;
    parameter.in_ = ParameterIn.query;
    parameter.schema = toOpenApiSchema(fieldDescription, fieldDescription.listType);

    static if(fieldDescription.description != "") {
      parameter.description = fieldDescription.description;
    }

    static if(hasUDA!(__traits(getMember, T, member), Example)) {
      static foreach(attr; getUDAs!(__traits(getMember, T, member), Example)) {
        parameter.examples[attr.value] = attr;
      }
    }

    parameters ~= parameter;
  }}

  return parameters;
}

/// convert parameters to open api
unittest {
  struct Query {
    string someString;
    bool someBool;
    int someNumber;
    float someFloat;
    string[] someList;
  }

  describeQueryParameters!Query.serializeToJson.should.equal(`[
    {"in":"query","schema":{"type":"string"},"name":"someString"},
    {"in":"query","schema":{"type":"boolean"},"name":"someBool"},
    {"in":"query","schema":{"type":"integer","format":"int32"},"name":"someNumber"},
    {"in":"query","schema":{"type":"number","format":"float"},"name":"someFloat"},
    {"in":"query","schema":{"type":"array","items":{"type":"string"}},"name":"someList"}]`.parseJsonString);
}

/// Add descriptions to the parameters
unittest {
  struct Query {
    @describe("This is some random string.")
    string someString;
  }

  describeQueryParameters!Query.serializeToJson.should.equal(`[
    {"description":"This is some random string.","in":"query","schema":{"type":"string"},"name":"someString"}
  ]`.parseJsonString);
}

Schema[string] getErrorListSchemas() {
  struct ErrorMessage {
    string error;
  }

  struct Error {
    string title;
    string description;
    int status;
  }

  struct ErrorList {
    Error[] errors;
  }

  Schema[string] result;
  result["ErrorMessage"] = describe!ErrorMessage.fields.toOpenApiSchema;
  result["Error"] = describe!Error.fields.toOpenApiSchema;
  result["ErrorList"] = new Schema;
  result["ErrorList"].type = SchemaType.array;
  result["ErrorList"].items = new Schema;
  result["ErrorList"].items._ref = "#/components/schemas/Error";

  return result;
}
