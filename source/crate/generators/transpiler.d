/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.generators.transpiler;

import std.algorithm;
import std.string;
import std.exception;
import std.conv;
import crate.ctfe;

string getAttribute(string attribute, T)(T field) {
  static immutable key = attribute ~ ":";
  static immutable start = key.length;

  auto customTypes = field.attributes.filter!(a => a.indexOf(key) == 0);

  string type;
  if(!customTypes.empty) {
    type = customTypes.front[start..$].strip;
  }

  return type;
}

bool isHidden(T)(T field) {
  return field.attributes.canFind("hidden()");
}

string upperCaseFirstLetter(string name) {
  enforce(name.length > 1, "The name is invalid.");

  return name[0..1].toUpper ~ name[1..$];
}

string lowerCaseSingular(ref ModelDescription model) {
  enforce(model.singular.length > 1, "The model " ~ model.to!string ~ " has an invalid singular.");

  return model.singular[0..1].toLower ~ model.singular[1..$];
}

string lowerCasePlural(ref ModelDescription model) {
  enforce(model.plural.length > 1, "The model " ~ model.to!string ~ " has an invalid plural.");

  return model.plural[0..1].toLower ~ model.plural[1..$];
}