/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.generators.typescriptStore;

import crate.base;
import crate.ctfe;
import crate.http.router;
import crate.generators.transpiler;
import crate.attributes;

import std.conv;
import std.array;
import std.algorithm;
import std.traits;
import std.datetime;
import std.string;
import openapi.definitions;

import vibe.http.common;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

string generateStore(CrateRule[] rules) {
  string[string] functions;
  string[string] imports;

  foreach(rule; rules) {
    foreach(key, value; rule.toStoreFunctions) {
      if(key == "") continue;
      functions[key] = value;
    }

    foreach(key, value; rule.toStoreImports) {
      if(key == "") continue;
      imports[key] = value;
    }
  }

  string code;

  foreach(key, value; imports) {
    code ~= value;
  }

  code ~= "\n" ~`export type GenericFetch = <T>(method: string, url: string, headers: Record<string, string>, body?: Record<string, any>) => Promise<T | ErrorResponse | ErrorListResponse>;

function toQueryParams(params: Record<string, string | number | boolean>) {
  const searchParams = new URLSearchParams();

  Object.keys(params).forEach(key => searchParams.append(key, params[key]));

  return searchParams.toString();
}

export interface Cache {
  upsert(id: string | number, type: string, value: any): Promise<void>;
  get(id: string | number, type: string): Promise<null | string>;
  getPending(type: string): Promise<string[]>;
  delete(id: string | number, type: string): Promise<void>;
  clear(): Promise<void>;
}

export interface Authenticator {
  authenticate(headers: Record<string, string>): Promise<Record<string, string>>;
}

export interface ErrorResponse {
  error: string;
}

export interface DetailedError {
  description: string;
  status: number;
  title: string;
}

export interface ErrorListResponse {
  errors: DetailedError[];
}

function handleErrorResponse(response: ErrorResponse | ErrorListResponse) {
  if("error" in response) {
    throw new Error(response.error);
  }

  if(response.errors?.length) {
    throw new Error(response.errors[0]?.title + ": " + response.errors[0]?.description);
  }
}

const STRING_CLASSIFY_REGEXP_1 = (/^(\-|_)+(.)?/);
const STRING_CLASSIFY_REGEXP_2 = (/(.)(\-|\_|\.|\s)+(.)?/g);
const STRING_CLASSIFY_REGEXP_3 = (/(^|\/|\.)([a-z])/g);

const clasifyCache: Record<string, string> = {};

function classify(str: string) {
  if (!clasifyCache[str]) {
    let replace1 = function (_: string, _separator: string, chr: string) {
      return chr ? ('_' + chr.toUpperCase()) : '';
    };

    let replace2 = function (_: string, initialChar: string, _separator: string, chr: string) {
      return initialChar + (chr ? chr.toUpperCase() : '');
    };

    let parts = str.split('/');
    for (let i = 0; i < parts.length; i++) {
      parts[i] = parts[i]!
        .replace(STRING_CLASSIFY_REGEXP_1, replace1)
        .replace(STRING_CLASSIFY_REGEXP_2, replace2);
    }
    clasifyCache[str] = parts.join('/')
      .replace(STRING_CLASSIFY_REGEXP_3, function (match, _separator, _chr) {
        return match.toUpperCase();
      });
  }

  return clasifyCache[str];
};

export type QueryListFunc = <T>(query: Record<string, string | number | boolean>) => Promise<T[]>;

export class LazyList<T> {
  buckets: T[][] = [];
  isLoading: boolean = false;
  lastError: string | null = null;

  itemsPerPage: number = 24;

  constructor(private readonly query: Record<string, string | number | boolean>, private readonly queryList: QueryListFunc) { }

  get loadedAny() {
    return this.skip;
  }

  get isEmpty() {
    return this.skip;
  }

  get lastBucket(): T[] {
    const len = this.buckets?.length;

    if (!len) {
      return [];
    }

    if(!this.buckets[len - 1]) {
      throw new Error("There is no bucket");
    }

    return this.buckets[len - 1]!;
  }

  get canLoadMore() {
    if (this.lastError) {
      return false;
    }

    if (this.query?.limit) {
      return this.buckets.length == 0;
    }

    if (this.buckets?.length == 0) {
      return true;
    }

    return this.lastBucket.length == this.itemsPerPage;
  }

  get items() {
    return this.buckets.flatMap(a => a);
  }

  get skip() {
    return this.buckets.map((a) => a.length).reduce((a, b) => a + b, 0);
  }

  buildParams(): Record<string, string> {
    let limit = this.itemsPerPage;
    let skip = this.skip;

    if (this.query.limit) {
      limit = parseInt(String(this.query.limit));
    }

    return {
      ...this.query,
      limit: ` ~ "`${limit}`" ~ `,
      skip: ` ~ "`${skip}`" ~ `,
    };
  }

  async loadNext() {
    if (!this.canLoadMore) {
      return;
    }

    this.isLoading = true;

    try {
      let results = (await this.queryList<T>(this.buildParams())) ?? [];

      this.buckets.push(results);
    } catch (err) {
      console.error(err);
      this.lastError = "" + err;
    }

    this.isLoading = false;
  }
}

export type AnyGISCollectiveModel = ` ~ rules.toAnyGISCollectiveModel ~ `;

export type GISCollectiveModel = Record<string, AnyGISCollectiveModel>;

export class Store {
  private static _instance: Store | undefined;
  private localCache: Record<string, any> = {};

  static set instance(value: Store) {
    this._instance = value;
  }

  static get instance() : Store {
    if(!this._instance) {
      throw new Error("The store instance is not set up.");
    }

    return this._instance;
  }

  /// the store interface
  async query<T>(model: string, query: Record<string, string | number | boolean>): Promise<T[]> {
    const key = "query" + classify(model) + "s" as keyof Store;

    if (typeof this[key] !== "function") {
      throw new Error(model + " is an unknown model.")
    }

    //@ts-ignore
    return this[key](query) as unknown as T[];
  }

  peekRecord<T>(model: string, id: string): T | null {
    return this.localCache[classify(model) + "-" + id] ?? null;
  }

  findRecord<T>(model: string, id: string): Promise<T> {
    const key = "find" + classify(model) as keyof Store;

    if (typeof this[key] !== "function") {
      throw new Error(model + " is an unknown model.")
    }

    //@ts-ignore
    return this[key](id) as unknown as T;
  }

  async queryRecord<T>(model: string, query: Record<string, string | number | boolean>): Promise<T> {
    const result = await this.query(model, { ...query, limit: 1 });

    if(!result?.[0]) {
      throw new Error(model + " with query does not exist!");
    }

    //@ts-ignore
    return result?.[0] as unknown as T;
  }
  ///

  constructor(
    private readonly baseUrl: string,
    private readonly fetch: GenericFetch,
    public readonly cache: Cache,
    public readonly authenticator: Authenticator) { }`;

  foreach(key, value; functions) {
    code ~= value;
  }

  code ~= "\n}";

  return code;
}

string toAnyGISCollectiveModel(CrateRule[] rules) {
  string[string] code;

  foreach(rule; rules) {
    foreach(key, value; rule.toAnyGISCollectiveModel) {
      if(key == "") continue;
      code[key] = value;
    }
  }

  return code.byValue.join(" |\n  ");
}

string[string] toAnyGISCollectiveModel(CrateRule rule) {
  string[string] models;

  auto model = rule.modelDescription;

  if(!model.singular) {
    return models;
  }

  models[model.singular] = model.singular ~ `Model`;
  models[model.singular ~ "List"] = model.singular ~ `Model[]`;
  models[model.singular ~ "Pages"] = `LazyList<` ~ model.singular ~ `Model>`;

  return models;
}

string[string] toStoreImports(CrateRule rule) {
  string[string] models;

  auto model = rule.modelDescription;

  if(!model.singular) {
    return models;
  }

  models[model.singular] = `import { ` ~ model.singular ~ `Model, ` ~ model.singular ~ `Json, ` ~ model.singular ~ `Response, ` ~ model.plural ~ `Response } from "./` ~ model.singular ~ `";` ~ "\n";

  return models;
}

string[string] toStoreFunctions(CrateRule rule) {
  string[string] models;

  auto model = rule.modelDescription;
  auto url = `this.baseUrl + "/` ~ model.plural.toLower ~ `"`;

  if(!model.singular) {
    return models;
  }

  models[model.singular] = `
  /// ` ~ model.singular ~ `
  async find` ~ model.singular ~ `(id: string): Promise<` ~ model.singular ~ `Model> {
    let cachedRecord: string | ` ~ model.singular ~ `Json = await this.cache.get(id, "` ~ model.singular ~ `") as unknown as string;

    if(typeof cachedRecord == "string") {
      cachedRecord = JSON.parse(cachedRecord)  as unknown as ` ~ model.singular ~ `Json;
    }

    const headers = {};

    if(!cachedRecord && id.indexOf("pending") != 0) {
      const result = await this.fetch<` ~ model.singular ~ `Response>("GET", ` ~ url ~ ` + "/" + id, headers);

      if("error" in result || "errors" in result) {
        handleErrorResponse(result);
      }

      if("` ~ model.lowerCaseSingular ~ `" in result) {
        await this.cache.upsert(id, "` ~ model.singular ~ `", result.` ~ model.lowerCaseSingular ~ `);
        cachedRecord = result.` ~ model.lowerCaseSingular ~ `;
      }
    }

    const record = new ` ~ model.singular ~ `Model(cachedRecord);
    this.localCache["` ~ model.singular ~ `-" + id] = record;

    return record;
  }

  async query` ~ model.plural ~ `(query: Record<string, string | number | boolean>): Promise<` ~ model.singular ~ `Model[]> {
    const headers = {};

    if(typeof query.id === "string" && query.id.indexOf(",") == -1) {
      const record = await this.find` ~ model.singular ~ `(query.id);

      return [record];
    }

    const result = await this.fetch<` ~ model.plural ~ `Response>("GET", ` ~ url ~ ` + "?" + toQueryParams(query), headers);

    if("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if("` ~ model.lowerCasePlural ~ `" in result) {
      const items = result.` ~ model.lowerCasePlural ~ `;
      await Promise.all(items.map((a: ` ~ model.singular ~ `Json) => this.cache.upsert(a._id, "` ~ model.singular ~ `", a)));

      const results = items.map((a: ` ~ model.singular ~ `Json) => new ` ~ model.singular ~ `Model(a)) ?? [];

      for(let record of results) {
        this.localCache["` ~ model.singular ~ `-" + record._id] = record;
      }

      return results;
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async delete` ~ model.singular ~ `(id: string): Promise<void> {
    const headers = {};

    await this.cache.delete(id, "` ~ model.singular ~ `");

    const result = await this.fetch<{}>("DELETE", ` ~ url ~ ` + "/" + id, headers);

    delete this.localCache["` ~ model.singular ~ `-" + id];

    if("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }
  }

  async create` ~ model.singular ~ `(input: ` ~ model.singular ~ `Model): Promise<` ~ model.singular ~ `Model> {
    const headers = {};
    const body = {
      "` ~ model.lowerCaseSingular ~ `": input.toJSON()
    };

    const initialId = input._id;
    const result = await this.fetch<` ~ model.singular ~ `Response>("POST", ` ~ url ~ `, headers, body);

    if("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if("` ~ model.lowerCaseSingular ~ `" in result && String(initialId).indexOf("pending") == 0) {
      await this.cache.delete(initialId, "` ~ model.singular ~ `");
    }

    if("` ~ model.lowerCaseSingular ~ `" in result) {
      await this.cache.upsert(result.` ~ model.lowerCaseSingular ~ `._id, "` ~ model.singular ~ `", result.` ~ model.lowerCaseSingular ~ `);

      const record = new ` ~ model.singular ~ `Model(result.` ~ model.lowerCaseSingular ~ `);
      this.localCache["` ~ model.singular ~ `-" + record._id] = record;

      return record
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }

  async save` ~ model.singular ~ `(input: ` ~ model.singular ~ `Model): Promise<` ~ model.singular ~ `Model> {
    const headers = {};
    const body = {
      "` ~ model.lowerCaseSingular ~ `": input.toJSON()
    };

    const result = await this.fetch<` ~ model.singular ~ `Response>("PUT", ` ~ url ~ ` + "/" + input.` ~ model.idField.publicName ~ `, headers, body);

    if("error" in result || "errors" in result) {
      handleErrorResponse(result);
    }

    if("` ~ model.lowerCaseSingular ~ `" in result) {
      await this.cache.upsert(result.` ~ model.lowerCaseSingular ~ `._id, "` ~ model.singular ~ `", result.` ~ model.lowerCaseSingular ~ `);

      const record = new ` ~ model.singular ~ `Model(result.` ~ model.lowerCaseSingular ~ `);
      this.localCache["` ~ model.singular ~ `-" + record._id] = record;

      return record;
    }

    return Promise.reject(new Error("Got an invaid server response!"));
  }
  ` ~ "\n";

  return models;
}
