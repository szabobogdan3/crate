/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.generators.typescript;

import crate.base;
import crate.ctfe;
import crate.http.router;
import crate.generators.transpiler;
import crate.attributes;

import std.conv;
import std.array;
import std.algorithm;
import std.traits;
import std.datetime;
import std.string;
import openapi.definitions;

import vibe.http.common;

import crate.generators.typescriptStore;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}


enum GeoJsonGeometryTs = `
export type GeoJsonGeometry = Point | MultiPoint | LineString | MultiLineString | Polygon | MultiPolygon;

export type Position = number[]; // [lon, lat] | [lon, lat, alt];

export type GeoJsonTypes = GeoJsonGeometry["type"];

export interface GeoJsonObject {
  type: GeoJsonTypes;
}

export interface Point extends GeoJsonObject {
  type: "Point";
  coordinates: Position;
}

export interface MultiPoint extends GeoJsonObject {
  type: "MultiPoint";
  coordinates: Position[];
}

export interface LineString extends GeoJsonObject {
  type: "LineString";
  coordinates: Position[];
}

export interface MultiLineString extends GeoJsonObject {
  type: "MultiLineString";
  coordinates: Position[][];
}

export interface Polygon extends GeoJsonObject {
  type: "Polygon";
  coordinates: Position[][];
}

export interface MultiPolygon extends GeoJsonObject {
  type: "MultiPolygon";
  coordinates: Position[][][];
}
`;

enum ArticleBodyTs = `export type ArticleBody = ArticleBodyJson | string;

export interface ArticleBodyJson {
  blocks: ArticleBlock[]
}

export interface ArticleBlock {
  type: string,
  data?: Record<string, any>;
}`;

///
string[string] toTSModels(T)(CrateRouter!T router) {
  string[string] models;

  foreach(rule; router.rules) {
    foreach(key, value; rule.toTSModels) {
      if(key == "") continue;
      models[key] = value;
    }
  }

  models["ArticleBody"] = ArticleBodyTs;
  models["GeoJsonGeometry"] = GeoJsonGeometryTs;
  models["store"] = generateStore(router.rules);

  models.remove("Polygon");

  return models;
}

///
string[string] toTSModels(CrateRule rule) {
  string[string] models;

  foreach(name, code; rule.modelDescription.toTSInterface) {
    models[name] = code;
  }

  return models;
}

string[string] toTSInterface(ObjectFieldDescription model) {
  string[string] models;

  string type;

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    foreach(type, localCode; field.toTSInterface) {
      models[type] = localCode;
    }
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    foreach(type, localCode; field.model.toTSInterface) {
      models[type] = localCode;
    }
  }

  type = model.type;

  string code = model.toTSImports ~ `export interface ` ~ type ~ ` {` ~ "\n";
  code ~= model.toProperties;
  code ~= "}\n\n";

  code ~= `export interface ` ~ type ~ `Json {` ~ "\n";
  code ~= model.toJsonProperties;
  code ~= "}\n\n";

  code ~= `export class ` ~ type ~ `Model {` ~ "\n";
  code ~= `  private json: ` ~ type ~ `Json;` ~ "\n";
  code ~= `  private _isDirty: boolean = false;` ~ "\n\n";
  code ~= `  constructor(json: ` ~ type ~ `Json | null | undefined) { this.json = json ?? ` ~ model.toDefaultValue ~ `; }` ~ "\n\n";
  code ~= model.toModelProperties;
  code ~= model.toIsDirty;
  code ~= `  get modelName() { return "` ~ type.dasherize ~ `"; }` ~ "\n";
  code ~= `  toJSON() : ` ~ type ~ `Json { return this.json; }` ~ "\n";
  code ~= "}\n\n";

  models[type] = code;

  return models;
}

string[string] toTSInterface(ModelDescription model) {
  string[string] models;

  string type;

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    foreach(type, localCode; field.toTSInterface) {
      if(!["string", "SysTime", "PositionT!double"].canFind(type)) {
        models[type] = localCode;
      }
    }
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    foreach(type, localCode; field.model.toTSInterface) {
      models[type] = localCode;
    }
  }

  type = model.singular;

  if(!model.singular) {
    return models;
  }

  string code = model.toTSImports ~ `export interface ` ~ type ~ ` {` ~ "\n";
  code ~= model.toProperties;
  code ~= "}\n\n";

  code ~= `export interface ` ~ model.singular ~ `Response {` ~ "\n";
  code ~= "  " ~ model.lowerCaseSingular ~ ": " ~ type ~ "Json;\n";
  code ~= "}\n\n";

  code ~= `export interface ` ~ model.plural ~ `Response {` ~ "\n";
  code ~= "  " ~ model.lowerCasePlural ~ ": " ~ type ~ "Json[];\n";
  code ~= "}\n\n";

  code ~= `export interface ` ~ type ~ `Json {` ~ "\n";
  code ~= model.toJsonProperties;
  code ~= "}\n\n";

  auto getterTypes = model.toGetterTypes;

  code ~= `export class ` ~ type ~ `Model {` ~ "\n";
  code ~= `  private json: ` ~ type ~ `Json;` ~ "\n";
  code ~= `  private _isDirty: boolean = false;` ~ "\n\n";
  code ~= `  constructor(json: ` ~ type ~ `Json | null | undefined) { this.json = json ?? ` ~ model.toDefaultValue ~ `; }` ~ "\n\n";
  code ~= `  get(key: string): ` ~ getterTypes ~ ` {` ~ "\n";
  code ~= `    if(key in this) {` ~ "\n";
  code ~= `      //@ts-ignore` ~ "\n";
  code ~= `      return this[key] as ` ~ getterTypes ~ `;` ~ "\n";
  code ~= `    }` ~ "\n";
  code ~= `    return null; }` ~ "\n\n";
  code ~= model.toTeamIdGetter;
  code ~= model.toModelProperties;
  code ~= model.toIsDirty;

  if(model.singular) {
    code ~= `  async save() : Promise<` ~ type ~ `Model> {` ~ "\n";
    code ~= `    let result:` ~ type ~ `Model;` ~ "\n\n";
    code ~= `    if(!this.` ~ model.idField.publicName ~ ` || this.` ~ model.idField.publicName ~ `?.indexOf("pending") == 0) {` ~ "\n";
    code ~= `      result = await Store.instance.create` ~ type ~ `(this);` ~ "\n";
    code ~= `    } else {` ~ "\n";
    code ~= `      result = await Store.instance.save` ~ type ~ `(this);` ~ "\n";
    code ~= `    }` ~ "\n\n";
    code ~= `    this.json = result.toJSON();` ~ "\n\n";
    code ~= `    return this;` ~ "\n";
    code ~= "  }\n";
  }

  code ~= `  get modelName() { return "` ~ type.dasherize ~ `"; }` ~ "\n";
  code ~= `  toJSON() : ` ~ type ~ `Json { return this.json; }` ~ "\n";
  code ~= "}\n\n";

  models[type] = code;

  return models;
}

/// It converts a schema with a relation to a ts interface
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Other {
    string id;
    string value;
  }

  struct TestModel {
    string id;
    Other field;
  }

  auto code = describe!TestModel.toTSInterface;

  code["TestModel"].should.contain("export interface TestModel {");
  code["TestModel"].should.contain(`id: string;`);
  code["TestModel"].should.contain(`field: Other;`);
  code["TestModel"].should.contain(`}`);

  code["Other"].should.contain("export interface Other {");
  code["Other"].should.contain(`id: string;`);
  code["Other"].should.contain(`value: string;`);
  code["Other"].should.contain(`}`);
}

/// It converts a schema with a relation to a ts interface and array fields
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct BaseMap {
    string id;
  }

  struct BaseMaps {
    ///
    bool useCustomList;

    ///
    BaseMap[] list;
  }

  struct TestModel {
    string id;
    BaseMaps[] fields;
  }

  auto code = describe!TestModel.toTSInterface;

  code["TestModel"].should.contain("export interface TestModelJson {
  id: string;
  fields: BaseMapsJson[];
}");

  code["BaseMaps"].should.contain(`get list(): Promise<BaseMapModel[]> {
    const promises = this.json.list?.map(a => Store.instance.findBaseMap(a)) ?? [];

    return Promise.all(promises);
  }`);
}

string toIsDirty(T)(T model) {
  string[] code = [`  get isDirty() : boolean {`];

  code ~= `    if(this._isDirty) {`;
  code ~= `      return true;`;
  code ~= `    }` ~ "\n";


  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    if(field.type == "GeoJsonGeometry" || field.type == "SysTime" || field.type == "string") {
      continue;
    }

    if(field.listType.length == 0) {
      code ~= `    if(this.` ~ field.publicName ~ `?.isDirty) {`;
      code ~= `      return true;`;
      code ~= `    }` ~ "\n";
    }

    if(field.listType.length > 0) {
      string spaces;
      size_t level = 0;
      string[] endLoops;
      string prevLevel = `this.` ~ field.publicName;

      foreach(item; field.listType) {
        code ~= spaces ~ `    for(let level` ~ level.to!string ~ ` of ` ~ prevLevel ~ `) {`;
        endLoops ~= spaces ~ `    }`;
        prevLevel = `level` ~ level.to!string;

        spaces ~= "  ";
        level++;
      }

      code ~= spaces ~ `    if(level` ~ (level-1).to!string ~ `?.isDirty) {`;
      code ~= spaces ~ `      return true;`;
      code ~= spaces ~ `    }`;
      code ~= endLoops ~ "\n";
    }
  }

  code ~= `    return false;`;
  code ~= `  }` ~ "\n\n";

  return code.join("\n");
}

string toTSImports(T)(T model) if(is(T == ObjectFieldDescription) || is(T == ModelDescription)) {
  string[string] code;

  code["store"] = `import { Store } from "./store";`;

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    auto type = field.toTSType;

    code[field.model.singular] = `import { ` ~ field.model.singular ~ `, ` ~ field.model.singular ~ `Model } from "./` ~ field.model.singular ~ `";`;
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    auto type = field.toTSType;

    if(type == "") {
      type = field.type;
    }

    if(type == "string") {
      continue;
    }

    if(type == "DateTime") {
      code[type] = `import { DateTime } from "luxon";`;
      continue;
    }

    if(type == "ArticleBody" || type == "GeoJsonGeometry") {
      code[type] = `import { ` ~ type ~ ` } from "./` ~ type ~ `";`;
      continue;
    }

    code[type] = `import { ` ~ type ~ `, ` ~ type ~ `Json, ` ~ type ~ `Model } from "./` ~ type ~ `";`;
  }

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    auto customType = getAttribute!("ts type")(field);

    if(customType != "") {
      code[customType] = `import { ` ~ customType ~ ` } from "./` ~ customType ~ `";`;
    }
  }

  return code.byValue.joiner("\n").array.to!string ~ "\n\n";
}

/// It generates the imports for a model
unittest {
  import vibe.data.json;
  import crate.api.rest.policy;
  import crate.api.rest.serializer;

  struct Other {
    string id;
    string value;
  }

  struct TestModel {
    string id;
    Other field;
    @("ts type: ArticleBody")
    Json content;
  }

  auto code = describe!TestModel.toTSImports;

  code.should.equal(`import { Other, OtherModel } from "./Other";` ~ "\n"
    ~ `import { ArticleBody } from "./ArticleBody";` ~ "\n"
    ~ `import { Store } from "./store";`
    ~ "\n\n");
}


///
string toJsonProperties(T)(T model) {
  string code;

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    code ~= "  " ~ field.publicName ~ field.toTSTypeDefinition ~ ";\n";
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    auto glue = field.isOptional ? "?: " : ": ";

    if(field.type == "SysTime") {
      code ~= "  " ~ field.publicName ~ glue ~ "string;\n";
      continue;
    }

    if(field.type == "GeoJsonGeometry") {
      code ~= "  " ~ field.publicName ~ glue ~ "GeoJsonGeometry;\n";
      continue;
    }

    if(field.type == "string") {
      code ~= "  " ~ field.publicName ~ glue ~ "string;\n";
      continue;
    }

    code ~= "  " ~ field.publicName ~ field.toTSTypeDefinition("Json") ~ ";\n";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    code ~= "  " ~ field.publicName ~ field.toTSIdTypeDefinition ~ ";\n";
  }

  return code;
}

/// It generates the list of strings json props for strings
unittest {
  struct TestModel {
    string id;
    @optional string value;
    string[] field1;
    string[][] field2;
    string[string][string] field3;
    @optional OtherString other;
  }

  auto description = describe!TestModel;

  auto code = description.toJsonProperties;
  code.should.equal("  id: string;
  value?: string;
  field1: string[];
  field2: string[][];
  field3: Record<string, Record<string, string>>;
  other?: string;\n");
}

///
string toProperties(T)(T model) {
  string code;

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    code ~= "  " ~ field.publicName ~ field.toTSTypeDefinition ~ ";\n";
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    code ~= "  " ~ field.publicName ~ field.toTSTypeDefinition ~ ";\n";
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    auto relationIdField = field.model.idField;
    relationIdField.listType = field.listType;
    relationIdField.isOptional = field.isOptional;

    code ~= "  " ~ field.publicName ~ field.toTSTypeDefinition ~ ";\n";
  }

  return code;
}

struct OtherString {
  string toString() const {
    return "";
  }

  static OtherString fromString(string encoded) {
    return OtherString();
  }
}

/// It generates the list of strings props for strings
unittest {
  struct TestModel {
    string id;
    @optional string value;
    string[] field1;
    string[][] field2;
    string[string][string] field3;
    @optional OtherString other;
    @optional OtherString[] otherList;
  }

  auto description = describe!TestModel;

  auto code = description.toProperties;
  code.should.equal("  id: string;
  value?: string;
  field1: string[];
  field2: string[][];
  field3: Record<string, Record<string, string>>;
  other?: string;
  otherList?: string[];\n");
}

/// It generates the list of strings props for strings
unittest {
  struct TestModel {
    string id;
    SysTime field1;
  }

  auto description = describe!TestModel;

  auto code = description.toProperties;
  code.should.equal("  id: string;
  field1: DateTime;\n");
}

/// It generates a string list for a list of relations
unittest {
  struct Other {
    string id;
  }

  struct TestModel {
    string id;
    Other[] others;
  }

  auto description = describe!TestModel;

  auto code = description.toJsonProperties;
  code.should.equal("  id: string;
  others: string[];\n");
}

/// It generates a string type for SysTime
unittest {
  struct TestModel {
    string id;
    SysTime date;
  }

  auto description = describe!TestModel;

  auto code = description.toJsonProperties;
  code.should.equal("  id: string;
  date: string;\n");
}

///
string toTSType(T)(T field) {
  string type = "unknown";

  switch(field.type) {
    case "string":
      type = "string";
      break;
    case "SysTime":
    case "Date":
      type = "DateTime";
      break;
    case "bool":
      type = "boolean";
      break;
    case "double":
    case "float":
    case "long":
    case "ulong":
    case "int":
    case "uint":
      type = "number";
      break;
    case "Json":
      type = "any";
      break;
    default:
      type = field.type;
  }

  auto customType = getAttribute!("ts type")(field);
  if(customType != "") {
    type = customType;
  }

  return type;
}

///
string toTSTypeDefinition(T)(T field, string postfix = "") {
  string type = field.toTSType ~ postfix;

  auto customType = getAttribute!("ts type")(field);
  if(customType == "") {
    type = type.wrapList(field.listType);
  }

  if(field.isOptional) {
    return "?: " ~ type;
  }

  return ": " ~ type;
}

///
string toTSIdTypeDefinition(T)(T field) {

  auto type = "string".wrapList(field.listType);

  if(field.isOptional) {
    return "?: " ~ type;
  }

  return ": " ~ type;
}

/// It matches D types to swift types
unittest {
  struct TestModel {
    string _String;
    SysTime _Date;
    bool _Bool;
    double _Double;
    float _Double2;
    long _Int;
    ulong _UInt;
    uint _UInt32;
    Json _json;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toTSTypeDefinition.should.equal(": string");
  description.fields.basic[1].toTSTypeDefinition.should.equal(": boolean");
  description.fields.basic[2].toTSTypeDefinition.should.equal(": number");
  description.fields.basic[3].toTSTypeDefinition.should.equal(": number");
  description.fields.basic[4].toTSTypeDefinition.should.equal(": number");
  description.fields.basic[5].toTSTypeDefinition.should.equal(": number");
  description.fields.basic[6].toTSTypeDefinition.should.equal(": number");
  description.fields.basic[7].toTSTypeDefinition.should.equal(": any");
}

/// It matches D optional types to swift optional types
unittest {
  struct TestModel {
    @optional:
      string _String;
      SysTime _Date;
      bool _Bool;
      double _Double;
      float _Double2;
      long _Int;
      ulong _UInt;
      uint _UInt32;
      Json _json;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toTSTypeDefinition.should.equal("?: string");
  description.fields.basic[1].toTSTypeDefinition.should.equal("?: boolean");
  description.fields.basic[2].toTSTypeDefinition.should.equal("?: number");
  description.fields.basic[3].toTSTypeDefinition.should.equal("?: number");
  description.fields.basic[4].toTSTypeDefinition.should.equal("?: number");
  description.fields.basic[5].toTSTypeDefinition.should.equal("?: number");
  description.fields.basic[6].toTSTypeDefinition.should.equal("?: number");
  description.fields.basic[7].toTSTypeDefinition.should.equal("?: any");
}

string wrapList(string type, ListType[] list) {

  foreach(item; list) {
    if(item == ListType.list) {
      type = type ~ `[]`;
    } else {
      type = `Record<string, ` ~ type ~ `>`;
    }
  }

  return type;
}

///
string toTSDateGetSetProperty(T)(T field) {
  string type = field.toTSType.wrapList(field.listType);
  string optionalCheck;

  if(field.isOptional) {
    type ~= " | undefined";
  }

  if(type == "DateTime") {
    return
    `  get ` ~ field.publicName ~ `(): ` ~ type ~ ` {` ~ "\n" ~
    `    return DateTime.fromISO(this.json.` ~ field.publicName ~ `);` ~ "\n" ~
    `  }` ~ "\n\n" ~
    `  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {` ~ "\n" ~
    `    this._isDirty = true;` ~ "\n" ~
    `    this.json.` ~ field.publicName ~ ` = newValue.toISO()!;` ~ "\n" ~
    `  }` ~ "\n\n";
  }

  if(type == "DateTime[]") {
    return
    `  get ` ~ field.publicName ~ `(): ` ~ type ~ ` {` ~ "\n" ~
    `    return this.json.` ~ field.publicName ~ `.map(a => DateTime.fromISO(a));` ~ "\n" ~
    `  }` ~ "\n\n" ~
    `  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {` ~ "\n" ~
    `    this._isDirty = true;` ~ "\n" ~
    `    this.json.` ~ field.publicName ~ ` = newValue.map(a => a.toISO());` ~ "\n" ~
    `  }` ~ "\n\n";
  }

  if(type == "DateTime | undefined") {
    return
    `  get ` ~ field.publicName ~ `(): ` ~ type ~ ` {` ~ "\n" ~
    `    if(!this.json.` ~ field.publicName ~ `) {` ~ "\n" ~
    `      return undefined;` ~ "\n" ~
    `    }` ~ "\n" ~
    `    return DateTime.fromISO(this.json.` ~ field.publicName ~ `);` ~ "\n" ~
    `  }` ~ "\n\n" ~
    `  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {` ~ "\n" ~
    `    this._isDirty = true;` ~ "\n" ~
    `    this.json.` ~ field.publicName ~ ` = newValue?.toISO() ?? undefined;` ~ "\n" ~
    `  }` ~ "\n\n";
  }

  if(type == "DateTime[] | undefined") {
    return
    `  get ` ~ field.publicName ~ `(): ` ~ type ~ ` {` ~ "\n" ~
    `    return this.json.` ~ field.publicName ~ `?.map(a => DateTime.fromISO(a)) ?? [];` ~ "\n" ~
    `  }` ~ "\n\n" ~
    `  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {` ~ "\n" ~
    `    this._isDirty = true;` ~ "\n" ~
    `    this.json.` ~ field.publicName ~ ` = newValue?.map(a => a.toISO()) ?? [];` ~ "\n" ~
    `  }` ~ "\n\n";
  }

  return `  get ` ~ field.publicName ~ `(): ` ~ type ~ ` {
    return this.json.` ~ field.publicName ~ `;
  }

  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {
    this._isDirty = true;
    this.json.` ~ field.publicName ~ ` = newValue;
  }` ~ "\n\n";
}

///
string toTSGetSetProperty(T)(T field) {
  string type = field.toTSType.wrapList(field.listType);
  string optionalCheck;

  if(field.isOptional) {
    type ~= " | undefined";
  }

  if(type.canFind("DateTime")) {
    return field.toTSDateGetSetProperty;
  }

  return `  get ` ~ field.publicName ~ `(): ` ~ type ~ ` {
    return this.json.` ~ field.publicName ~ `;
  }

  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {
    this._isDirty = true;
    this.json.` ~ field.publicName ~ ` = newValue;
  }` ~ "\n\n";
}

/// It generates the getter and setter for properties
unittest {
  struct TestModel {
    string name;
    SysTime[] date;

    @optional bool someFlag;
  }

  auto description = describe!TestModel;

  description.fields.basic[0].toTSGetSetProperty.should.equal("  get name(): string {
    return this.json.name;
  }

  set name(newValue: string) {
    this._isDirty = true;
    this.json.name = newValue;
  }\n\n");

  description.fields.basic[1].toTSGetSetProperty.should.equal("  get someFlag(): boolean | undefined {
    return this.json.someFlag;
  }

  set someFlag(newValue: boolean | undefined) {
    this._isDirty = true;
    this.json.someFlag = newValue;
  }\n\n");

  description.fields.objects[0].toTSGetSetProperty.should.equal("  get date(): DateTime[] {
    return this.json.date.map(a => DateTime.fromISO(a));
  }

  set date(newValue: DateTime[]) {
    this._isDirty = true;
    this.json.date = newValue.map(a => a.toISO());
  }\n\n");
}

/// It generates the getter and setter for a SysTime
unittest {
  struct TestModel {
    SysTime date;
    SysTime[] dates;
  }

  auto description = describe!TestModel;

  description.fields.objects[0].toTSGetSetProperty.should.equal("  get date(): DateTime {
    return DateTime.fromISO(this.json.date);
  }

  set date(newValue: DateTime) {
    this._isDirty = true;
    this.json.date = newValue.toISO();
  }\n\n");

  description.fields.objects[1].toTSGetSetProperty.should.equal("  get dates(): DateTime[] {
    return this.json.dates.map(a => DateTime.fromISO(a));
  }

  set dates(newValue: DateTime[]) {
    this._isDirty = true;
    this.json.dates = newValue.map(a => a.toISO());
  }\n\n");
}

/// It generates the getter and setter for an optional SysTime
unittest {
  struct TestModel {
    @optional SysTime date;
    @optional SysTime[] dates;
  }

  auto description = describe!TestModel;

  description.fields.objects[0].toTSGetSetProperty.should.equal("  get date(): DateTime | undefined {
    if(!this.json.date) {
      return undefined;
    }
    return DateTime.fromISO(this.json.date);
  }

  set date(newValue: DateTime | undefined) {
    this._isDirty = true;
    this.json.date = newValue?.toISO() ?? undefined;
  }\n\n");

  description.fields.objects[1].toTSGetSetProperty.should.equal("  get dates(): DateTime[] | undefined {
    return this.json.dates?.map(a => DateTime.fromISO(a)) ?? [];
  }

  set dates(newValue: DateTime[] | undefined) {
    this._isDirty = true;
    this.json.dates = newValue?.map(a => a.toISO()) ?? [];
  }\n\n");
}


///
string toTSGetSetObject(T)(T field) {
  string type = field.toTSType;
  string modelType = field.toTSType;

  if(modelType != "GeoJsonGeometry") {
    modelType ~= "Model";
  }

  string returnType = modelType;
  string returnModelType = modelType.wrapList(field.listType);
  string getter = `new ` ~ modelType ~ `(this.json.` ~ field.publicName ~ `)`;
  string setter = `this.json.` ~ field.publicName ~ ` = newValue?.toJSON();`;

  if(field.listType.length) {
    getter = `this.json.` ~ field.publicName ~ `?.map(a => new ` ~ modelType ~ `(a)) ?? []`;
    setter = `this.json.` ~ field.publicName ~ ` = newValue?.map(a => a.toJSON()) ?? [];`;
    returnType = returnType.wrapList(field.listType);
  }

  if(field.isOptional) {
    returnType ~= " | undefined";
  }

  if(modelType == "GeoJsonGeometry") {
    getter = `this.json.` ~ field.publicName;
    setter = `this.json.` ~ field.publicName ~ ` = newValue;`;
  }

  return `  get ` ~ field.publicName ~ `(): ` ~ returnModelType ~ ` {
    return ` ~ getter ~ `;
  }

  set ` ~ field.publicName ~ `(newValue: ` ~ returnType ~ `) {
    this._isDirty = true;

    ` ~ setter ~ `
  }` ~ "\n\n";
}

/// It generates the getter and setter for objects
unittest {
  struct Other {
    string name;
    SysTime[] date;

    @optional bool someFlag;
  }

  struct TestModel {
    Other other;
  }

  auto description = describe!TestModel;

  description.fields.objects[0].toTSGetSetObject.should.equal("  get other(): OtherModel {
    return new OtherModel(this.json.other);
  }

  set other(newValue: OtherModel) {
    this._isDirty = true;

    this.json.other = newValue?.toJSON();
  }\n\n");
}

/// It generates the getter and setter for list objects
unittest {
  struct Other {
    string name;
    SysTime[] date;

    @optional bool someFlag;
  }

  struct TestModel {
    Other[] other;
  }

  auto description = describe!TestModel;


  description.fields.objects[0].toTSGetSetObject.should.equal("  get other(): OtherModel[] {
    return this.json.other?.map(a => new OtherModel(a)) ?? [];
  }

  set other(newValue: OtherModel[]) {
    this._isDirty = true;

    this.json.other = newValue?.map(a => a.toJSON()) ?? [];
  }\n\n");
}

///
string toTSGetSetRelation(T)(T field, string store) {
  string type = field.toTSType ~ "Model";
  type = type.wrapList(field.listType);
  string checkOptionalGetter;

  if(field.isOptional) {
    type ~= " | undefined";
    checkOptionalGetter = "\n    " ~ `if(!this.json.` ~ field.publicName ~ `) {` ~ "\n      return Promise.resolve(undefined);\n    }";
  }

  if(field.listType.length == 0) {
    return `  get ` ~ field.publicName ~ `(): Promise<` ~ type ~ `> {` ~ checkOptionalGetter ~ `
    return ` ~ store ~ `.find` ~ field.model.singular ~ `(this.json.` ~ field.publicName ~ `);
  }

  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {
    this._isDirty = true;
    this.json.` ~ field.publicName ~ ` = newValue?.` ~ field.model.idField.publicName ~ `;
  }` ~ "\n\n";
  }

  return `  get ` ~ field.publicName ~ `(): Promise<` ~ type ~ `> {
    const promises = this.json.` ~ field.publicName ~ `?.map(a => ` ~ store ~ `.find` ~ field.model.singular ~ `(a)) ?? [];

    return Promise.all(promises);
  }

  set ` ~ field.publicName ~ `(newValue: ` ~ type ~ `) {
    this._isDirty = true;
    this.json.` ~ field.publicName ~ ` = newValue?.map(a => a.` ~ field.model.idField.publicName ~ `) ?? [];
  }` ~ "\n\n";
}

/// It generates the getter and setter for a relation
unittest {
  struct Other {
    string id;
  }

  struct TestModel {
    Other other;
  }

  auto description = describe!TestModel;

  description.fields.relations[0].toTSGetSetRelation("STORE").should.equal("  get other(): Promise<OtherModel> {
    return STORE.findOther(this.json.other);
  }

  set other(newValue: OtherModel) {
    this._isDirty = true;
    this.json.other = newValue?.id;
  }\n\n");
}

/// It generates the getter and setter for a list of relations
unittest {
  struct Other {
    string id;
  }

  struct TestModel {
    Other[] other;
  }

  auto description = describe!TestModel;

  description.fields.relations[0].toTSGetSetRelation("STORE").should.equal("  get other(): Promise<OtherModel[]> {
    const promises = this.json.other?.map(a => STORE.findOther(a)) ?? [];

    return Promise.all(promises);
  }

  set other(newValue: OtherModel[]) {
    this._isDirty = true;
    this.json.other = newValue?.map(a => a.id) ?? [];
  }\n\n");
}

string toModelProperties(T)(T model) {
  string code;

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    code ~= `  /// field: ` ~ field.type ~ "\n";
    code ~= field.toTSGetSetProperty;
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    code ~= `  /// object: ` ~ field.type ~ "\n";
    if(["string", "SysTime", "PositionT!double", "ArticleBody"].canFind(field.type)) {
      code ~= field.toTSGetSetProperty;
    } else {
      code ~= field.toTSGetSetObject;
    }
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    code ~= `  /// model: ` ~ field.type ~ "\n";
    code ~= field.toTSGetSetRelation("Store.instance");
  }

  return code;
}

string toGetterTypes(T)(T model) {
  string[] types;

  foreach(field; model.fields.basic) {
    if(field.isHidden) {
      continue;
    }

    types ~= field.toTSType.wrapList(field.listType);
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    if(["string", "SysTime", "PositionT!double", "ArticleBody", "GeoJsonGeometry"].canFind(field.type)) {
      types ~= field.toTSType.wrapList(field.listType);
    } else {
      types ~= (field.toTSType ~ "Model").wrapList(field.listType);
    }
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden) {
      continue;
    }

    types ~= `Promise<` ~ ((field.toTSType ~ "Model").wrapList(field.listType)) ~ ">";
  }

  return types.sort.uniq.join(" | ") ~ " | null | undefined";
}

string toTeamIdGetter(T)(T model) {
  string[] types;

  foreach(field; model.fields.objects) {
    if(field.isHidden) {
      continue;
    }

    if(field.type == "Visibility") {
      return `  get teamId(): string {` ~ "\n" ~
      `    return this.json?.` ~ field.publicName ~ `?.team ?? "";` ~ "\n" ~
      `  }` ~ "\n\n";
    }
  }

  return "";
}

string getBasicDefaultValue(string tsType) {
  switch(tsType) {
    case "string":
    case "DateTime":
    case "SysTime":
      return `""`;
    case "boolean":
      return "false";
    case "number":
      return "0";
    case "Json":
    case "ArticleBody":
      return "{ blocks: [] }";
    case "any":
      return "null";
    default:
      throw new Exception("Unknown default value for type " ~ tsType);
  }
}

string toDefaultValue(T)(T model) {
  string[] code;

  foreach(field; model.fields.basic) {
    if(field.isHidden || field.isOptional) {
      continue;
    }

    if(field.listType.length) {
      code ~= `"` ~ field.publicName ~ `": []`;
      continue;
    }

    auto type = field.toTSType;

    code ~= field.publicName ~ `: ` ~ type.getBasicDefaultValue;
  }

  foreach(field; model.fields.objects) {
    if(field.isHidden || field.isOptional) {
      continue;
    }

    if(field.listType.length) {
      code ~= field.publicName ~ `: []`;
      continue;
    }

    if(["string", "SysTime", "DateTime"].canFind(field.type)) {
      code ~=  field.publicName ~ `: ""`;
      continue;
    }

    if(["PositionT!double"].canFind(field.type)) {
      continue;
    }

    if(["ArticleBody"].canFind(field.type)) {
      code ~= field.publicName ~ `: { blocks: [] }`;
      continue;
    }

    if(field.type == "GeoJsonGeometry") {
      code ~= field.publicName ~ `: { type: "Point", coordinates: [0, 0] }` ~ "\n";
      continue;
    }

    code ~= field.publicName ~ `: ` ~ field.toDefaultValue;
  }

  foreach(field; model.fields.relations) {
    if(field.isHidden || field.isOptional) {
      continue;
    }

    if(field.listType.length) {
      code ~= field.publicName ~ `: []`;
      continue;
    }

    code ~= field.publicName ~ `: ""`;
  }

  if(code.length == 0) {
    return "{}";
  }

  return "{ " ~ code.join(", ") ~ " }";
}

/// it can generate tan object default for a struct with basif and object fields
unittest {
  struct Other {
    int d;
  }

  struct SomeModel {
    int id;
  }

  struct Test {
    string a;
    SysTime now;
    Other b;
    SomeModel c;
  }

  auto description = describe!Test;

  expect(description.toDefaultValue).to.equal(`{ a: "", now: "", b: { d: 0 }, c: "" }`);
}

/// it can generates an empty object when everything is optional
unittest {
  struct Other {
    int d;
  }

  struct SomeModel {
    int id;
  }

  struct Test {
    @optional:
      string a;
      Other b;
      SomeModel c;
  }

  auto description = describe!Test;

  expect(description.toDefaultValue).to.equal(`{}`);
}

string dasherize(string value) {
  string result;
  size_t i;

  foreach(char ch; value) {
    auto newCh = ch.toLower;

    if(newCh != ch && i != 0) {
      result ~= '-';
    }

    result ~= newCh;
    i++;
  }

  return result;
}