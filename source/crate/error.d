/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.error;

public import std.exception;
import std.functional;

import vibe.data.json;
import vibe.http.server;
import vibe.core.log;
import vibe.data.json;
import vibe.http.router;

version(unittest) {
  import fluent.asserts;
}

/// A generic server exception
/// given when an unexpected condition was encountered and no more specific message is suitable.
class CrateException : Exception
{
  int statusCode = 500;
  string title = "Crate error";

  this(string msg = null, Throwable next = null) @safe nothrow {
    super(msg, next);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe nothrow
  {
    super(msg, file, line, next);
  }
}
/// Thrown when the requested resource could not be found but may be available in the future.
/// Subsequent requests by the client are permissible.
class CrateNotImplementedException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 404;
    title = "This operation is not supported.";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 404;
    title = "This operation is not supported.";
  }
}

/// Thrown when the requested resource could not be found but may be available in the future.
/// Subsequent requests by the client are permissible.
class CrateNotFoundException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 404;
    title = "Crate not found";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 404;
    title = "Crate not found";
  }
}

/// Thrown when a crate relation was not found
class CrateRelationNotFoundException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 400;
    title = "Crate relation id missing";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 400;
    title = "Crate relation id missing";
  }
}

/// Thrown when a rquest timed out
class CrateRequestTimeoutException : CrateException {
  this(string msg = null, Throwable next = null) @safe {
    super(msg, next);

    statusCode = 408;
    title = "Request timed out";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 408;
    title = "Request timed out";
  }
}

/// Exception for authorization errors
class UnauthorizedException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 401;
    title = "Unauthorized";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 401;
    title = "Unauthorized";
  }
}

class ForbiddenException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 403;
    title = "Forbidden";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 403;
    title = "Forbidden";
  }
}


/// Thrown when the server cannot or will not process the request due to an apparent client error
/// e.g., malformed request syntax, size too large, invalid request message framing,
/// or deceptive request routing
class CrateValidationException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 400;
    title = "Validation error";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 400;
    title = "Validation error";
  }
}

///
class CrateToManyRequestsException : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 429;
    title = "Too many requests";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 429;
    title = "Too many requests";
  }
}

/// Thrown for errors inside the Lazy values
class LazyValueError : CrateException {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);

    statusCode = 500;
    title = "Lazy value error";
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);

    statusCode = 500;
    title = "Lazy value error";
  }
}

/// Convert an exception to a json object
auto toJson(Exception exception) {
  import vibe.data.json;
  auto e = cast(CrateException) exception;

  Json data = Json.emptyObject;
  data["errors"] = Json.emptyArray;
  data["errors"] ~= Json.emptyObject;

  data["errors"][0]["status"] = e is null ? 500 : e.statusCode;
  data["errors"][0]["title"] = e is null ? "Server error" : e.title;
  data["errors"][0]["description"] = exception.msg;

  return data;
}

/// Convert Crate Exception to json
unittest {
  auto exception = new CrateException("message");
  exception.statusCode = 400;
  exception.title = "title";

  auto data = exception.toJson;

  assert(data["errors"].length == 1);
  assert(data["errors"][0]["title"] == "title");
  assert(data["errors"][0]["description"] == "message");
  assert(data["errors"][0]["status"] == 400);
}

/// Convert Exception to json
unittest {
  auto exception = new Exception("message");

  auto data = exception.toJson;

  assert(data["errors"].length == 1);
  assert(data["errors"][0]["title"] == "Server error");
  assert(data["errors"][0]["description"] == "message");
  assert(data["errors"][0]["status"] == 500);
}

CrateException toException(Json item) @safe nothrow {
  CrateException exception;

  try {
    if(item.type == Json.Type.array) {
      if(item.length == 0) {
        return new CrateException();
      }

      return toException(item[0]);
    }

    if(item.type != Json.Type.object) {
      return new CrateException(item.to!string);
    }

    if("description" in item) {
      exception = new CrateException(item["description"].to!string);
    } else {
      exception = new CrateException("Unknown error: " ~ item.to!string);
    }

    if("title" in item) {
      exception.title = item["title"].to!string;
    }

    if("status" in item) {
      exception.statusCode = item["status"].to!int;
    }
  } catch(Exception) {
    exception = new CrateException("Unknown error");
  }

  return exception;
}

/// Convert json to exception
unittest {
  auto exception = `[{
          "title": "Invalid operation",
          "description": "The name is missing",
          "status": 400 }]`.parseJsonString.toException;

  exception.title.should.equal("Invalid operation");
  exception.statusCode.should.equal(400);
  exception.message.should.equal("The name is missing");
}

auto requestErrorHandler(void function(HTTPServerRequest, HTTPServerResponse) @safe next) {
  return requestErrorHandler(next.toDelegate);
}

auto requestErrorHandler(void delegate(HTTPServerRequest, HTTPServerResponse) @safe next) {
  void check(HTTPServerRequest request, HTTPServerResponse response) @trusted {
    try {
      next(request, response);
    } catch(CrateException e) {
      response.writeJsonBody(e.toJson, e.statusCode);
      version(unittest) {
        version(Have_trial) {
          import trial.interfaces;
          Attachment.fromString(e.toString);
        } else {
          logWarn(e.toString);
        }
      } else {
        logWarn(e.toString);
      }
    } catch (Exception e) {
      version(unittest) {
        version(Have_trial) {
          import trial.interfaces;
          Attachment.fromString(e.toString);
        } else {
          logError(e.toString);
        }
      } else {
        logError(e.toString);
      }

      Json data = e.toJson;
      response.writeJsonBody(data, data["errors"][0]["status"].to!int);
    }
  }

  return &check;
}

auto requestErrorHandler(void delegate(HTTPServerRequest, HTTPServerResponse) next) {
  void check(HTTPServerRequest request, HTTPServerResponse response) @trusted {
    try {
      next(request, response);
    } catch(CrateException e) {
      response.writeJsonBody(e.toJson, e.statusCode);
      version(unittest) {
        version(Have_trial) {
          import trial.interfaces;
          Attachment.fromString(e.toString);
        } else {
          logWarn(e.toString);
        }
      } else {
        logWarn(e.toString);
      }
    } catch (Exception e) {
      version(unittest) {
        version(Have_trial) {
          import trial.interfaces;
          Attachment.fromString(e.toString);
        } else {
          logError(e.toString);
        }
      } else {
        logError(e.toString);
      }

      Json data = e.toJson;
      response.writeJsonBody(data, data["errors"][0]["status"].to!int);
    }
  }

  return &check;
}
