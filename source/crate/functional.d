/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.functional;

import vibe.data.json;
import vibe.http.router;

///
auto toJsonDelegate(R, P)(R delegate(P value) @safe value) if(!is(P == string)) {
  auto jsonWrapper(Json parameter) {
    static if(is(P == Json)) {
      auto param = parameter;
    } else {
      auto param = parameter.deserializeJson!P;
    }

    Json jsonResult;
    static if(is(R == void)) {
      jsonResult = Json.undefined;
      value(param);
    } else static if(is(R == Json)) {
      jsonResult = value(param);
    } else {
      jsonResult = value(param).serializeToJson;
    }

    return jsonResult;
  }
  
  return &jsonWrapper;
}


///
auto toJsonDelegate(R)(R delegate(string) @safe value) {
  auto jsonWrapper(string id) {
    Json jsonResult;

    static if(is(R == void)) {
      jsonResult = Json.undefined;
      value(param);
    } else static if(is(R == Json)) {
      jsonResult = value(id);
    } else {
      jsonResult = value(id).serializeToJson;
    }

    return jsonResult;
  }
  
  return &jsonWrapper;
}


///
auto toJsonDelegate(R)(R delegate() @safe value) {
  auto jsonWrapper() {
    Json jsonResult;

    static if(is(R == void)) {
      jsonResult = Json.undefined;
      value(param);
    } else static if(is(R == Json)) {
      jsonResult = value();
    } else {
      jsonResult = value().serializeToJson;
    }

    return jsonResult;
  }
  
  return &jsonWrapper;
}


///
auto toJsonDelegate(R, P)(R delegate(P value, HTTPServerResponse res) @safe value) {
  void jsonWrapper(Json parameter, HTTPServerResponse res) {
    static if(is(P == Json)) {
      auto param = parameter;
    } else {
      auto param = parameter.deserializeJson!P;
    }

    value(param, res);
  }
  
  return &jsonWrapper;
}
