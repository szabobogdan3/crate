/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.queryParams;

import std.conv;
import std.string;
import std.traits;

import vibe.core.log;
import vibe.data.serialization;
import described : Describe = describe;

import crate.error;

version(unittest) {
  import fluent.asserts;
}

T parseQueryParams(T, U)(U params) @safe {
  T result;

  parseQueryParamsRef!(T, U)(result, params);

  return result;
}

void parseQueryParamsRef(T, U)(ref T result, U params) @safe {
  string[string] keys;

  foreach(pair; params.byKeyValue) {
    string keyName = pair.key.toLower.dup;

    if(keyName in keys) {
      keys[keyName] ~= "," ~ pair.value.dup;
    } else {
      keys[keyName] = pair.value.dup;
    }
  }

  enum structDescription = Describe!T;

  static foreach(member; structDescription.properties) {
    try {
      if(member.name.toLower in keys ) {
        mixin(`result.` ~ member.name ~ ` = parseQueryParam!(typeof(result.` ~ member.name ~ `))(keys["` ~ member.name.toLower ~ `"]);`);
      }
    } catch (Exception e) {
      (() @trusted {logError(e.message);})();
      throw new CrateValidationException("Can't parse the `" ~ member.name ~ "` parameter.");
    }
  }

  static foreach(member; __traits(allMembers, T)) {
    try {
      static if(hasUDA!(__traits(getMember, T, member), NameAttribute!DefaultPolicy)) {
        static foreach(attr; getUDAs!(__traits(getMember, T, member), NameAttribute!DefaultPolicy)) {
          if(attr.name.toLower in keys) {
            mixin(`result.` ~ member ~ ` = parseQueryParam!(typeof(result.` ~ member ~ `))(keys["` ~ attr.name.toLower ~ `"]);`);
          }
        }
      }
    } catch (Exception e) {
      (() @trusted {logError(e.message);})();
      throw new CrateValidationException("Can't parse the `" ~ member ~ "` parameter.");
    }
  }
}

/// It should parse an integer
unittest {
  struct Test {
    int val;
  }

  string[string] params;
  params["val"] = "12";

  params.parseQueryParams!Test.val.should.equal(12);
}

/// It should parse a float list
unittest {
  struct Test {
    int[] val;
  }

  string[string] params;
  params["val"] = "1,2";

  auto value = params.parseQueryParams!Test.val;
  value.should.equal([1,2]);
}

/// It should parse a float
unittest {
  struct Test {
    float val;
  }

  string[string] params;
  params["val"] = "12.2";

  params.parseQueryParams!Test.val.should.equal(12.2);
}

/// It should parse a bool
unittest {
  struct Test {
    bool val;
  }

  string[string] params;
  params["val"] = "true";

  params.parseQueryParams!Test.val.should.equal(true);
}

/// It should parse a string
unittest {
  struct Test {
    string val;
  }

  string[string] params;
  params["val"] = "true";

  params.parseQueryParams!Test.val.should.equal("true");
}


/// It should use a different name when the name attribute is present
unittest {
  struct Test {
    @name("other")
    string val;
  }

  string[string] params;
  params["other"] = "true";

  params.parseQueryParams!Test.val.should.equal("true");
}

/// It should parse a list of strings
unittest {
  struct Test {
    string[] val;
  }

  string[string] params;
  params["val"] = "1,2,3,4";

  params.parseQueryParams!Test.val.should.equal(["1", "2", "3", "4"]);
}

/// It should ignore the case
unittest {
  struct Test {
    string[] val;
  }

  string[string] params;
  params["VAL"] = "1,2,3,4";

  params.parseQueryParams!Test.val.should.equal(["1", "2", "3", "4"]);
}

/// It should throw a crate error on invalid values
unittest {
  struct Test {
    bool val;
  }

  string[string] params;
  params["val"] = "missing";

  params.parseQueryParams!Test.val
    .should.throwException!(CrateValidationException)
    .withMessage("Can't parse the `val` parameter.");
}

///
string parseQueryParam(T)(string value) @safe pure if(is(T == string)) {
  return value;
}

///
T parseQueryParam(T : U[], U)(string value) @safe pure if(is(U == string)) {
  return value.split(",");
}

///
T parseQueryParam(T : U[], U)(string value) @safe pure if(!is(U == string) && !is(Unqual!U == char)) {
  T result;

  foreach(item; value.split(",")) {
    result ~= item.to!U;
  }

  return result;
}

///
T parseQueryParam(T)(string value) @safe pure if(!is(T == string)) {
  return value.to!T;
}
