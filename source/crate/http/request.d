/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.request;

import std.variant;
import std.range;
import std.conv;

import vibe.data.json;
import vibe.http.common;
import vibe.http.server;
import vibe.http.websockets;
import vibe.container.dictionarylist;
import vibe.inet.url;
import vibe.core.log;

alias typeof(HTTPServerResponse.bodyWriter) BodyWriter;
alias typeof(HTTPServerRequest.bodyReader) BodyReader;
alias typeof(HTTPServerRequest.files) Files;

interface CrateHttpRequest {
  nothrow @safe {
    ///
    HTTPServerRequest toVibeRequest();

    /// Get the json body
    Json json();

    /// Get the selected item
    InputRange!Json values();

    /// Set the selected item
    void values(InputRange!Json);

    /// Query parameters
    string[string] query();

    ///
    string[string] headers();

    ///
    HTTPMethod method();

    ///
    BodyReader bodyReader();

    ///
    string requestURI();

    ///
    Files files();

    ///
    ref DictionaryList!(Variant, true, 2) context();

    ///
    ref const(DictionaryList!(Variant, true, 2)) context() const;

    ///
    ref DictionaryList!(string, true, 8) params();

    ///
    ref const(DictionaryList!(string, true, 8)) params() const;

    URL fullURL() @safe const;
  }
}

///
interface CrateHttpResponse {
  @safe {

    ///
    bool headerWritten();

    ///
    void setHeader(string key, string value);

    ///
    void statusCode(uint);

    ///
    BodyWriter bodyWriter();

    ///
    void writeBody(InputRange!string data, int code, string contentType);

    ///
    void writeVoidBody(int code, string contentType);

    ///
    void writeJsonBody(Json data, int code, string contentType);

    ///
    HTTPServerResponse toVibeResponse();
  }
}

/// Converts the vibe server request to a crate request
CrateHttpRequest toCrateRequest(HTTPServerRequest request) nothrow @safe {
  return new CrateVibeHttpRequest(request);
}

/// Converts the vibe server request to a crate request
CrateHttpRequest toCrateRequest(const HTTPServerRequest request) @trusted {
  auto newRequest = new HTTPServerRequest(request.timeCreated, 80);

  foreach(pair; request.context.byKeyValue) {
    newRequest.context[pair.key] = pair.value;
  }

  return new CrateVibeHttpRequest(newRequest);
}

///
CrateHttpResponse toCrateResponse(HTTPServerResponse response) nothrow @safe {
  return new CrateVibeHttpResponse(response);
}
/// Crate http request implementation with memory storage
class CrateMemoryHttpRequest : CrateHttpRequest {
  private {
    Json _json;
    string[string] _query;
    string[string] _headers;
    HTTPMethod _method;
    DictionaryList!(Variant, true, 2) _context;
    DictionaryList!(string, true, 8) _params;
    URL _fullURL;
    InputRange!Json _values;
  }


  nothrow @safe {
    HTTPServerRequest toVibeRequest() {
      return null;
    }

    URL fullURL() const {
      return _fullURL;
    }

    ///
    string requestURI() {
      return "";
    }

    /// Get the json body
    Json json() {
      return _json;
    }

    ///
    Files files() {
      return Files.init;
    }

    /// Query parameters
    string[string] query() {
      return _query;
    }

    ///
    string[string] headers() {
      return _headers;
    }

    ///
    HTTPMethod method() {
      return _method;
    }

    ///
    BodyReader bodyReader() {
      return BodyReader.init;
    }

    /// Get the selected item
    InputRange!Json values() {
      return this._values;
    }

    /// Set the selected item
    void values(InputRange!Json value) {
      try {
        this._values = value;
      } catch(Exception) {}
    }

    ///
    ref DictionaryList!(Variant, true, 2) context() {
      return _context;
    }

    ///
    ref const(DictionaryList!(Variant, true, 2)) context() const {
      return _context;
    }

    ///
    ref DictionaryList!(string, true, 8) params() {
      return _params;
    }

    ///
    ref const(DictionaryList!(string, true, 8)) params() const {
      return _params;
    }
  }
}

/// Crate http request implementation for Vibe.d
class CrateVibeHttpRequest : CrateHttpRequest {

  private {
    HTTPServerRequest request;
    URL defaultUrl;
    InputRange!Json _values;
  }

  nothrow @safe {
    HTTPServerRequest toVibeRequest() {
      return request;
    }

    URL fullURL() const {
      try {
        return request.fullURL;
      } catch(Exception e) {
        return defaultUrl;
      }
    }

    ///
    string requestURI() {
      return request.requestURI;
    }

    this(HTTPServerRequest request) {
      this.request = request;
    }

    /// Get the json body
    Json json() {
      try {
        return request.json;
      } catch(Exception e) {
        return Json();
      }
    }

    ///
    Files files() {
      try {
        return request.files;
      } catch(Exception e) {
        return Files.init;
      }
    }

    /// Get the selected item
    InputRange!Json values() {
      return this._values;
    }

    /// Set the selected item
    void values(InputRange!Json values) {
      try {
        this._values = values;
      } catch(Exception) {}
    }

    ///
    BodyReader bodyReader() {
      return request.bodyReader;
    }

    /// Query parameters
    string[string] query() {
      string[string] result;

      try {
        foreach(key, value; request.query.byKeyValue) {
          if(key in result) {
            result[key] ~= "," ~ value;
          } else {
            result[key] = value;
          }
        }
      } catch(Exception e) {}

      return result;
    }

    ///
    string[string] headers() {
      string[string] result;

      foreach(key, value; request.headers.byKeyValue) {
        result[key] = value;
      }

      return result;
    }

    ///
    HTTPMethod method() {
      return request.method;
    }

    ///
    ref const(DictionaryList!(Variant, true, 2)) context() @trusted const {
      return request.context;
    }

    ///
    ref DictionaryList!(Variant, true, 2) context() @trusted {
      return request.context;
    }

    ///
    ref DictionaryList!(string, true, 8) params() {
      return request.params;
    }

    ///
    ref const(DictionaryList!(string, true, 8)) params() const {
      return request.params;
    }
  }
}

/// Crate http response implementation for Vibe.d
class CrateVibeHttpResponse : CrateHttpResponse {

  private HTTPServerResponse response;

  @safe {
    ///
    this(HTTPServerResponse response) nothrow {
      this.response = response;
    }

    ///
    bool headerWritten() {
      return response.headerWritten;
    }

    ///
    void setHeader(string key, string value) {
      response.headers[key] = value;
    }

    ///
    void statusCode(uint value) {
      response.statusCode = value;
    }

    BodyWriter bodyWriter() {
      return response.bodyWriter;
    }

    ///
    void writeJsonBody(Json data, int code, string contentType) {
      response.writeJsonBody(data, code, contentType);
    }

    ///
    void writeVoidBody(int code, string contentType) {
      response.statusCode = code;
      response.contentType = contentType;
      response.writeVoidBody;
    }

    void writeBody(InputRange!string data, int code, string contentType) @trusted {
      response.contentType = contentType;
      response.statusCode = code;

      foreach(chunk; data) {
        response.bodyWriter.write(chunk);
      }

      response.bodyWriter.finalize;
    }

    ///
    HTTPServerResponse toVibeResponse() {
      return response;
    }
  }
}

void setHeader(HTTPServerResponse response, string key, string value) {
  response.headers[key] = value;
}

void writeEmptyBody(HTTPServerResponse response, int code, string contentType) {
  response.statusCode = code;
  response.contentType = contentType;
  response.writeVoidBody;
}