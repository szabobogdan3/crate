/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.wrapper;

import described : Describe = describe, where, has;
import introspection.callable;
import introspection.parameter : DescribeParameter = Parameter;
import introspection.type : DescribeType = Type;
import introspection.aggregate : DescribeAggregate = Aggregate;

import crate.http.queryParams;
import crate.base;

import openapi.definitions : OpenApiParameter = Parameter, ParameterIn, Example;

import std.conv;
import std.traits;
import std.range;
import std.algorithm;
import std.array;

struct Wrapped(Storage) {
  alias WrappedStorageDelegate = InputRange!string delegate(Storage) @safe;
  alias WrappedStringDelegate = InputRange!string delegate(string) @safe;

  WrappedStorageDelegate funcStorage;
  WrappedStringDelegate funcString;

  string name;
  Callable callable;
  bool hasQueries;

  bool canBeCalledWithStorage;
  bool canBeCalledWithString;

  InputRange!string returnValue;

  OpenApiParameter[] parameters;

  auto call(Storage storage) @safe {
    returnValue = funcStorage(storage);
  }

  void call(string value) @safe {
    returnValue = funcString(value);
  }

  void updateRule(ref CrateRule rule) @safe {
    auto existingNames = rule.request.parameters.map!(a => a.name).array;

    auto newParams = parameters.filter!(a => !existingNames.canFind(a.name)).array;

    rule.request.parameters ~= newParams;
  }
}

interface IMiddlewareWrapper(Storage) {
  @safe {
    pure nothrow {
      Wrapped!Storage[] getItem();
      Wrapped!Storage[] getList();
      Wrapped!Storage[] create();
      Wrapped!Storage[] update();
      Wrapped!Storage[] replace();
      Wrapped!Storage[] delete_();
      Wrapped!Storage[] mapper();

      string name();
    }

    void updateRule(ref CrateRule);
  }
}

class MiddlewareWrapper(Storage, T) : IMiddlewareWrapper!Storage {
  @safe pure nothrow {
    T instance;

    this(T instance) {
      this.instance = instance;
    }

    string name() {
      return T.stringof;
    }

    Wrapped!Storage[] wrap(string[] attributes)() {
      return wireMiddlewares!(Storage, attributes)(instance);
    }

    Wrapped!Storage[] getItem() {
      return this.wrap!([ "any", "getItem", "get" ]);
    }

    Wrapped!Storage[] getList() {
      return this.wrap!([ "any", "getList", "get" ]);
    }

    Wrapped!Storage[] create() {
      return this.wrap!([ "any", "create", "post" ]);
    }

    Wrapped!Storage[] update() {
      return this.wrap!([ "any", "update", "patch" ]);
    }

    Wrapped!Storage[] replace() {
      return this.wrap!([ "any", "replace", "put" ]);
    }

    Wrapped!Storage[] delete_() {
      return this.wrap!([ "any", "delete_" ]);
    }

    Wrapped!Storage[] mapper() {
      return this.wrap!([ "mapper" ]);
    }
  }

  @safe {
    void updateRule(ref CrateRule rule) {
      enum description = Describe!T;
      enum methods = where(description.methods).anyOfAttributes(["rule"]);

      static foreach(method; methods) {
        mixin("instance." ~ method.name ~ "(rule);");
      }
    }
  }
}

string wireByType(alias storageDescription)(string fullyQualifiedName) {
  bool isMatch;

  static foreach(storageMember; storageDescription.properties) {
    isMatch = storageMember.type.fullyQualifiedName == fullyQualifiedName ||
      `const(` ~ storageMember.type.fullyQualifiedName ~ `)` == fullyQualifiedName;

    if(isMatch) {
      return "storage." ~ storageMember.name;
    }
  }

  return "";
}

string wireByTypeDebug(alias storageDescription)(string fullyQualifiedName) {
  string result = "(";
  string glue;
  bool isMatch;

  static foreach(storageMember; storageDescription.properties) {
    isMatch = storageMember.type.fullyQualifiedName == fullyQualifiedName ||
      `const(` ~ storageMember.type.fullyQualifiedName ~ `)` == fullyQualifiedName;

    if(isMatch) {
      result ~= glue ~ storageMember.type.fullyQualifiedName ~ " == " ~ fullyQualifiedName;
    } else {
      result ~= glue ~ storageMember.type.fullyQualifiedName ~ " != " ~ fullyQualifiedName;
    }

    glue = ", ";
  }

  return result ~ ")";
}

string localWire(Callable callable, DescribeAggregate storageDescription)() {
  string result = "";
  string glue = "";
  string strParameter;

  foreach(i, parameter; callable.parameters) {
    strParameter = "";
    strParameter = wireByType!storageDescription(parameter.type.fullyQualifiedName);

    if(strParameter == "") {
      strParameter = "storage.request.query.parseQueryParams!(Parameter[" ~ i.to!string ~ "])()";
    }

    result ~= glue ~ strParameter;
    glue = ", ";
  }

  return result;
}

OpenApiParameter[] queryParams(Callable callable, DescribeAggregate storageDescription, Parameter...)() @trusted {
  OpenApiParameter[] parameters;

  static foreach(i, parameter; callable.parameters) {
    static if(wireByType!storageDescription(parameter.type.fullyQualifiedName) == "") {{
      mixin("enum currentDescription = Describe!(Parameter[" ~ i.to!string ~ "]);");
      parameters ~= extractQueryParams!(currentDescription);
    }}
  }

  return parameters;
}

auto storageWrapperFactory(Storage, Callable callable, bool canBeCalledWithStorage, string storageCall, T, Parameter...)(T instance) @trusted {

  InputRange!string storageWrapper(Storage storage) @trusted {
    enum storageDescription = Describe!Storage;

    static if(!callable.returns.isBasicType && !callable.returns.isBuiltinType) {
      enum typeStorage = wireByType!storageDescription(callable.returns.fullyQualifiedName);
    } else {
      enum typeStorage = "";
    }

    static if(canBeCalledWithStorage) {
      static if(typeStorage != "") {
        mixin(typeStorage ~ " = " ~ storageCall ~ ";");
        return null;
      } else static if(callable.returns.fullyQualifiedName != "void") {
        mixin(`return [` ~ storageCall ~ `.to!string].inputRangeObject;`);
      } else {
        mixin(storageCall ~ ";");
        return null;
      }
    } else {
      assert(false, "You can not call `" ~ T.stringof ~ "." ~ callable.name ~ "`. The generated code is: " ~ storageCall);
    }
  }

  return &storageWrapper;
}

auto stringWrapperFactory(Storage, Callable callable, bool canBeCalledWithString, T, Parameter...)(T instance) @trusted {

  InputRange!string stringWrapper(string value) @trusted {
    static if(canBeCalledWithString) {
      static if(callable.name == "") {
        enum call = "instance(value.to!(Parameter[0]))";
      } else {
        enum call = "instance." ~ callable.name ~ "(value.to!(Parameter[0]))";
      }

      static if(callable.returns.fullyQualifiedName != "void") {
        mixin(`return [` ~ call ~ `.to!string].inputRangeObject;`);
      } else {
        mixin(call ~ ";");
        return null;
      }
    } else {
      assert(false, "You can not call this wrapper using a `string` param.");
    }
  }

  return &stringWrapper;
}

Wrapped!Storage wire(Storage, Callable callable, T)(T instance, bool hasQueries) if(isAggregateType!(T)) {
  enum storageDescription = Describe!Storage;

  alias overloads = __traits(getOverloads, T, callable.name);
  alias Parameter = Parameters!(overloads[callable.overloadIndex]);

  enum storageCall = "instance." ~ callable.name ~ "(" ~ localWire!(callable, storageDescription) ~ ")";
  void test(bool b)(Storage storage) { mixin(storageCall ~ ";"); }

  enum canBeCalledWithString = Parameter.length == 1 && (isSomeString!(Parameter[0]) || isBasicType!(Parameter[0]));
  enum canBeCalledWithStorage = Parameter.length == 0 || __traits(compiles, test!true(new Storage())) || __traits(compiles, test!true(Storage()));

  auto storageWrapper = storageWrapperFactory!(Storage, callable, canBeCalledWithStorage, storageCall, T, Parameter)(instance);
  auto stringWrapper = stringWrapperFactory!(Storage, callable, canBeCalledWithString, T, Parameter)(instance);

  return Wrapped!Storage(storageWrapper, stringWrapper, T.stringof ~ "." ~ callable.name, callable, hasQueries, canBeCalledWithStorage, canBeCalledWithString, null, queryParams!(callable, storageDescription, Parameter));
}

Wrapped!Storage wire(Storage, Callable callable, T)(T instance, bool hasQueries) if(isCallable!(T)) {
  enum storageDescription = Describe!Storage;

  alias Parameter = Parameters!(instance);

  enum storageCall = "instance(" ~ localWire!(callable, storageDescription) ~ ")";
  void test(bool b)(Storage storage) { mixin(storageCall ~ ";"); }

  enum canBeCalledWithString = Parameter.length == 1 && (isSomeString!(Parameter[0]) || isBasicType!(Parameter[0]));
  enum canBeCalledWithStorage = Parameter.length == 0 || __traits(compiles, test!true(new Storage())) || __traits(compiles, test!true(Storage()));

  auto storageWrapper = storageWrapperFactory!(Storage, callable, canBeCalledWithStorage, storageCall, T, Parameter)(instance);
  auto stringWrapper = stringWrapperFactory!(Storage, callable, canBeCalledWithString, T, Parameter)(instance);

  return Wrapped!Storage(storageWrapper, stringWrapper, T.stringof ~ "." ~ callable.name, callable, hasQueries, canBeCalledWithStorage, canBeCalledWithString, null, queryParams!(callable, storageDescription, Parameter));
}

auto wireMiddlewares(Storage, string[] attributes, T)(T instance) {
  enum description = Describe!T;
  enum storageDescription = Describe!Storage;

  Wrapped!Storage[] items = [];
  enum methods = where(description.methods).anyOfAttributes(attributes);

  static foreach(method; methods) {
    static if(!method.hasLinksWith("IQuery")) {
      items ~= wire!(Storage, method)(instance, false);
    }
  }

  static foreach(method; methods) {
    static if(method.hasLinksWith("IQuery")) {
      items ~= wire!(Storage, method)(instance, true);
    }
  }

  return items;
}

bool hasLinksWith(Callable callable, string typeName) {
  if(callable.returns.unqualName == typeName) {
    return true;
  }

  if(callable.has.anyParameterOfType(typeName)) {
    return true;
  }

  return false;
}

OpenApiParameter[] extractQueryParams(DescribeAggregate aggregate)() @safe pure nothrow {
  OpenApiParameter[] parameters;

  static foreach(property; aggregate.properties) {{
    import std.stdio;

    auto parameter = OpenApiParameter();
    parameter.name = property.name;
    parameter.in_ = ParameterIn.query;

    static foreach(attribute; property.attributes) {{
      static if(attribute.type.unqualName == "DescribeAttribute") {
        mixin(`auto attr = ` ~ attribute.name ~ `;`);
        parameter.description = attr.description;
      }

      static if(attribute.type.unqualName == "Example") {
        mixin(`auto attr = ` ~ attribute.name ~ `;`);
        parameter.examples[attr.value] = attr;
      }
    }}

    parameters ~= parameter;
  }}

  return parameters;
};

OpenApiParameter[] extractQueryParams(DescribeType parameter)() @safe pure nothrow {
  return [];
};