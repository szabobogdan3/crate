/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.headers;

import vibe.http.common;
import std.stdio;
import std.string;
import std.array;
import std.algorithm;
import vibe.container.dictionarylist;

void addHeaderValues(HTTPResponse response, const string name, string[] values) {
  if(name in response.headers) {
    values = response.headers[name].split(",") ~ values;
  }

  response.headers[name] = values.map!(a => a.strip).uniq.filter!(a => a != "").join(", ");
}

@("it should create the header if does not exist")
unittest {
  auto response = new HTTPResponse;

  response.addHeaderValues("key1", ["value"]);
  response.addHeaderValues("key2", ["value1", "value2"]);

  assert(response.headers["key1"] == "value");
  assert(response.headers["key2"] == "value1, value2");
}

@("it should append the header values if already exists")
unittest {
  auto response = new HTTPResponse;

  response.addHeaderValues("key1", ["value1"]);
  response.addHeaderValues("key1", ["value2"]);

  response.addHeaderValues("key2", ["value1", "value2"]);
  response.addHeaderValues("key2", ["value3"]);

  assert(response.headers["key1"] == "value1, value2");
  assert(response.headers["key2"] == "value1, value2, value3");
}


@("it should add twice the same value")
unittest {
  auto response = new HTTPResponse;

  response.addHeaderValues("key", ["value"]);
  response.addHeaderValues("key", ["value"]);

  assert(response.headers["key"] == "value");
}

bool ifNoneMatch(DictionaryList!(string,false,12L,false) headers, string etag) {
  if("If-None-Match" !in headers) {
    return false;
  }

  auto isNoneMatch = headers["If-None-Match"];
  auto isMatch = isNoneMatch == `"` ~ etag ~ `"` || isNoneMatch == etag;

  return isMatch;
}