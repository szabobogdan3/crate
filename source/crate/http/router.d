/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.router;

import crate.base;
import crate.ctfe;
import crate.error;
import crate.api.rest.policy;
import crate.resource.base;
import crate.http.queryParams;
import crate.http.request;

import crate.json;
import vibe.data.json;
import crate.lazydata.base;
import crate.error;

import vibe.inet.url;
import vibe.http.router;
import vibe.core.log;
import vibe.stream.operations : readAllUTF8;

import vibeauth.authenticators.OAuth2;

import std.traits;
import std.conv;
import std.meta;
import std.exception;
import std.string;
import std.range;
import std.functional;
import std.algorithm;

import described : Describe = describe;
import introspection.callable;
import crate.http.wrapper;

public import crate.http.operations.base;
public import crate.http.operations.getItem;
public import crate.http.operations.getList;
public import crate.http.operations.deleteItem;
public import crate.http.operations.replaceItem;
public import crate.http.operations.updateItem;
public import crate.http.operations.createItem;
public import crate.http.operations.getResource;
public import crate.http.operations.setResource;
public import crate.http.operations.customResource;
public import crate.http.operations.itemMethod;
public import crate.http.operations.itemOperation;

///
auto crateSetup(T)(URLRouter router) {
  return new CrateRouter!T(router);
}

///
auto crateSetup(URLRouter router) {
  return new CrateRouter!RestApi(router);
}

///
class CrateRouter(RouterPolicy) {
  private {
    bool[string] mimeList;
    CrateRouterBis!(RouterPolicy, DefaultStorage) crateRouter;
  }

  URLRouter router;

  ///
  this(URLRouter router) {
    this.router = router;
    crateRouter = new CrateRouterBis!(RouterPolicy, DefaultStorage);
    router.any("*", requestErrorHandler(&crateRouter.handler));
  }

  /// Add authentication
  CrateRouter enable(OAuth2 auth) {
    router.any("*", &auth.tokenHandlers);

    return this;
  }

  private void addResources(Policy, string prefix, U, Type, T...)(Crate!Type crate, T middlewareList) {
    static if(prefix == "") {
      enum memberPath = "";
    } else {
      enum memberPath = prefix ~ ".";
    }

    static foreach(member; __traits(allMembers, U)) {
      static if(__traits(getProtection, __traits(getMember, U, member)) == "public" &&
        !isCallable!(__traits(getMember, U, member)) &&
        member != "Monitor" && __traits(compiles, typeof(__traits(getMember, U, member)))) {{
        alias MemberType = typeof(__traits(getMember, U, member));

        static if(staticIndexOf!(CrateResource, InterfacesTuple!MemberType) != -1) {
          version(crate_verbose) pragma(msg, "🥞 resource ", Type, ".", memberPath ~ member);
          auto operation = new ResourceOperation!(RouterPolicy, Type, memberPath ~ member)(router, &crate.getItem, &crate.updateItem);
          operation.bind(middlewareList);

          rules ~= operation.rules;
        }

        static if(is(MemberType == class) || is(MemberType == struct)) {
          static if(!__traits(hasMember, MemberType, "_id")) {
            addResources!(Policy, memberPath ~ member, MemberType)(crate, middlewareList);
          }
        }

        static if(listType!(MemberType).length == 1 && (is(ModelType!MemberType == class) || is(ModelType!MemberType == struct))) {
          static if(!__traits(hasMember, ModelType!MemberType, "_id")) {
            addResources!(Policy, memberPath ~ member ~ "[:index]", ModelType!MemberType)(crate, middlewareList);
          }
        }
      }}
    }
  }

  CrateRule[] rules() {
    return crateRouter.rules;
  }

  /// Add HTTP routes to interact with a crate using the default Policy
  CrateRouter add(Type, T...)(Crate!Type crate, T middlewareList) {
    enforce(crate !is null, "Got a null crate `" ~ Type.stringof ~ "`");
    crateGetters[Type.stringof] = &crate.getItem;

    auto tmp = crateRouter.using(crate);

    static foreach(member; __traits(allMembers, Type)) {
      static if(!IsEnumType!(Type, member)) {
        static if(isAction!(typeof(__traits(getMember, Type, member)), member)) {{
          tmp.expose!member;
        }}
      }
    }

    static foreach (middleware; middlewareList) {
      tmp.and(middleware);
    }

    return this;
  }

  /// Prepare an object route
  TypeRouter!(RouterPolicy, Type, DefaultStorage) prepare(Type)(Crate!Type crate) {
    enforce(crate !is null, "Got a null crate `" ~ Type.stringof ~ "`");
    crateGetters[Type.stringof] = &crate.getItem;

    auto tmp = crateRouter.using(crate);

    static foreach(member; __traits(allMembers, Type)) {
      static if(!IsEnumType!(Type, member)) {
        static if(isAction!(typeof(__traits(getMember, Type, member)), member)) {{
          tmp.expose!member;
        }}
      }
    }

    return tmp;
  }

  ///
  BlankRouter!DefaultStorage blank() {
    return crateRouter.blank;
  }
}

///
class CrateRouterBis(Policy, Storage) {
  ITypeRouter!Storage[] types;

  TypeRouter!(Policy, Type, Storage) using(Type)(Crate!Type crate) {
    auto typeRouter = new TypeRouter!(Policy, Type, Storage)(crate);
    this.types ~= typeRouter;

    return typeRouter;
  }

  ///
  BlankRouter!DefaultStorage blank() {
    auto typeRouter = new BlankRouter!DefaultStorage;
    this.types ~= typeRouter;

    return typeRouter;
  }

  CrateRule[] rules() {
    CrateRule[] result;

    foreach(type; types) {
      result ~= type.rules;
    }

    return result;
  }

  void options(string routeName, HTTPServerResponse response) {
    string[] methods;
    string[] headers;

    foreach (type; types) {
      methods ~= type.methods(routeName);
      headers ~= type.headers(routeName);
    }

    if(methods.length == 0) {
      return;
    }

    methods ~= "OPTIONS";

    response.headers["Access-Control-Allow-Origin"] = "*";
    response.headers["Access-Control-Allow-Headers"] = headers.uniq.join(", ");
    response.headers["Access-Control-Allow-Methods"] = methods.uniq.join(", ");
  }

  ApiOperation!Storage match(HTTPMethod method, string routeName) {
    if(types.length == 0) {
      return null;
    }

    foreach (type; types) {
      auto match = type.match(method, routeName);

      if(match !is null) {
        return match;
      }
    }

    return null;
  }

  void handler(HTTPServerRequest request, HTTPServerResponse response) {
    this.options(request.requestPath.toString, response);

    if(request.method == HTTPMethod.OPTIONS) {
      if("Access-Control-Allow-Origin" !in response.headers) {
        return;
      }

      response.statusCode = 204;
      response.writeVoidBody();
      return;
    }

    auto operation = match(request.method, request.requestPath.toString);

    if(operation is null) {
      return;
    }

    scope storage = new Storage;

    storage.request = request;
    storage.response = response;

    operation.handle(storage);
  }
}

interface ITypeRouter(Storage) {
  ApiOperation!Storage match(HTTPMethod method, string routeName);
  string[] methods(string routeName);
  string[] headers(string routeName);
  CrateRule[] rules();
}

///
class BlankRouter(Storage) : ITypeRouter!Storage {
  ApiOperation!Storage[] operations;

  ApiOperation!Storage match(HTTPMethod method, string routeName) {
    foreach(operation; operations) {
      if(operation.rule.request.method == method && matchTemplateRoute(routeName, operation.rule.request.path)) {
        return operation;
      }
    }

    return null;
  }

  string[] headers(string routeName) {
    return ["Content-Type, Authorization, If-None-Match"];
  }

  string[] methods(string routeName) {
    string[] result;

    foreach(operation; operations) {
      if(matchTemplateRoute(routeName, operation.rule.request.path)) {
        result ~= operation.rule.request.method.to!string;
      }
    }

    return result;
  }

  CrateRule[] rules() {
    CrateRule[] result;

    foreach(operation; operations) {
      result ~= operation.rule;
    }

    return result;
  }

  auto withCustomOperation(ApiOperation!Storage customOperation) {
    operations ~= customOperation;

    return this;
  }

  auto and(T)(T middlewares) {
    auto wrappedCalls = new MiddlewareWrapper!(Storage, T)(middlewares);

    foreach(operation; operations) {
      operation.addMiddlewares(wrappedCalls);
    }

    return this;
  }
}

///
class TypeRouter(Policy, Type, Storage) : ITypeRouter!Storage {
  static immutable ModelDescription definition = describe!Type;

  Crate!Type crate;

  private {
    BlankRouter!Storage blankRouter;
  }

  this(Crate!Type crate) {
    this.crate = crate;
    this.blankRouter = new BlankRouter!Storage();

    this.blankRouter.operations ~= new GetItemApiOperation!Storage(crate, Policy.getItem(definition));
    this.blankRouter.operations ~= new GetListApiOperation!Storage(crate, Policy.getList(definition));
    this.blankRouter.operations ~= new DeleteItemApiOperation!(Type, Storage)(crate, Policy.delete_(definition));
    this.blankRouter.operations ~= new ReplaceItemApiOperation!(Type, Storage)(crate, Policy.replace(definition));
    this.blankRouter.operations ~= new UpdateItemApiOperation!(Type, Storage)(crate, Policy.patch(definition));
    this.blankRouter.operations ~= new CreateItemApiOperation!(Type, Storage)(crate, Policy.create(definition));

    addResources!("", Type);
  }

  private void addResources(string prefix, U)() {
    static if(prefix == "") {
      enum memberPath = "";
    } else {
      enum memberPath = prefix ~ ".";
    }

    static foreach(member; __traits(allMembers, U)) {
      static if(__traits(getProtection, __traits(getMember, U, member)) == "public" && !isCallable!(__traits(getMember, U, member)) && member != "Monitor" && __traits(compiles, typeof(__traits(getMember, U, member)))) {{
        alias MemberType = typeof(__traits(getMember, U, member));
        enum path = memberPath ~ member;

        static if(staticIndexOf!(CrateResource, InterfacesTuple!MemberType) != -1) {
          version(crate_verbose) pragma(msg, "🥞 resource ", Type, ".", path);

          auto getResourceOperation = new GetResourceApiOperation!(Type, MemberType, Storage, path)(crate, Policy.getResource!path(definition));

          this.blankRouter.operations ~= getResourceOperation;
          this.blankRouter.operations ~= new SetResourceApiOperation!(Type, MemberType, Storage, path)(crate, Policy.setResource!path(definition));

          static if(__traits(hasMember, MemberType, "customOperations")) {
            this.blankRouter.operations ~= MemberType.customOperations(getResourceOperation);
          }
        }

        static if(is(MemberType == class) || is(MemberType == struct)) {
          static if(!__traits(hasMember, MemberType, "_id")) {
            addResources!(path, MemberType);
          }
        }

        static if(listType!(MemberType).length == 1 && (is(ModelType!MemberType == class) || is(ModelType!MemberType == struct))) {
          static if(!__traits(hasMember, ModelType!MemberType, "_id")) {
            addResources!(path ~ "[:index]", ModelType!MemberType);
          }
        }
      }}
    }
  }

  auto expose(string methodName)() {
    alias Method = typeof(__traits(getMember, Type, methodName));
    alias Return = ReturnType!Method;

    static if(Parameters!Method.length == 0) {
      alias Parameter = void;
    } else static if(Parameters!Method.length == 1) {
      alias Parameter = Parameters!Method[0];
    } else {
      static assert(false, "actions work only with no or one parameter");
    }

    this.blankRouter.operations ~= new ItemMethodApiOperation!(Type, Storage, methodName)(crate, Policy.action!(Return, Parameter, methodName)(definition));

    return this;
  }

  auto itemOperation(string operationId, T)(T operation) {
    auto operationDescription = describeCallable!T;
    auto rule = Policy.itemOperation!(operationId)(definition, operationDescription);

    this.blankRouter.operations ~= new ItemApiOperation!(Type, Storage, T)(crate, rule, operation);

    return this;
  }

  string[] methods(string routeName) {
    return blankRouter.methods(routeName);
  }

  string[] headers(string routeName) {
    return blankRouter.headers(routeName);
  }

  CrateRule[] rules() {
    return blankRouter.rules;
  }

  ApiOperation!(DefaultStorage) match(HTTPMethod method, string routeName) {
    return blankRouter.match(method, routeName);
  }

  TypeRouter!(Policy, Type, Storage) and(T)(T middlewares) {
    blankRouter.and(middlewares);

    return this;
  }

  TypeRouter!(Policy, Type, DefaultStorage) withCustomOperation(ApiOperation!Storage customOperation) {
    blankRouter.withCustomOperation(customOperation);

    return this;
  }
}

///
bool matchTemplateRoute(string name, string templateRoute) {
  scope namePieces = name.split("/");
  scope templatePieces = templateRoute.split("/");

  if(templatePieces.length != namePieces.length) {
    return false;
  }

  foreach(i, item; templatePieces) {
    if(item.length > 0 && item[0] == ':') continue;

    if(item != namePieces[i]) {
      return false;
    }
  }

  return true;
}
