/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.itemOperation;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.lazydata.base;
import crate.http.request;

import described : Describe = describe;

import vibe.stream.operations;
import vibe.http.server;

import std.traits;
import std.conv;
import std.functional;

class ItemApiOperation(Type, Storage, T) : CrateOperation!Storage {

  private {
    Wrapped!Storage wiredCall;
    enum description = Describe!(T);
  }

  this(CrateAccess crate, CrateRule rule, T callable) {
    super(crate, rule);
    wiredCall = wire!(Storage, description, T)(callable, false);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getItem";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    storage.item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    if(wiredCall.canBeCalledWithString) {
      wiredCall.call(storage.request.bodyReader.readAllUTF8);
    } else {
      wiredCall.call(storage);
    }

    if(storage.response.headerWritten) {
      return;
    }

    if(wiredCall.returnValue !is null) {
      storage.response.writeRangeBody(wiredCall.returnValue, rule.response.statusCode, rule.response.mime);
      return;
    }

    storage.response.writeBody("", rule.response.statusCode, rule.response.mime);
  }
}