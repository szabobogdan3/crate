/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.itemMethod;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.lazydata.base;
import crate.http.request;

import described : Describe = describe;
import introspection.aggregate;

import vibe.stream.operations;
import vibe.http.server;

import std.traits;
import std.conv;
import std.functional;

class ItemMethodApiOperation(Type, Storage, string methodName) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getItem";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    mixin(`enum description = Describe!(Type.` ~ methodName ~ `);`);
    auto item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    auto value = Lazy!Type(item, (&itemResolver).toDelegate);
    mixin("auto nativeValue = value.toType;");

    auto wiredCall = wire!(Storage, description)(nativeValue, false);

    if(wiredCall.canBeCalledWithString) {
      wiredCall.call(storage.request.bodyReader.readAllUTF8);
    } else {
      wiredCall.call(storage);
    }

    value.fromModel(nativeValue);
    crate.updateItem(value.toJson);

    if(storage.response.headerWritten) {
      return;
    }

    if(wiredCall.returnValue !is null) {
      storage.response.writeRangeBody(wiredCall.returnValue, rule.response.statusCode, rule.response.mime);
      return;
    }

    storage.response.writeBody("", rule.response.statusCode, rule.response.mime);
  }
}