/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.createItem;

import std.functional;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.error;
import crate.json;
import crate.lazydata.base;

import vibe.data.json;

class CreateItemApiOperation(Type, Storage) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"create";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    handleMiddlewares!"create"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    auto clientData = rule.response.serializer.normalise("", storage.request.json);
    Json result;

    try {
      auto value = Lazy!Type(clientData, (&itemResolver).toDelegate);
      value = Lazy!Type.fromModel(value.toType);
      result = crate.addItem(value.toJson);
    } catch (JSONException e) {
      throw new CrateValidationException("Can not deserialize data. " ~ e.msg, e.file, e.line);
    }
    storage.properties.itemId = result["_id"].to!string;
    this.applyRule(storage);

    auto responseData = rule.response.serializer.denormalise(result, this.mapper(storage));
    storage.response.writeRangeBody(responseData, rule.response.statusCode, rule.response.mime);
  }
}
