/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.getItem;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.http.headers;

/// Handles an operation like get or update
class GetItemApiOperation(Storage) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getItem";

    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);
    if(storage.response.headerWritten) {
      return;
    }

    if(storage.etag) {
      storage.response.headers["ETag"] = `"` ~ storage.etag ~ `"`;
    }

    if(ifNoneMatch(storage.request.headers, storage.etag)) {
      storage.response.statusCode = 304;
      storage.response.contentType = rule.response.mime;
      storage.response.writeVoidBody;
      return;
    }

    this.applyRule(storage);

    auto responseData = rule.response.serializer.denormalise(item, this.mapper(storage));
    storage.response.writeRangeBody(responseData, rule.response.statusCode, rule.response.mime);
  }
}
