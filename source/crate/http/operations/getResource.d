/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.getResource;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.http.request;
import crate.error;
import crate.base;
import crate.lazydata.base;
import crate.http.headers;

import vibe.data.json;

import std.functional;
import std.conv;

/// Get a resource field
class GetResourceApiOperation(Type, ResourceType, Storage, string path) : ResourceApiOperation!(ResourceType, Storage) {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  Lazy!Type getItem(Storage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return Lazy!Type();
    }

    return Lazy!Type(item, (&itemResolver).toDelegate);
  }

  override ResourceType getResource(Storage storage) {
    auto value = getItem(storage);
    if(storage.response.headerWritten) {
      return null;
    }

    mixin("auto nativeValue = value.toType;");
    mixin("auto obj = nativeValue" ~ createResourceAccess!path ~ ";");

    return obj;
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getItem";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    auto value = getItem(storage);
    if(storage.response.headerWritten) {
      return;
    }

    mixin("auto nativeValue = value.toType;");
    mixin("auto obj = nativeValue" ~ createResourceAccess!path ~ ";");

    enforce!CrateNotFoundException(obj !is null, "The resource does not exist.");

    this.applyRule(storage);

    storage.response.headers["Content-Type"] = obj.contentType;
    storage.response.headers["ETag"] = `"` ~ obj.etag ~ `"`;
    storage.response.headers["Cache-Control"] = "public";

    if(ifNoneMatch(storage.request.headers, obj.etag)) {
      storage.response.statusCode = 304;
      storage.response.contentType = obj.contentType;
      storage.response.writeVoidBody;
      return;
    }

    if(obj.hasSize) {
      storage.response.headers["Content-Length"] = obj.size.to!string;
    }

    storage.response.statusCode = rule.response.statusCode;
    obj.write(storage.response.bodyWriter);

    mixin("auto originalValue = value.toJson" ~ createJsonResourceAccess!path ~ ".to!string;");

    if(originalValue != obj.toString) {
      crate.updateItem(nativeValue.serializeToJson);
    }
  }
}