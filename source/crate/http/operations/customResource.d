/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.customResource;

import crate.http.operations.base;
import crate.http.operations.getResource;
import crate.base;
import crate.lazydata.base;
import crate.http.wrapper;

import described : Describe = describe;

import vibe.data.json;

import std.functional;
import std.conv;

class CustomResourceOperation(ResourceType, Storage, string methodName) : CrateOperation!Storage {
  alias ParentOperation = ResourceApiOperation!(ResourceType, Storage);
  ParentOperation parentOperation;

  this(string path, ParentOperation parentOperation) {
    this.parentOperation = parentOperation;
    auto rule = parentOperation.rule;

    rule.request.parameters = parentOperation.rule.request.parameters;
    rule.request.path ~= path;

    super(parentOperation.crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getItem";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    auto resource = parentOperation.getResource(storage);
    mixin(`enum description = Describe!(ResourceType.` ~ methodName ~ `);`);

    auto wiredCall = wire!(Storage, description, ResourceType)(resource, false);

    wiredCall.call(storage);

    if(storage.response.headerWritten) {
      return;
    }

    if(wiredCall.returnValue !is null) {
      storage.response.writeRangeBody(wiredCall.returnValue, rule.response.statusCode, rule.response.mime);
    } else {
      storage.response.writeBody("", rule.response.statusCode, rule.response.mime);
    }
  }
}