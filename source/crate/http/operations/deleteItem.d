/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.deleteItem;

import std.functional;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.ctfe;
import crate.lazydata.base;

import vibe.core.log;

class DeleteItemApiOperation(Type, Storage) : CrateOperation!Storage {
  enum resources = resourceMembers!Type;

  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  void removeRelatedResources(Storage storage) {
    auto value = Lazy!Type(storage.query.exec.front, (&itemResolver).toDelegate).toType;

    static foreach(member; resources) {
      try {
        mixin("value." ~ member ~ ".remove();");
      } catch(Exception e) {
        logError("Can't delete the `" ~ member ~ "` storage.resource. " ~ e.message);
      }
    }
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"delete_";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    this.prepareItemOperation!"delete_"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    static if(resources.length > 0) {
      removeRelatedResources(storage);
    }

    this.applyRule(storage);

    crate.deleteItem(storage.properties.itemId);
    storage.response.writeBody("", rule.response.statusCode, rule.response.mime);
  }
}
