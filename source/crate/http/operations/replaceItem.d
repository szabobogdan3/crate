/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.replaceItem;

import std.functional;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.error;
import crate.lazydata.base;

import vibe.data.json;
import vibe.core.log;

class ReplaceItemApiOperation(Type, Storage) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"replace";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    logTrace("Preparing replace operation.");
    this.prepareItemOperation!"replace"(storage);

    if(storage.response.headerWritten) {
      logTrace("The header is already written.");
      return;
    }

    storage.query = crate.getItem(storage.properties.itemId);

    auto clientData = rule.response.serializer.normalise(storage.properties.itemId, storage.request.json);

    Json result;

    try {
      auto value = Lazy!Type(clientData, (&itemResolver).toDelegate);
      value = Lazy!Type.fromModel(value.toType);
      result = crate.updateItem(value.toJson.clone);
    } catch (JSONException e) {
      throw new CrateValidationException("Can not deserialize data. " ~ e.msg, e.file, e.line);
    }

    this.applyRule(storage);

    auto responseData = rule.response.serializer.denormalise(result, this.mapper(storage));
    storage.response.writeRangeBody(responseData, rule.response.statusCode, rule.response.mime);
  }
}
