/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.setResource;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.lazydata.base;

import vibe.data.json;

import std.functional;
import std.conv;


class SetResourceApiOperation(Type, ResourceType, Storage, string path) : ResourceApiOperation!(ResourceType, Storage) {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override ResourceType getResource(Storage storage) {
    assert(false, "not implemented");
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getItem";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    auto item = this.prepareItemOperation!"getItem"(storage);

    if(storage.response.headerWritten) {
      return;
    }

    auto value = Lazy!Type(item, (&itemResolver).toDelegate);
    mixin("auto nativeValue = value.toType;");
    mixin("auto obj = nativeValue" ~ createResourceAccess!path ~ ";");

    foreach(key, file; storage.request.files.byKeyValue) {
      obj.read(file);
      break;
    }
    this.applyRule(storage);

    crate.updateItem(value.serializeToJson);

    storage.response.writeBody("", rule.response.statusCode);
  }
}
