/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.base;

import std.string;
import std.exception;
import std.range;
import std.algorithm;
import std.datetime;
import std.conv;
import std.digest.crc;

import vibe.http.server;
import vibe.inet.url;
import vibe.data.json;
import vibe.core.log;

import crate.base;
import crate.error;
import crate.ctfe;
import crate.http.request;
import crate.http.wrapper;
import crate.lifecycle;

version(unittest) {
  import fluent.asserts;
}

struct RequestProperties {
  string itemId;
}

class DefaultStorage {
  HTTPServerRequest request;
  HTTPServerResponse response;

  CrateHttpRequest crateRequest;
  CrateHttpResponse crateResponse;

  RequestProperties properties;

  Json item;
  IQuery query;
  string etag;
}

/// Handles an operation like get or update
class ApiOperation(Storage) {
  alias Middleware = void delegate(Storage);

  CrateRule rule;

  IMiddlewareWrapper!Storage[] middlewares;
  private size_t lastRuleIndex;

  this(CrateRule rule) {
    this.rule = rule;
  }

  void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    this.middlewares ~= middlewares;
  }

  void updateRule(string attribute)() {
    foreach(i; lastRuleIndex..this.middlewares.length) {
      auto item = this.middlewares[i];

      mixin("auto middlewareList = item." ~ attribute ~ ";");
      foreach(index, middleware; middlewareList) {
        middleware.updateRule(rule);
      }

      foreach(index, mapper; item.mapper) {
        mapper.updateRule(rule);
      }
    }

    lastRuleIndex = this.middlewares.length;
  }

  auto mapper(Storage storage) {
    Json innerMapper(Json item) nothrow @safe {
      try {
        storage.item = item;

        foreach(middleware; this.middlewares) {
          foreach(mapper; middleware.mapper) {
            mapper.call(storage);
          }
        }
      } catch(Exception) {
        return Json();
      }

      return storage.item;
    }

    return &innerMapper;
  }

  void handleMiddlewares(string attribute)(Storage storage) {
    handleMiddlewaresWithoutQueries!attribute(storage);
    handleMiddlewaresWithQueries!attribute(storage);
  }

  void handleMiddlewaresWithQueries(string attribute)(Storage storage) {
    foreach(item; this.middlewares) {
      mixin("auto middlewareList = item." ~ attribute ~ ";");
      foreach(middleware; middlewareList) {
        if(storage.response.headerWritten) {
          logDebug("Skip `" ~ item.name ~ "` middleware. Headers sent.");
          return;
        }

        if(middleware.hasQueries) {
          logDebug("Running the `" ~ item.name ~ "` middleware");

          auto begin = Clock.currTime;
          middleware.call(storage);
          auto diff = Clock.currTime - begin;

          CrateLifecycle.instance.middlewareBenchmark(rule.request.path, middleware.name, diff);
        } else {
          logDebug("Skip `" ~ item.name ~ "` middleware. Has no query.");
        }
      }
    }
  }

  void handleMiddlewaresWithoutQueries(string attribute)(Storage storage) {
    foreach(item; this.middlewares) {
      mixin("auto middlewareList = item." ~ attribute ~ ";");
      foreach(middleware; middlewareList) {
        if(storage.response.headerWritten) {
          logDebug("Skip `" ~ item.name ~ "` middleware. Headers sent.");
          return;
        }

        if(!middleware.hasQueries) {
          logDebug("Running the `" ~ item.name ~ "` middleware");

          auto begin = Clock.currTime;
          middleware.call(storage);
          auto diff = Clock.currTime - begin;

          CrateLifecycle.instance.middlewareBenchmark(rule.request.path, middleware.name, diff);
        } else {
          logDebug("Skip `" ~ item.name ~ "` middleware. Has query.");
        }
      }
    }

    if("id" in storage.request.params && storage.request.params["id"] != storage.properties.itemId) {
      storage.properties.itemId = storage.request.params["id"];
    }
  }

  void applyRule(Storage storage) {
    auto url = URL(storage.request.fullURL.toString);
    url.pathString = "";
    string baseUri = url.toString;

    foreach(key, value; rule.response.headers) {
      value = value
        .replace(":id", storage.properties.itemId)
        .replace(":base_uri", baseUri);
      storage.response.headers[key] = value;
    }
  }

  abstract void handle(Storage storage);
}

/// Handles an operation like get or update using crates
class CrateOperation(Storage) : ApiOperation!Storage {
  alias Middleware = void delegate(Storage);

  CrateAccess crate;
  CrateRule rule;

  this(CrateAccess crate, CrateRule rule) {
    this.crate = crate;
    this.rule = rule;

    super(rule);
  }

  Json prepareItemOperation(string attribute)(Storage storage) {
    logDebug("Prepare item operation for `" ~ attribute ~ "`");
    storage.properties.itemId = getPathVar!"id"(storage.request.requestPath.to!string, rule.request.path);
    storage.request.params["id"] = storage.properties.itemId;

    logDebug("Handle middlewares without queries.");
    handleMiddlewaresWithoutQueries!attribute(storage);
    if(storage.response.headerWritten) {
      logDebug("Header is already written.");
      return Json();
    }

    logDebug("Handle middlewares with queries.");
    storage.query = crate.get.where("_id").equal(ObjectId.fromString(storage.properties.itemId)).and;
    handleMiddlewaresWithQueries!attribute(storage);

    if(storage.response.headerWritten) {
      logDebug("Header is already written.");
      return Json();
    }

    enforce(storage !is null, "The storage was not set!");

    auto range = storage.query.exec;
    enforce!CrateNotFoundException(!range.empty, "There is no item with id `" ~ storage.properties.itemId ~ "`");

    logDebug("Prepared item operation for `" ~ attribute ~ "`");
    auto item = range.front;

    if("hash" in item) {
      storage.etag = item["hash"].to!string;
    }

    if("hash" !in item) {
      storage.etag = hexDigest!CRC32(item.to!string).to!string;
    }

    return item;
  }

  void prepareListOperation(string attribute)(Storage storage) {
    storage.properties.itemId = "";
    storage.query = crate.get;

    handleMiddlewares!attribute(storage);
  }
}

version(unittest) {
  import vibe.core.stream;
  import vibe.stream.memory;
  import vibe.http.common;
  import crate.collection.memory;
  import std.experimental.allocator.gc_allocator;
  import std.experimental.allocator;
  import vibe.container.internal.utilallocator: RegionListAllocator;

  class TestCrateOperation(Storage) : CrateOperation!Storage {
    this(CrateAccess crate, CrateRule rule) {
      super(crate, rule);
    }
    override void handle(Storage storage) {
    }
  }

  class MockConnectionStream : ConnectionStream {
    void close() @safe { }
    bool waitForData (core.time.Duration timeout = 0.seconds) @safe { return true; }
    void finalize() @safe {}
    void flush() @safe {}
    const(ubyte)[] peek() @safe { return []; }
    ulong read (scope ubyte[] dst, IOMode mode) @safe { return 0; }
    ulong write (scope const(ubyte)[] bytes, IOMode mode) @safe { return 0; }

    bool empty() @property @safe { return true; }
    ulong leastSize() @property @safe { return 0; }
    bool dataAvailableForRead() @property @safe { return false; }
    bool connected() const @property @safe { return true; }
  }
}

/// etag when the item has a hash field
unittest {
  struct Record {
    ObjectId _id;
    string hash;
  }

  auto data = new MemoryCrate!Record;
  data.addItem(`{
    "_id": "000000000000000000000001",
    "hash": "test-hash"
  }`.parseJsonString);

  auto rule = CrateRule();
  rule.request.path = "/:id";

  auto operation = new TestCrateOperation!DefaultStorage(data, rule);

  auto conn = createMemoryStream([]);
  auto storage = new DefaultStorage;
  auto allocator = new RegionListAllocator!(shared(GCAllocator), true)(1024, GCAllocator.instance);
  storage.request = new HTTPServerRequest(Clock.currTime, 80);
  storage.response = new HTTPServerResponse(conn, new MockConnectionStream, new HTTPServerSettings(), allocator);
  storage.request.requestPath = GenericPath!(vibe.core.path.InetPathFormat)("/" ~ ObjectId("1").to!string);

  operation.prepareItemOperation!"getItem"(storage);

  storage.etag.should.equal("test-hash");
}

/// etag when the item has no hash field
unittest {
  struct Record {
    ObjectId _id;
    string other;
  }

  auto data = new MemoryCrate!Record;
  data.addItem(`{
    "_id": "000000000000000000000001",
    "other": "test-hash"
  }`.parseJsonString);

  auto rule = CrateRule();
  rule.request.path = "/:id";

  auto operation = new TestCrateOperation!DefaultStorage(data, rule);

  auto conn = createMemoryStream([]);
  auto storage = new DefaultStorage;
  auto allocator = new RegionListAllocator!(shared(GCAllocator), true)(1024, GCAllocator.instance);
  storage.request = new HTTPServerRequest(Clock.currTime, 80);
  storage.response = new HTTPServerResponse(conn, new MockConnectionStream, new HTTPServerSettings(), allocator);
  storage.request.requestPath = GenericPath!(vibe.core.path.InetPathFormat)("/" ~ ObjectId("1").to!string);

  operation.prepareItemOperation!"getItem"(storage);

  storage.etag.should.equal("1A34A18E");
}

class ResourceApiOperation(ResourceType, Storage) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  abstract ResourceType getResource(Storage storage);
}

string getPathVar(string varName)(string name, string templateRoute) {
  enum var = ":" ~ varName;

  scope namePieces = name.split("/");
  scope templatePieces = templateRoute.split("/");

  foreach(i, item; templatePieces) {
    if(i >= namePieces.length) {
      break;
    }

    if(item == var) {
      return namePieces[i];
    }
  }

  return "";
}

string getPathVar(string varName)(GenericPath!InetPathFormat routePath, string templateRoute) {
  return getPathVar!"id"(routePath.toString, templateRoute);
}

/// get the id from a path value
unittest {
  auto routePath = GenericPath!InetPathFormat("/a/b/c");
  auto result = getPathVar!"id"(routePath, "/a/:id/c");

  expect(result).to.equal("b");
}

///
void writeRangeBody(HTTPServerResponse response, InputRange!string data, int code, string contentType) {
  response.contentType = contentType;
  response.statusCode = code;

  foreach(chunk; data) {
    response.bodyWriter.write(chunk);
  }

  response.bodyWriter.finalize;
}

///
void checkFields(ref Json data, ModelDescription definition) @trusted {
  foreach(field; definition.fields.relations) {
    if(field.listType.length == 0) {
      enforce!CrateValidationException( data[field.publicName].type != Json.Type.object,
        "`" ~ field.publicName ~ "` is a relation and it should contain an id.");
    }
  }
}

string createJsonResourceAccess(string resourcePath)() {
  enum parts = resourcePath.split(".").map!(a => a.split("[")).join;

  string res = "";

  foreach(part; parts) {
    if(part == "") {
      continue;
    }

    string key;

    if(part[0] == ':') {
      key = "getPathVar!(`" ~ part[1 .. $-1] ~ "`)(storage.request.path, rule.request.path).to!ulong";
    } else {
      key = `"` ~ part ~ `"`;
    }


    res ~= "[" ~ key ~ "]";
  }

  return res;
}

string createResourceAccess(string resourcePath)() {
  enum parts = resourcePath.split(".").map!(a => a.split("[")).join;

  string res = "";

  foreach(part; parts) {
    if(part == "") {
      continue;
    }

    if(part[0] == ':') {
      res ~= "[getPathVar!(`" ~ part[1 .. $-1] ~ "`)(storage.request.path, rule.request.path).to!ulong]";
    } else {
      res ~= "." ~ part;
    }
  }

  return res;
}