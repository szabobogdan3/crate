/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.updateItem;

import std.functional;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;
import crate.error;
import crate.json;
import crate.lazydata.base;

import vibe.data.json;

/// Operation used to update parts of the object
class UpdateItemApiOperation(Type, Storage) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"update";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    auto storedData = this.prepareItemOperation!"update"(storage);

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto clientData = rule.response.serializer.normalise(storage.properties.itemId, storage.request.json);

    Json result;

    try {
      auto mixResult = mix(storedData, clientData);
      checkFields(mixResult, rule.request.serializer.definition);

      auto value = Lazy!Type(mixResult, (&itemResolver).toDelegate);
      value = Lazy!Type.fromModel(value.toType);
      result = crate.updateItem(value.toJson.clone);
    } catch (JSONException e) {
      throw new CrateValidationException("Can not deserialize data. " ~ e.msg, e.file, e.line);
    }

    auto responseData = rule.response.serializer.denormalise(result, this.mapper(storage));
    storage.response.writeRangeBody(responseData, rule.response.statusCode, rule.response.mime);
  }
}
