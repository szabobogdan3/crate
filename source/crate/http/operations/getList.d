/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.http.operations.getList;

import crate.http.operations.base;
import crate.http.wrapper;
import crate.base;

/// Handles an operation like get or update
class GetListApiOperation(Storage) : CrateOperation!Storage {
  this(CrateAccess crate, CrateRule rule) {
    super(crate, rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!Storage middlewares) {
    super.addMiddlewares(middlewares);
    this.updateRule!"getList";
    middlewares.updateRule(rule);
  }

  override void handle(Storage storage) {
    this.prepareListOperation!"getList"(storage);

    if(storage.response.headerWritten) {
      return;
    }
    this.applyRule(storage);

    auto responseData = rule.response.serializer.denormalise(storage.query.exec, this.mapper(storage));

    storage.response.writeRangeBody(responseData, rule.response.statusCode, rule.response.mime);
  }
}
