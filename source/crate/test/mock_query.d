/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.test.mock_query;

import crate.base;
import std.conv;
import std.range;
import std.string;
import std.datetime;
import vibe.data.json;
import vibe.data.bson;

class MockFieldQuery : IFieldQuery {

  @safe {
    MockQuery parent;

    this(MockQuery parent) {
      this.parent = parent;
    }

    bool isRootQuery() @safe {
      return false;
    }

    /// Match an item if exactly one field value
    IFieldQuery isNull() {
      parent.log ~= "isNull";
      return this;
    }

    /// Match an item if exactly one field value
    IFieldQuery equal(string value) {
      parent.log ~= "equal " ~ value.to!string;
      return this;
    }

    /// ditto
    IFieldQuery equal(bool value) {
      parent.log ~= "equal " ~ value.to!string;
      return this;
    }

    /// ditto
    IFieldQuery equal(int value) {
      parent.log ~= "equal " ~ value.to!string;
      return this;
    }

    /// ditto
    IFieldQuery equal(ulong value) {
      parent.log ~= "equal " ~ value.to!string;
      return this;
    }

    /// ditto
    IFieldQuery equal(ObjectId value) {
      parent.log ~= "equal " ~ value.to!string;
      return this;
    }

    /// Check if the field exists
    IFieldQuery exists() {
      parent.log ~= "exists";
      return this;
    }

    /// Match an item if the field equals the same string by ignoring the case
    IFieldQuery equalIgnoreCase(string value) {
      parent.log ~= "equal " ~ value.to!string;
      return this;
    }

    ///
    IFieldQuery greaterThan(SysTime value) {
      parent.log ~= "gt " ~ value.to!string;

      return this;
    }

    ///
    IFieldQuery lessThan(SysTime value) {
      parent.log ~= "lt " ~ value.to!string;

      return this;
    }

    /// Match an item if a value from an array contains one field value
    IFieldQuery arrayContains(string value) {
      parent.log ~= "arrayContains " ~ value.to!string;
      return this;
    }

    /// ditto
    IFieldQuery arrayContains(ulong value) {
      parent.log ~= "arrayContains " ~ value.to!string;
      return this;
    }

    /// ditto
    IFieldQuery arrayContains(ObjectId value) {
      parent.log ~= "arrayContains " ~ value.to!string;
      return this;
    }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery anyOf(string[] values) {
      parent.log ~= "anyOf " ~ values.to!string;
      return this;
    }

    /// ditto
    IFieldQuery anyOf(ObjectId[] values) {
      parent.log ~= "anyOf " ~ values.to!string;
      return this;
    }

    /// ditto
    IFieldQuery anyOf(int[] values) {
      parent.log ~= "anyOf " ~ values.to!string;
      return this;
    }

    /// Match an item using a substring
    IFieldQuery like(string value) {
      parent.log ~= "like " ~ value;
      return this;
    }

    /// Match an item if an array field value contains all values from the values list
    IFieldQuery containsAll(string[] values) {
      parent.log ~= "containsAll " ~ values.to!string;
      return this;
    }
    /// ditto
    IFieldQuery containsAll(ObjectId[] values) {
      parent.log ~= "containsAll " ~ values.to!string;
      return this;
    }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery containsAny(string[] values) {
      parent.log ~= "containsAny " ~ values.to!string;
      return this;
    }
    /// ditto
    IFieldQuery containsAny(ObjectId[] values) {
      parent.log ~= "containsAny " ~ values.to!string;
      return this;
    }

    /// Match an item if an array field contains an object that has the field equals with the value
    IFieldQuery arrayFieldContains(string field, string value) {
      parent.log ~= "arrayFieldContains " ~ field ~ ":" ~ value;
      return this;
    }

    /// ditto
    IFieldQuery arrayFieldContains(string field, ObjectId value) {
      parent.log ~= "arrayFieldContains " ~ field ~ ":" ~ value.to!string;
      return this;
    }

    /// Match an item if an array field contains an object that has the field contains the value
    IFieldQuery arrayFieldLike(string field, string value) {
      parent.log ~= "arrayFieldLike " ~ field ~ ":" ~ value;
      return this;
    }

    /// Match an item if it's coordinates is inside a polygon
    IFieldQuery insidePolygon(double[][][] value) {
      parent.log ~= "insidePolygon " ~ value.to!string;
      return this;
    }

    /// Match an item if it's coordinates intersects a polygon
    IFieldQuery intersectsPolygon(double[][][] value) {
      parent.log ~= "intersectsPolygon " ~ value.to!string;
      return this;
    }

    /// Match an item if it's coordinates intersects a geometry
    IFieldQuery intersects(Json value) {
      parent.log ~= "intersects " ~ value.to!string;
      return this;
    }

    /// Match an item if the polygon contains a point
    IFieldQuery containsPoint(double longitude, double latitude) {
      parent.log ~= "containsPoint " ~ longitude.to!string ~ ", " ~ latitude.to!string;
      return this;
    }

    /// Match/sort an item relative to a geographical point
    IFieldQuery near(double longitude, double latitude, double meters) {
      parent.log ~= "near " ~ longitude.to!string ~ ", " ~ latitude.to!string ~ ", " ~ meters.to!string ~ "m";
      return this;
    }

    ///
    IFieldQuery not() {
      parent.log ~= "not";
      return this;
    }

    ///
    IQuery and() {
      parent.log ~= "and";
      return parent;
    }

      /// Add a query for a new field
      IQuery or() {
        parent.log ~= "or";
        return parent.or;
      }
  }
}

class MockQuery : IQuery {
  @safe {
    string[] log;

    string debugString() {
      return log.join(" ");
    }

    /// Sort by fields
    IQuery sort(string field, int order) {
      log ~= "sort by " ~ field ~ ":" ~ order.to!string;
      return this;
    }

    IQuery searchText(string query) {
      log ~= "searchText:" ~ query;
      return this;
    }

    IQuery and() {
      log ~= "and";
      return this;
    }

    ///
    IFieldQuery where(string field) {
      log ~= "where " ~ field;
      return new MockFieldQuery(this);
    }

    ///
    IQuery withProjection(string[] fields) {
      log ~= "withProjection " ~ "(" ~ fields.join(", ") ~ ")";
      return this;
    }

    /// Perform an `or` logical operation
    IQuery or() {
      log ~= "or";
      return this;
    }

    /// Execute the selector and return a range of JSONs
    InputRange!Json exec() {
      log ~= "exec";

      Json[] empty;
      return empty.inputRangeObject;
    }

    /// Execute the selector and return a range of JSONs
    InputRange!Bson execBson() {
      log ~= "execBson";

      Bson[] empty;
      return empty.inputRangeObject;
    }

    /// Execute the selector and return all distinct values of a key
    InputRange!Json distinct(string key) {
      log ~= "distinct (" ~ key ~ ")";

      Json[] empty;
      return empty.inputRangeObject;
    }

    /// Limit the number of results
    IQuery limit(size_t nr) {
      log ~= "limit (" ~ nr.to!string ~ ")";
      return this;
    }

    /// Skip a number of results
    IQuery skip(size_t nr) {
      log ~= "skip (" ~ nr.to!string ~ ")";
      return this;
    }

    /// Create a clone of a current selector
    IQuery dup() {
      log ~= "dup";
      return this;
    }

    /// Get the size of the selected items
    size_t size() {
      log ~= "size";
      return 0;
    }
  }
}
