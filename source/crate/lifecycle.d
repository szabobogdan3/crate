/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.lifecycle;

import std.datetime;

class CrateLifecycle {
  alias MiddlewareBenchmark = void delegate(const string route,const string middelewareName, const Duration duration) nothrow @safe;
  static CrateLifecycle instance;

  private MiddlewareBenchmark _onMiddlewareBenchmark;

  static this() {
    CrateLifecycle.instance = new CrateLifecycle();
  }

  void middlewareBenchmark(const string route,const string middelewareName, const Duration duration) nothrow @safe {
    if(_onMiddlewareBenchmark is null) {
      return;
    }

    this._onMiddlewareBenchmark(route, middelewareName, duration);
  }

  void onMiddlewareBenchmark(MiddlewareBenchmark onMiddlewareBenchmark) {
    this._onMiddlewareBenchmark = onMiddlewareBenchmark;
  }
}