/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.BsonQuery;

import crate.base;

import vibe.data.bson;
import vibe.data.json;

import std.range;
import std.uni;
import std.datetime;

version (unittest) {
  import crate.http.router;
  import fluent.asserts;
  import vibe.data.serialization;
}

///
class BsonFieldQuery : IFieldQuery {
  private {
    bool negation;
    IQuery parent;
  }

  static Bson crs;

  static this() {
    Bson properties = Bson.emptyObject;
    properties["name"] = "urn:x-mongodb:crs:strictwinding:EPSG:4326";

    crs = Bson.emptyObject;
    crs["type"] = "name";
    crs["properties"] = properties;
  }

  Bson data;

  @safe {
    this(IQuery parent) {
      this.parent = parent;
    }

    bool isRootQuery() {
      return true;
    }

    ///
    IFieldQuery equalImpl(T)(T value) {
      if(negation) {
        data = [ "$ne": value ].serializeToBson;
      } else {
        data = value.serializeToBson;
      }

      return this;
    }

    ///
    IFieldQuery containsAny(T)(T[] values) {
      data = ["$in": values].serializeToBson;

      if(negation) {
        data = [ "$not": data ].serializeToBson;
      }

      return this;
    }

    ///
    IFieldQuery containsAll(T)(T[] values) {
      data = ["$all": values].serializeToBson;

      if(negation) {
        data = [ "$not": data ].serializeToBson;
      }

      return this;
    }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery anyOf(T)(T[] values) {
      if(negation) {
        data = ["$not": ["$in": values]].serializeToBson;
      } else {
        data = ["$in": values].serializeToBson;
      }

      return this;
    }

    override {
      ///
      IFieldQuery isNull() {
        return this.equalImpl!Json(Json(null));
      }

      ///
      IFieldQuery equal(string value) {
        return this.equalImpl!string(value);
      }

      /// ditto
      IFieldQuery equal(bool value) {
        if(negation) {
          value = !value;
        }

        if(value) {
          data = value.serializeToBson;
        }

        if(!value) {
          data = [ "$ne": true ].serializeToBson;
        }

        return this;
      }

      /// ditto
      IFieldQuery equal(int value) {
        return this.equalImpl!int(value);
      }

      /// ditto
      IFieldQuery equal(ulong value) {
        return this.equalImpl!ulong(value);
      }

      /// ditto
      IFieldQuery equal(ObjectId value) {
        return this.equalImpl!ObjectId(value);
      }

      ///
      IFieldQuery greaterThan(SysTime value) {
        if(data.type != Bson.Type.object) {
          data = Bson.emptyObject;
        }

        if(negation) {
          data["$lte"] = BsonDate(value);
        } else {
          data["$gt"] = BsonDate(value);
        }

        return this;
      }

      ///
      IFieldQuery lessThan(SysTime value) {
        if(data.type != Bson.Type.object) {
          data = Bson.emptyObject;
        }

        if(negation) {
          data["$gte"] = BsonDate(value);
        } else {
          data["$lt"] = BsonDate(value);
        }

        return this;
      }

      /// Check if the field exists
      IFieldQuery exists() {
        auto value = Json.emptyObject;
        value["$exists"] = !negation;

        data = value.serializeToBson;

        return this;
      }

      /// Match an item if the field equals the same string by ignoring the case
      IFieldQuery equalIgnoreCase(string value) {
        auto lowerValue = value.toLower.escapeRegexValue;

        if(negation) {
          data = ["$regex": "^(?!" ~ lowerValue ~ ")", "$options" : "i"].serializeToBson;
        } else {
          data = ["$regex": "^" ~ lowerValue, "$options" : "i"].serializeToBson;
        }

        return this;
      }

      /// Match an item if a value from an array contains one field value
      IFieldQuery arrayContains(string value) {
        return equalImpl!string(value);
      }

      /// ditto
      IFieldQuery arrayContains(ulong value) {
        return equalImpl!ulong(value);
      }

      /// ditto
      IFieldQuery arrayContains(ObjectId value) {
        return equalImpl!ObjectId(value);
      }

      /// Match an item if a filed value contains at least one value from the values list
      IFieldQuery anyOf(string[] values) {
        return anyOf!string(values);
      }
      /// ditto
      IFieldQuery anyOf(ObjectId[] values) {
        return anyOf!ObjectId(values);
      }
      /// ditto
      IFieldQuery anyOf(int[] values) {
        return anyOf!int(values);
      }

      /// Match an item using a substring
      IFieldQuery like(string value) {

        if(negation) {
          data = ["$regex": "?!.*" ~ value ~ ".*", "$options" : "i"].serializeToBson;
        } else {
          data = ["$regex": ".*" ~ value ~ ".*", "$options" : "i"].serializeToBson;
        }

        return this;
      }

      /// Match an item if an array field value contains all values from the values list
      IFieldQuery containsAll(string[] values) {
        return containsAll!string(values);
      }
      /// ditto
      IFieldQuery containsAll(ObjectId[] values) {
        return containsAll!ObjectId(values);
      }

      /// Match an item if a filed value contains at least one value from the values list
      IFieldQuery containsAny(string[] values) {
        return containsAny!string(values);
      }
      /// ditto
      IFieldQuery containsAny(ObjectId[] values) {
        return containsAny!ObjectId(values);
      }

      /// Match an item if an array field contains an object that has the field equals with the value
      IFieldQuery arrayFieldContains(string field, string value) {
        if(negation) {
          data = ["$not": ["$elemMatch": [field: value]]].serializeToBson;
        } else {
          data = ["$elemMatch": [field: value]].serializeToBson;
        }

        return this;
      }

      /// ditto
      IFieldQuery arrayFieldContains(string field, ObjectId value) {
        if(negation) {
          data = ["$not": ["$elemMatch": [field: value]]].serializeToBson;
        } else {
          data = ["$elemMatch": [field: value]].serializeToBson;
        }

        return this;
      }

      IFieldQuery arrayFieldLike(string field, string value) {
        if(negation) {
          data = ["$not": ["$elemMatch": [field: ["$regex": "?!.*" ~ value ~ ".*", "$options" : "i"]]]].serializeToBson;
        } else {
          data = ["$elemMatch": [field: ["$regex": ".*" ~ value ~ ".*", "$options" : "i"]]].serializeToBson;
        }

        return this;
      }

      /// Match an item if it's coordinates is inside a polygon
      IFieldQuery insidePolygon(double[][][] value) {
        foreach(ref list; value) {
          foreach(ref point; list) {
            fixLonLat(point);
          }
        }

        data = ["$geoWithin":[
          "$geometry":[
            "type": Bson("Polygon"),
            "coordinates": value.serializeToBson,
            "crs": BsonFieldQuery.crs
        ]]].serializeToBson;

        if(negation) {
          data = [ "$not": data ].serializeToBson;
        }

        return this;
      }

      /// Match an item if it's coordinates intersects a polygon
      IFieldQuery intersectsPolygon(double[][][] value) {
        foreach(ref list; value) {
          foreach(ref point; list) {
            fixLonLat(point);
          }
        }

        data = ["$geoIntersects":[
          "$geometry":[
            "type": Bson("Polygon"),
            "coordinates": value.serializeToBson,
            "crs": BsonFieldQuery.crs
        ]]].serializeToBson;

        if(negation) {
          data = [ "$not": data ].serializeToBson;
        }

        return this;
      }

      /// Match an item if it's coordinates intersects a geometry
      IFieldQuery intersects(Json geometry) {
        data = ["$geoIntersects":[
          "$geometry": geometry.serializeToBson,
        ]].serializeToBson;

        if(negation) {
          data = [ "$not": data ].serializeToBson;
        }

        return this;
      }

      /// Match an item if the polygon contains a point
      IFieldQuery containsPoint(double longitude, double latitude) {
        fixLonLat(longitude, latitude);

        data = ["$geoIntersects":[
          "$geometry":[
            "type": Bson("Point"),
            "coordinates": [longitude, latitude].serializeToBson
        ]]].serializeToBson;

        if(negation) {
          data = [ "$not": data ].serializeToBson;
        }

        return this;
      }

      /// Match/sort an item relative to a geographical point
      IFieldQuery near(double longitude, double latitude, double meters) {
        fixLonLat(longitude, latitude);

        data = ["$near":[
          "$geometry":[
            "type": Bson("Point"),
            "coordinates": [longitude, latitude].serializeToBson
        ]]].serializeToBson;

        if(negation) {
          data = [ "$not": data ].serializeToBson;
        }

        return this;
      }

      /// Negate the query
      IFieldQuery not() {
        negation = true;
        return this;
      }

      /// Add a new
      IQuery and() {
        return parent;
      }

      /// Add a query for a new field
      IQueryBuilder or() {
        return parent.or;
      }
    }
  }
}

/// It should create an exists query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.exists;
  q.data.toJson.should.equal(`{"$exists": true}`.parseJsonString);

  q.not.exists;
  q.data.toJson.should.equal(`{"$exists": false}`.parseJsonString);
}

/// It should create an equal query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.equal("test");
  q.data.toJson.should.equal(`"test"`.parseJsonString);

  q.not.equal("test");
  q.data.toJson.should.equal(`{"$ne": "test"}`.parseJsonString);
}

/// It should create an equal query with bool
unittest {
  auto q = new BsonFieldQuery(null);

  q.equal(true);
  q.data.toJson.should.equal(`true`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.equal(true);
  q.data.toJson.should.equal(`{"$ne": true}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.equal(false);
  q.data.toJson.should.equal(`{"$ne": true}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.equal(false);
  q.data.toJson.should.equal(`true`.parseJsonString);
}

/// It should create an equal query with int
unittest {
  int val = 34;
  auto q = new BsonFieldQuery(null);

  q.equal(val);
  q.data.toJson.should.equal(`34`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.equal(val);
  q.data.toJson.should.equal(`{"$ne": 34}`.parseJsonString);
}

/// It should create an equal query with ulong
unittest {
  ulong val = 34;
  auto q = new BsonFieldQuery(null);

  q.equal(val);
  q.data.toJson.should.equal(`34`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.equal(val);
  q.data.toJson.should.equal(`{"$ne": 34}`.parseJsonString);
}

/// It should create an equal query with objectId
unittest {
  auto val = ObjectId.fromString("000000000000000000000002");
  auto q = new BsonFieldQuery(null);

  q.equal(val);
  q.data.type.should.equal(Bson.Type.objectID);
  q.data.toString.should.equal(`ObjectID(000000000000000000000002)`);

  q = new BsonFieldQuery(null);
  q.not.equal(val);
  q.data["$ne"].type.should.equal(Bson.Type.objectID);
  q.data.toJson.should.equal(`{"$ne": "000000000000000000000002"}`.parseJsonString);
}


/// It should create an array contains query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.arrayContains("test");
  q.data.toJson.should.equal(`"test"`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.arrayContains("test");
  q.data.toJson.should.equal(`{ "$ne": "test" }`.parseJsonString);
}

/// It should create an array contains query with ulong
unittest {
  ulong val = 34;
  auto q = new BsonFieldQuery(null);

  q.arrayContains(val);
  q.data.toJson.should.equal(`34`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.arrayContains(val);
  q.data.toJson.should.equal(`{"$ne": 34}`.parseJsonString);
}

/// It should create a query equal with objectId
unittest {
  auto val = ObjectId.fromString("000000000000000000000002");
  auto q = new BsonFieldQuery(null);

  q.arrayContains(val);
  q.data.type.should.equal(Bson.Type.objectID);
  q.data.toString.should.equal(`ObjectID(000000000000000000000002)`);

  q = new BsonFieldQuery(null);
  q.not.arrayContains(val);
  q.data["$ne"].type.should.equal(Bson.Type.objectID);
  q.data.toJson.should.equal(`{"$ne": "000000000000000000000002"}`.parseJsonString);
}


/// greaterThan
unittest {
  auto val = SysTime.fromISOExtString("2023-02-22T13:00:00Z");
  auto q = new BsonFieldQuery(null);

  q.greaterThan(val);

  q.data["$gt"].type.should.equal(Bson.Type.date);
  q.data.toString.should.equal(`{"$gt":"2023-02-22T13:00:00Z"}`);
}

/// lessThan
unittest {
  auto val = SysTime.fromISOExtString("2023-02-22T13:00:00Z");
  auto q = new BsonFieldQuery(null);

  q.lessThan(val);

  q.data["$lt"].type.should.equal(Bson.Type.date);
  q.data.toString.should.equal(`{"$lt":"2023-02-22T13:00:00Z"}`);
}

/// lessThan and greaterThan
unittest {
  auto val1 = SysTime.fromISOExtString("2023-02-22T13:00:00Z");
  auto val2 = SysTime.fromISOExtString("2023-02-23T13:00:00Z");
  auto q = new BsonFieldQuery(null);

  q.lessThan(val1).greaterThan(val2);

  q.data["$lt"].type.should.equal(Bson.Type.date);
  q.data["$gt"].type.should.equal(Bson.Type.date);
  q.data.toString.should.equal(`{"$lt":"2023-02-22T13:00:00Z","$gt":"2023-02-23T13:00:00Z"}`);
}

/// greaterThan and lessThan
unittest {
  auto val1 = SysTime.fromISOExtString("2023-02-22T13:00:00Z");
  auto val2 = SysTime.fromISOExtString("2023-02-23T13:00:00Z");
  auto q = new BsonFieldQuery(null);

  q.greaterThan(val2).lessThan(val1);

  q.data["$lt"].type.should.equal(Bson.Type.date);
  q.data["$gt"].type.should.equal(Bson.Type.date);
  q.data.toString.should.equal(`{"$gt":"2023-02-23T13:00:00Z","$lt":"2023-02-22T13:00:00Z"}`);
}


/// It should create an any of query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.anyOf(["test1", "test2"]);
  q.data.toJson.should.equal(`{"$in":["test1", "test2"]}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.anyOf(["test1", "test2"]);
  q.data.toJson.should.equal(`{"$not": {"$in":["test1", "test2"]}}`.parseJsonString);
}

/// It should create an any of query with objectId
unittest {
  auto q = new BsonFieldQuery(null);

  q.anyOf([ObjectId.fromString("000000000000000000000002"), ObjectId.fromString("000000000000000000000003")]);
  q.data.toJson.should.equal(`{"$in":["000000000000000000000002", "000000000000000000000003"]}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.anyOf([ObjectId.fromString("000000000000000000000002"), ObjectId.fromString("000000000000000000000003")]);
  q.data.toJson.should.equal(`{"$not": {"$in":["000000000000000000000002", "000000000000000000000003"]}}`.parseJsonString);
}


/// It should create a like query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.like("test");
  q.data.toJson.should.equal(`{"$options": "i", "$regex": ".*test.*"}`.parseJsonString);

  q.not.like("test");
  q.data.toJson.should.equal(`{"$options": "i", "$regex": "?!.*test.*"}`.parseJsonString);
}

/// It should create a like query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.equalIgnoreCase("Test");
  q.data.toJson.should.equal(`{"$options": "i", "$regex": "^test"}`.parseJsonString);

  q.equalIgnoreCase(".*+?^${}()|[]\\/");
  q.data.toJson.should.equal(`{"$options": "i", "$regex": "^\\.\\*\\+\\?\\^\\$\\{\\}\\(\\)\\|\\[\\]\\\\\\/"}`.parseJsonString);

  q.equalIgnoreCase("Test");
  q.data.toJson.should.equal(`{"$options": "i", "$regex": "^test"}`.parseJsonString);
}

/// It should create an array field contains query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.arrayFieldContains("key", "value");
  q.data.toJson.should.equal(`{"$elemMatch": { "key": "value" }}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.arrayFieldContains("key", "value");
  q.data.toJson.should.equal(`{"$not": {"$elemMatch": { "key": "value" }}}`.parseJsonString);
}


/// It should create a contains any query with string
unittest {
  auto q = new BsonFieldQuery(null);

  q.containsAny(["test1", "test2"]);
  q.data.toJson.should.equal(`{"$in":["test1", "test2"]}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.containsAny(["test1", "test2"]);
  q.data.toJson.should.equal(`{"$not": {"$in":["test1", "test2"]}}`.parseJsonString);
}

/// It should create a contains any query with objectId
unittest {
  auto q = new BsonFieldQuery(null);

  q.containsAny([ObjectId.fromString("000000000000000000000002"), ObjectId.fromString("000000000000000000000003")]);
  q.data.toJson.should.equal(`{"$in":["000000000000000000000002", "000000000000000000000003"]}`.parseJsonString);

  q = new BsonFieldQuery(null);
  q.not.containsAny([ObjectId.fromString("000000000000000000000002"), ObjectId.fromString("000000000000000000000003")]);
  q.data.toJson.should.equal(`{"$not": {"$in":["000000000000000000000002", "000000000000000000000003"]}}`.parseJsonString);
}

alias FieldQueryList = BsonFieldQuery[string];

///
Bson build(FieldQueryList fieldQueries, ref Bson result) @safe {
  foreach(string field, query; fieldQueries) {
    result[field] = query.data;
  }

  return result;
}

/// ditto
Bson build(FieldQueryList fieldQueries) @safe {
  auto result = Bson.emptyObject;

  return build(fieldQueries, result);
}

///
class BsonListQueryBuilder : IQuery {
  private {
    FieldQueryList[] fieldQueries;
    IQuery parent;
  }

  this(IQuery parent) {
    FieldQueryList empty;
    fieldQueries ~= empty;

    this.parent = parent;
  }

  @safe bool isEmpty() {
    return fieldQueries.length == 0;
  }

  IFieldQuery where(string field) @trusted {
    auto last = fieldQueries.length - 1;

    if(field !in fieldQueries[last]) {
      fieldQueries[last][field] = new BsonFieldQuery(this);
    }

    return fieldQueries[last][field];
  }

  IQueryBuilder or() @trusted {
    FieldQueryList empty;
    fieldQueries ~= empty;
    return this;
  }

  IQuery searchText(string query) {
    assert(false, "not supported");
  }

  IQuery and() {
    return parent;
  }

  bool hasMany() @safe {
    return fieldQueries.length > 1;
  }

  /// Return the computed Mongo query
  Bson build(ref Bson result) @safe {
    if(fieldQueries.length == 1) {
      return fieldQueries[0].build(result);
    }

    Bson[] orResult;

    foreach(query; fieldQueries) {
      orResult ~= query.build();
    }

    if(orResult.length > 0) {
      result["$or"] = orResult.serializeToBson;
    }

    return result;
  }

  string debugString() {
    auto result = Bson.emptyObject;

    return build(result).toJson.toPrettyString;
  }

  IQuery withProjection(string[] fields) {
    return parent.withProjection(fields);
  }

  IQuery sort(string field, int order) {
    return parent.sort(field, order);
  }

  InputRange!(Json) exec() {
    return parent.exec();
  }

  InputRange!Bson execBson() {
    return parent.execBson();
  }

  InputRange!(Json) distinct(string key) {
    return parent.distinct(key);
  }

  IQuery limit(ulong nr) {
    return parent.limit(nr);
  }

  IQuery skip(ulong nr) {
    return parent.skip(nr);
  }

  ulong size() {
    return parent.size();
  }
}

///
class BsonQueryBuilder : IQueryBuilder {
  private {
    FieldQueryList fieldQueries;
    IQuery parent;

    BsonListQueryBuilder[] orList;
  }

  this(IQuery parent) {
    this.parent = parent;
  }

  @safe {
    void searchText(string query) {
      fieldQueries["$text"] = new BsonFieldQuery(parent);
      fieldQueries["$text"].data = [ "$search": query ].serializeToBson;
    }

    IFieldQuery where(string field) @trusted {
      if(field !in fieldQueries) {
        fieldQueries[field] = new BsonFieldQuery(parent);
      }

      return fieldQueries[field];
    }

    IQueryBuilder or() @trusted {
      auto newBuilder = new BsonListQueryBuilder(parent);
      orList ~= newBuilder;

      return newBuilder;
    }

    IQuery and() {
      return parent;
    }
  }

  /// Return the computed Mongo query
  Bson build() @safe {
    auto result = Bson.emptyObject;

    return this.build(result);
  }

  /// Return the computed Mongo query
  Bson build(ref Bson result) @safe {
    if(orList.length > 1) {
      fieldQueries.build(result);

      Bson[] queryList;

      foreach(group; orList) {
        if(!group.hasMany) {
          group.build(result);
          continue;
        }

        auto tmp = Bson.emptyObject;
        group.build(tmp);

        queryList ~= tmp;
      }

      if(queryList.length) {
        result["$and"] = queryList;
      }

      return result;
    }

    fieldQueries.build(result);


    if(orList.length == 1) {
      orList[0].build(result);
    }

    return result;
  }
}


/// It should build an empty query
unittest {
  auto builder = new BsonQueryBuilder(null);

  builder.build.toJson.should.equal(`{}`.parseJsonString);
}

/// It should search by text
unittest {
  auto builder = new BsonQueryBuilder(null);
  builder.searchText("some string");

  builder.build.toJson.should.equal(`{
    "$text": {
      "$search": "some string"
    }
  }`.parseJsonString);
}

/// It should build a query with one key
unittest {
  auto builder = new BsonQueryBuilder(null);
  builder.where("key").equal("value");
  builder.build.toJson.should.equal(`{"key": "value"}`.parseJsonString);
}

/// It should build a query with two keys
unittest {
  auto builder = new BsonQueryBuilder(null);
  builder.where("key1").equal("value1");
  builder.where("key2").equal("value2");
  builder.build.toJson.should.equal(`{"key1": "value1", "key2": "value2"}`.parseJsonString);
}

/// It should build a query with two keys and two or keys
unittest {
  import crate.test.mock_query;

  auto builder = new BsonQueryBuilder(new MockQuery);
  builder.where("key1").equal("value1");
  builder.where("key2").equal("value2");

  builder.or
    .where("k1").equal("value1").and.where("l1").equal("v1")
    .or
    .where("k2").equal("value2");

  builder.build.toJson.should.equal(`{
    "key1": "value1", "key2": "value2",
    "$or": [
      { "k1": "value1", "l1": "v1" },
      { "k2": "value2" }
    ]
  }`.parseJsonString);
}

/// It should colapse an or query with one item
unittest {
  import crate.test.mock_query;

  auto builder = new BsonQueryBuilder(new MockQuery);
  builder.where("key1").equal("value1");
  builder.where("key2").equal("value2");

  builder.or
    .where("k1").equal("value1");

  builder.build.toJson.should.equal(`{
    "key1": "value1",
    "key2": "value2",
    "k1": "value1"
  }`.parseJsonString);
}


/// It should build a query with two keys and two or groups
unittest {
  import crate.test.mock_query;

  auto builder = new BsonQueryBuilder(new MockQuery);
  builder.where("key1").equal("value1");
  builder.where("key2").equal("value2");

  builder.or
    .where("k1").equal("value1")
    .or
    .where("k2").equal("value2");

  builder.or
    .where("k12").equal("value1")
    .or
    .where("k22").equal("value2");

  builder.build.toJson.should.equal(`{
    "key1": "value1",
    "key2": "value2",
    "$and": [
      { "$or": [
        { "k1": "value1" },
        { "k2": "value2" }
      ]},
      { "$or": [
        { "k12": "value1" },
        { "k22": "value2" }
      ]}
    ]
  }`.parseJsonString);
}

private {
  void fixLonLat(ref double longitude, ref double latitude) @safe nothrow {
    if(longitude > 180) {
      longitude = 180;
    }

    if(longitude < -180) {
      longitude = -180;
    }

    if(latitude > 90) {
      latitude = 90;
    }

    if(latitude < -90) {
      latitude = -90;
    }
  }

  void fixLonLat(ref double[] point) @safe nothrow {
    if(point[0] > 180) {
      point[0] = 180;
    }

    if(point[0] < -180) {
      point[0] = -180;
    }

    if(point[1] > 90) {
      point[1] = 90;
    }

    if(point[1] < -90) {
      point[1] = -90;
    }
  }
}

string escapeRegexValue(string value) @safe {
  auto specialChars = "\\.*+?^${}()|[]/";
  auto result = value;

  foreach(ch; specialChars) {
    result = result.replace(ch, "\\" ~ ch);
  }

  return result;
}