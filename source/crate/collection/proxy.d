/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.proxy;

import crate.base;
import crate.ctfe;
import vibe.data.json;
import vibe.data.bson;

import std.traits, std.algorithm, std.array;
import std.stdio, std.string, std.datetime;
import std.range.interfaces;

///
class CrateProxy: Crate!void {

  private {
    CrateConfig!void configProxy;

    IQuery delegate() getRef;
    Json delegate(const Json) addItemRef;
    IQuery delegate(const string) getItemRef;
    Json delegate(const Json) updateItemRef;
    void delegate(const string) deleteItemRef;

    ModelDescription _definition;
  }

  this(T)(ref Crate!T crate) {
    getRef = &crate.get;
    addItemRef = &crate.addItem;
    getItemRef = &crate.getItem;
    updateItemRef = &crate.updateItem;
    deleteItemRef = &crate.deleteItem;

    static if(isAggregateType!T) {
      _definition = describe!T;
      _definition.singular = crate.config.singular;
      _definition.plural = crate.config.plural;
    } else {
      _definition = ModelDescription();
    }
  }

  @trusted:
    ModelDescription definition()
    {
      return _definition;
    }

    IQuery get() {
      return getRef();
    }

    Json addItem(const Json item)
    {
      return addItemRef(item);
    }

    IQuery getItem(const string id)
    {
      return getItemRef(id);
    }

    Json updateItem(const Json item)
    {
      return updateItemRef(item);
    }

    void deleteItem(const string id)
    {
      deleteItemRef(id);
    }
}

class CrateCollection {

  private {
    CrateProxy[string] crates;
    string[string] types;
  }

  void addByPath(T)(string basePath, ref Crate!T crate) {
    crates[basePath] = new CrateProxy(crate);
    types[T.stringof] = basePath;
  }

  string[] paths() {
    return crates.keys;
  }

  CrateProxy getByPath(string path) {
    foreach (basePath, crate; crates)
    {
      if (path.indexOf(basePath) == 0)
      {
        return crate;
      }
    }

    assert(false, "No crate found found at `" ~ path ~ "`");
  }

  CrateProxy getByType(string type) {
    if (type in types)
    {
      return crates[types[type]];
    }

    assert(false, "Crate not found");
  }
}

///
class ProxySelector: IQuery {

  protected {
    IQuery[] selectors;
  }

  ///
  this(IQuery selector) @safe {
    this.selectors = [ selector ];
  }

  ///
  this(IQuery[] selector) @safe {
    this.selectors = selectors;
  }

  override @safe {
    string debugString() {
      return this.selectors.map!(a => a.debugString).join(" ");
    }

    IFieldQuery where(string field) {
      IFieldQuery[] list;
      foreach(selector; selectors) {
        list ~= selector.where(field);
      }

      return new ProxyFieldQuery(list);
    }

    ///
    IQuery withProjection(string[] fields) {
      IQuery[] list;

      foreach(selector; selectors) {
        list ~= selector.withProjection(fields);
      }

      return new ProxySelector(list);
    }

    IQuery searchText(string query) {
      IQuery[] list;

      foreach(selector; selectors) {
        list ~= selector.searchText(query);
      }

      return new ProxySelector(list);
    }

    /// Perform an or logical operation
    IQueryBuilder or() {
      return selectors[0].or;
    }

    IQuery and() {
      return this;
    }

    /// Execute the selector and return a range of JSONs
    InputRange!Json exec() @trusted {
      InputRange!Json[] ranges;

      ranges.length = selectors.length;
      foreach(index, selector; selectors) {
        ranges[index] = selector.exec;
      }

      return ranges.joiner.inputRangeObject;
    }

    /// Execute the selector and return a range of BSONs
    InputRange!Bson execBson() @trusted {
      InputRange!Bson[] ranges;

      ranges.length = selectors.length;
      foreach(index, selector; selectors) {
        ranges[index] = selector.execBson;
      }

      return ranges.joiner.inputRangeObject;
    }

    /// Execute the selector and return all distinct values of a key
    InputRange!Json distinct(string key) @trusted {
      InputRange!Json[] ranges;

      ranges.length = selectors.length;
      foreach(index, selector; selectors) {
        ranges[index] = selector.distinct(key);
      }

      return ranges.joiner.inputRangeObject;
    }

    /// Limit the number of results
    IQuery limit(size_t nr) {
      foreach(selector; selectors) {
        selector.limit(nr);
      }

      return this;
    }

    ///
    IQuery sort(string field, int order) {
      foreach(selector; selectors) {
        selector.sort(field, order);
      }

      return this;
    }

    /// Skip a number of results
    IQuery skip(size_t nr) {
      foreach(selector; selectors) {
        selector.skip(nr);
      }

      return this;
    }

    /// Get the size of the selected items
    size_t size() @trusted {
      return this.selectors.map!(a => a.size).reduce!((a, b) => a + b);
    }
  }
}

/// Apply the rule to all queries
class ProxyFieldQuery : IFieldQuery {
  private {
    IFieldQuery[] list;
    IQuery parent;
  }

  @safe {
    this(IFieldQuery[] list, IQuery parent = null) {
      this.list = list;
      this.parent = parent;
    }

    override {
      bool isRootQuery() @safe {
        bool result = true;

        foreach(fieldQuery; list) {
          result = result && fieldQuery.isRootQuery;
        }

        return result;
      }

      ///
      IFieldQuery isNull() {
        foreach(fieldQuery; list) {
          fieldQuery.isNull;
        }

        return this;
      }

      IFieldQuery equal(string value) {
        foreach(fieldQuery; list) {
          fieldQuery.equal(value);
        }

        return this;
      }

      /// ditto
      IFieldQuery equal(bool value) {
        foreach(fieldQuery; list) {
          fieldQuery.equal(value);
        }

        return this;
      }

      /// ditto
      IFieldQuery equal(int value) {
        foreach(fieldQuery; list) {
          fieldQuery.equal(value);
        }

        return this;
      }

      /// ditto
      IFieldQuery equal(ulong value) @safe {
        foreach(fieldQuery; list) {
          fieldQuery.equal(value);
        }

        return this;
      }

      ///
      IFieldQuery greaterThan(SysTime value) {
        foreach(fieldQuery; list) {
          fieldQuery.greaterThan(value);
        }

        return this;
      }

      ///
      IFieldQuery lessThan(SysTime value) {
        foreach(fieldQuery; list) {
          fieldQuery.greaterThan(value);
        }

        return this;
      }

      /// ditto
      IFieldQuery equal(ObjectId value) {
        foreach(fieldQuery; list) {
          fieldQuery.equal(value);
        }

        return this;
      }

      /// Check if the field exists
      IFieldQuery exists() {
        foreach(fieldQuery; list) {
          fieldQuery.exists;
        }

        return this;
      }

      /// Match an item if the field equals the same string by ignoring the case
      IFieldQuery equalIgnoreCase(string value)  {
        foreach(fieldQuery; list) {
          fieldQuery.equalIgnoreCase(value);
        }

        return this;
      }

      /// Match an item if a value from an array contains one field value
      IFieldQuery arrayContains(string value) {
        foreach(fieldQuery; list) {
          fieldQuery.arrayContains(value);
        }

        return this;
      }

      /// ditto
      IFieldQuery arrayContains(ObjectId value) {
        foreach(fieldQuery; list) {
          fieldQuery.arrayContains(value);
        }

        return this;
      }

      /// ditto
      IFieldQuery arrayContains(ulong value) {
        foreach(fieldQuery; list) {
          fieldQuery.arrayContains(value);
        }

        return this;
      }

      /// Match an item if a filed value contains at least one value from the values list
      IFieldQuery anyOf(string[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.anyOf(values);
        }

        return this;
      }
      /// ditto
      IFieldQuery anyOf(ObjectId[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.anyOf(values);
        }

        return this;
      }
      /// ditto
      IFieldQuery anyOf(int[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.anyOf(values);
        }

        return this;
      }

      /// Match an item using a substring
      IFieldQuery like(string value) {
        foreach(fieldQuery; list) {
          fieldQuery.like(value);
        }

        return this;
      }

      /// Match an item if an array field value contains all values from the values list
      IFieldQuery containsAll(string[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.containsAll(values);
        }

        return this;
      }

      /// ditto
      /// Match an item if an array field value contains all values from the values list
      IFieldQuery containsAll(ObjectId[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.containsAll(values);
        }

        return this;
      }

      /// Match an item if a filed value contains at least one value from the values list
      IFieldQuery containsAny(string[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.containsAny(values);
        }

        return this;
      }
      /// ditto
      IFieldQuery containsAny(ObjectId[] values) {
        foreach(fieldQuery; list) {
          fieldQuery.containsAny(values);
        }

        return this;
      }

      /// Match an item if an array field contains an object that has the field equals with the value
      IFieldQuery arrayFieldContains(string field, string value) {
        foreach(fieldQuery; list) {
          fieldQuery.arrayFieldContains(field, value);
        }

        return this;
      }

      /// ditto
      IFieldQuery arrayFieldContains(string field, ObjectId value) {
        foreach(fieldQuery; list) {
          fieldQuery.arrayFieldContains(field, value);
        }

        return this;
      }

      /// Match an item if an array field contains an object that has the field contains the value
      IFieldQuery arrayFieldLike(string field, string value) {
        foreach(fieldQuery; list) {
          fieldQuery.arrayFieldLike(field, value);
        }

        return this;
      }

      /// Match an item if it's coordinates is inside a polygon
      IFieldQuery insidePolygon(double[][][] value) {
        foreach(fieldQuery; list) {
          fieldQuery.insidePolygon(value);
        }

        return this;
      }

      /// Match an item if it's coordinates intersects a polygon
      IFieldQuery intersectsPolygon(double[][][] value) {
        foreach(fieldQuery; list) {
          fieldQuery.intersectsPolygon(value);
        }

        return this;
      }

      /// Match an item if it's coordinates intersects a geometry
      IFieldQuery intersects(Json geometry) {
        foreach(fieldQuery; list) {
          fieldQuery.intersects(geometry);
        }

        return this;
      }

      /// Match an item if the polygon contains a point
      IFieldQuery containsPoint(double longitude, double latitude) {
        foreach(fieldQuery; list) {
          fieldQuery.containsPoint(longitude, latitude);
        }

        return this;
      }

      /// Match/sort an item relative to a geographical point
      IFieldQuery near(double longitude, double latitude, double meters) {
        foreach(fieldQuery; list) {
          fieldQuery.near(longitude, latitude, meters);
        }

        return this;
      }

      IFieldQuery not() {
        foreach(fieldQuery; list) {
          fieldQuery.not;
        }

        return this;
      }

      IQuery and() {
        if(this.parent !is null) {
          return this.parent;
        }

        return list.front.and;
      }


      /// Add a query for a new field
      IQueryBuilder or() {
        return and.or;
      }
    }
  }
}
