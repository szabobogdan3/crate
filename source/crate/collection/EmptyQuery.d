/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.EmptyQuery;

import crate.base;
import std.range;
import std.datetime;
import vibe.data.json;
import vibe.data.bson;

static this() {
  EmptyQuery.instance = new EmptyQuery;
  EmptyFieldQuery.instance = new EmptyFieldQuery;
}

class EmptyFieldQuery : IFieldQuery {
  static EmptyFieldQuery instance;


  @safe {
    /// Match an item if exactly one field value
    bool isRootQuery() {
      return true;
    }

    ///
    IFieldQuery isNull() { return this; }

    /// Match an item if exactly one field value
    IFieldQuery equal(string value) { return this; }
    /// ditto
    IFieldQuery equal(int value) { return this; }
    /// ditto
    IFieldQuery equal(ulong value) { return this; }
    /// ditto
    IFieldQuery equal(bool value) { return this; }
    /// ditto
    IFieldQuery equal(ObjectId value) { return this; }

    ///
    IFieldQuery greaterThan(SysTime value) { return this; }

    ///
    IFieldQuery lessThan(SysTime) { return this; }

    /// Check if the field exists
    IFieldQuery exists() { return this; }

    /// Match an item if the field equals the same string by ignoring the case
    IFieldQuery equalIgnoreCase(string value) { return this; }

    /// Match an item if a value from an array contains one field value
    IFieldQuery arrayContains(string value) { return this; }
    /// ditto
    IFieldQuery arrayContains(ulong value) { return this; }
    /// ditto
    IFieldQuery arrayContains(ObjectId value) { return this; }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery anyOf(string[] values) { return this; }
    /// ditto
    IFieldQuery anyOf(ObjectId[] values) { return this; }
    /// ditto
    IFieldQuery anyOf(int[] values) { return this; }

    /// Match an item using a substring
    IFieldQuery like(string value) { return this; }


    /// Match an item if an array field value contains all values from the values list
    IFieldQuery containsAll(string[] values) { return this; }
    ///ditto
    IFieldQuery containsAll(ObjectId[] values) { return this; }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery containsAny(string[] values) { return this; }
    /// ditto
    IFieldQuery containsAny(ObjectId[] values) { return this; }

    /// Match an item if an array field contains an object that has the field equals with the value
    IFieldQuery arrayFieldContains(string field, string value) { return this; }
    /// ditto
    IFieldQuery arrayFieldContains(string field, ObjectId value) { return this; }

    /// Match an item if an array field contains an object that has the field contains the value
    IFieldQuery arrayFieldLike(string field, string value) { return this; }

    /// Match an item if it's coordinates is inside a polygon
    IFieldQuery insidePolygon(double[][][] value) { return this; }

    /// Match an item if it's coordinates intersects a polygon
    IFieldQuery intersectsPolygon(double[][][] value) { return this; }

    /// Match an item if it's coordinates intersects a geometry
    IFieldQuery intersects(Json geometry) { return this; }

    /// Match an item if the polygon contains a point
    IFieldQuery containsPoint(double longitude, double latitude) { return this; }

    /// Match/sort an item relative to a geographical point
    IFieldQuery near(double longitude, double latitude, double meters) { return this; }

    ///
    IFieldQuery not() { return this; }

    ///
    IQuery and() { return EmptyQuery.instance; }

    ///
    IQueryBuilder or() { return EmptyQuery.instance; }
  }
}

///
class EmptyQuery : IQuery {

  static EmptyQuery instance;

  @safe {
    static opCall() {
      return EmptyQuery.instance;
    }

    override {
      IFieldQuery where(string field) {
        return EmptyFieldQuery.instance;
      }

      string debugString() {
        return "{ }";
      }

      ///
      IQuery sort(string field, int order) {
        return this;
      }

      ///
      IQuery withProjection(string[] fields) {
        return this;
      }

      IQueryBuilder or() {
        return this;
      }

      IQuery searchText(string query) {
        return this;
      }

      IQuery and() {
        return this;
      }

      /// Execute the selector and return a range of JSONs
      InputRange!Json exec() @trusted {
        Json[] result;

        return result.inputRangeObject;
      }

      /// Execute the selector and return a range of BSONs
      InputRange!Bson execBson() @trusted {
        Bson[] result;

        return result.inputRangeObject;
      }

      /// Execute the selector and return all distinct values of a key
      InputRange!Json distinct(string key) {
        return exec;
      }

      /// Limit the number of results
      IQuery limit(size_t nr) {
        return this;
      }

      /// Skip a number of results
      IQuery skip(size_t nr) {
        return this;
      }

      /// Get the size of the selected items
      size_t size() @trusted {
        return 0;
      }
    }
  }
}