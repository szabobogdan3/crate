/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.memory;

import std.conv;
import std.algorithm;
import std.array;
import std.range;
import std.stdio;
import std.exception;
import std.typecons;
import std.string;
import std.datetime;
import std.regex : regex, matchFirst;

import crate.ctfe;
import crate.base;
import crate.error;
import geo.json;
import crate.json;

import crate.collection.BsonQuery;

import vibe.data.bson;
import vibe.data.json;

import geo.algorithm;
import crate.geo;

version(unittest) {
  import fluent.asserts;
}
/// Collection that stores items in memory
class MemoryCrate(T) : Crate!T {
  protected {
    ulong lastId;
    Json[] list;
    CrateConfig!T _config;
    enum string idField = getIdField!(describe!T).name;
    enum string idType = getIdField!(describe!T).type;
  }

  this(CrateConfig!T config = CrateConfig!T()) {
    this._config = config;
  }

  @trusted:
    IQuery get() {
      return MemoryQuery(list);
    }

    bool exists(ObjectId id) {
      return exists(id.toString);
    }

    bool exists(string id) {
      if(id == "" || id == "null" || id == "undefined") {
        return false;
      }

      return !list.filter!(a => a[idField] == id).empty;
    }

    bool isInvalid(string id) {
      return id == "" || id == "null" || id == "undefined";
    }

    ///
    Json addItem(const Json item) {
      Json tmp = item.clone;

      if(isObjectId(tmp[idField])) {
        tmp[idField] = ObjectId(tmp[idField]).toJson;
      } else {
        tmp.remove(idField);
      }

      if(idField !in tmp || exists(ObjectId(item[idField]))) {
        ObjectId newId;

        do {
          lastId++;
          newId = ObjectId(lastId.to!string);
        } while(exists(newId));

        tmp[idField] = ObjectId.fromString(lastId.to!string).toJson;
      }

      list ~= tmp;

      return tmp;
    }

    IQuery getItem(const string id) {
      if (list.filter!(a => a[idField] == id).empty) {
        throw new CrateNotFoundException("There is no item with id `" ~ id ~ "`");
      }

      auto query = this.get;
      query.where(idField).equal(id);
      query.limit(1);

      return query;
    }

    Json updateItem(const Json item) {
      auto match = list.enumerate
        .filter!(a => a[1][idField].to!string.stripLeft('0') == item[idField].to!string.stripLeft('0'));

      enforce!CrateNotFoundException(!match.empty, "No item found.");

      list[match.front[0]] = item;

      return item;
    }

    void deleteItem(const string id)
    {
      auto match = list
        .filter!(a => a[idField] == id);

      enforce!CrateNotFoundException(!match.empty, "No item found.");

      list = list.filter!(a => a[idField].to!string.stripLeft('0') != id.stripLeft('0')).array;
    }
}

///
class MemoryQuery : IQuery {

  private {
    InputRange!Json list;
    size_t resultLimit;
    size_t resultSkip;

    int[string] sortedFields;
    string[] projectionFields;

    BsonQueryBuilder queryBuilder;
  }

  @safe {
    static opCall(Json[] list = []) {
      return new MemoryQuery(list);
    }

    this(Json[] list = []) @trusted {
      this.list = list.inputRangeObject;
      this.queryBuilder = new BsonQueryBuilder(this);
    }

    this(T)(T list) @trusted if(isInputRange!T) {
      this.list = list.inputRangeObject;
      this.queryBuilder = new BsonQueryBuilder(this);
    }

    InputRange!Json applySort(Json[] list) {

      bool compare(Json a, Json b) {
        auto item1 = a.toFlatJson;
        auto item2 = b.toFlatJson;

        bool result;

        foreach(field, type; sortedFields) {
          if(field in item1 && field in item2) {
            if(type == 1) {
              result = item1[field] < item2[field];
            }

            if(type == -1) {
              result = item1[field] > item2[field];
            }
          }
        }

        return result;
      }

      return list.array.sort!compare.inputRangeObject;
    }

    override {
      IFieldQuery where(string field) {
        return queryBuilder.where(field);
      }

      ///
      IQuery sort(string field, int order) {
        this.sortedFields[field] = order;
        return this;
      }

      ///
      IQuery withProjection(string[] fields) {
        this.projectionFields = fields;
        return this;
      }

      IQueryBuilder or() {
        return queryBuilder.or;
      }

      IQuery searchText(string query) {
        queryBuilder.searchText(query);
        return this;
      }

      IQuery and() {
        return this;
      }

      /// Execute the selector and return a range of JSONs
      InputRange!Json exec() @trusted {
        InputRange!Json result = list.filter!(a => itemFilter(a)).inputRangeObject;

        if(resultSkip > 0) {
          result = result.drop(resultSkip).map!(a => a.clone).inputRangeObject;
        }

        if(resultLimit > 0) {
          result = result.take(resultLimit).map!(a => a.clone).inputRangeObject;
        }

        if(projectionFields.length > 0) {
          result = result.map!(a => applyProjection(a)).inputRangeObject;
        }

        if(this.sortedFields.length > 0) {
          result = applySort(result.array);
        }

        return result.inputRangeObject;
      }

      /// Execute the selector and return a range of BSONs
      InputRange!Bson execBson() @trusted {
        auto range = this.exec.array;

        return range.map!(a => a.serializeToBson).inputRangeObject;
      }

      /// Execute the selector and return all distinct values of a key
      InputRange!Json distinct(string key) {
        return list.filter!(a => itemFilter(a)).map!(a => get(a, key)).uniq.inputRangeObject;
      }

      /// Limit the number of results
      IQuery limit(size_t nr) {
        this.resultLimit = nr;
        return this;
      }

      /// Skip a number of results
      IQuery skip(size_t nr) {
        this.resultSkip = nr;

        return this;
      }

      /// Get the size of the selected items
      size_t size() @trusted {
        return exec.count;
      }
    }

    Json applyProjection(Json value) {
      auto item = Json.emptyObject;

      foreach(field; projectionFields) {
        if(field in value) {
          item[field] = value[field];
        }
      }

      return item;
    }

    string debugString() {
      return queryBuilder.build.toJson.toPrettyString;
    }

    bool itemFilter(const Json item) {
      auto query = queryBuilder.build.toJson;
      auto result = item.matches(query);

      return result;
    }
  }
}


///
bool handleOperation(const Json value, const Json operation) @trusted {
  if(operation.type == Json.Type.object) {
    foreach(string key, query; operation) {
      if(key == "$or") {
        return orMatch(value, query);
      }

      if(key == "$and") {
        return andMatch(value, query);
      }

      if(key == "$ne") {
        return !handleOperation(value, query);
      }

      if(key == "$not") {
        return !matches(value, query);
      }

      if(key == "$in") {
        return inMatch(value, query);
      }

      if(key == "$all") {
        return allMatch(value, query);
      }

      if(key == "$elemMatch") {
        return elementMatch(value, query);
      }

      if(key == "$geoWithin") {
        return geoWithinMatch(value, query);
      }

      if(key == "$geoIntersects") {
        return geoIntersectsMatch(value, query);
      }

      if(key == "$regex") {
        return regexMatch(value, query);
      }

      if(key == "$text") {
        return textMatch(value, query);
      }

      if(key == "$near") {
        return true;
      }

      if(key == "$gt") {
        return gtMatch(value, query);
      }

      if(key == "$lt") {
        return ltMatch(value, query);
      }

      if(key == "$exists") {
        return existsMatch(value, query);
      }
    }
  }

  if(value.type == Json.Type.array) {
    foreach(element; value) {
      if(element == operation) {
        return true;
      }
    }

    return false;
  }

  return value == operation;
}

///
bool geoWithinMatch(const Json value, Json expectedGeometry) {

  if("$geometry" in expectedGeometry) {
    return value.geoWithinMatch(expectedGeometry["$geometry"]);
  }

  Polygon polygon;

  if(expectedGeometry["type"] == "MultiPolygon") {
    auto multiPolygon = expectedGeometry.deserializeJson!MultiPolygon;
    polygon = Polygon(multiPolygon.coordinates[0]);
  } else {
    polygon = expectedGeometry.deserializeJson!Polygon;
  }

  if(value.type != Json.Type.object) {
    return false;
  }

  if("type" !in value) {
    return false;
  }

  if(value["type"] == "Point") {
    return value.deserializeJson!Point.isInside(polygon);
  }

  if(value["type"] == "MultiPoint") {
    return value.deserializeJson!MultiPoint.isInside(polygon);
  }

  if(value["type"] == "LineString") {
    return value.deserializeJson!LineString.isInside(polygon);
  }

  if(value["type"] == "MultiLineString") {
    return value.deserializeJson!MultiLineString.isInside(polygon);
  }

  if(value["type"] == "Polygon") {
    return value.deserializeJson!Polygon.isInside(polygon);
  }

  if(value["type"] == "MultiPolygon") {
    return value.deserializeJson!MultiPolygon.isInside(polygon);
  }

  return false;
}

///
bool geoIntersectsMatch(const Json value, Json expectedGeometry) {
  if("$geometry" in expectedGeometry) {
    return value.geoIntersectsMatch(expectedGeometry["$geometry"]);
  }

  if(expectedGeometry["type"] == "Point" && value["type"] == "Point") {
    return false;
  }

  if(expectedGeometry["type"] == "Point" && value["type"] != "Point") {
    return geoIntersectsMatch(expectedGeometry, value);
  }

  Polygon polygon;

  if(expectedGeometry["type"] == "MultiPolygon") {
    auto multiPolygon = expectedGeometry.deserializeJson!MultiPolygon;
    polygon = Polygon(multiPolygon.coordinates[0]);
  } else {
    polygon = expectedGeometry.deserializeJson!Polygon;
  }

  if(value.type != Json.Type.object) {
    return false;
  }

  if("type" !in value) {
    return false;
  }

  if(value["type"] == "Point") {
    return value.deserializeJson!Point.isInside(polygon);
  }

  if(value["type"] == "MultiPoint") {
    return value.deserializeJson!MultiPoint.intersects(polygon) || value.deserializeJson!MultiPoint.isInside(polygon);
  }

  if(value["type"] == "LineString") {
    return value.deserializeJson!LineString.intersects(polygon) || value.deserializeJson!LineString.isInside(polygon);
  }

  if(value["type"] == "MultiLineString") {
    return value.deserializeJson!MultiLineString.intersects(polygon) || value.deserializeJson!MultiLineString.isInside(polygon);
  }

  if(value["type"] == "Polygon") {
    return value.deserializeJson!Polygon.intersects(polygon) || value.deserializeJson!Polygon.isInside(polygon);
  }

  if(value["type"] == "MultiPolygon") {
    return value.deserializeJson!MultiPolygon.intersects(polygon) || value.deserializeJson!MultiPolygon.isInside(polygon);
  }

  return false;
}

///
bool inMatch(const Json value, const Json expectedList) {
  if(value.type == Json.Type.array) {
    foreach(element; value) {
      if(element.inMatch(expectedList)) {
        return true;
      }
    }

    return false;
  }

  foreach(expected; expectedList) {
    if(value == expected) {
      return true;
    }
  }

  return false;
}

///
bool allMatch(const Json value, const Json expectedList) {
  if(value.type != Json.Type.array || expectedList.type != Json.Type.array) {
    return false;
  }

  auto strValue = value.byValue.map!(a => a.to!string).array;
  auto strExpected = expectedList.byValue.map!(a => a.to!string).array;

  foreach(item; strExpected) {
    if(!strValue.canFind(item)) {
      return false;
    }
  }

  return true;
}

///
bool regexMatch(const Json value, const Json query) {
  auto re = regex(query.to!string);
  return !matchFirst(value.to!string, re).empty;
}

bool textMatch(const Json value, const Json query) {
  auto re = regex(query["$search"].to!string);
  return !matchFirst(value.to!string, re).empty;
}

bool gtMatch(const Json value, const Json query) {
  auto a = SysTime.fromISOExtString(value.to!string);
  auto b = SysTime.fromISOExtString(query.to!string);

  return a > b;
}

bool ltMatch(const Json value, const Json query) {
  auto a = SysTime.fromISOExtString(value.to!string);
  auto b = SysTime.fromISOExtString(query.to!string);

  return a < b;
}

///
bool existsMatch(const Json value, const Json query) {
  auto a = value.type != Json.Type.null_ && value.type != Json.Type.undefined;
  auto b = query.to!bool;

  return a == b;
}

///
bool elementMatch(const Json list, const Json query) {
  if(list.type != Json.Type.array) {
    return false;
  }

  foreach(item; list) {
    if(matches(item, query)) {
      return true;
    }
  }

  return false;
}

///
bool orMatch(const Json item, const Json queryList) {

  foreach(query; queryList) {
    if(item.matches(query)) {
      return true;
    }
  }

  return false;
}

///
bool andMatch(const Json item, const Json queryList) {
  foreach(query; queryList) {
    if(!item.matches(query)) {
      return false;
    }
  }

  return true;
}

///
bool matches(const Json item, const Json query) @trusted {
  if(item.type != Json.Type.object && item.type != Json.Type.array) {
    return item.handleOperation(query);
  }

  if(item.type == Json.Type.array) {
    return item.handleOperation(query);
  }

  bool result = true;
  foreach(string key, operation; query) {
    if(key[0] == '$') {
      result = result && item.handleOperation(query);
      continue;
    }

    auto flatIndex = key.indexOf(".");

    if(flatIndex != -1) {
      string firstKey = key[0..flatIndex];
      string nextKey = key[flatIndex+1..$];
      auto nextOperation = Json.emptyObject;
      nextOperation[nextKey] = operation;

      bool partialResult = item[firstKey].matches(nextOperation);

      if(item[firstKey].type == Json.Type.array) {
        foreach(arrayItem; item[firstKey]) {
          partialResult = partialResult || arrayItem.matches(nextOperation);
        }
      }

      result = result && partialResult;
    } else {
      result = result && item[key].handleOperation(operation);
    }
  }

  return result;
}

/// It should select by json field
unittest {
  auto query = MemoryQuery();
  query.where("field").equal("1");

  [ `{ "field": "1" }`.parseJsonString,
    `{ "field": "2" }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ "field": "1" }`.parseJsonString ]);
}

/// It should find text in the document
unittest {
  auto query = MemoryQuery();
  query.searchText("1");

  [ `{ "field": "1" }`.parseJsonString,
    `{ "field": "2" }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ "field": "1" }`.parseJsonString ]);
}

/// It should be able to get a projection
unittest {
  auto query = MemoryQuery([ `{ "a": "1", "b": "11" }`.parseJsonString,
    `{ "a": "2", "c": "22" }`.parseJsonString,
    `{ }`.parseJsonString ]);
  query.withProjection([ "b", "c" ]);

  query.exec.array
    .should
      .equal([ `{ "b": "11" }`.parseJsonString, `{ "c": "22" }`.parseJsonString, `{}`.parseJsonString ]);
}

/// It should select by json field negation
unittest {
  auto query = MemoryQuery();
  query.where("field").not.equal("1");

  [ `{ "field": "1" }`.parseJsonString,
    `{ "field": "2" }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ "field": "2" }`.parseJsonString, `{ }`.parseJsonString ]);
}

/// It should sort items
unittest {
  auto items = [
    `{"a": "none", "b": "3"}`.parseJsonString,
    `{"a": "first", "b": "1"}`.parseJsonString,
    `{"a": "first", "b": "2"}`.parseJsonString,
    `{"a": "first", "b": "3"}`.parseJsonString,
    `{"a": "first", "b": "4"}`.parseJsonString ];

  auto query = MemoryQuery(items);

  query.sort("b", 1);
  query.exec.map!(a => a["b"].to!string).array.should.equal(["1", "2", "3", "3", "4"]);
}

/// It should select by anyOf
unittest {
  auto query = MemoryQuery();
  query.where("field").anyOf(["1", "2"]);

  [ `{ "field": ["1"] }`.parseJsonString,
    `{ "field": ["2"] }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ "field": ["1"] }`.parseJsonString, `{ "field": ["2"] }`.parseJsonString ]);
}

/// It should select by anyOf negation
unittest {
  auto query = MemoryQuery();
  query.where("field").not.anyOf(["1", "2"]);

  [ `{ "field": ["1"] }`.parseJsonString,
    `{ "field": ["2"] }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ }`.parseJsonString ]);
}

/// It should select by containsAny
unittest {
  auto query = MemoryQuery();
  query.where("field").containsAny(["1", "2"]);

  [ `{ "field": "1" }`.parseJsonString,
    `{ "field": "2" }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ "field": "1" }`.parseJsonString, `{ "field": "2" }`.parseJsonString ]);
}

/// It should select by containsAny negation
unittest {
  auto query = MemoryQuery();
  query.where("field").not.containsAny(["1", "2"]);

  [ `{ "field": "1" }`.parseJsonString,
    `{ "field": "2" }`.parseJsonString,
    `{ }`.parseJsonString ]
    .filter!(a => query.itemFilter(a))
    .should
      .equal([ `{ }`.parseJsonString ]);
}

/// test or aggregation
unittest {
  auto query = MemoryQuery([
    `{ "field1": "value1", "field3": "value3" }`.parseJsonString,
    `{ "field2": "value2", "field3": "value3" }`.parseJsonString,
    `{ }`.parseJsonString ]);

  query.or
    .where("field1").equal("value1")
    .or
    .where("field2").equal("value2")
    .or
    .where("field3").equal("value3");

  query.exec.array.should
      .equal([
        `{ "field1": "value1", "field3": "value3" }`.parseJsonString,
        `{ "field2": "value2", "field3": "value3" }`.parseJsonString ]);
}

/// It should find data by polygon
unittest {
  auto data = `{
    "_id": "000000000000000000000001",
    "location": {
      "type": "Point", "coordinates": [-73.856077, 40.848447] }}`.parseJsonString;

  auto query = new MemoryQuery([data]);

  query.where("location")
    .insidePolygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]);

  query.exec.front.should.equal(data);
  query.where("location")
    .not.insidePolygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]);

  query.exec.array.length.should.equal(0);
}

/// It should find data by polygon intersection
unittest {
  auto data = `{
    "_id": "000000000000000000000001",
    "location": {
      "type": "Polygon", "coordinates": [[[0, 0], [10, 0], [10, 10], [0, 10]]] } }`.parseJsonString;

  auto query = new MemoryQuery([data]);

  query.where("location")
    .intersectsPolygon([[[5, 5], [30, 5], [30, 30], [5, 30]]]);

  query.exec.front.should.equal(data);
  query.where("location")
    .not.intersectsPolygon([[[5, 5], [30, 5], [30, 30], [5, 30]]]);

  query.exec.array.length.should.equal(0);
}

/// It should find data by multi polygon intersection
unittest {
  auto data = `{
    "_id": "000000000000000000000001",
    "location": {
      "type": "MultiPolygon", "coordinates": [[[[0, 0], [10, 0], [10, 10], [0, 10]]]] } }`.parseJsonString;

  auto query = new MemoryQuery([data]);

  query.where("location")
    .intersectsPolygon([[[5, 5], [30, 5], [30, 30], [5, 30]]]);

  query.exec.front.should.equal(data);
  query.where("location")
    .not.intersectsPolygon([[[5, 5], [30, 5], [30, 30], [5, 30]]]);

  query.exec.array.length.should.equal(0);
}

/// It should find data by point
unittest {
  Json data = `{
    "_id": "573cbc2fc3b7025427000000",
    "location": {
      "type": "Polygon", "coordinates": [[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]] },
    "name": "Morris Park Bake Shop" }`.parseJsonString;

  auto query = new MemoryQuery([data]);

  query.where("location")
    .containsPoint(-75.41015624999999,41.31082388091818);

  query.exec.front.should.equal(data);

  query = new MemoryQuery([data]);
  query.where("location")
    .not.containsPoint(-75.41015624999999,41.31082388091818);

  query.exec.array.length.should.equal(0);
}

/// It should find data by arraycontains
unittest {
  auto data = `{
    "_id": "000000000000000000000001",
    "location": ["1", "2"]}`.parseJsonString;

  auto query = new MemoryQuery([data]);
  query.where("location").arrayContains("1");
  query.exec.front.should.equal(data);

  query = new MemoryQuery([data]);
  query.where("location").not.arrayContains("3");
  query.exec.front.should.equal(data);

  query = new MemoryQuery([data]);
  query.where("location").not.arrayContains("1");
  query.exec.array.length.should.equal(0);
}

/// It should find distinct data
unittest {
  Json[] data = [
    `{"_id": "1", "a": "none", "b": "1"}`.parseJsonString,
    `{"_id": "2", "a": "first", "b": "2"}`.parseJsonString,
    `{"_id": "3", "a": "first", "b": "3"}`.parseJsonString,
    `{"_id": "4", "a": "first", "b": "3"}`.parseJsonString,
    `{"_id": "5", "a": "first", "b": "4"}`.parseJsonString
  ];

  auto query = new MemoryQuery(data);

  query.where("a").equal("first");
  query.distinct("b").map!(a => a.to!string).array.should.equal(["2", "3", "4"]);
}

/// It should find by all values in array
unittest {
  auto data = `{
    "_id": "000000000000000000000001",
    "location": ["1", "2", "3"]}`.parseJsonString;

  auto query = new MemoryQuery([data]);
  query.where("location").containsAll(["1", "2"]);
  query.exec.front.should.equal(data);

  query = new MemoryQuery([data]);
  query.where("location").not.containsAll(["4", "2"]);
  query.exec.front.should.equal(data);

  query = new MemoryQuery([data]);
  query.where("location").not.containsAll(["1", "2"]);
  query.exec.array.length.should.equal(0);
}
