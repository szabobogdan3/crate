/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.csv;

import std.string;
import std.conv;
import std.algorithm;
import std.csv;
import std.array;
import std.file;
import std.stdio : File;
import std.range;

import vibe.data.json;
import vibe.core.file;

import crate.base;
import crate.json;
import crate.error;
import crate.collection.memory;

version(unittest) {
  import fluent.asserts;
}

///
struct Head {
  ulong index;
  alias index this;
}

/// Create a csv line range from file
auto csvLineRange(string fileName) {
  auto range = File(fileName)
      .byLine
        .map!(a => a.to!string)
        .drop(1);

  auto r = CsvLineRange!(typeof(range))(range);

  if(!r.empty) {
    r.front;
  }

  return r;
}

/// Range used to iterate the Csv Lines
struct CsvLineRange(T) {
  T range;

  private {
    string value;
  }

  private bool isLoaded() {
    return value.count(`"`) % 2 == 0;
  }

  string front() {
    if(value == "") {
      value = range.front;
      range.popFront;

      while(!isLoaded && !range.empty) {
        value ~= "\n" ~ range.front;
        range.popFront;
      }
    }

    return value;
  }

  void popFront() {
    value = "";

    if(!range.empty) {
      front();
    }
  }

  bool empty() {
    return range.empty && value == "";
  }
}

/// Get the csv header
Head[string] getHeader(string header) {
  Head[string] result;

  auto range = csvReader!string(header).front;

  if(range.empty) {
    return result;
  }

  string[] pieces = range.array;

  foreach(index, value; pieces) {
    result[value] = Head(index);
  }

  return result;
}

/// It should get mappings for a csv table header
unittest {
  auto header = "country,map,name".getHeader;

  header["country"].index.should.equal(0);
  header["map"].index.should.equal(1);
  header["name"].index.should.equal(2);
}

/// Get a row value by header name
string get(string[] row, string name, Head[string] header) {
  return row[header[name].index];
}

/// ditto
string get(string name)(string[] row, Head[string] header) {
  return row[header[name].index];
}

/// It should map row values to header values
unittest {
  auto header = "country,map,name"
    .getHeader;

  auto row = "a,b,c".split(",");

  row.get!"country"(header).should.equal("a");
  row.get("country", header).should.equal("a");

  row.get!"map"(header).should.equal("b");
  row.get("map", header).should.equal("b");

  row.get!"name"(header).should.equal("c");
  row.get("name", header).should.equal("c");
}

/// Deserialize a csv row to a structure
Json csvToJson(string value, Head[string] header) {
  auto obj = Json.emptyObject;
  string[] pieces;

  try {
    pieces = csvReader!string(value).front.array;
  } catch(Exception e) {
    throw new CrateException("Can not parse csv line: `" ~ value ~ "`: " ~ e.message.to!string, e.file, e.line);
  }

  foreach(string key, head; header) {
    obj.setNestedValue(key.split("."), pieces.get(key, header));
  }

  return obj;
}

/// Set a value in a json object
void setNestedValue(ref Json obj, string[] path, string value) {
  if(path.length == 0) {
    return;
  }

  string key = path[0];

  if(obj.type != Json.Type.object) {
    obj = Json.emptyObject;
  }

  if(path.length == 1) {
    if(isNumeric(value) || value == "true" || value == "false") {
      try {
        obj[path[0]] = parseJsonString(value);
      } catch(JSONException) {
        obj[path[0]] = value;
      }
    } else {
      obj[path[0]] = value;
    }
    return;
  }

  if(key !in obj) {
    obj[key] = Json.emptyObject;
  }

  setNestedValue(obj[key], path[1..$], value);
}

/// Serialize a json to a csv row string
string jsonToCsv(Json item, Head[string] header) {
  string[] row;

  row.length = header.length;

  foreach(key, value; item.byKeyValue) {
    if(key !in header) {
      continue;
    }

    auto index = header[key].index;
    row[index] = value.to!string.escapeCsvValue;
  }

  return row.join(",");
}

/// Csv row to json convertion
unittest {
  auto header = "a,b,c,level1.a,level1.b,level1.c".getHeader;
  auto result = (ulong.max.to!string ~ ",2.3," ~ long.min.to!string ~",true,5,6").csvToJson(header);

  result.should.equal(`{
    "a": 18446744073709551615,
    "b": 2.3,
    "c": -9223372036854775808,
    "level1": {
      "a": true,
      "b": 5,
      "c": 6
    }
  }`.parseJsonString);
}

/// Csv row to json convertion with string escaping
unittest {
  auto header = "a,b".getHeader;
  auto result = `a,",r"""""`.csvToJson(header);

  result.should.equal(`{
    "a": "a",
    "b": ",r\"\"" }`.parseJsonString);
}

///
string escapeCsvValue(string value) {
  if(value.canFind(",") || value.canFind(`"`)) {
    value = `"` ~ value.replace(`"`, `""`) ~ `"`;
  }

  if(value.canFind("\n") && !value.startsWith('"') && !value.endsWith('"')) {
    value = "\"" ~ value ~ "\"";
  }

  return value;
}

/// it should wrap value with quotes if it contains a new line
unittest {
  escapeCsvValue("1\n2").should.equal("\"1\n2\"");
}

///
class CsvCrate(T): Crate!T {
  private {
    immutable string filename;
    Head[string] header;
  }

  this(const string filename) {
    this.filename = filename;

    if(existsFile(filename) && getFileInfo(filename).size > 0) {
      auto stream = openFile(filename);
      string fileData;

      while(!fileData.canFind("\n") && !stream.empty) {
        ubyte[] tmp = new ubyte[stream.leastSize];
        stream.read(tmp);

        fileData ~= cast(string) tmp;
      }

      auto pos = fileData.indexOf("\n");
      if(pos == -1) pos = fileData.length;

      try {
        header = fileData[0..pos].strip.getHeader;
      } catch (Exception e) {
        throw new CrateException("Can not parse the csv header(" ~ filename ~ "): " ~ e.message.to!string, e.file, e.line);
      }

      stream.close;
    }
  }

  @trusted:
    /// Get a range of parsed objects
    IQuery get() {
      if(header.length == 0) {
        return new MemoryQuery([]);
      }

      auto records = csvLineRange(filename).map!(a => a.csvToJson(header));

      return new MemoryQuery(records);
    }

    /// Add a new row to the csv file
    Json addItem(const Json item) {
      Json flattenItem = item.toFlatJson;

      if(!existsFile(filename) || getFileInfo(filename).size == 0) {
        auto strHeader = flattenItem
            .byKeyValue
            .map!(a => a.key.escapeCsvValue)
            .joiner(",")
            .array.to!string;

        header = strHeader.getHeader;
        appendToFile(filename, strHeader);
      }

      auto strRow = flattenItem.jsonToCsv(header);
      appendToFile(filename, "\n" ~ strRow);

      return strRow.csvToJson(header);
    }

    /// Get an item by id. If the `_id` field is missing from header, the
    /// row number will be used as an id
    IQuery getItem(const string id) {
      if(header.length == 0) {
        return new MemoryQuery([]);
      }

      auto records = csvLineRange(filename);

      if("_id" !in header) {
        auto line = id.to!ulong;
        auto items = records.drop(line).take(1).map!(a => a.csvToJson(header));

        return new MemoryQuery(items);
      }

      auto query = new MemoryQuery(records.map!(a => a.csvToJson(header)));
      query.where("_id").equal(id);

      return query;
    }

    /// Edit an item
    Json editItem(const string id, const Json fields) {
      auto records = csvLineRange(filename);

      string data;

      if("_id" !in header) {
        auto lineNumber = id.to!ulong;
        rangeWriter(filename, records
          .enumerate(0UL)
          .map!(a => a.index != lineNumber ? a.value : fields.jsonToCsv(header))
        );
      } else {
        rangeWriter(filename, records
          .map!(a => a.csvToJson(header))
          .map!(a => a["_id"].to!string != id ? a : fields)
          .map!(a => a.jsonToCsv(header)));
      }

      return fields.jsonToCsv(header).csvToJson(header);
    }

    /// Update an item by id
    Json updateItem(const Json item) {
      return editItem(item["_id"].to!string, item);
    }

    private void rangeWriter(T)(string filename, T data) {
      auto file = File(filename ~ ".tmp", "w");

      string strHeader = header
        .byKeyValue
        .array
        .sort!((a, b) => a.value.index < b.value.index)
        .map!(a => a.key)
        .joiner(",")
        .array
        .to!string;

      file.writeln(strHeader);

      foreach(item; data) {
        file.writeln(item);
      }

      filename.remove;
      rename(filename ~ ".tmp", filename);
    }

    /// Delete an item by id. If the `_id` field is missing from header, the
    /// row number will be used as an id
    void deleteItem(const string id) {
      auto records = csvLineRange(filename);

      if("_id" !in header) {
        auto lineNumber = id.to!ulong;
        rangeWriter(filename, records
          .enumerate(0UL)
          .filter!(a => a.index != lineNumber)
          .map!(a => a.value));
      } else {
        rangeWriter(filename, records
          .map!(a => a.csvToJson(header))
          .filter!(a => a["_id"].to!string != id)
          .map!(a => a.jsonToCsv(header)));
      }
    }
}

version(unittest) {
  struct SimpleTest {
    int a;
  }
}

/// It should add two lines to the csv file
unittest {
  import std.random;

  if("test.csv".exists) {
    "test.csv".remove;
  }

  auto crate = new CsvCrate!SimpleTest("test.csv");

  auto randomGenerator = Random(unpredictableSeed);
  auto key = uniform(0, 1000, randomGenerator).to!string;
  auto value = uniform(0, 1000, randomGenerator).to!string;

  crate.addItem((`{"` ~ key ~ `": ` ~ value ~ `}`).parseJsonString);
  crate.addItem((`{"` ~ key ~ `": ` ~ value ~ `}`).parseJsonString);

  readText("test.csv").should.equal(key ~ "\n" ~ value ~"\n" ~ value);
}

/// It should add items on multiple lines
unittest {
  import std.random;

  if("test.csv".exists) {
    "test.csv".remove;
  }

  auto crate = new CsvCrate!SimpleTest("test.csv");

  auto randomGenerator = Random(unpredictableSeed);
  auto key = uniform(0, 1000, randomGenerator).to!string;
  auto value = uniform(0, 1000, randomGenerator).to!string ~ "\n" ~ uniform(0, 1000, randomGenerator).to!string;

  crate.addItem((`{"` ~ key ~ `": "` ~ value ~ `"}`).parseJsonString);
  crate.addItem((`{"` ~ key ~ `": "` ~ value ~ `"}`).parseJsonString);

  readText("test.csv").should.equal(key ~ "\n\"" ~ value ~ "\"\n\"" ~ value ~ "\"");
}

/// It should add a header on the first add if the file is empty
unittest {
  writeFile("test.csv", []);

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.addItem(`{ "a": 1 }`.parseJsonString);

  readText("test.csv").should.equal("a\n1");
}

/// It should add items from more levels of the object
unittest {
  if("test.csv".exists) {
    "test.csv".remove;
  }

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.addItem(`{"a": { "b" : 1},"c": 2, "d": 3, "e": 4}`.parseJsonString);

  auto lines = readText("test.csv").split("\n");
  auto names = lines[0].split(",");
  auto values = lines[1].split(",");

  auto result = zip(names, values).assocArray;

  result["a.b"].should.equal("1");
  result["c"].should.equal("2");
  result["d"].should.equal("3");
  result["e"].should.equal("4");
}

/// It should escape the csv values on adding items
unittest {
  if("test.csv".exists) {
    "test.csv".remove;
  }

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.addItem(`{"\"lorem,ipsum": "\"lorem,ipsum"}`.parseJsonString);

  readText("test.csv").should.equal("\"\"\"lorem,ipsum\"\n\"\"\"lorem,ipsum\"");
}

/// It should keep the header order on adding items
unittest {
  writeFile("test.csv", cast(ubyte[]) "a,b,c,d,e");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.addItem(`{"a": 1, "b": 2, "c": 3, "d": 4, "e": 5}`.parseJsonString);
  readText("test.csv").should.equal("a,b,c,d,e\n1,2,3,4,5");
}

/// It should add only the existing fields
unittest {
  writeFile("test.csv", cast(ubyte[]) "a,b");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  auto result = crate.addItem(`{"a": 1, "b": 2, "c": 3, "d": 4, "e": 5}`.parseJsonString);
  result.should.equal(`{"a": 1, "b": 2}`.parseJsonString);
  readText("test.csv").should.equal("a,b\n1,2");
}

/// Get an object from a csv file by line number
unittest {
  writeFile("test.csv", cast(ubyte[]) "a,b\n1,2\n3,4");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.getItem("1").exec.front.should.equal(`{ "a": 3, "b": 4 }`.parseJsonString);
}

/// Get csv values on multiple lines
unittest {
  writeFile("test.csv", cast(ubyte[]) "a,b\n\"q\nw\",\"a\ns\"");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.getItem("0").exec.front.should.equal(`{ "a": "q\nw", "b": "a\ns" }`.parseJsonString);
}

/// Get an object from a csv file by _id if exists
unittest {
  writeFile("test.csv", cast(ubyte[]) "_id,b\na,2\nb,4");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.getItem("b").exec.front.should.equal(`{ "_id": "b", "b": 4 }`.parseJsonString);
}

/// Get a range of parsed objects
unittest {
  writeFile("test.csv", cast(ubyte[]) "_id,b\na,2\nb,4");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.get.exec.array.serializeToJson.should.equal(`[
    { "_id": "a", "b": 2 },
    { "_id": "b", "b": 4 }]`.parseJsonString);
}

/// Delete a csv item by row number
unittest {
  writeFile("test.csv", cast(ubyte[]) "a,b\n1,2\n3,4");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.deleteItem("0");
  crate.get.exec.array.serializeToJson.should.equal(`[
    { "a": 3, "b": 4 }
  ]`.parseJsonString);
}

/// Delete a csv item by `_id` field
unittest {
  writeFile("test.csv", cast(ubyte[]) "_id,b\n1,2\n3,4");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  crate.deleteItem("1");
  crate.get.exec.array.serializeToJson.should.equal(`[
    { "_id": 3, "b": 4 }
  ]`.parseJsonString);
}

/// Edit a csv item by row number
unittest {
  writeFile("test.csv", cast(ubyte[]) "a,b\n1,2\n3,4\n5,6");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  auto newValue = `{"a": 11, "b": 22}`.parseJsonString;

  crate.editItem("0", newValue);
  crate.get.exec.array.serializeToJson.should.equal(`[
    { "a": 11, "b": 22 },
    { "a": 3, "b": 4 },
    { "a": 5, "b": 6 }
  ]`.parseJsonString);
}

/// Edit a csv item by id
unittest {
  writeFile("test.csv", cast(ubyte[]) "_id,b\n1,2\n3,4\n5,6");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  auto newValue = `{"_id": 11, "b": 22}`.parseJsonString;

  crate.editItem("1", newValue);
  crate.get.exec.array.serializeToJson.should.equal(`[
    { "_id": 11, "b": 22 },
    { "_id": 3, "b": 4 },
    { "_id": 5, "b": 6 }
  ]`.parseJsonString);
}

/// After editing a csv you should have a json containing only the stored fields
unittest {
  writeFile("test.csv", cast(ubyte[]) "_id,b\n1,2\n3,4\n5,6");

  auto crate = new CsvCrate!SimpleTest("test.csv");
  auto newValue = `{"_id": 11, "b": 22, "c": 1}`.parseJsonString;

  auto result = crate.editItem("1", newValue);

  result.should.equal(`{"_id": 11, "b": 22}`.parseJsonString);
}
