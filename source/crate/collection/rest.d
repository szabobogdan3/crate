/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.rest;

import crate.base;
import crate.error;

import std.range;
import std.algorithm;
import std.string;
import std.conv;
import std.datetime;

import vibe.http.common;
import vibe.data.json;
import vibe.data.bson;

alias RestCall = Json delegate(HTTPMethod method, string url, string bodyData) @safe;

InputRange!Json unwrapResponse(Json result, string key) @safe {
  Json[] list;

  if(result.type == Json.Type.array) {
    return (cast(Json[]) result).inputRangeObject;
  }

  if(result.type != Json.Type.object) {
    return list.inputRangeObject;
  }

  () @trusted {
    if(result[key].type == Json.Type.array) {
      list = (cast(Json[]) result[key]);
    }

    if(result[key].type == Json.Type.object) {
      list = [ result[key] ];
    }
  } ();

  return list.inputRangeObject;
}

Json validateResponse(Json result) @safe {
  if(result.type != Json.Type.object) {
    return result;
  }

  if("errors" in result) {
    throw result["errors"].toException;
  }

  if("error" in result) {
    throw result["error"].toException;
  }

  return result;
}

class RestCrate : CrateAccess {
  private {
    string baseUrl;
    RestCall restCall;
  }

  public {
    string plural;
    string singular;
  }

  @safe:
    this(string baseUrl, RestCall restCall) {
      this.baseUrl = baseUrl.stripRight("/");
      this.restCall = restCall;
    }

    /// Get items from the crate
    IQuery get() {
      return new RestQuery(this.baseUrl, this.restCall, plural);
    }

    /// Get one item by id
    IQuery getItem(const string id) {
      return new RestQuery(this.baseUrl, this.restCall, id, singular);
    }

    /// Add a new item
    Json addItem(const Json item) @trusted {
      auto newItem = Json.emptyObject;

      if(singular != "") {
        newItem[singular] = item;
      } else {
        newItem = item;
      }

      auto response = restCall(HTTPMethod.POST, baseUrl, newItem.to!string).validateResponse;
      auto range = response.unwrapResponse(singular);

      if(range.empty) {
        return Json();
      }

      return range.front;
    }

    /// Update an existing item
    Json updateItem(const Json item) @trusted {
      string id;

      if("_id" in item) {
        id = item["_id"].to!string;
      }

      if("id" in item) {
        id = item["id"].to!string;
      }

      auto newItem = Json.emptyObject;

      if(singular != "") {
        newItem[singular] = item;
      } else {
        newItem = item;
      }

      auto response = restCall(HTTPMethod.PUT, baseUrl ~ "/" ~ id, newItem.to!string).validateResponse;
      auto range = response.unwrapResponse(singular);

      if(range.empty) {
        return Json();
      }

      return range.front;
    }

    /// Delete an existing item
    void deleteItem(const string id) {
      restCall(HTTPMethod.DELETE, baseUrl ~ "/" ~ id, "").validateResponse;
    }
}

///
class RestQuery : IQuery {
  private {
    string baseUrl;
    string id;
    string key;

    RestCall restCall;
    RestFieldQuery[string] params;
  }

  @safe:
    this(string baseUrl, RestCall restCall, string key) {
      this.baseUrl = baseUrl;
      this.restCall = restCall;
      this.key = key;
    }

    this(string baseUrl, RestCall restCall, const string id, string key) {
      this.baseUrl = baseUrl ~ "/" ~ id;
      this.restCall = restCall;
      this.key = key;
    }

    string debugString() {
      return params.serializeToJson.toPrettyString;
    }

    ///
    IFieldQuery where(string field) {
      params[field] = new RestFieldQuery(this);
      return params[field];
    }

    ///
    IQuery withProjection(string[] fields) {
      throw new CrateNotImplementedException;
    }

    /// Perform an `or` logical operation
    IQuery or() {
      throw new CrateNotImplementedException;
    }

    IQuery searchText(string query) {
      assert(false, "not implemented");
    }

    IQuery and() {
      return this;
    }

    /// Sort by fields
    IQuery sort(string field, int order) {
      throw new CrateNotImplementedException;
    }

    /// Execute the selector and return a range of JSONs
    InputRange!Json exec() {
      auto url = baseUrl;
      auto keys = () @trusted { return std.algorithm.sort(params.keys); } ();
      string glue = "?";

      foreach(key; keys) {
        url ~= glue ~ key ~ "=" ~ params[key].toString();
        glue = "&";
      }

      auto result = restCall(HTTPMethod.GET, url, "").validateResponse;

      return result.unwrapResponse(key);
    }

    /// Execute the selector and return a range of JSONs
    InputRange!Bson execBson() {
      throw new CrateNotImplementedException;
    }

    /// Execute the selector and return all distinct values of a key
    InputRange!Json distinct(string key) {
      throw new CrateNotImplementedException;
    }

    /// Limit the number of results
    IQuery limit(size_t nr) {
      return this;
    }

    /// Skip a number of results
    IQuery skip(size_t nr) {
      return this;
    }

    /// Create a clone of a current selector
    IQuery dup() {
      throw new CrateNotImplementedException;
    }

    /// Get the size of the selected items
    size_t size() {
      throw new CrateNotImplementedException;
    }
}

///
class RestFieldQuery : IFieldQuery {

  private {
    RestQuery parent;
    string value;
  }

  @safe {
    this(RestQuery parent) {
      this.parent = parent;
    }

    override string toString() {
      return value;
    }

    bool isRootQuery() @safe {
      return false;
    }

    IFieldQuery isNull() { return equal("null"); }

    /// Match an item if exactly one field value
    IFieldQuery equal(string value) {
      this.value = value;
      return this;
    }

    /// ditto
    IFieldQuery equal(bool value) { return equal(value ? "true" : "false"); }

    /// ditto
    IFieldQuery equal(int value) { return equal(value.to!string); }

    /// ditto
    IFieldQuery equal(ulong value) { return equal(value.to!string); }

    /// ditto
    IFieldQuery equal(ObjectId value) { return equal(value.toString); }

    ///
    IFieldQuery greaterThan(SysTime value) { assert(false, "not implemented"); }

    ///
    IFieldQuery lessThan(SysTime) { assert(false, "not implemented"); }

    /// Check if the field exists
    IFieldQuery exists() { assert(false, "not implemented"); }

    /// Match an item if a value from an array contains one field value
    IFieldQuery arrayContains(string value) { assert(false, "not implemented"); }

    /// Match an item if the field equals the same string by ignoring the case
    IFieldQuery equalIgnoreCase(string value) { assert(false, "not implemented"); }

    /// ditto
    IFieldQuery arrayContains(ulong value) {
      return this.arrayContains(value.to!string);
    }

    /// ditto
    IFieldQuery arrayContains(ObjectId value) {
      return this.arrayContains(value.toString);
    }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery anyOf(string[] values) { assert(false, "not implemented"); }
    /// ditto
    IFieldQuery anyOf(ObjectId[] values) { assert(false, "not implemented"); }
    /// ditto
    IFieldQuery anyOf(int[] values) { assert(false, "not implemented"); }

    /// Match an item using a substring
    IFieldQuery like(string value) { assert(false, "not implemented"); }

    /// Match an item if an array field value contains all values from the values list
    IFieldQuery containsAll(string[] values) { assert(false, "not implemented"); }
    /// ditto
    IFieldQuery containsAll(ObjectId[] values) { assert(false, "not implemented"); }

    /// Match an item if a filed value contains at least one value from the values list
    IFieldQuery containsAny(string[] values) { assert(false, "not implemented"); }
    /// ditto
    IFieldQuery containsAny(ObjectId[] values) { assert(false, "not implemented"); }

    /// Match an item if an array field contains an object that has the field equals with the value
    IFieldQuery arrayFieldContains(string field, string value) { assert(false, "not implemented"); }

    /// ditto
    IFieldQuery arrayFieldContains(string field, ObjectId value) { assert(false, "not implemented"); }

    /// Match an item if an array field contains an object that has the field contains the value
    IFieldQuery arrayFieldLike(string field, string value) { assert(false, "not implemented"); }

    /// Match an item if it's coordinates is inside a polygon
    IFieldQuery insidePolygon(double[][][] value) { assert(false, "not implemented"); }

    /// Match an item if it's coordinates intersects a polygon
    IFieldQuery intersectsPolygon(double[][][] value) { assert(false, "not implemented"); }

    ///
    IFieldQuery intersects(Json geometry) { assert(false, "not implemented"); }

    /// Match an item if the polygon contains a point
    IFieldQuery containsPoint(double longitude, double latitude) { assert(false, "not implemented"); }

    /// Match/sort an item relative to a geographical point
    IFieldQuery near(double longitude, double latitude, double meters) { assert(false, "not implemented"); }

    ///
    IFieldQuery not() { assert(false, "not implemented"); }

    ///
    IQuery and() {
      return parent;
    }

    /// Add a query for a new field
    IQuery or() {
      return parent.or;
    }
  }
}

version(unittest) {
  import std.functional;
  import fluent.asserts;

  HTTPMethod lastMethod;
  string lastUrl;
  string lastBodyData;
  Json preparedResponse;

  Json successMockRest(HTTPMethod method, string url, string bodyData) @safe {
    lastMethod = method;
    lastUrl = url;
    lastBodyData = bodyData;

    return preparedResponse;
  }
}

/// it should receive an empty range when the response is an empty object
unittest {
  preparedResponse = Json.emptyObject;
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.get.exec.array.length.should.equal(0);
  lastMethod.should.equal(HTTPMethod.GET);
  lastUrl.should.equal("http://test/model");
  lastBodyData.should.equal("");
}

/// it remove all slashes at the end of the url
unittest {
  preparedResponse = Json.emptyObject;
  auto crate = new RestCrate("http://test/model////", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.get.exec.array.length.should.equal(0);
  lastMethod.should.equal(HTTPMethod.GET);
  lastUrl.should.equal("http://test/model");
  lastBodyData.should.equal("");
}

/// it returns a range with the items
unittest {
  preparedResponse = `{ "items": [{ "a": 1 }, { "a": 2 }] }`.parseJsonString;
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.get.exec.array.length.should.equal(2);
  lastMethod.should.equal(HTTPMethod.GET);
  lastUrl.should.equal("http://test/model");
  lastBodyData.should.equal("");
}

/// it returns a range with the items if they are not wrapped to an object
unittest {
  preparedResponse = `[{ "a": 1 }, { "a": 2 }]`.parseJsonString;
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.get.exec.array.length.should.equal(2);
  lastMethod.should.equal(HTTPMethod.GET);
  lastUrl.should.equal("http://test/model");
  lastBodyData.should.equal("");
}

/// it should query by a variable name
unittest {
  preparedResponse = Json.emptyObject;
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.get
    .where("key1").equal("value1").and
    .where("key2").equal("value2").and
    .exec;

  lastMethod.should.equal(HTTPMethod.GET);
  lastUrl.should.equal("http://test/model?key1=value1&key2=value2");
  lastBodyData.should.equal("");
}

/// it should get items by id
unittest {
  preparedResponse = `{"item": { "a": 1 }}`.parseJsonString;
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.getItem("1").exec.array.length.should.equal(1);

  lastMethod.should.equal(HTTPMethod.GET);
  lastUrl.should.equal("http://test/model/1");
  lastBodyData.should.equal("");
}

/// it should delete items by id
unittest {
  preparedResponse = Json();
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));

  crate.deleteItem("1");

  lastMethod.should.equal(HTTPMethod.DELETE);
  lastUrl.should.equal("http://test/model/1");
  lastBodyData.should.equal("");
}

/// it should throw an exception when the server returns an error
unittest {
  preparedResponse = `{"errors": [{
          "title": "Invalid operation",
          "description": "The name is missing",
          "status": 400 }]}`.parseJsonString;
  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));

  crate.deleteItem("1").should.throwException!CrateException.withMessage("The name is missing");
}

/// it should post new items
unittest {
  preparedResponse = `{"item": {
    "_id": 3,
    "a": 1
  }}`.parseJsonString;

  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.addItem(`{"a":1}`.parseJsonString).should.equal(`{"_id":3,"a":1}`.parseJsonString);

  lastMethod.should.equal(HTTPMethod.POST);
  lastUrl.should.equal("http://test/model");
  lastBodyData.should.equal(`{"item":{"a":1}}`);
}

/// it should return an empty range if the response is empty
unittest {
  preparedResponse = `{}`.parseJsonString;

  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.addItem(`{"a":1}`.parseJsonString).type.should.equal(Json.Type.undefined);

  lastMethod.should.equal(HTTPMethod.POST);
  lastUrl.should.equal("http://test/model");
  lastBodyData.should.equal(`{"item":{"a":1}}`);
}

/// it should update items
unittest {
  preparedResponse = `{"item": {
    "_id": 3,
    "a": 1
  }}`.parseJsonString;

  auto crate = new RestCrate("http://test/model", toDelegate(&successMockRest));
  crate.singular = "item";
  crate.plural = "items";

  crate.updateItem(`{"_id": 3, "a":1}`.parseJsonString).should.equal(`{"_id":3,"a":1}`.parseJsonString);

  lastMethod.should.equal(HTTPMethod.PUT);
  lastUrl.should.equal("http://test/model/3");
  lastBodyData.should.equal(`{"item":{"_id":3,"a":1}}`);
}
