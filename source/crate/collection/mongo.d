/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.mongo;

import crate.collection.BsonQuery;
import crate.collection.proxy;
import crate.base;
import crate.error;
import crate.collection.memory;
import described, introspection.aggregate, introspection.property, introspection.protection;

import vibe.inet.url;
import vibe.http.router;
import vibe.data.json;
import vibe.data.bson;
import vibe.data.serialization;
import vibe.db.mongo.collection;
import vibe.db.mongo.mongo;
import vibe.core.log;

import std.conv, std.array, std.range, std.datetime;
import std.algorithm, std.typecons, std.exception;

import crate.ctfe : ArrayType;
import std.traits;

version(Have_vibe_service) {
  import vibe.service.stats;
}

///
class MongoCrate(T): Crate!T {
  private {
    MongoCollection collection;
    CrateConfig!T _config;
  }

  this(MongoCollection collection, CrateConfig!T config = CrateConfig!T()) {
    this.collection = collection;
    this._config = config;
  }

  this(MongoClient client, string collection, CrateConfig!T config = CrateConfig!T()) {
    this(client.getCollection(collection), config);
  }

  @trusted:
    IQuery get() {
      return new MongoQuery(collection);
    }

    Json addItem(const Json item) {
      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "insert"]);
      }

      auto tmp = item.to!string.parseJsonString;

      if("_id" !in tmp || !isObjectId(tmp["_id"])) {
        tmp["_id"] = ObjectId.generate().toString;
      }

      auto bsonData = toBson!T(tmp);
      collection.insertOne(bsonData);

      CountOptions countOptions;
      countOptions.limit = 1;

      auto inserted = collection.countDocuments(["_id" : fixId(tmp["_id"].to!string, collection.name)], ) > 0;

      if(!inserted) {
        logError("%s", collection.database.getLastError);
      }

      enforce(inserted, "The record was not added.");

      return tmp;
    }

    IQuery getItem(const string id) {
      if (collection.countDocuments(["_id" : fixId(id, collection.name)]) == 0) {
        throw new CrateNotFoundException("There is no item with id `" ~ id ~ "` inside `" ~ collection.name ~ "`");
      }

      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "findOne"]);
      }

      return new MemoryQuery([
        collection.findOne!Json(["_id" : fixId(id, collection.name)]).get
      ]);
    }

    Json editItem(string id, Json fields) {
      CountOptions countOptions;
      countOptions.limit = 1;

      if (collection.countDocuments(["_id" : fixId(id, collection.name)], countOptions) == 0) {
        throw new CrateNotFoundException("There is no item with id `" ~ id ~ "` inside `" ~ collection.name ~ "`");
      }

      auto data = toBson!T(fields);

      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "edit"]);
      }

      collection.replaceOne(["_id" : fixId(id, collection.name)], data);

      return getItem(id).exec.front;
    }

    Json updateItem(const Json item) {
      string id = item["_id"].to!string;

      CountOptions countOptions;
      countOptions.limit = 1;

      if (collection.countDocuments(["_id" : fixId(id, collection.name)], countOptions) == 0) {
        throw new CrateNotFoundException("There is no item with id `" ~ id ~ "` inside `" ~ collection.name ~ "`");
      }

      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "update"]);
      }

      auto updateItem = toBson!T(item);
      collection.replaceOne(["_id" : fixId(id, collection.name)], updateItem);

      return item;
    }

    void deleteItem(const string id) {
      if (collection.countDocuments(["_id" : fixId(id, collection.name)]) == 0) {
        throw new CrateNotFoundException("There is no item with id `" ~ id ~ "` inside `" ~ collection.name ~ "`");
      }

      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "remove"]);
      }

      collection.deleteOne(["_id" : fixId(id, collection.name)]);
    }
}

version (unittest) {
  import crate.http.router;
  import fluent.asserts;
  import vibe.data.serialization;

  bool isTestActionCalled;

  struct EmbededModel {
    string field;
    TestModel relation;
  }

  struct RelationModel
  {
    ObjectId _id;
    string name = "";

    EmbededModel embeded;
    TestModel relation;
    TestModel[] relations;
  }

  struct TestModel
  {
    @optional
    {
      ObjectId _id;
      string other = "";
    }

    string name = "";

    void action()
    {
      isTestActionCalled = true;
    }

    string actionResponse()
    {
      isTestActionCalled = true;

      return "ok.";
    }

    void actionChange()
    {
      name = "changed";
    }
  }
}

///
class MongoQuery : IQuery {
  private {
    BsonQueryBuilder queryBuilder;

    MongoCollection collection;
    size_t resultCount;
    int skipCount;

    int[string] sortedFields;
    string[] projectionFields;
  }

  this() @trusted {
    this.queryBuilder = new BsonQueryBuilder(this);
  }

  this(MongoCollection collection) @trusted {
    this.collection = collection;
    this.queryBuilder = new BsonQueryBuilder(this);
  }

  override @safe {
    string debugString() {
      return this.queryBuilder.serializeToJson.toPrettyString;
    }

    ///
    IQuery sort(string field, int order) {
      this.sortedFields[field] = order;
      return this;
    }

    IFieldQuery where(string field) {
      return queryBuilder.where(field);
    }

    ///
    IQuery withProjection(string[] fields) {
      this.projectionFields = fields;
      return this;
    }

    IQueryBuilder or() {
      return queryBuilder.or();
    }

    IQuery searchText(string query) {
      queryBuilder.searchText(query);
      return this;
    }

    IQuery and() {
      return this;
    }

    /// Execute the selector and return a range of JSONs
    InputRange!Json exec() {
      auto data = queryBuilder.build;

      enforce(collection.name != "", "The collection name is not set");
      logDiagnostic("mongo `%s` exec json: %s sort: %s limit: %s skip: %s", collection.name, data.to!string, this.sortedFields.to!string, resultCount, skipCount);

      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "get"]);
      }

      if(projectionFields.length > 0) {
        Bson[string] fields;

        foreach(i, field; projectionFields) {
          fields[field] = Bson(1);
        }

        return collection.find(data, fields)
          .sort(this.sortedFields)
          .limit(resultCount)
          .skip(skipCount)
          .map!(a => a.toJson)
          .inputRangeObject;
      }

      FindOptions options;
      options.allowDiskUse = true;

      return collection.find!Json(data, options)
        .sort(this.sortedFields)
        .limit(resultCount)
        .skip(skipCount)
        .inputRangeObject;
    }

    /// Execute the selector and return a range of BSONs
    InputRange!Bson execBson() {
      auto data = queryBuilder.build;

      enforce(collection.name != "", "The collection name is not set");
      logDiagnostic("mongo `%s` exec json: %s sort: %s limit: %s skip: %s", collection.name, data.to!string, this.sortedFields.to!string, resultCount, skipCount);

      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "get"]);
      }

      if(projectionFields.length > 0) {
        Bson[string] fields;

        foreach(i, field; projectionFields) {
          fields[field] = Bson(1);
        }

        return collection.find(data, fields)
          .sort(this.sortedFields)
          .limit(resultCount)
          .skip(skipCount)
          .inputRangeObject;
      }

      return collection.find!Bson(data)
        .sort(this.sortedFields)
        .limit(resultCount)
        .skip(skipCount)
        .inputRangeObject;
    }

    /// Execute the selector and return all distinct values of a key
    InputRange!Json distinct(string key) {
      auto data = queryBuilder.build;

      enforce(collection.name != "", "The collection name is not set");
      logDiagnostic("mongo `%s` distinct: %s", collection.name, data.to!string);


      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "distinct"]);
      }

      return collection.distinct!Json(key, data).inputRangeObject;
    }

    /// Limit the number of results
    IQuery limit(size_t nr) {
      this.resultCount = nr;
      return this;
    }

    /// Skip a number of results
    IQuery skip(size_t nr) {
      this.skipCount = nr.to!int;
      return this;
    }

    /// Get the size of the selected items
    size_t size() {
      version(Have_vibe_service) {
        Stats.instance.inc("mongo", ["collection": collection.name, "op": "count"]);
      }

      return collection.countDocuments(queryBuilder.build);
    }
  }
}

/// test field equals
unittest {
  auto query = new MongoQuery();
  query.where("field").equal("value");

  query.queryBuilder.build.toJson.should.equal(`{
    "field": "value"
  }`.parseJsonString);
}

/// and should return the parent query
unittest {
  auto query = new MongoQuery();
  query.where("field").equal("value")
    .and
    .where("other").equal("other value");

  query.queryBuilder.build.toJson.should.equal(`{
    "field": "value",
    "other": "other value"
  }`.parseJsonString);
}

/// test field not equal
unittest {
  auto query = new MongoQuery();
  query.where("field").not.equal("value");

  query.queryBuilder.build.toJson.should.equal(`{
    "field": { "$ne" : "value" }
  }`.parseJsonString);
}

/// test mixed or aggregation
unittest {
  auto query = new MongoQuery();
  query.where("field1").equal("value1");

  query.or.where("field2").equal("value2");
  query.or.where("field4").equal("value4");

  query.where("field3").equal("value3");

  query.queryBuilder.build.toJson.should.equal(`{
    "field2": "value2",
    "field4": "value4",
    "field3": "value3",
    "field1": "value1"
  }`.parseJsonString);
}

auto fixId(string id, string type = "") {
  enforce!CrateNotFoundException(id.length == 24, "Invalid id `" ~ id ~ "` inside `" ~ type ~ "`");

  try {
    return ObjectId.fromString(id).bsonObjectID;
  } catch (ConvException e) {
    throw new CrateNotFoundException("Can't convert id `" ~ id ~ "` inside `" ~ type ~ "`");
  }
}

Bson fixSysTime(Bson result) {
  try {
    string strDate = result.get!string;

    return cast(Bson) BsonDate(SysTime.fromISOExtString(strDate));
  } catch (Exception e) {
    logError(e.to!string);
  }

  return result;
}

///
Bson toBson(T)(Json model) if(!is(T == Aggregate) && !is(T == Property)) {
  auto result = Bson.fromJson(model);

  enum description = describe!T;

  return result.toBson!(T, description);
}

Bson toBson(PT, alias property, alias description)(Bson value) {
  static if(property.type.name == "ObjectId") {
    return value.get!string.fixId(description.type.name ~ " " ~ description.name);
  } else static if(property.type.name == "SysTime") {
    return value.fixSysTime;
  } else static if(property.type.name == "Json" || property.type.name == "Bson") {
    return value;
  } else static if((property.type.isStruct || property.type.isClass) && property.protection == Protection.public_ && property.type.name != "ObjectId" && property.type.name != "SysTime") {
    auto bsonProperty = value;

    enum propertyDescription = describe!PT;
    return bsonProperty.toBson!(PT, propertyDescription, true);
  } else {
    return value;
  }
}

string hasId(alias description)() {
  string result;

  static foreach (property; description.properties) {
    static if(property.name == "id" || property.name == "_id") {
      result = property.name;
    }
  }

  return result;
}

Bson toBson(T, alias description, bool isNested = false)(ref Bson result) if(is(typeof(description) == Aggregate)) {
  static if(description.type.isStruct || description.type.isClass) {
    static if(isNested && hasId!description != "") {
      if(result.type == Bson.Type.string) {
        return result.get!string.fixId(description.type.name ~ " " ~ description.name);
      }

      if(result.type != Bson.Type.object) {
        return result;
      }

      string key = hasId!description;

      if(result[key].type == Bson.Type.string) {
        return result[key].get!string.fixId(description.type.name ~ " " ~ description.name);
      }

      return result[key];
    } else {
      static foreach (property; description.properties) {{
        enum type = property.type;

        static if(property.protection == Protection.public_ && type.name != "Json" && type.valueType != "Json" && type.name != "Bson") {
          mixin("alias PT = typeof(T." ~ property.name ~ ");");

          static if(type.isArray) {
            enum propertyDescription = describe!(ArrayType!PT);

            static if(is(typeof(propertyDescription) == Aggregate)) {
              static if(propertyDescription.type.isStruct || propertyDescription.type.isClass) {
                Bson[] newList;

                if(result[property.name].type == Bson.Type.array) {
                  foreach(size_t index; 0..result[property.name].length) {
                    auto bsonProperty = result[property.name][index];

                    newList ~= bsonProperty.toBson!(ArrayType!PT, propertyDescription, true);
                  }
                }

                result[property.name] = newList.serializeToBson;
              }
            }
          } else static if(type.isAssociativeArray) {
            enum propertyDescription = describe!(ValueType!PT);

            static if(is(typeof(propertyDescription) == Aggregate)) {
              static if(propertyDescription.type.isStruct || propertyDescription.type.isClass) {
                Bson[string] newList;

                if(result[property.name].type == Bson.Type.object) {
                  foreach(pair; result[property.name].byKeyValue) {
                    auto bsonProperty = result[property.name][pair.key];

                    newList[pair.key] = bsonProperty.toBson!(ValueType!PT, propertyDescription, true);
                  }
                }

                result[property.name] = newList.serializeToBson;
              }
            }
          } else if(result.type == Bson.Type.object) {
            result[property.name] = toBson!(PT, property, description)(result[property.name]);
          }
        }
      }}

      return result;
    }
  }
}

/// It converts a SysTime date to a BsonDate
unittest {
  struct Test {
    string _id;
    SysTime time;
  }

  auto bson = toBson!Test(`{ "time": "2015-12-05T12:35:45Z" }`.parseJsonString);

  bson["time"].type.should.equal(Bson.Type.date);
  bson["time"].to!string.should.equal(`"2015-12-05T12:35:45Z"`);
}

/// It converts a SysTime date from nested structs
unittest {
  struct Nested {
    SysTime time;
  }

  struct Test {
    string _id;
    Nested nested;
  }

  auto bson = toBson!Test(`{ "nested": { "time": "2015-12-05T12:35:45Z" } }`.parseJsonString);

  bson["nested"]["time"].type.should.equal(Bson.Type.date);
  bson["nested"]["time"].to!string.should.equal(`"2015-12-05T12:35:45Z"`);
}

/// It converts a SysTime date from nested list structs
unittest {
  struct Nested {
    SysTime time;
  }

  struct Test {
    string _id;
    Nested[] nested;
  }

  auto bson = toBson!Test(`{ "nested": [{ "time": "2015-12-05T12:35:45Z" }] }`.parseJsonString);

  bson["nested"][0]["time"].type.should.equal(Bson.Type.date);
  bson["nested"][0]["time"].to!string.should.equal(`"2015-12-05T12:35:45Z"`);
}

/// It converts a SysTime date from nested hash map structs
unittest {
  struct Nested {
    SysTime time;
  }

  struct Test {
    string _id;
    Nested[string] nested;
  }

  auto bson = toBson!Test(`{ "nested": {"key": { "time": "2015-12-05T12:35:45Z" } } }`.parseJsonString);

  bson["nested"]["key"]["time"].type.should.equal(Bson.Type.date);
  bson["nested"]["key"]["time"].to!string.should.equal(`"2015-12-05T12:35:45Z"`);
}

/// It ignores an undefined SysTime
unittest {
  struct Test {
    string _id;
    SysTime time;
  }

  auto bson = toBson!Test(`{ }`.parseJsonString);

  bson.serializeToJson.should.equal(`{ }`.parseJsonString);
}

/// It keeps the original value when it is not a valid date
unittest {
  struct Nested {
    SysTime time;
  }

  struct Test {
    string _id;
    Nested nested;
  }

  auto bson = toBson!Test(`{ "nested": { "time": "invalid" } }`.parseJsonString);

  bson["nested"]["time"].type.should.equal(Bson.Type.string);
  bson["nested"]["time"].to!string.should.equal(`"invalid"`);
}

/// It ignores missing arrays
unittest {
  struct Nested {
    int value;
  }

  struct Test {
    string _id;
    Nested[] nested;
  }

  auto bson = toBson!Test(`{ }`.parseJsonString);

  bson["nested"].type.should.equal(Bson.Type.array);
  bson["nested"].length.should.equal(0);
  bson.serializeToJson.should.equal(`{ "nested": [] }`.parseJsonString);
}

/// It keeps an id when the nested struct is already an id
unittest {
  struct Nested {
    ObjectId _id;
  }

  struct Test {
    string _id;
    Nested nested;
  }

  auto bson = toBson!Test(`{ "nested": "573cbc2fc3b7025427000000" }`.parseJsonString);

  bson["nested"].type.should.equal(Bson.Type.objectID);
  bson["nested"].to!string.should.equal(`ObjectID(573cbc2fc3b7025427000000)`);
}


/// It converts an ObjectId value type to a bson id
unittest {
  struct Test {
    string _id;
    ObjectId nested;
  }

  auto bson = toBson!Test(`{ "nested": "573cbc2fc3b7025427000000" }`.parseJsonString);

  bson["nested"].type.should.equal(Bson.Type.objectID);
  bson["nested"].to!string.should.equal(`ObjectID(573cbc2fc3b7025427000000)`);
}

bool isValidType(Json.Type type) {
  if(type == Json.Type.undefined) {
    return false;
  }

  if(type == Json.Type.null_) {
    return false;
  }

  return true;
}

version(unittest) {
  import fluent.asserts;
}

/// Check model to bson conversion
unittest {
  RelationModel model;

  model.embeded.field = "field";
  model.embeded.relation._id = ObjectId.generate;
  model._id = ObjectId.generate;
  model.relation = TestModel(ObjectId.generate, "other1");
  model.relations = [ TestModel(ObjectId.generate, "other1") ];
  model.name = "test";

  auto result = model.serializeToJson.toBson!RelationModel;

  result["_id"].toJson.to!string.should.equal(model._id.to!string);
  result["name"].get!string.should.equal("test");
  result["relation"].toJson.to!string.should.equal(model.relation._id.to!string);
  result["relations"].length.should.equal(1);
  result["relations"][0].toJson.to!string.should.equal(model.relations[0]._id.to!string);
  result["embeded"]["field"].get!string.should.equal("field");
  result["embeded"]["relation"].toJson.to!string.should.equal(model.embeded.relation._id.to!string);
}

/// It should find items with whereAny with ids selector
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.idlist");

  Bson data = Bson.emptyObject;
  Bson id1 = BsonObjectID.generate;
  Bson id2 = BsonObjectID.generate;

  data["search"] = Bson([ id1, id2 ]);

  collection.insertOne(data);

  auto mongoCrate = new MongoCrate!TestModel(collection);
  auto query = mongoCrate.get;
  query.where("search").containsAny([ ObjectId.fromBson(id1) ]);
  auto result = query.exec.array;

  result.length.should.equal(1);

  query = mongoCrate.get;
  query.where("search").containsAny([ ObjectId.fromBson(id1) ]);
  query.size.should.equal(1);
}

/// It should find items with or selector
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.idlist");

  try {
    collection.drop;
  } catch(Exception e) {}

  auto crate = new MongoCrate!TestModel(collection);

  Bson data = Bson.emptyObject;
  Bson id1 = Bson("1");
  Bson id2 = Bson("2");
  Bson id3 = Bson("3");

  data["value"] = "1";
  data["search"] = id1;
  collection.insertOne(data);

  data["search"] = id2;
  collection.insertOne(data);

  data["search"] = id3;
  collection.insertOne(data);

  auto mongoCrate = new MongoCrate!TestModel(collection);

  auto query = mongoCrate.get;

  ///
  query.or
    .where("search").equal("1").or
    .where("search").equal("2").or
    .where("search").equal("3");

  query.exec.array.map!(a => a["search"].to!string).array.should.equal(["1", "2", "3"]);

  ///
  query = mongoCrate.get;
  query.or
    .where("search").equal("1").or
    .where("search").equal("2").or
    .where("search").equal("3");

  query.size.should.equal(3);

  ///
  query = mongoCrate.get;
  query.where("value").equal("1");
  query.or
    .where("search").equal("1").or
    .where("search").equal("2").or
    .where("search").equal("3");

  query.exec.array.map!(a => a["search"].to!string).array.should.equal(["1", "2", "3"]);
}

/// It should save data to mongo db using JSON API
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  router.crateSetup!JsonApi.add(crate);

  Json data = Json.emptyObject;
  data["data"] = Json.emptyObject;
  data["data"]["type"] = "testmodels";
  data["data"]["attributes"] = Json.emptyObject;
  data["data"]["attributes"]["name"] = "test name";
  data["data"]["attributes"]["other"] = "";

  request(router).post("/testmodels")
    .header("Content-Type", "application/vnd.api+json")
    .send(data)
    .expectHeader("Content-Type", "application/vnd.api+json")
    .expectStatusCode(201)
    .end((Response response) => () {
      response.bodyJson.byKeyValue.map!"a.key".should.contain("data");
      auto id = response.bodyJson["data"]["id"].to!string;

      response.headers.byKey.should.contain("Location");
      response.headers["Location"].should.equal("http://localhost/testmodels/" ~ id);
    });
}

/// get a list of items with mongo crate
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) {}

  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000001")));
  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000")));

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  router.crateSetup!JsonApi.add(crate);

  request(router)
    .get("/testmodels")
    .expectHeader("Content-Type", "application/vnd.api+json")
    .expectStatusCode(200)
      .end((Response response) => () {
        response.bodyJson.byKeyValue.map!"a.key".should.contain("data");

        response.bodyJson["data"]
          .byValue.map!`a["id"]`
          .map!"a.get!string"
          .should
            .containOnly(["573cbc2fc3b7025427000000", "573cbc2fc3b7025427000001"]);
      });
}

/// get a projection for items with mongo crate
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) {}

  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000001"), "name1"));
  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000"), "name2"));

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  crate.get.withProjection([ "other" ]).exec().array.should.equal(`[{"_id":"573cbc2fc3b7025427000001", "other":"name1" },
    {"_id":"573cbc2fc3b7025427000000","other":"name2"}]`.parseJsonString);
}

/// testing mongo collection with get json apis
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) {}

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  crate.addItem(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000")).serializeToJson);

  router.crateSetup!JsonApi.add(crate);

  request(router)
    .get("/testmodels/573cbc2fc3b7025427000000")
    .expectHeader("Content-Type", "application/vnd.api+json")
    .expectStatusCode(200)
    .end((Response response) => () {
      response.bodyJson["data"]["id"].to!string.should.equal("573cbc2fc3b7025427000000");
    });
}

/// It should return existing resources
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) {}

  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000"), "", "testName"));

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  router.crateSetup!JsonApi.add(crate);

  auto data = Json.emptyObject;
  data["data"] = Json.emptyObject;
  data["data"]["type"] = "testmodels";
  data["data"]["id"] = "573cbc2fc3b7025427000000";
  data["data"]["attributes"] = Json.emptyObject;
  data["data"]["attributes"]["other"] = "other value";

  request(router).patch("/testmodels/573cbc2fc3b7025427000000").send(data).expectStatusCode(200)
    .expectHeader("Content-Type", "application/vnd.api+json").end((Response response) => () {
      response.bodyJson["data"].should.equal(`{
        "id": "573cbc2fc3b7025427000000",
        "type": "testmodels",
        "relationships": {},
        "attributes": {
          "name": "testName",
          "other": "other value"
        }
      }`.parseJsonString);
    });
}

/// It should get a 404 error on missing resources
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  bool actionCalled;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch (Exception) {}

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  router.crateSetup!JsonApi.add(crate);

  request(router).get("/testmodels/1").expectStatusCode(404).end((Response response) => () {
    response.bodyJson.should.equal(parseJsonString(`{"errors": [{
        "status": 404,
        "title": "Crate not found",
        "description": ` ~ "\"There is no item with id `1`\"" ~ `
    }]}`));
  });
}

/// Call an action with JSON API and mongo crate
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  isTestActionCalled = false;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception e) {}

  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000")));

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  router
    .crateSetup!JsonApi
    .add(crate);

  request(router)
    .get("/testmodels/573cbc2fc3b7025427000000/action")
    .expectStatusCode(200)
    .end((Response response) => () {
      response.bodyString.should.equal("");
      isTestActionCalled.should.equal(true);
    });
}

/// call action using mongo crate and JsonApi
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;
  import crate.api.json.policy : JsonApi;

  isTestActionCalled = false;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception e) {}

  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000")));

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  auto testModel = TestModel(ObjectId.fromString("573cbc2fc3b7025427000000"));

  router.crateSetup!JsonApi
    .add(crate);

  request(router).get("/testmodels/573cbc2fc3b7025427000000/actionResponse").expectStatusCode(200)
    .end((Response response) => () {
      response.bodyString.should.equal("ok.");
      isTestActionCalled.should.equal(true);
    });
}

/// it should add the access control headers
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  isTestActionCalled = false;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(TestModel(ObjectId.fromString("573cbc2fc3b7025427000000")));

  auto router = new URLRouter();
  auto crate = new MongoCrate!TestModel(collection);

  router.crateSetup.add(crate);

  request(router)
    .get("/testmodels")
    .expectHeader("Access-Control-Allow-Origin", "*")
    .expectHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
    .end();
}

/// It should find data by polygon
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");
  Json data = `{
    "_id": "573cbc2fc3b7025427000000",
    "location": {
      "type": "Point", "coordinates": [-73.856077, 40.848447] },
    "name": "Morris Park Bake Shop" }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);

  auto query = crate.get;
  query.where("location")
    .insidePolygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]);

  query.exec.front.should.equal(data);

  query = crate.get;
  query.where("location")
    .not.insidePolygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]);

  query.exec.array.length.should.equal(0);
}

/// It should find data by multi polygon
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");
  Json data = `{
    "_id": "573cbc2fc3b7025427000000",
    "location": {
      "type": "Point", "coordinates": [-73.856077, 40.848447] },
    "name": "Morris Park Bake Shop" }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);

  auto query = crate.get;
  query.where("location")
    .insidePolygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]);

  query.exec.front.should.equal(data);

  query = crate.get;
  query.where("location")
    .not.insidePolygon([[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]]);

  query.exec.array.length.should.equal(0);
}

/// It should find data by polygon intersection
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  auto data = `{
    "_id": "573cbc2fc3b7025427000000",
    "name": "Morris Park Bake Shop",
    "location": {
      "type": "Polygon", "coordinates": [[[0, 0], [10, 0], [10, 10], [0, 10], [0, 0]]] } }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) {}

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);
  auto query = crate.get;

  query.where("location")
    .intersectsPolygon([[[5, 5], [30, 5], [30, 30], [5, 30], [5, 5]]]);

  query.exec.front.should.equal(data);
  query.where("location")
    .not.intersectsPolygon([[[5, 5], [30, 5], [30, 30], [5, 30],[5, 5]]]);

  query.exec.array.length.should.equal(0);
}


/// It should find data by point
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");
  Json data = `{
    "_id": "573cbc2fc3b7025427000000",
    "location": {
      "type": "Polygon", "coordinates": [[[-70, 30], [-70, 50], [-80, 50], [-80, 30], [-70,30]]] },
    "name": "Morris Park Bake Shop" }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);

  auto query = crate.get;
  query.where("location")
    .containsPoint(-75.41015624999999,41.31082388091818);

  query.exec.front.should.equal(data);

  query = crate.get;
  query.where("location")
    .not.containsPoint(-75.41015624999999,41.31082388091818);

  query.exec.array.length.should.equal(0);
}

/// It should find documents by bool values
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");
  Json data = `{
    "_id": "573cbc2fc3b7025427000000",
    "value": false,
    "set": true
  }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);

  auto query = crate.get;
  query.where("field").equal(false);
  query.exec.front.should.equal(data);

  query = crate.get;
  query.where("value").equal(false);
  query.exec.front.should.equal(data);

  query = crate.get;
  query.where("field").equal(true);
  query.exec.empty.should.equal(true);

  query = crate.get;
  query.where("value").equal(true);
  query.exec.empty.should.equal(true);

  query = crate.get;
  query.where("set").equal(true);
  query.exec.front.should.equal(data);

  query = crate.get;
  query.where("set").equal(false);
  query.exec.empty.should.equal(true);
}

/// It should exclude documents with array fields
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");
  Json data1 = `{
    "_id": "573cbc2fc3b7025427000000",
    "values": ["1","2","3"],
    "set": true
  }`.parseJsonString;

  Json data2 = `{
    "_id": "573cbc2fc3b7025427000001",
    "values": ["3","4","5"],
    "set": true
  }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data1);
  collection.insertOne(data2);

  auto crate = new MongoCrate!TestModel(collection);

  auto query = crate.get;
  query.where("values").not.anyOf(["1", "2"]);
  query.exec.array.should.equal([data2]);

  query = crate.get;
  query.where("values").not.anyOf(["1", "2", "3"]);
  query.exec.array.should.equal([]);
}

/// It should find documents with arrayFieldContains
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");
  Json data = `{
    "_id": "573cbc2fc3b7025427000000",
    "value": [ { "name": "value" } ],
    "set": true
  }`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);
  crate.get.where("value").arrayFieldContains("name", "value").and.exec.array.should.equal([data]);
}

/// It should find data by arraycontains
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  auto data = `{
    "_id": "000000000000000000000001",
    "location": ["1", "2"]}`.parseJsonString;

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(data);

  auto crate = new MongoCrate!TestModel(collection);
  auto query = crate.get;
  query.where("location").arrayContains("1");
  query.exec.front.should.equal(data);
/*
  crate = new MongoCrate!TestModel(collection);
  query = crate.get;
  query.where("location").not.arrayContains("3");
  query.exec.front.should.equal(data);

  crate = new MongoCrate!TestModel(collection);
  query = crate.get;
  query.where("location").not.arrayContains("1");
  query.exec.array.length.should.equal(0);*/
}

/// It should find distinct data
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(`{"a": "none", "b": "1"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "2"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "3"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "3"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "4"}`.parseJsonString);

  auto crate = new MongoCrate!TestModel(collection);
  auto query = crate.get;
  query.where("a").equal("first");
  query.distinct("b").map!(a => a.to!string).array.should.equal(["2", "3", "4"]);
}

// It should put the second `$near` query defined inside `$or`
// unittest {
//   import vibe.db.mongo.mongo : connectMongoDB;

//   auto client = connectMongoDB("127.0.0.1");
//   auto collection = client.getCollection("test.model");

//   auto crate = new MongoCrate!TestModel(collection);
//   MongoQuery query = cast(MongoQuery) crate.get;

//   query
//     .where("b").equal("b")
//     .or
//     .where("a").near(1.1, 1.1, 100);

//   query.queryBuilder.build.toJson.should.equal(`{
//     "b": "b",
//     "$or": [{"a": {
//       "$near": {
//         "$geometry": {
//           "type": "Point",
//           "coordinates": [ 1.1, 1.1 ]
//         }
//       }
//     }}]
//   }`.parseJsonString);
// }

/// It should find distinct data
unittest {
  import vibe.db.mongo.mongo : connectMongoDB;

  auto client = connectMongoDB("127.0.0.1");
  auto collection = client.getCollection("test.model");

  try {
    collection.drop;
  } catch(Exception) { }

  collection.insertOne(`{"a": "none", "b": "3"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "1"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "2"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "3"}`.parseJsonString);
  collection.insertOne(`{"a": "first", "b": "4"}`.parseJsonString);

  auto crate = new MongoCrate!TestModel(collection);
  auto query = crate.get;
  query.sort("b", 1);
  query.exec.map!(a => a["b"].to!string).array.should.equal(["1", "2", "3", "3", "4"]);
}


void ensureIndex(T)(MongoDatabase db, string collection, string key, T value) {
  ensureIndex(db, collection, [key], value);
}

void ensureIndex(T)(MongoDatabase db, string collection, string[] keys, T value) {
  auto indexType = Json(value);
  auto indexes = db.getIndexes(collection);

  string name = collection ~ "_" ~ keys.join("_") ~ "_" ~ value.to!string;
  bool hasIndex = indexes.canFind(name);

  if(hasIndex) {
    logInfo("Mongo collection `" ~ collection ~ "` already has an index for `" ~ keys.join(",") ~ "` = `" ~ value.to!string ~ "`.");
    return;
  }

  logWarn("Mongo collection `" ~ collection ~ "` does not have an index for `" ~ keys.join(",") ~ "` = `" ~ value.to!string ~ "`.");
  auto result = db.addIndex(collection, keys, indexType);

  if(result["ok"].to!double != 1) {
    if("errmsg" in result) {
      logError("[" ~ result["codeName"].to!string ~ "]" ~result["errmsg"].to!string);
    }
    logError("Can't set index.");
  }
}

void ensureIndex(MongoDatabase db, string collection, Json keys) {
  auto indexes = db.getIndexes(collection);

  string name = collection;
  foreach(string key, Json value; keys) {
    name ~= "_" ~ key ~ "_" ~ value.to!string;
  }

  bool hasIndex = indexes.canFind(name);

  if(hasIndex) {
    logInfo("Mongo collection `" ~ collection ~ "` already has an index for `" ~ keys.toString ~ "`.");
    return;
  }

  logWarn("Mongo collection `" ~ collection ~ "` does not have an index for `" ~ keys.toString ~ "`.");
  auto result = db.addIndex(collection, keys);

  if(result["ok"].to!double != 1) {
    if("errmsg" in result) {
      logError("[" ~ result["codeName"].to!string ~ "]" ~result["errmsg"].to!string);
    }
    logError("Can't set index.");
  }
}

auto addIndex(MongoDatabase db, string collection, string[] keys, Json value) {
  logInfo("Creating mongo collection `" ~ collection ~ "` index for `" ~ keys.join(", ") ~ "` = `" ~ value.to!string ~ "`.");

  auto command = Json.emptyObject;
  command["createIndexes"] = collection;
  command["indexes"] = Json.emptyArray;

  command["indexes"] ~= Json.emptyObject;
  command["indexes"][0]["key"] = Json.emptyObject;

  foreach(i, key; keys) {
    command["indexes"][0]["key"][key] = value;
  }
  command["indexes"][0]["name"] = collection ~ "_" ~ keys.join("_") ~ "_" ~ value.to!string;

  return db.runCommand(command).toJson;
}

auto addIndex(MongoDatabase db, string collection, Json keys) {
  logInfo("Creating mongo collection `" ~ collection ~ "` index for `" ~ keys.toString ~ "`.");

  auto command = Json.emptyObject;
  command["createIndexes"] = collection;
  command["indexes"] = Json.emptyArray;

  command["indexes"] ~= Json.emptyObject;
  command["indexes"][0]["key"] = Json.emptyObject;

  string name = collection;
  foreach(string key, Json value; keys) {
    command["indexes"][0]["key"][key] = value;
    name ~= "_" ~ key ~ "_" ~ value.to!string;
  }
  command["indexes"][0]["name"] = name;

  return db.runCommand(command).toJson;
}

string[] getIndexes(MongoDatabase db, string collection) {
  auto command = Json.emptyObject;
  command["listIndexes"] = collection;

  auto result = db.runCommand(command).toJson;

  if(result["code"] == 26) { /// if collection does not exist
    return [];
  }

  enforce(result["ok"].to!double == 1, "Could not get indexes for `" ~ collection ~ "` db.");

  enforce("cursor" in result, "The cursor data is missing.");
  enforce(result["cursor"].byKeyValue.map!(a => a.key).array.length == 3, "Unexpected cursor data.");

  string[] list;

  foreach(item; result["cursor"]["firstBatch"]) {
    list ~= item["name"].to!string;
  }

  return list;
}
