/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.notifications;

import crate.base;

import vibe.core.log;
import vibe.data.json;

version(unittest) {
  import crate.collection.memory;
  import fluent.asserts;
  import std.array;
}

enum CrateChangeType : string {
  add = "add",
  update = "update",
  delete_ = "delete",
  snapshot = "snapshot"
}

struct CrateChange {
  string modelName;
  CrateChangeType type;

  @optional {
    Json before;
    Json after;
  }
}

alias CrateNotification = void delegate(CrateChange) @safe;

class NotificationCrate(T) : Crate!T {
  private {
    Crate!T crate;
    CrateNotification notification;
  }

  this(Crate!T crate, CrateNotification notification) {
    this.crate = crate;
    this.notification = notification;
  }

  @safe {
    /// Get items from the crate
    IQuery get() {
      return this.crate.get;
    }

    /// Get one item by id
    IQuery getItem(const string id) {
      return this.crate.getItem(id);
    }

    /// Add a new item
    Json addItem(const Json item) {
      auto result = this.crate.addItem(item);
      logInfo("`%s` was added.", T.stringof);

      notification(CrateChange(T.stringof, CrateChangeType.add, Json(), result));
      logInfo("`%s` notification sent.", T.stringof);

      return result;
    }

    /// Update an existing item
    Json updateItem(const Json item) {
      logInfo("`%s` with id `%s` was updated.", T.stringof, item["_id"]);
      Json before;

      () @trusted {
        before = this.crate.getItem(item["_id"].to!string).exec.front;
      }();

      auto result = this.crate.updateItem(item);

      notification(CrateChange(T.stringof, CrateChangeType.update, before, result));

      return result;
    }

    /// Delete an existing item
    void deleteItem(const string id) {
      logInfo("`%s` with id `%s` was deleted.", T.stringof, id);

      Json before;

      () @trusted {
        before = this.crate.getItem(id).exec.front;
      }();

      this.crate.deleteItem(id);

      notification(CrateChange(T.stringof, CrateChangeType.delete_, before, Json()));
    }
  }
}

/// A get request should not trigger a notification
unittest {
  bool called;

  struct Test {
    string _id;
  }

  void notification(CrateChange) {
    called = true;
  }

  auto crate = new MemoryCrate!Test();
  crate.addItem(`{ "_id": "1" }`.parseJsonString);

  auto notificationCrate = new NotificationCrate!Test(crate, &notification);

  notificationCrate.get.size.should.equal(1);
  called.should.equal(false);
}

/// A get item request should not trigger a notification
unittest {
  bool called;

  struct Test {
    string _id;
  }

  void notification(CrateChange) {
    called = true;
  }

  auto crate = new MemoryCrate!Test();
  crate.addItem(`{ "_id": "1" }`.parseJsonString);

  auto notificationCrate = new NotificationCrate!Test(crate, &notification);

  notificationCrate.getItem("000000000000000000000001").exec.front.should.equal(`{ "_id": "000000000000000000000001" }`.parseJsonString);
  called.should.equal(false);
}

/// An add item request should trigger a notification
unittest {
  bool called;

  struct Test {
    string _id;
  }

  void notification(CrateChange change) {
    change.modelName = "Test";
    change.type.should.equal(CrateChangeType.add);
    change.before.to!string.should.equal("null");
    change.after.should.equal(`{ "_id": "000000000000000000000001" }`.parseJsonString);
    called = true;
  }

  auto crate = new MemoryCrate!Test();
  auto notificationCrate = new NotificationCrate!Test(crate, &notification);

  notificationCrate.addItem(`{ "_id": "1" }`.parseJsonString);

  notificationCrate.getItem("000000000000000000000001").exec.front.should.equal(`{ "_id": "000000000000000000000001" }`.parseJsonString);
  called.should.equal(true);
}

/// An updateItem request should trigger a notification
unittest {
  bool called;

  struct Test {
    string _id;
    int value;
  }

  void notification(CrateChange change) {
    change.modelName = "Test";
    change.type.should.equal(CrateChangeType.update);
    change.before.should.equal(`{ "_id": "000000000000000000000001", "value": 1 }`.parseJsonString);
    change.after.should.equal(`{ "_id": "000000000000000000000001", "value": 2 }`.parseJsonString);
    called = true;
  }

  auto crate = new MemoryCrate!Test();
  crate.addItem(`{ "_id": "000000000000000000000001", "value": 1 }`.parseJsonString);

  auto notificationCrate = new NotificationCrate!Test(crate, &notification);
  notificationCrate.updateItem(`{ "_id": "000000000000000000000001", "value": 2 }`.parseJsonString);


  notificationCrate.getItem("000000000000000000000001").exec.front.should.equal(`{ "_id": "000000000000000000000001", "value": 2 }`.parseJsonString);
  called.should.equal(true);
}

/// An deleteItem request should trigger a notification
unittest {
  bool called;

  struct Test {
    string _id;
  }

  void notification(CrateChange change) {
    change.modelName = "Test";
    change.type.should.equal(CrateChangeType.delete_);
    change.before.should.equal(`{ "_id": "000000000000000000000001" }`.parseJsonString);
    change.after.to!string.should.equal("null");
    called = true;
  }

  auto crate = new MemoryCrate!Test();
  crate.addItem(`{ "_id": "000000000000000000000001" }`.parseJsonString);

  auto notificationCrate = new NotificationCrate!Test(crate, &notification);
  notificationCrate.deleteItem("000000000000000000000001");

  notificationCrate.get.size.should.equal(0);
  called.should.equal(true);
}

///
auto notification(T)(Crate!T crate, CrateNotification notification) {
  return new NotificationCrate!T(crate, notification);
}