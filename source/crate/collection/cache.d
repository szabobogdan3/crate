/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module crate.collection.cache;

import crate.base;
import crate.ctfe;
import crate.collection.memory;
import vibe.data.json;

import std.traits, std.algorithm, std.array;
import std.stdio, std.string;
import std.range.interfaces;

version (unittest) {
  import fluent.asserts;
}

///
class CrateCache(T): Crate!T {

  private {
    Crate!T crate;

    Json[string] cache;
  }

  this(Crate!T crate) {
    this.crate = crate;
  }

  @safe:
    IQuery get() {
      return crate.get();
    }

    Json addItem(const Json item) {
      cache[item["_id"].to!string] = item.clone;
      return crate.addItem(item);
    }

    IQuery getItem(const string id) @trusted {
      if(id !in cache) {
        scope range = crate.getItem(id).exec;

        if(!range.empty) {
          cache[id] = range.front.clone;
        }
      }

      if(id !in cache) {
        return new MemoryQuery([ ]);
      }

      return new MemoryQuery([ cache[id] ]);
    }

    Json updateItem(const Json item) {
      auto result = crate.updateItem(item);

      cache[item["_id"].to!string] = item.clone;
      return result;
    }

    void deleteItem(const string id) {
      if(id in cache) {
        cache.remove(id);
      }

      crate.deleteItem(id);
    }
}

version(unittest) {
  struct TestItem {
    string _id;
    string val;
  }
}

/// It should set and store a value
unittest {
  auto baseCrate = new MemoryCrate!TestItem;
  auto crate = new CrateCache!TestItem(baseCrate);

  Json item = `{
    "_id": "000000000000000000000001",
    "val": "1"
  }`.parseJsonString;

  crate.addItem(item);

  crate.cache.length.should.equal(1);
  crate.cache["000000000000000000000001"]["val"].to!string.should.equal("1");
}

/// It should get a value from cache if it exists
unittest {
  auto baseCrate = new MemoryCrate!TestItem;
  auto crate = new CrateCache!TestItem(baseCrate);

  Json item = `{
    "_id": "000000000000000000000001",
    "val": "1"
  }`.parseJsonString;

  crate.cache["000000000000000000000001"] = item;

  crate.getItem("000000000000000000000001").exec.front["val"].to!string.should.equal("1");
}

/// It should query a value from base crate if it is not in cache
unittest {
  auto baseCrate = new MemoryCrate!TestItem;
  auto crate = new CrateCache!TestItem(baseCrate);

  Json item = `{
    "_id": "000000000000000000000001",
    "val": "1"
  }`.parseJsonString;

  baseCrate.addItem(item);

  crate.getItem("000000000000000000000001").exec.front["val"].to!string.should.equal("1");

  crate.cache.length.should.equal(1);
  crate.cache["000000000000000000000001"]["val"].to!string.should.equal("1");
}

/// It should update a value and store it in cache
unittest {
  auto baseCrate = new MemoryCrate!TestItem;
  auto crate = new CrateCache!TestItem(baseCrate);

  Json item1 = `{
    "_id": "000000000000000000000001",
    "val": "1"
  }`.parseJsonString;

  Json item2 = `{
    "_id": "000000000000000000000001",
    "val": "2"
  }`.parseJsonString;

  baseCrate.addItem(item1);

  crate.updateItem(item2)["val"].to!string.should.equal("2");

  crate.cache.length.should.equal(1);
  crate.cache["000000000000000000000001"]["val"].to!string.should.equal("2");
}


/// It should delete an item from cache and base crate
unittest {
  auto baseCrate = new MemoryCrate!TestItem;
  auto crate = new CrateCache!TestItem(baseCrate);

  Json item = `{
    "_id": "000000000000000000000001",
    "val": "1"
  }`.parseJsonString;

  crate.addItem(item);
  crate.deleteItem("000000000000000000000001");

  crate.cache.length.should.equal(0);
  baseCrate.get.where("_id").equal("000000000000000000000001").and.exec.empty.should.equal(true);
}
