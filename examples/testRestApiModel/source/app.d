import vibe.d;

import crate.generators.openapi;
import crate.collection.mongo;
import crate.base;
import crate.http.router;
import crate.api.rest.policy;
import std.stdio;

struct Comment {
	ObjectId userId;
	string message;
}

@("plural:Categories")
struct Category {
	ObjectId _id;

	string name;
	string color;
}

struct Book
{
	@optional {
		ObjectId _id;
	}

	string name;
	string author;
	Category[] category;

	@optional
	int something;

	double price;
	bool inStock;

	@optional
	Comment[] comments;

	void action()
	{
		inStock = false;
	}

	string actionResponse()
	{
		return inStock ? "ok." : "not ok.";
	}
}


shared static this()
{
	auto settings = new HTTPServerSettings;
	settings.port = 9090;
	settings.options = HTTPServerOption.parseQueryString
		| HTTPServerOption.parseFormBody | HTTPServerOption.parseJsonBody;

	auto client = connectMongoDB("127.0.0.1");

	auto router = new URLRouter;

	auto bookCollection = client.getCollection("test.books");
	auto categoryCollection = client.getCollection("test.bookCategories");

	auto bookCrate = new MongoCrate!Book(bookCollection);
	auto categoryCrate = new MongoCrate!Category(categoryCollection);

	auto crateRouter = router
		.crateSetup
		.add(bookCrate)
			/*.enableAction!(Book, "action")
			.enableAction!(Book, "actionResponse")*/
		.add(categoryCrate);

	crateRouter.generateOpenApi;

	listenHTTP(settings, router);
}

void generateOpenApi(T)(T crateRouter)
{
	auto api = crateRouter.toOpenApi;
	api.info.title = "A sample book store api";
	api.info.version_ = "v1.0.0";

	auto f = File("openApi.json", "w");

	f.write(api.serializeToJson.toPrettyString);
}

/*
void generateEmber(T)(T crateRouter)
{
	crateRouter.toEmber!Book("./ember/");
}
*/
