/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.resources.gridfs;

import trial.interfaces;
import trial.discovery.spec;
import fluent.asserts;

import crate.resource.gridfs;
import crate.collection.memory;
import crate.http.router;

import vibe.db.mongo.mongo;
import vibe.stream.memory;
import vibe.core.path;
import vibe.core.stream;
import vibe.inet.webform;
import vibe.http.router;

import std.base64;
import std.stdio;
import std.file;

struct Item {
  @optional string _id;
  Child child;
  GridFsResource!GridFsConfig file = new GridFsResource!GridFsConfig;
}

struct Child {
  GridFsResource!GridFsConfig file = new GridFsResource!GridFsConfig;
}


struct CustomConfig {
  static {
    MongoGridFsCollection files;
    MongoGridFsCollection chunks;

    size_t chunkSize = 255 * 1024;
  }
}

struct CustomItem {
  @optional string _id;
  GridFsResource!CustomConfig file = new GridFsResource!CustomConfig;
}

alias s = Spec!({
  describe("GridFS", {
    GridFsResource!GridFsConfig file;
    MongoClient client;
    MongoCollection chunks;
    MongoCollection files;
    ubyte[] rawData;

    before({
      client = connectMongoDB("127.0.0.1");
      chunks = client.getCollection("test.fs.chunks");
      files = client.getCollection("test.fs.files");

      foreach(j; 0..2) {
        foreach(i; 0..256) {
          rawData ~= cast(ubyte) i;
        }
      }
    });

    beforeEach({
      try {
        chunks.drop;
        files.drop;
      } catch(Exception) { }

      GridFsConfig.files = files;
      GridFsConfig.chunks = chunks;
      GridFsConfig.chunkSize = 256;

      string encoded = Base64.encode(rawData);
      file = GridFsResource!GridFsConfig.fromBase64("data:text/plain;base64," ~ encoded);
    });

    afterEach({
      if("test.txt".exists) {
        std.file.remove("test.txt");
      }
    });

    describe("reading all chunks", {
      it("should read a file with 2 chunks in grid fs", {
        ubyte[] result;
        result.length = rawData.length;

        auto input = file.inputStream();
        input.read(result, IOMode.all);

        result.length.should.equal(rawData.length);
        result.should.equal(rawData);
      });

      it("should the filename to equal the id by default", {
        file.id.should.equal(file.fileName);
      });

      it("should read a file with a small buffer", {
        ubyte[] result;
        ubyte[] tmp;
        tmp.length = 100;

        auto input = file.inputStream();

        while(!input.empty) {
          auto size = input.read(tmp, IOMode.all);
          result ~= tmp[0..size];
        }

        result.length.should.equal(rawData.length);
        result.should.equal(rawData);
      });

      it("should read the file using a new reference", {
        auto file2 = new GridFsResource!GridFsConfig(file.id);
        ubyte[] result;
        result.length = rawData.length;

        auto input = file2.inputStream();
        input.read(result, IOMode.all);

        result.length.should.equal(rawData.length);
        result.should.equal(rawData);
      });

      it("should create a file by name", {
        auto file2 = new GridFsResource!GridFsConfig("some new file");
        file2.exists.should.equal(false);
        file2.fileName.should.equal("some new file");
      });
    });

    describe("reading one chunk at a time", {
      it("should read the file in small chunks", {
        ubyte[] result;
        ubyte[] tmp;
        tmp.length = 100;

        auto input = file.inputStream();

        while(!input.empty) {
          auto size = input.read(tmp, IOMode.once);
          result ~= tmp[0..size];
        }

        result.length.should.equal(rawData.length);
        result.should.equal(rawData);
      });

      it("should read the file in big chunks", {
        ubyte[] result;
        ubyte[] tmp;
        tmp.length = 1000;

        auto input = file.inputStream();

        while(!input.empty) {
          auto size = input.read(tmp, IOMode.once);
          result ~= tmp[0..size];
        }

        result.length.should.equal(rawData.length);
        result.should.equal(rawData);
      });
    });

    it("should read the file using a new reference", {
      auto file2 = new GridFsResource!GridFsConfig(file.id);
      file2.contentType.should.equal("text/plain");
    });

    it("should get an unique etag", {
      auto file2 = new GridFsResource!GridFsConfig(file.id);
      file2.etag.should.equal(file.etag);
      file2.etag.should.not.equal("");
    });

    it("should get the file size", {
      auto file2 = new GridFsResource!GridFsConfig(file.id);
      file.hasSize.should.equal(true);
      file2.hasSize.should.equal(true);

      file2.size.should.equal(file.size);
      file2.size.should.equal(512);
    });

    it("should remove a file", {
      auto file2 = new GridFsResource!GridFsConfig(file.id);
      file.exists.should.equal(true);
      file2.exists.should.equal(true);

      file.remove;

      file.exists.should.equal(false);
      file2.exists.should.equal(false);

      files
        .countDocuments(["_id" : BsonObjectID.fromString(file.id)]).should.equal(0);

      chunks
        .countDocuments(["files_id" : BsonObjectID.fromString(file.id)]).should.equal(0);
    });

    it("should replace a file content", {
      ubyte[] result;
      ubyte[] newRawData;
      foreach(j; 0..3) {
        foreach(i; 0..256) {
          newRawData ~= cast(ubyte) j;
        }
      }

      auto stream = createMemoryStream(newRawData, false);
      auto file2 = new GridFsResource!GridFsConfig(file.id);
      file2.read(stream);

      result.length = newRawData.length;

      auto input = file2.inputStream();
      auto size = input.read(result, IOMode.all);

      size.should.equal(newRawData.length);
      result.should.equal(newRawData);
    });

    it("should write a file using FilePart", {
      ubyte[] result;
      ubyte[] newRawData;
      foreach(j; 0..3) {
        foreach(i; 0..256) {
          newRawData ~= cast(ubyte) j;
        }
      }

      auto filePart = FilePart();
      filePart.tempPath = GenericPath!(PosixPathFormat).fromString("test.txt");
      filePart.headers["Content-Type"] = "text/plain";

      auto f = File("test.txt", "w");
      f.rawWrite(newRawData);
      f.close;

      file.read(filePart);

      result.length = newRawData.length;

      auto size = file.inputStream.read(result, IOMode.all);

      size.should.equal(newRawData.length);
      result.should.equal(newRawData);
      file.contentType.should.equal("text/plain");
    });

    it("should write a file using chunks", {
      ubyte[] result;
      ubyte[] newRawData;

      foreach(j; 0..3) {
        ubyte[] data;

        foreach(i; 0..256) {
          data ~= cast(ubyte) j;
        }

        newRawData ~= data;
        file.setChunk(j, data);
      }

      result.length = newRawData.length;

      auto size = file.inputStream.read(result, IOMode.all);

      size.should.equal(newRawData.length);
      result.should.equal(newRawData);
      file.contentType.should.equal("text/plain");

      file.hasSize.should.equal(true);
      file.size.should.equal(result.length);
    });

    describe("using crate router", {
      URLRouter router;
      MemoryCrate!Item baseCrate;

      beforeEach({
        router = new URLRouter();
        baseCrate = new MemoryCrate!Item;

        router
          .crateSetup
            .add(baseCrate);
      });

      it("it should accept a base64 string", {

        Json data = `{
          "item": {
            "file": "data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==",
            "child": {
              "file": "data:text/plain;base64,aGVsbG8gd29ybGQ="
            }
          }
        }`.parseJsonString;

        request(router)
          .post("/items")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                file = new GridFsResource!GridFsConfig(response.bodyJson["item"]["file"].to!string);

                file.exists.should.equal(true);
                file.size.should.equal(19);
              });

        request(router)
          .get("/items/000000000000000000000001/file")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("this is a text file");
              });

        request(router)
          .get("/items/000000000000000000000001/child/file")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("hello world");
              });
      });
    });

    describe("using MongoGridFsCollection", {
      URLRouter router;
      MemoryCrate!CustomItem baseCrate;

      beforeEach({
        router = new URLRouter();
        CustomConfig.files = new MongoGridFsCollection(files);
        CustomConfig.chunks = new MongoGridFsCollection(chunks);

        try {
          chunks.drop;
          files.drop;
        } catch(Exception) { }

        baseCrate = new MemoryCrate!CustomItem;

        router
          .crateSetup
            .add(baseCrate);
      });

      it("it should accept a base64 string", {
        Json data = `{
          "customItem": {
            "file": "data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==",
          }
        }`.parseJsonString;

        request(router)
          .post("/customitems")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                file = new GridFsResource!GridFsConfig(response.bodyJson["customItem"]["file"].to!string);

                file.exists.should.equal(true);
                file.size.should.equal(19);
              });

        request(router)
          .get("/customitems/000000000000000000000001/file")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("this is a text file");
              });
      });
    });
  });
});
