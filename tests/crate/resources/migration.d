/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.resources.migration;

import trial.interfaces;
import trial.discovery.spec;
import fluent.asserts;

import crate.resource.file;
import crate.resource.gridfs;
import crate.resource.migration;
import crate.collection.memory;
import crate.http.router;

import vibe.db.mongo.mongo;
import vibe.stream.memory;
import vibe.core.path;
import vibe.core.stream;
import vibe.inet.webform;
import vibe.http.router;

import std.base64;
import std.stdio;
import std.file;
import std.conv;
import std.path;

struct CustomConfig {
  static {
    string path;

    MongoGridFsCollection files;
    MongoGridFsCollection chunks;

    size_t chunkSize = 255 * 1024;
  }
}

alias TestResource = MigrationResource!(CrateFileResource!CustomConfig, GridFsResource!CustomConfig);

struct CustomItem {
  @optional string _id = "item_id";
  TestResource file = new TestResource;
}


alias s = Spec!({
  describe("Migration", {
    describe("from local files to GridFs", {
      URLRouter router;
      MemoryCrate!CustomItem baseCrate;

      beforeEach({
        router = new URLRouter();
        auto client = connectMongoDB("127.0.0.1");
        CustomConfig.files = new MongoGridFsCollection(client.getCollection("test.fs.files"));
        CustomConfig.chunks = new MongoGridFsCollection(client.getCollection("test.fs.chunks"));
        CustomConfig.path = "files".asAbsolutePath.to!string;

        try {
          client.getCollection("test.fs.chunks").drop;
          client.getCollection("test.fs.files").drop;
        } catch(Exception) { }

        "files".mkdirRecurse;

        auto f = File("files/item.txt", "w");
        f.write("this is the item");
        f.close;

        baseCrate = new MemoryCrate!CustomItem;

        auto jsonItem = `{
          "_id": "000000000000000000000001",
          "file": "item.txt"
        }`.parseJsonString;
        baseCrate.addItem(jsonItem);


        router
          .crateSetup
            .add(baseCrate);
      });

      afterEach({
        if("files".exists) {
          //"files/".rmdirRecurse;
        }
      });

      it("should accept a base64 string", {
        Json data = `{
          "customItem": {
            "file": "data:text/plain;base64,dGhpcyBpcyBhIHRleHQgZmlsZQ==",
          }
        }`.parseJsonString;

        request(router)
          .post("/customitems")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                auto file = new TestResource(response.bodyJson["customItem"]["file"].to!string);

                file.exists.should.equal(true);
                file.size.should.equal(19);
              });

        request(router)
          .get("/customitems/000000000000000000000001")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyJson.should.equal(`{ "customItem": {
                  "_id": "000000000000000000000001",
                  "file": "item.txt"
                }}`.parseJsonString);
              });

        request(router)
          .get("/customitems/000000000000000000000001/file")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("this is the item");
              });

        request(router)
          .get("/customitems/000000000000000000000001")
            .send(data)
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyJson["customItem"]["file"].to!string.should.not.equal("item.txt");
              });
      });
    });
  });
});
