/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.router.resource;

import tests.crate.http.router.mocks;
import tests.crate.http.crateRouter.mocks;

import trial.interfaces;

import trial.step;
import trial.runner;
import trial.discovery.spec;
import fluent.asserts;

import std.file;
import std.path;
import std.algorithm;
import std.array;
import std.conv;
import vibe.data.json;
import vibe.http.router;

import crate.resource.file;
import crate.api.rest.policy;
import crate.http.router;
import crate.collection.memory;
import crate.error;

alias s = Spec!({
  describe("The crate router", {
    URLRouter router;

    describe("with a CrateImageCollectionResource", {
      CrateRouterBis!(RestApi, DefaultStorage) crateRouter;
      MemoryCrate!ResourceCollectionCrate resourceCrate;
      TypeRouter!(RestApi, ResourceCollectionCrate, DefaultStorage) typeRouter;

      beforeEach({
        router = new URLRouter();
        std.file.mkdirRecurse(DefaultCrateFileSettings.path);

        std.file.write(buildPath(DefaultCrateFileSettings.path, "test.jpg"), "test");
        std.file.write(buildPath(DefaultCrateFileSettings.path, "test.jpg.small.jpg"), "small");

        resourceCrate = new MemoryCrate!ResourceCollectionCrate();
        Json itemData = `{
          "_id": "000000000000000000000001",
          "resourceCollection": "test.jpg"
        }`.parseJsonString;

        resourceCrate.addItem(itemData);

        crateRouter = new CrateRouterBis!(RestApi, DefaultStorage);
        typeRouter = crateRouter.using!ResourceCollectionCrate(resourceCrate);
        typeRouter.and(new TestQueryParamsMiddleware);

        router.any("*", requestErrorHandler(&crateRouter.handler));
      });

      it("should get a subresource", {
        request(router)
          .get("/resourcecollectioncrates/000000000000000000000001/resourceCollection/small")
          .expectStatusCode(200)
          .expectHeader("Content-Type", "image/jpeg")
          .end((Response response) => () {
            response.bodyString.should.equal("small");
          });
      });

      it("should add the query params to the get sub resource rule", {
        typeRouter.and(new TestQueryParamsMiddleware);
        auto rule = crateRouter.rules.filter!(a => a.request.path == "/resourcecollectioncrates/:id/resourceCollection/small" && a.request.method.to!string == "GET").front;

        rule.request.parameters.serializeToJson.should.equal(`[
          {"description":"The id of the object that you want to access","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
          { "in": "query", "name": "value1" },
          { "in": "query", "name": "value2" }
        ]`.parseJsonString);
      });

      it("should add the query params from 2 different middlewares", {
        typeRouter.and(new OtherTestQueryParamsMiddleware);
        auto rule = crateRouter.rules.filter!(a => a.request.path == "/resourcecollectioncrates/:id/resourceCollection/small" && a.request.method.to!string == "GET").front;

        rule.request.parameters.serializeToJson.should.equal(`[
          {"description":"The id of the object that you want to access","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
          { "in": "query", "name": "value1" },
          { "in": "query", "name": "value2" },
          { "in": "query", "name": "value3" },
          { "in": "query", "name": "value4" }
        ]`.parseJsonString);
      });
    });

    describe("using a model with a resource and REST API", {
      CrateRouterBis!(RestApi, DefaultStorage) crateRouter;
      TypeRouter!(RestApi, ResourceCrate, DefaultStorage) typeRouter;
      MemoryCrate!ResourceCrate resourceCrate;

      beforeEach({
        TestResource.removed = false;
        resourceCrate = new MemoryCrate!ResourceCrate;

        Json itemData = `{
          "_id": "000000000000000000000001",
          "resource": ""
        }`.parseJsonString;

        resourceCrate.addItem(itemData);

        crateRouter = new CrateRouterBis!(RestApi, DefaultStorage);
        typeRouter = crateRouter.using!ResourceCrate(resourceCrate);

        router = new URLRouter();
        router.any("*", requestErrorHandler(&crateRouter.handler));
      });

      describe("with a GET REST Api request", {
        it("should get a resource", {
          request(router)
            .get("/resourcecrates/000000000000000000000001/resource")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("test body");
              });
        });

        it("should deny a resource with a filter", {
          typeRouter.and(new FilterAll);

          request(router)
            .get("/resourcecrates/000000000000000000000001/resource")
              .expectStatusCode(404)
              .end();
        });

        it("should call the `any` middleware for a resource", {
          typeRouter.and(new MockAnyMiddleware);

          request(router)
            .get("/resourcecrates/000000000000000000000001/resource")
              .expectStatusCode(412)
              .end();
        });

        it("should expose the custom operations provided by a resource", {
          request(router)
            .get("/resourcecrates/000000000000000000000001/resource/custom")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal("test custom");
              });
        });
      });

      describe("with a DELETE Api request", {
        it("should delete a resource when the item is deleted", {
          router
            .crateSetup
              .add(resourceCrate);

          request(router)
            .delete_("/resourcecrates/000000000000000000000001")
              .expectStatusCode(204)
              .end((Response response) => () {
                TestResource.removed.should.equal(true);
              });
        });

        it("should deny a resource with a filter", {
          typeRouter.and(new FilterAll);

          request(router)
            .get("/resourcecrates/000000000000000000000001/resource")
              .expectStatusCode(404)
              .end();
        });

        it("should call the `any` middleware for a resource", {
          typeRouter.and(new MockAnyMiddleware);

          request(router)
            .get("/resourcecrates/000000000000000000000001/resource")
              .expectStatusCode(412)
              .end();
        });
      });

      describe("with a POST request", {
        string data;

        beforeEach({
          TestResource.lastRead = "";

          data = "-----------------------------9855312492823326321373169801\r\n";
          data ~= "Content-Disposition: form-data; name=\"resource\"; filename=\"resource.txt\"\r\n";
          data ~= "Content-Type: text/plain\r\n\r\n";
          data ~= "hello\r\n";
          data ~= "-----------------------------9855312492823326321373169801--\r\n";
        });

        it("should update a resource", {
          request(router)
            .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
            .post("/resourcecrates/000000000000000000000001/resource")
            .send(data)
              .expectStatusCode(201)
              .end((Response response) => () {
                TestResource.lastRead.should.equal("hello");
              });
        });

        it("should not update a filtered resource", {
          typeRouter.and(new FilterAll);

          request(router)
            .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
            .post("/resourcecrates/000000000000000000000001/resource")
            .send(data)
              .expectStatusCode(404)
              .end();
        });
      });

      describe("handling query parameters", {
        URLRouter urlRouter;
        TestQueryParamsMiddleware middleware;

        beforeEach({
          middleware = new TestQueryParamsMiddleware;
          typeRouter.and(middleware);
        });

        it("should add the query params to the get resource rule", {
          auto rule = crateRouter.rules.filter!(a => a.request.path == "/resourcecrates/:id/resource" && a.request.method.to!string == "GET").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to access","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the set resource rule", {
          auto rule = crateRouter.rules.filter!(a => a.request.path == "/resourcecrates/:id/resource" && a.request.method.to!string == "POST").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to access","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });
      });
    });
  });
});
