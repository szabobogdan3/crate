/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.router.mocks;

import vibe.http.router;
import core.time;
import core.thread;
import vibe.data.json;

public import crate.base;
public import tests.crate.http.mocks;


static immutable restSiteFixture = `{
  "site": {
    "position": {
      "type": "Point",
      "coordinates": [0, 0]
    }
  }
}`;

static immutable jsonSiteFixture = `{
  "data": {
    "type": "sites",
    "attributes": {
      "position": {
        "type": "Point",
        "coordinates": [0, 0]
      }
    }
  }
}`;

import crate.http.request;

struct TestModel {
  ObjectId _id;

  string name;
  long value;

  string getName() {
    return name;
  }

  string handlerRequest(CrateHttpRequest req) {
    return "called";
  }

  string setName(string name) {
    this.name = name;

    return this.name;
  }
}

class TestMapper {

  @mapper
  Json mapper1(HTTPServerRequest request, const Json item) @trusted nothrow {
    Json result = item;

    try result["other1"] = "value1";
    catch(Exception) {}

    return result;
  }

  @mapper
  Json mapper2(HTTPServerRequest request, const Json item) @trusted nothrow {
    Json result = item;

    try result["other2"] = "value2";
    catch(Exception) {}

    return result;
  }
}

class TestQueryParamsMiddleware {
  struct Parameters {
    int value1;
    string value2;
  }

  Parameters params;

  @any
  void any(Parameters params) @safe {
    this.params = params;
  }
}

class OtherTestQueryParamsMiddleware {
  struct Parameters {
    int value3;
    string value4;
  }

  Parameters params;

  @any
  void any(Parameters params) @safe {
    this.params = params;
  }
}

class TestAllQueryParamsMiddleware {
  struct AnyParams {
    string anyValue;
  }

  struct GetItemParams {
    string getItem;
  }

  struct MapperParams {
    string mapper;
  }

  string[] values;

  @any
  void any(AnyParams params) @safe {
    values ~= params.anyValue;
  }

  @getItem
  void getItem(GetItemParams params) @safe {
    values ~= params.getItem;
  }

  @mapper
  Json mapper(Json item, MapperParams params) @trusted nothrow {
    values ~= params.mapper;

    return item;
  }
}

class TestMiddleware {
  string[] operations;

  @any
  void any() @safe {
    operations ~= "any";
  }

  @getItem
  void getItem() @safe {
    operations ~= "getItem";
  }

  @getList
  void getList() @safe {
    operations ~= "getList";
  }

  @create
  void create() @safe {
    operations ~= "create";
  }

  @update
  void update() @trusted {
    operations ~= "update";
    Thread.sleep(2.msecs);
  }

  @replace
  void replace() @safe {
    operations ~= "replace";
  }

  @delete_
  void delete_() @safe {
    operations ~= "delete_";
  }
}

class TestExceptionMiddleware {
  @any
  void any() @safe {
    throw new Exception("this is a test");
  }
}