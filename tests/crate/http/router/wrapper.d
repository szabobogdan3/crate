/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.router.wrapper;

import trial.interfaces;
import trial.step;
import trial.runner;
import trial.discovery.spec;

import crate.http.wrapper;
import crate.base;

import std.conv;
import std.array : array;

import vibe.data.json;

import fluent.asserts;

struct TestStorage {
  IQuery query;
}

class Middlewares {
  string[] history;

  @replace
  void a(IQuery) @safe {
    history ~= "a";
  }

  @replace
  void b() @safe {
    history ~= "b";
  }

  @replace
  IQuery c() @safe {
    history ~= "c";

    return null;
  }
}

class BasicMiddlewares {
  string value;

  @replace
  void stringParam(string value) @safe {
    this.value = value;
  }

  @create
  int intParam(int value) @safe {
    this.value = value.to!string;

    return value;
  }
}

class ReturnMiddleware {
  @replace
  string simpleReturn(string value) {
    return "the value";
  }

  @create
  string simpleReturn() {
    return "the value";
  }
}

class QueryParamsMiddleware {
  struct Params {
    @describe("the a query param")
    @example("value", "a random value")
    string a;


    int b;
  }

  @create
  void something(Params a) {

  }
}

alias s = Spec!({
  describe("The wire middlewares process", {
    it("should put the IQuery middlewares at the end", {
      auto instance = new Middlewares();
      auto result = wireMiddlewares!(TestStorage, [ "replace" ])(instance);
      auto storage = TestStorage();

      foreach (wrappedCall; result) {
        wrappedCall.call(storage);
        wrappedCall.canBeCalledWithStorage.should.equal(true);
      }

      instance.history.should.equal(["b", "a", "c"]);
    });

    it("should allow calling a function with a string param using a string param", {
      auto instance = new BasicMiddlewares();
      auto result = wireMiddlewares!(TestStorage, [ "replace" ])(instance);

      result[0].call("some test");

      result.length.should.equal(1);
      result[0].canBeCalledWithString.should.equal(true);
      instance.value.should.equal("some test");
    });

    it("captures the return value of a string call", {
      auto instance = new ReturnMiddleware();
      auto result = wireMiddlewares!(TestStorage, [ "replace" ])(instance);

      result[0].call("");

      result.length.should.equal(1);
      result[0].canBeCalledWithStorage.should.equal(false);
      result[0].canBeCalledWithString.should.equal(true);
      result[0].returnValue.array.should.equal(["the value"]);
    });

    it("captures the return value of a storage call", {
      auto instance = new ReturnMiddleware();
      auto result = wireMiddlewares!(TestStorage, [ "create" ])(instance);

      result[0].call(TestStorage());
      result.length.should.equal(1);
      result[0].canBeCalledWithStorage.should.equal(true);
      result[0].canBeCalledWithString.should.equal(false);
      result[0].returnValue.array.should.equal(["the value"]);
    });

    it("updates a rule with the right query params", {
      auto instance = new QueryParamsMiddleware();
      CrateRule rule;

      auto result = wireMiddlewares!(TestStorage, [ "create" ])(instance);
      result[0].updateRule(rule);

      rule.request.parameters.serializeToJson.should.equal(`[
        {"description":"the a query param","examples":{"value":{"value":"value","summary":"a random value"}},"in":"query","name":"a"},
        {"in":"query","name":"b"}]`.parseJsonString);
    });
  });
});