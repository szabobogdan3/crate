/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.router.operations;

import trial.interfaces;

import std.functional;
import std.algorithm;
import std.conv;
import std.array;

import trial.step;
import trial.runner;
import trial.discovery.spec;
import fluent.asserts;
import fluentasserts.vibe.request;

import vibe.data.json;
import vibe.inet.url;
import vibe.http.router;
import tests.crate.http.router.mocks;

import crate.base;
import crate.error;
import crate.http.router;
import crate.collection.memory;
import crate.api.rest.policy;
import crate.http.request;

alias s = Spec!({
  describe("The crate router", {
    describe("using the RestApi", {
      CrateRouterBis!(RestApi, DefaultStorage) router;
      MemoryCrate!TestModel crate;
      TypeRouter!(RestApi, TestModel, DefaultStorage) typeRouter;

      beforeEach({
        crate = new MemoryCrate!TestModel;
        router = new CrateRouterBis!(RestApi, DefaultStorage);
        typeRouter = router.using(crate)
          .expose!"getName"
          .expose!"setName"
          .expose!"handlerRequest";
      });

      describe("defining the default routes", {
        it("should not match other models that are not defined", {
          auto result = router.match(HTTPMethod.GET, "/othermodels/000000000000000000000001");
          result.should.equal(null);
        });

        it("should define the get item route", {
          auto result = router.match(HTTPMethod.GET, "/testmodels/000000000000000000000001");
          result.should.not.equal(null);
        });

        it("should define the get list route", {
          auto result = router.match(HTTPMethod.GET, "/testmodels");
          result.should.not.equal(null);
        });

        it("should define the put item route", {
          auto result = router.match(HTTPMethod.PUT, "/testmodels/000000000000000000000001");
          result.should.not.equal(null);
        });

        it("should define the patch item route", {
          auto result = router.match(HTTPMethod.PATCH, "/testmodels/000000000000000000000001");
          result.should.not.equal(null);
        });

        it("should define the delete item route", {
          auto result = router.match(HTTPMethod.DELETE, "/testmodels/000000000000000000000001");
          result.should.not.equal(null);
        });

        it("should not define a random get route", {
          auto result = router.match(HTTPMethod.GET, "/testmodels/000000000000000000000001/random");
          result.should.equal(null);
        });

        it("should not define the get item route with a POST method", {
          auto result = router.match(HTTPMethod.POST, "/testmodels/000000000000000000000001");
          result.should.equal(null);
        });

        it("should define the getName route", {
          auto result = router.match(HTTPMethod.GET, "/testmodels/000000000000000000000001/getName");
          result.should.not.equal(null);
        });

        it("should define the setName route", {
          auto result = router.match(HTTPMethod.POST, "/testmodels/000000000000000000000001/setName");
          result.should.not.equal(null);
        });
      });

      describe("the http rules", {
        it("should generate a rule for each operation type", {
          router.rules.map!(a => a.request.method.to!string ~ " " ~ a.request.path).array.serializeToJson.should.equal(`[
            "GET /testmodels/:id",
            "GET /testmodels",
            "DELETE /testmodels/:id",
            "PUT /testmodels/:id",
            "PATCH /testmodels/:id",
            "POST /testmodels",
            "GET /testmodels/:id/getName",
            "POST /testmodels/:id/setName",
            "GET /testmodels/:id/handlerRequest"]`.parseJsonString);
        });
      });

      describe("the http item operations", {
        URLRouter urlRouter;

        describe("a get operation", {
          beforeEach({
            @mime("custom/mime")
            string myOperation() {
              return "hello";
            }

            typeRouter.itemOperation!("my-operation")(&myOperation);
            urlRouter = new URLRouter;

            crate.addItem(`{
              "name": "test",
              "value": 1
            }`.parseJsonString);

            urlRouter.any("*", requestErrorHandler(&router.handler));
          });

          it("should be defined", {
            auto result = router.match(HTTPMethod.GET, "/testmodels/000000000000000000000001/my-operation");
            result.should.not.equal(null);
          });

          it("should be described", {
            auto rule = typeRouter.rules
              .filter!(a => a.request.path == "/testmodels/:id/my-operation").front;

            rule.request
              .serializeToJson
                .should.equal(`{
                  "method": 0,
                  "parameters": [{
                    "description": "The id of the object that you want to use",
                    "in": "path",
                    "schema": {
                      "type": "string"
                    },
                    "required": true,
                    "name": "id"
                  }],
                  "mime": "text/plain",
                  "path": "/testmodels/:id/my-operation"
                }`.parseJsonString);
          });

          it("should respond with the function output on get", {
            urlRouter
              .request
              .get("/testmodels/000000000000000000000001/my-operation")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyString.should.equal(`hello`);
              });
          });
        });

        describe("a post operation", {
          beforeEach({
            @mime("custom/mime")
            int myOperation(int value) {
              return value;
            }

            typeRouter.itemOperation!("post-operation")(&myOperation);
            urlRouter = new URLRouter;

            crate.addItem(`{
              "name": "test",
              "value": 1
            }`.parseJsonString);

            urlRouter.any("*", requestErrorHandler(&router.handler));
          });

          it("should be defined", {
            auto result = router.match(HTTPMethod.POST, "/testmodels/000000000000000000000001/post-operation");
            result.should.not.equal(null);
          });

          it("should be described", {
            auto rule = typeRouter.rules
              .filter!(a => a.request.path == "/testmodels/:id/post-operation").front;

            rule.request
              .serializeToJson
                .should.equal(`{
                  "method": 3,
                  "schema": {
                    "type": "string"
                  },
                  "parameters": [{
                    "description": "The id of the object that you want to use",
                    "in": "path",
                    "schema": {
                      "type": "string"
                    },
                    "required": true,
                    "name": "id"
                  }],
                  "mime": "text/plain",
                  "path": "/testmodels/:id/post-operation"
                }`.parseJsonString);
          });
        });
      });

      describe("handling action requests", {
        URLRouter urlRouter;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should get the method return value", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001/getName")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`test`);
            });
        });

        it("should call the method with the right param type", {
          urlRouter
            .request
            .post("/testmodels/000000000000000000000001/setName")
            .send("other test")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`other test`);
            });
        });

        it("should call the method with a http request", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001/handlerRequest")
            .send("other test")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal(`called`);
            });
        });
      });

      describe("handling default requests", {
        URLRouter urlRouter;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should get an existing item using a get item request", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000001",
                  "name": "test",
                  "value": 1
                }
              }`.parseJsonString);
            });
        });

        it("should get the headers fot the options request", {
          urlRouter
            .request
            .customMethod!(HTTPMethod.OPTIONS)("/testmodels/000000000000000000000001")
            .expectStatusCode(204)
            .end((Response response) => () {
              response.headers.serializeToJson.should.equal(`{
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, DELETE, PUT, PATCH, OPTIONS",
                "Access-Control-Allow-Headers": "Content-Type, Authorization, If-None-Match"
              }`.parseJsonString);
              response.bodyString.should.equal(``);
            });
        });

        it("should return 404 for an options request that was not defined", {
          urlRouter
            .request
            .customMethod!(HTTPMethod.OPTIONS)("/missing/000000000000000000000001")
            .expectStatusCode(404)
            .end((Response response) => () {
              response.headers.serializeToJson.should.equal(`{}`.parseJsonString);
              response.bodyString.should.equal(``);
            });
        });

        it("should get an error using a get item request for a missing item", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000002")
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                "description": "There is no item with id ` ~ "`000000000000000000000002`" ~ `",
                "status": 404,
                "title": "Crate not found"
              }]}`).parseJsonString);
            });
        });

        it("should get the list of stored items", {
          urlRouter
            .request
            .get("/testmodels")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModels": [{
                  "_id": "000000000000000000000001",
                  "name": "test",
                  "value": 1
                }]
              }`.parseJsonString);
            });
        });

        it("should be able to delete an item", {
          urlRouter
            .request
            .delete_("/testmodels/000000000000000000000001")
            .expectStatusCode(204)
            .end((Response response) => () {
              response.bodyString.should.equal(``);
            });
        });

        it("should get an error on deleting a missing item", {
          urlRouter
            .request
            .delete_("/testmodels/000000000000000000000002")
            .expectStatusCode(404)
            .end((Response response) => () {
              response.bodyJson.should.equal((`{"errors": [{
                "description": "There is no item with id ` ~ "`000000000000000000000002`" ~ `",
                "status": 404,
                "title": "Crate not found"
              }]}`).parseJsonString);
            });
        });

        it("should be able to replace an item", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 5
          }}`.parseJsonString;

          urlRouter
            .request
            .put("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000001",
                  "name": "new name",
                  "value": 5
                }
              }`.parseJsonString);
            });
        });

        it("should be able to update an item", {
          auto data = `{ "testModel": {
            "name": "new name"
          }}`.parseJsonString;

          urlRouter
            .request
            .patch("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000001",
                  "name": "new name",
                  "value": 1
                }
              }`.parseJsonString);
            });
        });

        it("should be able to create an item", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 2
          }}`.parseJsonString;

          urlRouter
            .request
            .post("/testmodels")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000002",
                  "name": "new name",
                  "value": 2
                }
              }`.parseJsonString);
            });
        });
      });
    });
  });
});
