/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.router.router;

import trial.interfaces;

import std.functional;
import std.algorithm;
import std.array;
import std.conv;
import std.datetime;

import trial.step;
import trial.runner;
import trial.discovery.spec;
import fluent.asserts;
import fluentasserts.vibe.request;

import vibe.data.json;
import vibe.inet.url;
import vibe.http.router;

import crate.base;
import crate.lifecycle;
import crate.error;
import crate.http.router;
import crate.collection.memory;
import crate.api.rest.policy;
import tests.crate.http.router.mocks;
import crate.http.wrapper;

alias s = Spec!({
  describe("The crate router", {
    describe("using the RestApi", {
      CrateRouterBis!(RestApi, DefaultStorage) router;
      MemoryCrate!TestModel crate;
      TypeRouter!(RestApi, TestModel, DefaultStorage) typeRouter;

      beforeEach({
        crate = new MemoryCrate!TestModel;
        router = new CrateRouterBis!(RestApi, DefaultStorage);
        typeRouter = router.using(crate)
          .expose!"getName"
          .expose!"setName"
          .expose!"handlerRequest";
      });

      afterEach({
        CrateLifecycle.instance = new CrateLifecycle;
      });

      describe("handling query parameters", {
        URLRouter urlRouter;
        TestQueryParamsMiddleware middleware;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          middleware = new TestQueryParamsMiddleware;
          typeRouter.and(middleware);
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should parse and pass the query parameters to a middleware", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001?value1=1&value2=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.params.value1.should.equal(1);
              middleware.params.value2.should.equal("2");
            });
        });

        it("should add the query params to the get list rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels" && a.request.method.to!string == "GET").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the create rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels" && a.request.method.to!string == "POST").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the get item rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels/:id" && a.request.method.to!string == "GET").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to retreive","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the update rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels/:id" && a.request.method.to!string == "PATCH").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to patch","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the replace rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels/:id" && a.request.method.to!string == "PUT").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to replace","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the delete rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels/:id" && a.request.method.to!string == "DELETE").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to delete","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });

        it("should add the query params to the action rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/testmodels/:id/getName" && a.request.method.to!string == "GET").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            {"description":"The id of the object that you want to run the action","in":"path","schema":{"type":"string"},"required":true,"name":"id"},
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });
      });

      describe("using mappers", {
        URLRouter urlRouter;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          typeRouter.and(new TestMapper);
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should apply a mapper to a get item request", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000001",
                  "name": "test",
                  "value": 1,
                  "other1": "value1",
                  "other2": "value2",
                }
              }`.parseJsonString);
            });
        });

        it("should apply a mapper to a get list request", {
          urlRouter
            .request
            .get("/testmodels")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModels": [{
                  "_id": "000000000000000000000001",
                  "name": "test",
                  "value": 1,
                  "other1": "value1",
                  "other2": "value2"
                }]
              }`.parseJsonString);
            });
        });

        it("should apply a mapper to a replace item request", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 5
          }}`.parseJsonString;

          urlRouter
            .request
            .put("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000001",
                  "name": "new name",
                  "value": 5,
                  "other1": "value1",
                  "other2": "value2"
                }
              }`.parseJsonString);
            });
        });

        it("should apply a mapper to a update item request", {
          auto data = `{ "testModel": {
            "name": "new name"
          }}`.parseJsonString;

          urlRouter
            .request
            .patch("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000001",
                  "name": "new name",
                  "value": 1,
                  "other1": "value1",
                  "other2": "value2"
                }
              }`.parseJsonString);
            });
        });

        it("should apply a mapper to a create item request", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 2
          }}`.parseJsonString;

          urlRouter
            .request
            .post("/testmodels")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "testModel": {
                  "_id": "000000000000000000000002",
                  "name": "new name",
                  "value": 2,
                  "other1": "value1",
                  "other2": "value2"
                }
              }`.parseJsonString);
            });
        });
      });

      describe("using middlewares", {
        URLRouter urlRouter;
        TestMiddleware middleware;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          middleware = new TestMiddleware;
          typeRouter.and(middleware);
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should apply the any and getItem to a get item request", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.operations.should.equal(["any", "getItem"]);
            });
        });

        it("should apply a mapper to a get list request", {
          urlRouter
            .request
            .get("/testmodels")
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.operations.should.equal(["any", "getList"]);
            });
        });

        it("should apply any and replace to a replace item request", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 5
          }}`.parseJsonString;

          urlRouter
            .request
            .put("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.operations.should.equal(["any", "replace"]);
            });
        });

        it("should apply any and update to a update item request", {
          auto data = `{ "testModel": {
            "name": "new name"
          }}`.parseJsonString;

          urlRouter
            .request
            .patch("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.operations.should.equal(["any", "update"]);
            });
        });

        it("should apply any and delete_ to a delete item request", {
          auto data = `{ "testModel": {
            "name": "new name"
          }}`.parseJsonString;

          urlRouter
            .request
            .delete_("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(204)
            .end((Response response) => () {
              middleware.operations.should.equal(["any", "delete_"]);
            });
        });

        it("should apply a mapper to a create item request", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 2
          }}`.parseJsonString;

          urlRouter
            .request
            .post("/testmodels")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.operations.should.equal(["any", "create"]);
            });
        });

        it("should call the lifecycle middleware benchmark function", {
          auto data = `{ "testModel": {
            "name": "new name",
            "value": 2
          }}`.parseJsonString;

          string calledRoute;
          string calledMiddlewareName;
          Duration calledDuration;
          void benchmark(const string route, const string middelewareName, const Duration duration) {
            calledRoute = route;
            calledMiddlewareName = middelewareName;
            calledDuration = duration;
          }

          CrateLifecycle.instance.onMiddlewareBenchmark(&benchmark);

          urlRouter
            .request
            .patch("/testmodels/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              calledRoute.should.equal("/testmodels/:id");
              calledMiddlewareName.should.equal("TestMiddleware.update");
              calledDuration.total!"hnsecs".should.be.greaterThan(0);
            });
        });
      });

      describe("using middlewares with query params", {
        URLRouter urlRouter;
        TestAllQueryParamsMiddleware middleware;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          middleware = new TestAllQueryParamsMiddleware;
          typeRouter.and(middleware);
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should apply the any, getItem and mapper query", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.values.should.equal(["", "", ""]);
            });
        });

        it("should apply the any, getItem and mapper query", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001?anyValue=1&getItem=2&mapper=3")
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.values.should.equal(["1", "2", "3"]);
            });
        });
      });

      describe("handling exception middlewares", {
        URLRouter urlRouter;

        beforeEach({
          urlRouter = new URLRouter;

          crate.addItem(`{
            "name": "test",
            "value": 1
          }`.parseJsonString);

          typeRouter.and(new TestExceptionMiddleware);
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should apply the any and getItem to a get item request", {
          urlRouter
            .request
            .get("/testmodels/000000000000000000000001")
            .expectStatusCode(500)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "errors": [{
                  "description": "this is a test",
                  "status": 500,
                  "title": "Server error"
                }]
              }`.parseJsonString);
            });
        });
      });

      describe("handling actions", {
        URLRouter urlRouter;
        CrateRouterBis!(RestApi, DefaultStorage) router;
        MemoryCrate!TestModel crate;
        TypeRouter!(RestApi, TestModel, DefaultStorage) typeRouter;

        beforeEach({
          crate = new MemoryCrate!TestModel;
          router = new CrateRouterBis!(RestApi, DefaultStorage);
          typeRouter = router.using(crate).expose!"getName";

          urlRouter = new URLRouter;
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should call middlewares defined after the action was created", {
          typeRouter.and(new TestExceptionMiddleware);

          urlRouter
            .request
            .get("/testmodels/000000000000000000000001/getName")
            .expectStatusCode(500)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "errors": [{
                  "description": "this is a test",
                  "status": 500,
                  "title": "Server error"
                }]
              }`.parseJsonString);
            });
        });
      });
    });

    describe("using the blank router", {
      CrateRouterBis!(RestApi, DefaultStorage) router;
      BlankRouter!(DefaultStorage) blankRouter;

      beforeEach({
        router = new CrateRouterBis!(RestApi, DefaultStorage);
        blankRouter = router.blank;

        blankRouter.withCustomOperation(new BlankTestOperation);
      });

      afterEach({
        CrateLifecycle.instance = new CrateLifecycle;
      });

      describe("handling query parameters", {
        URLRouter urlRouter;
        TestQueryParamsMiddleware middleware;

        beforeEach({
          urlRouter = new URLRouter;

          middleware = new TestQueryParamsMiddleware;
          blankRouter.and(middleware);
          urlRouter.any("*", requestErrorHandler(&router.handler));
        });

        it("should parse and pass the query parameters to a middleware", {
          urlRouter
            .request
            .get("/blank/test?value1=1&value2=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              middleware.params.value1.should.equal(1);
              middleware.params.value2.should.equal("2");
            });
        });

        it("should add the query params to the custom operation rule", {
          auto rule = router.rules.filter!(a => a.request.path == "/blank/test" && a.request.method.to!string == "GET").front;

          rule.request.parameters.serializeToJson.should.equal(`[
            { "in": "query", "name": "value1" },
            { "in": "query", "name": "value2" }
          ]`.parseJsonString);
        });
      });
    });
  });
});


class BlankTestOperation : ApiOperation!DefaultStorage {

  this() {
    CrateRule rule;

    rule.request.path = "/blank/test";
    rule.request.method = HTTPMethod.GET;

    super(rule);
  }

  override void addMiddlewares(IMiddlewareWrapper!DefaultStorage middlewares) {
    super.addMiddlewares(middlewares);

    this.updateRule!"getItem";
  }

  ///
  override void handle(DefaultStorage storage) {
    auto res = storage.response;

    handleMiddlewaresWithoutQueries!"getItem"(storage);
    if(storage.response.headerWritten) {
      return;
    }

    res.writeBody(`done.`, 200, "text/plain");
  }
}