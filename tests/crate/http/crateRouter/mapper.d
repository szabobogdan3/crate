/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.mapper;


import tests.crate.http.crateRouter.mocks;

import vibe.http.router;

import trial.step;
import trial.runner;
import trial.discovery.spec;

import fluent.asserts;
import fluentasserts.vibe.request;
import fluentasserts.vibe.json;

import crate.http.router;
import crate.http.request;
import crate.collection.memory;
import vibe.data.json;

class MockMiddleware {
  @mapper
  Json mapper(CrateHttpRequest, const Json item) @safe {
    Json result = item;
    result["added with request"] = "later";

    return result;
  }

  @mapper
  Json mapper(const Json item) @safe {
    Json result = item;
    result["added"] = "later";

    return result;
  }
}

alias s = Spec!({
  describe("The crate router", {
    URLRouter router;

    describe("with a middleware", {
      before({
        router = new URLRouter;
          auto baseCrate = new MemoryCrate!Site;
          baseCrate.addItem(`{
            "position": { "type": "", "coordinates": [ 1, 2 ] },
          }`.parseJsonString);

          router
            .crateSetup
              .add(baseCrate, new MockMiddleware);
      });

      it("should run the mapper handler for get list", {
        request(router)
          .get("/sites")
            .end((Response response) => () {
              response.bodyJson
                .should
                .equal(`{"sites":[{
                  "_id":"000000000000000000000001",
                  "added with request": "later",
                  "added": "later",
                  "position":{"type":"","coordinates":[1, 2]}}
                ]}`.parseJsonString);
            });
      });

      it("should run the mapper handler for get item", {
        request(router)
          .get("/sites/000000000000000000000001")
            .end((Response response) => () {
              response.bodyJson
                .should
                .equal(`{"site":{
                  "_id":"000000000000000000000001",
                  "added with request": "later",
                  "added": "later",
                  "position":{"type":"","coordinates":[1, 2]}}
                }`.parseJsonString);
            });
      });
    });
  });
});
