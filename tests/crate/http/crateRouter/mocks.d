/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.mocks;

import crate.base;
import crate.collection.memory;
import crate.http.request;
import crate.http.router;

import vibe.data.json;
import vibe.http.router;

import fluentasserts.vibe.request;

import std.algorithm;
import std.conv;

public import tests.crate.http.mocks;

class TypeFilter {
  @any
  IQuery any(IQuery selector, HTTPServerRequest request) @safe {
    if("type" !in request.query) {
      return selector;
    }

    selector.where("position.type").equal(request.query["type"]);

    return selector;
  }
}

class MockAnyQueryFilter {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @any
  IQuery any(IQuery selector, QueryParams params) @safe {
    throw new Exception(params.serializeToJson.to!string);
  }
}

class MockQueryFilter {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @get
  IQuery get(IQuery selector, QueryParams params) @safe {
    throw new Exception(params.serializeToJson.to!string);
  }

  @update @replace
  IQuery update(IQuery selector, QueryParams params) @safe {
    throw new Exception(params.serializeToJson.to!string);
  }
}

class MockQueryFilterRequest {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @get
  IQuery get(IQuery selector, QueryParams params, HTTPServerRequest request) @safe {
    throw new Exception(`{"get": ` ~ params.serializeToJson.to!string ~ `}`);
  }

  @getList
  IQuery getList(IQuery selector, QueryParams params, HTTPServerRequest request) @safe {
    throw new Exception(`{"getList": ` ~ params.serializeToJson.to!string ~ `}`);
  }

  @getItem
  IQuery getItem(IQuery selector, QueryParams params, HTTPServerRequest request) @safe {
    throw new Exception(`{"getItem":` ~ params.serializeToJson.to!string ~ `}`);
  }

  @patch
  IQuery patch(IQuery selector, QueryParams params, HTTPServerRequest request) @safe {
    throw new Exception(`{"patch":` ~ params.serializeToJson.to!string ~ `}`);
  }

  @put
  IQuery put(IQuery selector, QueryParams params, HTTPServerRequest request) @safe {
    throw new Exception(`{"put" : ` ~ params.serializeToJson.to!string ~ `}`);
  }

  @update
  IQuery update(IQuery selector, QueryParams params, HTTPServerRequest request) @safe {
    throw new Exception(`{"update": ` ~ params.serializeToJson.to!string ~ `}`);
  }
}


class MockCrateMapperQueryFilterRequest {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @mapper
  Json mapper(CrateHttpRequest req, const Json item, QueryParams params) @trusted nothrow {
    import std.stdio;

    try {
      return params.serializeToJson;
    } catch(Exception) { return item; }
  }
}

class MockVibeMapperQueryFilterRequest {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @mapper
  Json mapper(HTTPServerRequest req, const Json item, QueryParams params) @trusted nothrow {
    try {
      return params.serializeToJson;
    } catch(Exception) { return item; }
  }
}

class FilterAll {

  @any
  IQuery any(IQuery selector, HTTPServerRequest request) @safe {
    selector.where("_id").equal("missing");

    return selector;
  }
}

class MockMiddleware {
  @getList @getItem @create @replace @patch @delete_
  void handler(HTTPServerRequest req, HTTPServerResponse res) @safe {
    res.statusCode = 412;
    res.writeBody("error");
  }
}

class MockAnyMiddleware {
  @any
  void any(HTTPServerRequest req, HTTPServerResponse res) @safe {
    res.statusCode = 412;
    res.writeBody("error");
  }
}

class MockAnyQueryMiddleware {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @any
  void any(QueryParams params, HTTPServerRequest req, HTTPServerResponse res) @safe {
    res.statusCode = 200;
    res.writeJsonBody(params);
  }
}

class MockQueryMiddleware {
  struct QueryParams {
    string someString;
    bool someBool;
    int someNumber;
    string[] someList;
  }

  @getList @getItem @create @replace @patch @delete_
  void handler(QueryParams params, HTTPServerRequest req, HTTPServerResponse res) @safe {
    res.statusCode = 200;
    auto jsonParams = params.serializeToJson;
    jsonParams["method"] = req.method.to!string;

    res.writeJsonBody(jsonParams);
  }
}

class SomeTestCrateFilter {
  @any
  IQuery any(IQuery selector, HTTPServerRequest request) @safe {
    return new MemoryQuery(selector.exec.filter!(a => a["position"]["type"] == "Point"));
  }
}

auto queryRouter() {
  auto router = new URLRouter();
  auto baseCrate = new MemoryCrate!Site;

  router
    .crateSetup
      .add(baseCrate, new SomeTestCrateFilter);

  Json data1 = `{
      "position": {
        "type": "Point",
        "coordinates": [0, 0]
      }
  }`.parseJsonString;

  Json data2 = `{
      "position": {
        "type": "Dot",
        "coordinates": [1, 1]
      }
  }`.parseJsonString;

  baseCrate.addItem(data1);
  baseCrate.addItem(data2);

  return router;
}

auto testRouter(T...)(T filters) {
  auto router = new URLRouter();
  auto baseCrate = new MemoryCrate!Site;

  router
    .crateSetup
      .add(baseCrate, filters);

  Json data = `{
      "position": {
        "type": "Point",
        "coordinates": [0, 0]
      }
  }`.parseJsonString;

  baseCrate.addItem(data);

  return request(router);
}
