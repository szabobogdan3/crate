/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.action;

import tests.crate.http.crateRouter.mocks;

import trial.interfaces;

import trial.step;
import trial.runner;
import trial.discovery.spec;
import fluent.asserts;

import vibe.data.json;
import vibe.http.router;

import crate.http.router;
import crate.collection.memory;


alias s = Spec!({
  describe("The crate router", {
    URLRouter router;
    MemoryCrate!MockActionModel actionCrate;

    beforeEach({
      actionCrate = new MemoryCrate!MockActionModel;
      router = new URLRouter();

      Json itemData = `{
        "_id": "000000000000000000000001",
        "name": "some name"
      }`.parseJsonString;

      actionCrate.addItem(itemData);
    });

    describe("with a GET REST Api request", {
      it("should call an action", {
        router
          .crateSetup
            .add(actionCrate);

        request(router)
          .get("/mockactionmodels/000000000000000000000001/actionChangeAndReturn")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal("changed and return");
            });
      });

      it("should not call actions of filtered items", {
        router
          .crateSetup
            .add(actionCrate, new FilterAll);

        request(router)
          .get("/mockactionmodels/000000000000000000000001/actionChangeAndReturn")
            .expectStatusCode(404)
            .end();
      });
    });
  });
});
