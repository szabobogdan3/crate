/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.queryParams;

import tests.crate.http.crateRouter.mocks;

import trial.interfaces;

import trial.step;
import trial.runner;
import trial.discovery.spec;
import fluent.asserts;
import fluentasserts.vibe.request;
import fluentasserts.vibe.json;

import vibe.data.json;
import vibe.http.router;

import crate.http.router;
import crate.collection.memory;

alias s = Spec!({
  describe("The crate router", {
    describe("with any query params middleware", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;

        router
          .crateSetup
            .add(baseCrate, new MockAnyQueryMiddleware);
      });

      it("should get empty query params when they are not provided", {
          request(router)
            .get("/sites")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyJson.should.equal(`{
                  "someList": [],
                  "someString": "",
                  "someBool": false,
                  "someNumber": 0 }`.parseJsonString);
              });
      });

      it("should get the parsed query params", {
          request(router)
            .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
              .expectStatusCode(200)
              .end((Response response) => () {
                response.bodyJson.should.equal(`{
                  "someList": ["1", "2", "3"],
                  "someString": "test",
                  "someBool": true,
                  "someNumber": 2 }`.parseJsonString);
              });
      });
    });

    describe("with methods query params middlewares", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;

        router
          .crateSetup
            .add(baseCrate, new MockQueryMiddleware);
      });

      it("should get empty query params when they are not provided", {
        request(router)
          .get("/sites")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "someList": [],
                "someString": "",
                "someBool": false,
                "method": "GET",
                "someNumber": 0 }`.parseJsonString);
            });
      });

      it("should get the parsed query params with GET", {
        request(router)
          .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "someList": ["1", "2", "3"],
                "someString": "test",
                "someBool": true,
                "method": "GET",
                "someNumber": 2 }`.parseJsonString);
            });
      });

      it("should get the parsed query params with POST", {
        request(router)
          .post("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "someList": ["1", "2", "3"],
                "someString": "test",
                "someBool": true,
                "method": "POST",
                "someNumber": 2 }`.parseJsonString);
            });
      });

      it("should get the parsed query params with PUT", {
        request(router)
          .put("/sites/1?someList=1,2,3&someString=test&someBool=true&someNumber=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "someList": ["1", "2", "3"],
                "someString": "test",
                "someBool": true,
                "method": "PUT",
                "someNumber": 2 }`.parseJsonString);
            });
      });

      it("should get the parsed query params with PATCH", {
        request(router)
          .patch("/sites/1?someList=1,2,3&someString=test&someBool=true&someNumber=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "someList": ["1", "2", "3"],
                "someString": "test",
                "someBool": true,
                "method": "PATCH",
                "someNumber": 2 }`.parseJsonString);
            });
      });

      it("should get the parsed query params with DELETE", {
        request(router)
          .delete_("/sites/1?someList=1,2,3&someString=test&someBool=true&someNumber=2")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(`{
                "someList": ["1", "2", "3"],
                "someString": "test",
                "someBool": true,
                "method": "DELETE",
                "someNumber": 2 }`.parseJsonString);
            });
      });
    });

    describe("with any query params filter", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;

        router
          .crateSetup
            .add(baseCrate, new MockAnyQueryFilter);
      });

      it("should get empty query params when they are not provided", {
          request(router)
            .get("/sites")
              .end((Response response) => () {
                response.bodyJson["errors"][0]["description"]
                  .to!string.parseJsonString.should.equal(`{
                    "someList": [],
                    "someString": "",
                    "someBool": false,
                    "someNumber": 0 }`.parseJsonString);
              });
      });

      it("should get the parsed query params", {
          request(router)
            .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
              .end((Response response) => () {
                response.bodyJson["errors"][0]["description"]
                  .to!string.parseJsonString.should.equal(`{
                    "someList": ["1", "2", "3"],
                    "someString": "test",
                    "someBool": true,
                    "someNumber": 2 }`.parseJsonString);
              });
      });
    });

    describe("with methods query params filter", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;
        baseCrate.addItem(`{}`.parseJsonString);

        router
          .crateSetup
            .add(baseCrate, new MockQueryFilter);
      });

      it("should get empty query params when they are not provided", {
          request(router)
            .get("/sites")
              .end((Response response) => () {
                response.bodyJson["errors"][0]["description"]
                  .to!string.parseJsonString.should.equal(`{
                    "someList": [],
                    "someString": "",
                    "someBool": false,
                    "someNumber": 0 }`.parseJsonString);
              });
      });

      it("should get the parsed query params on get requests", {
          request(router)
            .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
              .end((Response response) => () {
                response.bodyJson["errors"][0]["description"]
                  .to!string.parseJsonString.should.equal(`{
                    "someList": ["1", "2", "3"],
                    "someString": "test",
                    "someBool": true,
                    "someNumber": 2 }`.parseJsonString);
              });
      });

      it("should get the parsed query params on put requests", {
        request(router)
          .put("/sites/000000000000000000000001?someList=1,2,3&someString=test&someBool=true&someNumber=2")
            .end((Response response) => () {
              response.bodyJson["errors"][0]["description"]
                .to!string.parseJsonString.should.equal(`{
                  "someList": ["1", "2", "3"],
                  "someString": "test",
                  "someBool": true,
                  "someNumber": 2 }`.parseJsonString);
            });
      });
    });

    describe("with methods query params filter and request", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;
        baseCrate.addItem(`{}`.parseJsonString);

        router
          .crateSetup
            .add(baseCrate, new MockQueryFilterRequest);
      });

      it("should get empty query params when they are not provided", {
          request(router)
            .get("/sites")
              .end((Response response) => () {

                response.bodyJson["errors"][0]["description"]
                  .to!string.parseJsonString.should.equal(`{"get":{
                    "someList": [],
                    "someString": "",
                    "someBool": false,
                    "someNumber": 0 }}`.parseJsonString);
              });
      });

      it("should get the parsed query params on get requests", {
          request(router)
            .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
              .end((Response response) => () {
                response.bodyJson["errors"][0]["description"]
                  .to!string.parseJsonString.should.equal(`{"get": {
                    "someList": ["1", "2", "3"],
                    "someString": "test",
                    "someBool": true,
                    "someNumber": 2 }}`.parseJsonString);
              });
      });

      it("should get the parsed query params on update requests", {
        request(router)
          .put("/sites/000000000000000000000001?someList=1,2,3&someString=test&someBool=true&someNumber=2")
          .end((Response response) => () {
            response.bodyJson["errors"][0]["description"]
              .to!string.parseJsonString.should.equal(`{"put": {
                "someList": ["1", "2", "3"],
                "someString": "test",
                "someBool": true,
                "someNumber": 2 }}`.parseJsonString);
          });
      });
    });

    describe("with vibe mapper query params filter and request", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;
        baseCrate.addItem(`{}`.parseJsonString);

        router
          .crateSetup
            .add(baseCrate, new MockVibeMapperQueryFilterRequest);
      });

      it("should get empty query params when they are not provided", {
          request(router)
            .get("/sites")
              .end((Response response) => () {
                response.bodyJson.to!string.parseJsonString.should.equal(`{"sites": [{
                    "someList": [],
                    "someString": "",
                    "someBool": false,
                    "someNumber": 0 }]}`.parseJsonString);
              });
      });

      it("should get the parsed query params on get requests", {
          request(router)
            .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
              .end((Response response) => () {
                response.bodyJson.to!string.parseJsonString.should.equal(`{ "sites": [{
                    "someList": ["1", "2", "3"],
                    "someString": "test",
                    "someBool": true,
                    "someNumber": 2 }]}`.parseJsonString);
              });
      });
    });

    describe("with crate mapper query params filter and request", {
      URLRouter router;

      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;
        baseCrate.addItem(`{}`.parseJsonString);

        router
          .crateSetup
            .add(baseCrate, new MockCrateMapperQueryFilterRequest);
      });

      it("should get empty query params when they are not provided", {
          request(router)
            .get("/sites")
              .end((Response response) => () {
                response.bodyJson.to!string.parseJsonString.should.equal(`{"sites": [{
                    "someList": [],
                    "someString": "",
                    "someBool": false,
                    "someNumber": 0 }]}`.parseJsonString);
              });
      });

      it("should get the parsed query params on get requests", {
          request(router)
            .get("/sites?someList=1,2,3&someString=test&someBool=true&someNumber=2")
              .end((Response response) => () {
                response.bodyJson.to!string.parseJsonString.should.equal(`{ "sites": [{
                    "someList": ["1", "2", "3"],
                    "someString": "test",
                    "someBool": true,
                    "someNumber": 2 }]}`.parseJsonString);
              });
      });
    });
  });
});
