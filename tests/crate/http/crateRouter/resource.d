/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.resource;

import tests.crate.http.crateRouter.mocks;

import trial.interfaces;

import trial.step;
import trial.runner;
import trial.discovery.spec;
import fluent.asserts;

import vibe.data.json;
import vibe.http.router;

import crate.http.router;
import crate.collection.memory;


alias s = Spec!({
  describe("The crate router", {
    MemoryCrate!ResourceCrate resourceCrate;
    URLRouter router;

    beforeEach({
      TestResource.removed = false;
      resourceCrate = new MemoryCrate!ResourceCrate;

      Json itemData = `{
        "_id": "000000000000000000000001",
        "resource": ""
      }`.parseJsonString;

      resourceCrate.addItem(itemData);

      router = new URLRouter();
    });

    describe("with a GET REST Api request", {
      it("should get a resource", {
        router
          .crateSetup
            .add(resourceCrate);

        request(router)
          .get("/resourcecrates/000000000000000000000001/resource")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyString.should.equal("test body");
            });
      });

      it("should deny a resource with a filter", {
        router
          .crateSetup
            .add(resourceCrate, new FilterAll);

        request(router)
          .get("/resourcecrates/000000000000000000000001/resource")
            .expectStatusCode(404)
            .end();
      });

      it("should call the `any` middleware for a resource", {
        router
          .crateSetup
            .add(resourceCrate, new MockAnyMiddleware);

        request(router)
          .get("/resourcecrates/000000000000000000000001/resource")
            .expectStatusCode(412)
            .end();
      });
    });

    describe("with a DELETE Api request", {
      it("should get a resource", {
        router
          .crateSetup
            .add(resourceCrate);

        request(router)
          .delete_("/resourcecrates/000000000000000000000001")
            .expectStatusCode(204)
            .end((Response response) => () {
              TestResource.removed.should.equal(true);
            });
      });

      it("should deny a resource with a filter", {
        router
          .crateSetup
            .add(resourceCrate, new FilterAll);

        request(router)
          .get("/resourcecrates/000000000000000000000001/resource")
            .expectStatusCode(404)
            .end();
      });

      it("should call the `any` middleware for a resource", {
        router
          .crateSetup
            .add(resourceCrate, new MockAnyMiddleware);

        request(router)
          .get("/resourcecrates/000000000000000000000001/resource")
            .expectStatusCode(412)
            .end();
      });
    });

    describe("with a PUT REST Api request", {
      string data;

      beforeEach({
        TestResource.lastRead = "";

        data = "-----------------------------9855312492823326321373169801\r\n";
        data ~= "Content-Disposition: form-data; name=\"resource\"; filename=\"resource.txt\"\r\n";
        data ~= "Content-Type: text/plain\r\n\r\n";
        data ~= "hello\r\n";
        data ~= "-----------------------------9855312492823326321373169801--\r\n";
      });

      it("should update a resource", {
        router
          .crateSetup
            .add(resourceCrate);

        request(router)
          .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
          .post("/resourcecrates/000000000000000000000001/resource")
          .send(data)
            .expectStatusCode(201)
            .end((Response response) => () {
              TestResource.lastRead.should.equal("hello");
            });
      });

      it("should not update a filtered resource", {
        router
          .crateSetup
            .add(resourceCrate, new FilterAll);

        request(router)
          .header("Content-Type", "multipart/form-data; boundary=---------------------------9855312492823326321373169801")
          .post("/resourcecrates/000000000000000000000001/resource")
          .send(data)
            .expectStatusCode(404)
            .end();
      });
    });
  });
});
