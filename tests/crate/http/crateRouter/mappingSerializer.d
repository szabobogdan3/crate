/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.mappingSerializer;


import tests.crate.http.crateRouter.mocks;

import vibe.http.router;
import vibe.data.json;

import trial.step;
import trial.runner;
import trial.discovery.spec;

import fluent.asserts;
import fluentasserts.vibe.request;
import fluentasserts.vibe.json;

import crate.http.router;
import crate.http.request;
import crate.collection.memory;

class MockMiddleware {
  @mapper
  Json mapper(CrateHttpRequest req, const Json item) @safe {
    Json result = item;
    result["a"] = "b";

    return result;
  }
}


alias s = Spec!({
  describe("The crate router", {
    URLRouter router;

    describe("with a mapper middleware", {
      before({
        router = new URLRouter;
          auto baseCrate = new MemoryCrate!Site;
          Json data = `{
            "position": {
              "type": "Point",
              "coordinates": [0, 0]
            }
          }`.parseJsonString;

          baseCrate.addItem(data);
          router
            .crateSetup
              .add(baseCrate, new MockMiddleware);
      });

      it("should be applied to the GET ALL request", {
        request(router)
          .get("/sites")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(
                `{"sites":[{"_id":"000000000000000000000001","position":{"type":"Point","coordinates":[0,0]},"a":"b"}]}`
                .parseJsonString);
            });
      });

      it("should be applied to the GET request", {
        request(router)
          .get("/sites/000000000000000000000001")
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(
                `{"site":{"_id":"000000000000000000000001","position":{"type":"Point","coordinates":[0,0]},"a":"b"}}`
                .parseJsonString);
            });
      });

      it("should be applied to the POST request", {
        auto data = `{"site":{"position":{"type":"Point","coordinates":[0,0]}}}`
          .parseJsonString;

        request(router)
          .post("/sites")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(
                `{"site":{"_id":"000000000000000000000002","position":{"type":"Point","coordinates":[0,0]},"a":"b"}}`
                .parseJsonString);
            });
      });

      it("should be applied to the PUT request", {
        auto data = `{"site":{"position":{"type":"Point","coordinates":[0,0]}}}`
          .parseJsonString;

        request(router)
          .put("/sites/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(
                `{"site":{"_id":"000000000000000000000001","position":{"type":"Point","coordinates":[0,0]},"a":"b"}}`
                .parseJsonString);
            });
      });


      it("should be applied to the PATCH request", {
        auto data = `{"site":{"position":{"type":"Point","coordinates":[0,0]}}}`
          .parseJsonString;

        request(router)
          .patch("/sites/000000000000000000000001")
            .send(data)
            .expectStatusCode(200)
            .end((Response response) => () {
              response.bodyJson.should.equal(
                `{"site":{"_id":"000000000000000000000001","position":{"type":"Point","coordinates":[0,0]},"a":"b"}}`
                .parseJsonString);
            });
      });
    });
  });
});

