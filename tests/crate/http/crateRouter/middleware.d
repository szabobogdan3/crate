/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.crateRouter.middleware;


import tests.crate.http.crateRouter.mocks;

import vibe.http.router;
import vibe.data.json;

import trial.step;
import trial.runner;
import trial.discovery.spec;

import fluent.asserts;
import fluentasserts.vibe.request;
import fluentasserts.vibe.json;

import crate.http.router;
import crate.collection.memory;

alias s = Spec!({
  describe("The crate router", {
    URLRouter router;
    Json data;

    before({
      data = `{"site": {
        "position": {
          "type": "Point",
          "coordinates": [1,2]
        }}}`.parseJsonString;
    });

    describe("with a middleware", {
      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;
        baseCrate.addItem(data["site"]);

        router
          .crateSetup
            .add(baseCrate, new MockMiddleware);
      });

      it("should run the middleware handler for get list", {
        request(router)
          .get("/sites")
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for get item", {
        request(router)
          .get("/sites/000000000000000000000001")
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for put item", {
        request(router)
          .put("/sites/000000000000000000000001")
          .send(data)
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for patch item", {
        request(router)
          .patch("/sites/000000000000000000000001")
          .send(data)
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for patch item", {
        request(router)
          .delete_("/sites/000000000000000000000001")
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for post item", {
        request(router)
          .post("/sites")
          .send(data)
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });
    });

    describe("with an empty middleware", {
      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!Site;
        baseCrate.addItem(data["site"]);

          router
            .crateSetup
              .add(baseCrate, new Object);
      });

      it("should run the middleware handler for get list", {
        request(router)
          .get("/sites")
            .expectStatusCode(200)
            .end;
      });

      it("should run the middleware handler for get item", {
        request(router)
          .get("/sites/000000000000000000000001")
            .expectStatusCode(200)
            .end;
      });

      it("should run the middleware handler for put item", {
        request(router)
          .put("/sites/000000000000000000000001")
          .send(data)
            .expectStatusCode(200)
            .end;
      });

      it("should run the middleware handler for patch item", {
        request(router)
          .patch("/sites/000000000000000000000001")
          .send(data)
            .expectStatusCode(200)
            .end;
      });

      it("should run the middleware handler for delete item", {
        request(router)
          .delete_("/sites/000000000000000000000001")
            .expectStatusCode(204)
            .end;
      });

      it("should run the middleware handler for post item", {
        request(router)
          .post("/sites")
          .send(data)
            .expectStatusCode(200)
            .end;
      });
    });

    describe("with a middleware with the `any` method", {
      before({
        router = new URLRouter;
        auto baseCrate = new MemoryCrate!MockActionModel;
        baseCrate.addItem(`{"name": ""}`.parseJsonString);

        router
          .crateSetup
            .add(baseCrate, new MockAnyMiddleware);
      });

      it("should run the middleware handler for get list", {
        request(router)
          .get("/mockactionmodels")
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for get item", {
        request(router)
          .get("/mockactionmodels/000000000000000000000001")
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for put item", {
        request(router)
          .put("/mockactionmodels/000000000000000000000001")
          .send(data)
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for patch item", {
        request(router)
          .patch("/mockactionmodels/000000000000000000000001")
          .send(data)
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for patch item", {
        request(router)
          .delete_("/mockactionmodels/000000000000000000000001")
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for post item", {
        request(router)
          .post("/mockactionmodels")
          .send(data)
            .expectStatusCode(412)
            .end((Response response) => () {
              response.bodyString.should.equal("error");
            });
      });

      it("should run the middleware handler for actions", {
        request(router)
          .get("/mockactionmodels/000000000000000000000001/actionChange")
          .expectStatusCode(412)
          .end((Response response) => () {
            response.bodyString.should.equal("error");
          });
      });
    });
  });
});
