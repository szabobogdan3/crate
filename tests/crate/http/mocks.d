/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.http.mocks;

import vibe.data.json;
import vibe.inet.webform;
import vibe.http.server;

import crate.resource.base;
import crate.error;
import crate.http.router;
import crate.resource.image;
import crate.resource.file;


import std.file;
import std.conv;

struct TestModel
{
  @optional string _id = "1";
  string name = "";

  void actionChange()
  {
    name = "changed";
  }

  void actionParam(string data)
  {
    name = data;
  }
}

struct MockActionModel {
  @optional string _id = "1";
  string name = "";

  void actionChange() {
    name = "changed";
  }

  void actionParam(string data) {
    name = data;
  }

  string actionChangeAndReturn() {
    name = "changed and return";

    return name;
  }

  string actionParamAndReturn(string data) {
    name = data;

    return name;
  }
}

struct Point {
  string type = "Point";
  float[2] coordinates;
}

struct Site {
  string _id = "1";
  Point position;

  Json toJson() const @safe {
    Json data = Json.emptyObject;

    data["_id"] = _id;
    data["position"] = position.serializeToJson;

    return data;
  }

  static Site fromJson(Json src) @safe {
    return Site(
      src["_id"].to!string,
      Point("Point", [ src["position"]["coordinates"][0].to!int, src["position"]["coordinates"][1].to!int ])
    );
  }
}

class TestResource : CrateResource {
  alias ParentOperation = ResourceApiOperation!(TestResource, DefaultStorage);

  static string lastRead;
  static bool removed;

  string contentType() {
    return "test/resource";
  }

  void contentType(string) {
  }

  string fileName() {
    return "";
  }

  void fileName(string) {
  }

  string id() {
    return "";
  }

  string etag() {
    return "";
  }

  void remove() {
    removed = true;
  }

  /// sets the chunk data
  void setChunk(size_t index, ubyte[] data) {
    assert(false, "not implemented");
  }

  void write(BodyOutputStream bodyWriter) {
    bodyWriter.write("test body".to!(char[]));
  }

  void read(const FilePart file) {
    lastRead = readText(file.tempPath.toString);
  }

  void read(InputStream input) {
    assert(false, "not implemented");
  }

  InputStream inputStream() {
    assert(false, "not implemented");
  }

  bool exists() {
    return true;
  }

  bool hasSize() {
    return true;
  }

  void setMetadata(string key, string value) {
  }

  ulong size() {
    return "test body".length;
  }

  ulong chunkSize() {
    assert(false, "not implemented");
  }

  void chunkSize(ulong value) {
    assert(false, "not implemented");
  }

  override string toString() const @safe {
    return "test resource";
  }

  void httpHandler(HTTPServerRequest req, HTTPServerResponse response) {
    throw new Exception("not implemented");
  }

  static TestResource fromString(string src) @safe {
    return new TestResource;
  }

  string replyWithCustomMessage() {
    return "test custom";
  }

  static ApiOperation!DefaultStorage[] customOperations(ParentOperation operation) {
    auto customOperation = new CustomResourceOperation!(TestResource, DefaultStorage, "replyWithCustomMessage")("/custom", operation);

    return [ customOperation ];
  }
}

enum ImageSettings[] settings = [ ImageSettings("small", "100", "100") ];

struct ResourceCollectionCrate {
  string _id;
  CrateImageCollectionResource!(CrateFile, settings) resourceCollection;
}

struct ResourceCrate {
  string _id;
  TestResource resource;
}

class FailingMiddleware {

  /// Middleware applied before all operations. This has priority over any other
  /// binded middlewares
  @any
  void any(HTTPServerRequest, HTTPServerResponse) @safe {
    throw new CrateValidationException("error");
  }
}
