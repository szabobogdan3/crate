/**
  Copyright: © 2017-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.crate.lazydata.model;

import std.conv;
import std.datetime;
import std.functional;

import trial.interfaces;
import trial.discovery.spec;
import fluent.asserts;

import vibe.data.json;
import crate.base;
import crate.lazydata.base;

@("plural:childs")
class ChildObj {
  ObjectId _id;
  string value;
}

version(unittest) {

  bool called;

  Json resolveMock(string model, string id) @safe {
    called = true;
    model.should.equal("Child");
    return parseJsonString(`{ "_id": "` ~ id ~ `", "value": "other" }`);
  }

  Json resolveObj(string model, string id) @safe {
    called = true;
    model.should.equal("ChildObj");
    return parseJsonString(`{ "_id": "` ~ id ~ `", "value": "other" }`);
  }
}

alias s = Spec!({
  describe("Lazy model", {

    beforeEach({
      called = false;
    });

    describe("with a struct relation", {
      struct Child {
        ObjectId _id;
        string value;
      }

      struct Test {
        Child child;
      }

      it("can be converted to a json", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveMock));
        lazyTest.toJson.to!string.should.equal(`{"child":"1"}`);
      });

      it("will query the relation id to fill the data", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveMock));

        lazyTest.child.toType.should.equal(Child(ObjectId.fromString("1"), "other"));
        called.should.equal(true);
      });

      it("will serialize the relation to it's id after it was queried", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveMock));

        lazyTest.child.toType;

        lazyTest.toJson.to!string.should.equal(`{"child":"1"}`);
      });

      it("can replace a relation value", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveMock));

        lazyTest.child = Child(ObjectId.fromString("2"), "value");
        lazyTest.child.toType.should.equal(Child(ObjectId.fromString("2"), "value"));
        called.should.equal(false);
      });

      it("will serialize the id of the new relation", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveMock));

        lazyTest.child = Child(ObjectId.fromString("2"), "value");

        lazyTest.toJson.to!string.should.equal(`{"child":"000000000000000000000002"}`);
        lazyTest.child.toJson.to!string.should.equal(`{"_id":"000000000000000000000002","value":"value"}`);
      });

      it("can be converted to the non lazy type", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveMock));

        lazyTest.toType.child._id.to!string.should.equal("000000000000000000000001");
        lazyTest.toType.child.value.should.equal("other");
      });

      it("can create a lazy object from a model", {
        Test test;
        test.child._id = ObjectId.fromString("123");
        test.child.value = "test";

        auto lazyTest = Lazy!Test.fromModel(test, toDelegate(&resolveMock));

        lazyTest.toJson.to!string.should.equal(`{"child":"000000000000000000000123"}`);
        lazyTest.child.toJson.to!string.should.equal(`{"_id":"000000000000000000000123","value":"test"}`);
      });
    });

    describe("with a list of struct relations", {
      struct Child {
        ObjectId _id;
        string value;
      }

      struct Test {
        Child[] child;
      }

      it("can be converted to a json", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": ["1","2","3"] }`, toDelegate(&resolveMock));
        lazyTest.toJson.to!string.should.equal(`{"child":["1","2","3"]}`);
        called.should.equal(false);
      });

      it("can return the first relation", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": ["1","2","3"] }`, toDelegate(&resolveMock));
        lazyTest.child[0].toType.should.equal(Child(ObjectId.fromString("1"), "other"));
        called.should.equal(true);
      });

      it("always return relations as id, even if a query was done", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": ["1","2","3"] }`, toDelegate(&resolveMock));

        lazyTest.child[0].toType;
        called.should.equal(true);

        lazyTest.toJson.to!string.should.equal(`{"child":["1","2","3"]}`);
      });

      it("can set a new relation", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": ["1","2","3"] }`, toDelegate(&resolveMock));

        lazyTest.child[0] = Child(ObjectId.fromString("10"), "value");
        lazyTest.child[0].toType.should.equal(Child(ObjectId.fromString("10"), "value"));
        called.should.equal(false);
      });

      it("serializes the new relation id after was changed", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": ["1","2","3"] }`, toDelegate(&resolveMock));

        lazyTest.child[0] = Child(ObjectId.fromString("10"), "value");

        lazyTest.toJson.to!string.should.equal(`{"child":["000000000000000000000010","2","3"]}`);
        lazyTest.child.toJson.to!string.should.equal(`["000000000000000000000010","2","3"]`);
        lazyTest.child[0].toJson.to!string.should.equal(`{"_id":"000000000000000000000010","value":"value"}`);
      });

      it("can add a new Json relation to the list", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": ["1","2","3"] }`, toDelegate(&resolveMock));

        lazyTest.child ~= Json("4");

        lazyTest.toJson.to!string.should.equal(`{"child":["1","2","3","4"]}`);
      });

      it("can create a lazy object from a model", {
        Test test;
        test.child = [ Child(ObjectId.fromString("1"), "test1"), Child(ObjectId.fromString("2"), "test2") ];

        auto lazyTest = Lazy!Test.fromModel(test, toDelegate(&resolveMock));

        lazyTest.toJson.should.equal(`{"child":["000000000000000000000001","000000000000000000000002"]}`.parseJsonString);
        lazyTest.child[0].toJson.should.equal(`{"_id":"000000000000000000000001","value":"test1"}`.parseJsonString);
        lazyTest.child[1].toJson.should.equal(`{"_id":"000000000000000000000002","value":"test2"}`.parseJsonString);
      });
    });

    describe("with a class relation", {
      struct Test {
        ChildObj child;
      }

      it("can be converted to a json", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveObj));
        lazyTest.toJson.to!string.should.equal(`{"child":"1"}`);
      });

      it("can be converted to the non lazy type", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "child": "1" }`, toDelegate(&resolveObj));

        lazyTest.toType.child._id.to!string.should.equal("000000000000000000000001");
        lazyTest.toType.child.value.should.equal("other");
      });

      it("can be converted to a json when it is null", {
        auto lazyTest = Lazy!Test.fromModel(Test());
        lazyTest.toJson.to!string.should.equal(`{}`);
      });
    });

    describe("with a SysTime member", {
      struct Test {
        SysTime time;
      }

      it("can be converted to a json", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "time": "2019-01-01T00:00:00Z" }`);
        lazyTest.toJson.to!string.should.equal(`{"time":"2019-01-01T00:00:00Z"}`);
      });

      it("can read the time property", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "time": "2019-01-01T00:00:00Z" }`);
        lazyTest.time.toISOExtString.to!string.should.equal(`2019-01-01T00:00:00Z`);
      });

      it("can be converted to the non lazy type", {
        auto lazyTest = Lazy!Test.fromJsonString(`{"time":"2019-01-01T00:00:00Z"}`);
        lazyTest.toType.time.toISOExtString.should.equal("2019-01-01T00:00:00Z");
      });
    });

    describe("with a Json member", {
      struct Test {
        Json[] value;
      }

      it("can be converted to a json", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "value": ["some value"] }`);
        lazyTest.toJson.to!string.should.equal(`{"value":["some value"]}`);
      });

      it("can read the value property", {
        auto lazyTest = Lazy!Test.fromJsonString(`{ "value": ["some value"] }`);
        lazyTest.value[0].toJson.to!string.should.equal(`some value`);
      });

      it("can be converted to the non lazy type", {
        auto lazyTest = Lazy!Test.fromJsonString(`{"value":["some value"]}`);
        lazyTest.toType.value[0].to!string.should.equal("some value");
      });
    });
  });
});
